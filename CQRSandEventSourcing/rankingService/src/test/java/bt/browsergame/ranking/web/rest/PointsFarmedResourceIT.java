package bt.browsergame.ranking.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.ranking.IntegrationTest;
import bt.browsergame.ranking.domain.PointsFarmed;
import bt.browsergame.ranking.repository.PointsFarmedRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PointsFarmedResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PointsFarmedResourceIT {

    private static final String DEFAULT_USER_NAME = "user";
    private static final String UPDATED_USER_NAME = "user";

    private static final Long DEFAULT_POINTS = 1L;
    private static final Long UPDATED_POINTS = 2L;

    private static final Instant DEFAULT_CREATION_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATION_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_TIME_LIMIT = 1D;
    private static final Double UPDATED_TIME_LIMIT = 2D;

    private static final String ENTITY_API_URL = "/api/points-farmeds";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PointsFarmedRepository pointsFarmedRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPointsFarmedMockMvc;

    private PointsFarmed pointsFarmed;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointsFarmed createEntity(EntityManager em) {
        PointsFarmed pointsFarmed = new PointsFarmed()
            .userName(DEFAULT_USER_NAME)
            .points(DEFAULT_POINTS)
            .creationTime(DEFAULT_CREATION_TIME)
            .timeLimit(DEFAULT_TIME_LIMIT);
        return pointsFarmed;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointsFarmed createUpdatedEntity(EntityManager em) {
        PointsFarmed pointsFarmed = new PointsFarmed()
            .userName(UPDATED_USER_NAME)
            .points(UPDATED_POINTS)
            .creationTime(UPDATED_CREATION_TIME)
            .timeLimit(UPDATED_TIME_LIMIT);
        return pointsFarmed;
    }

    @BeforeEach
    public void initTest() {
        pointsFarmed = createEntity(em);
    }

    @Test
    @Transactional
    void createPointsFarmed() throws Exception {
        int databaseSizeBeforeCreate = pointsFarmedRepository.findAll().size();
        // Create the PointsFarmed
        restPointsFarmedMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointsFarmed)))
            .andExpect(status().isCreated());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeCreate + 2);
        PointsFarmed testPointsFarmed = pointsFarmedList.get(1);
        assertThat(testPointsFarmed.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testPointsFarmed.getPoints()).isEqualTo(DEFAULT_POINTS);
        assertThat(testPointsFarmed.getTimeLimit()).isEqualTo(DEFAULT_TIME_LIMIT);
    }

    @Test
    @Transactional
    void createPointsFarmedWithExistingId() throws Exception {
        // Create the PointsFarmed with an existing ID
        pointsFarmed.setId(1L);

        int databaseSizeBeforeCreate = pointsFarmedRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPointsFarmedMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointsFarmed)))
            .andExpect(status().isBadRequest());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkUserNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = pointsFarmedRepository.findAll().size();
        // set the field null
        pointsFarmed.setUserName(null);

        // Create the PointsFarmed, which fails.

        restPointsFarmedMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointsFarmed)))
            .andExpect(status().isBadRequest());

        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPointsIsRequired() throws Exception {
        int databaseSizeBeforeTest = pointsFarmedRepository.findAll().size();
        // set the field null
        pointsFarmed.setPoints(null);

        // Create the PointsFarmed, which fails.

        restPointsFarmedMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointsFarmed)))
            .andExpect(status().isBadRequest());

        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreationTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = pointsFarmedRepository.findAll().size();
        // set the field null
        pointsFarmed.setCreationTime(null);

        // Create the PointsFarmed, which fails.

        restPointsFarmedMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointsFarmed)))
            .andExpect(status().isBadRequest());

        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPointsFarmeds() throws Exception {
        // Initialize the database
        pointsFarmedRepository.saveAndFlush(pointsFarmed);

        // Get all the pointsFarmedList
        restPointsFarmedMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pointsFarmed.getId().intValue())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
            .andExpect(jsonPath("$.[*].points").value(hasItem(DEFAULT_POINTS.intValue())))
            .andExpect(jsonPath("$.[*].creationTime").value(hasItem(DEFAULT_CREATION_TIME.toString())))
            .andExpect(jsonPath("$.[*].timeLimit").value(hasItem(DEFAULT_TIME_LIMIT.doubleValue())));
    }

    @Test
    @Transactional
    void getPointsFarmed() throws Exception {
        // Initialize the database
        pointsFarmedRepository.saveAndFlush(pointsFarmed);

        // Get the pointsFarmed
        restPointsFarmedMockMvc
            .perform(get(ENTITY_API_URL_ID, pointsFarmed.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pointsFarmed.getId().intValue()))
            .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
            .andExpect(jsonPath("$.points").value(DEFAULT_POINTS.intValue()))
            .andExpect(jsonPath("$.creationTime").value(DEFAULT_CREATION_TIME.toString()))
            .andExpect(jsonPath("$.timeLimit").value(DEFAULT_TIME_LIMIT.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingPointsFarmed() throws Exception {
        // Get the pointsFarmed
        restPointsFarmedMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPointsFarmed() throws Exception {
        // Initialize the database
        pointsFarmedRepository.saveAndFlush(pointsFarmed);

        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();

        // Update the pointsFarmed
        PointsFarmed updatedPointsFarmed = pointsFarmedRepository.findById(pointsFarmed.getId()).get();
        // Disconnect from session so that the updates on updatedPointsFarmed are not directly saved in db
        em.detach(updatedPointsFarmed);
        updatedPointsFarmed
            .userName(UPDATED_USER_NAME)
            .points(UPDATED_POINTS)
            .creationTime(UPDATED_CREATION_TIME)
            .timeLimit(UPDATED_TIME_LIMIT);

        restPointsFarmedMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPointsFarmed.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPointsFarmed))
            )
            .andExpect(status().isOk());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate + 1);
        PointsFarmed testPointsFarmed = pointsFarmedList.get(0);
        assertThat(testPointsFarmed.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testPointsFarmed.getPoints()).isEqualTo(UPDATED_POINTS);
        assertThat(testPointsFarmed.getTimeLimit()).isEqualTo(UPDATED_TIME_LIMIT);
    }

    @Test
    @Transactional
    void putNonExistingPointsFarmed() throws Exception {
        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();
        pointsFarmed.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPointsFarmedMockMvc
            .perform(
                put(ENTITY_API_URL_ID, pointsFarmed.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPointsFarmed() throws Exception {
        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();
        pointsFarmed.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointsFarmedMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPointsFarmed() throws Exception {
        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();
        pointsFarmed.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointsFarmedMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointsFarmed)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePointsFarmedWithPatch() throws Exception {
        // Initialize the database
        pointsFarmedRepository.saveAndFlush(pointsFarmed);

        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();

        // Update the pointsFarmed using partial update
        PointsFarmed partialUpdatedPointsFarmed = new PointsFarmed();
        partialUpdatedPointsFarmed.setId(pointsFarmed.getId());

        partialUpdatedPointsFarmed.userName(UPDATED_USER_NAME).points(UPDATED_POINTS);

        restPointsFarmedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPointsFarmed.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPointsFarmed))
            )
            .andExpect(status().isOk());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
        PointsFarmed testPointsFarmed = pointsFarmedList.get(pointsFarmedList.size() - 1);
        assertThat(testPointsFarmed.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testPointsFarmed.getPoints()).isEqualTo(UPDATED_POINTS);
        assertThat(testPointsFarmed.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testPointsFarmed.getTimeLimit()).isEqualTo(DEFAULT_TIME_LIMIT);
    }

    @Test
    @Transactional
    void fullUpdatePointsFarmedWithPatch() throws Exception {
        // Initialize the database
        pointsFarmedRepository.saveAndFlush(pointsFarmed);

        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();

        // Update the pointsFarmed using partial update
        PointsFarmed partialUpdatedPointsFarmed = new PointsFarmed();
        partialUpdatedPointsFarmed.setId(pointsFarmed.getId());

        partialUpdatedPointsFarmed
            .userName(UPDATED_USER_NAME)
            .points(UPDATED_POINTS)
            .creationTime(UPDATED_CREATION_TIME)
            .timeLimit(UPDATED_TIME_LIMIT);

        restPointsFarmedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPointsFarmed.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPointsFarmed))
            )
            .andExpect(status().isOk());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
        PointsFarmed testPointsFarmed = pointsFarmedList.get(pointsFarmedList.size() - 1);
        assertThat(testPointsFarmed.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testPointsFarmed.getPoints()).isEqualTo(UPDATED_POINTS);
        assertThat(testPointsFarmed.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testPointsFarmed.getTimeLimit()).isEqualTo(UPDATED_TIME_LIMIT);
    }

    @Test
    @Transactional
    void patchNonExistingPointsFarmed() throws Exception {
        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();
        pointsFarmed.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPointsFarmedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, pointsFarmed.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPointsFarmed() throws Exception {
        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();
        pointsFarmed.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointsFarmedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPointsFarmed() throws Exception {
        int databaseSizeBeforeUpdate = pointsFarmedRepository.findAll().size();
        pointsFarmed.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointsFarmedMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(pointsFarmed))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PointsFarmed in the database
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePointsFarmed() throws Exception {
        // Initialize the database
        pointsFarmedRepository.saveAndFlush(pointsFarmed);

        int databaseSizeBeforeDelete = pointsFarmedRepository.findAll().size();

        // Delete the pointsFarmed
        restPointsFarmedMockMvc
                .perform(delete(ENTITY_API_URL_ID, pointsFarmed.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PointsFarmed> pointsFarmedList = pointsFarmedRepository.findAll();
        assertThat(pointsFarmedList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
    @Transactional
    void getRankingListSum() throws Exception {
        // Initialize the database
        PointsFarmed pointsFarmed = new PointsFarmed().points(20L).timeLimit(0.0).creationTime(Instant.now())
                .userName("user");
        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(20L).timeLimit(2.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        // Get all the pointsFarmedList
        restPointsFarmedMockMvc.perform(get(ENTITY_API_URL + "/rankingSum")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].userName").value(hasItem("user")))
                .andExpect(jsonPath("$.[*].points").value(hasItem(40)))
                .andExpect(jsonPath("$.[*].rank").value(hasItem(1)));
    }

    @Test
    @Transactional
    void getRankingListMax() throws Exception {
        // Initialize the database
        PointsFarmed pointsFarmed = new PointsFarmed().points(40L).timeLimit(0.0).creationTime(Instant.now())
                .userName("user");
        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(20L).timeLimit(2.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(19L).timeLimit(2.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        // Get all the pointsFarmedList
        restPointsFarmedMockMvc.perform(get(ENTITY_API_URL + "/rankingMax/2.0")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].userName").value(hasItem("user")))
                .andExpect(jsonPath("$.[*].points").value(hasItem(20)))
                .andExpect(jsonPath("$.[*].rank").value(hasItem(1)));
    }

    @Test
    @Transactional
    void userSum() throws Exception {
        // Initialize the database
        PointsFarmed pointsFarmed = new PointsFarmed().points(20L).timeLimit(0.0).creationTime(Instant.now())
                .userName("user");
        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(20L).timeLimit(2.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        // Get all the pointsFarmedList
        restPointsFarmedMockMvc.perform(get(ENTITY_API_URL + "/userSum")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.userName").value("user")).andExpect(jsonPath("$.points").value(40))
                .andExpect(jsonPath("$.rank").value(1));
    }

    @Test
    @Transactional
    void userMaxTime() throws Exception {
        // Initialize the database
        PointsFarmed pointsFarmed = new PointsFarmed().points(40L).timeLimit(0.0).creationTime(Instant.now())
                .userName("user");
        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(20L).timeLimit(6.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(23L).timeLimit(6.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        // Get all the pointsFarmedList
        restPointsFarmedMockMvc.perform(get(ENTITY_API_URL + "/userMaxTime/6.0")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.userName").value("user")).andExpect(jsonPath("$.points").value(23))
                .andExpect(jsonPath("$.rank").value(1));
    }

    @Test
    @Transactional
    void getPointsFarmedSumUser() throws Exception {
        // Initialize the database
        PointsFarmed pointsFarmed = new PointsFarmed().points(20L).timeLimit(0.0).creationTime(Instant.now())
                .userName("user");
        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(20L).timeLimit(2.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        // Get all the pointsFarmedList
        restPointsFarmedMockMvc.perform(get(ENTITY_API_URL + "/userSum/user")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.userName").value("user")).andExpect(jsonPath("$.points").value(40))
                .andExpect(jsonPath("$.rank").value(1));
    }

    @Test
    @Transactional
    void getPointsFarmedMaxTimeUser() throws Exception {
        // Initialize the database
        PointsFarmed pointsFarmed = new PointsFarmed().points(40L).timeLimit(0.0).creationTime(Instant.now())
                .userName("user");
        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(20L).timeLimit(6.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        pointsFarmed = new PointsFarmed().points(23L).timeLimit(6.0).creationTime(Instant.now()).userName("user");

        restPointsFarmedMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(pointsFarmed))).andExpect(status().isCreated());

        // Get all the pointsFarmedList
        restPointsFarmedMockMvc.perform(get(ENTITY_API_URL + "/userMaxTime/6.0/user")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.userName").value("user")).andExpect(jsonPath("$.points").value(23))
                .andExpect(jsonPath("$.rank").value(1));
    }
}
