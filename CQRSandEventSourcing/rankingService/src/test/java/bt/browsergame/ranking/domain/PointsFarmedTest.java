package bt.browsergame.ranking.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.ranking.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PointsFarmedTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PointsFarmed.class);
        PointsFarmed pointsFarmed1 = new PointsFarmed();
        pointsFarmed1.setId(1L);
        PointsFarmed pointsFarmed2 = new PointsFarmed();
        pointsFarmed2.setId(pointsFarmed1.getId());
        assertThat(pointsFarmed1).isEqualTo(pointsFarmed2);
        pointsFarmed2.setId(2L);
        assertThat(pointsFarmed1).isNotEqualTo(pointsFarmed2);
        pointsFarmed1.setId(null);
        assertThat(pointsFarmed1).isNotEqualTo(pointsFarmed2);
    }
}
