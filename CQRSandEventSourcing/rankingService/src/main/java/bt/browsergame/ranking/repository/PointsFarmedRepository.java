package bt.browsergame.ranking.repository;

import bt.browsergame.ranking.domain.PointsFarmed;
import bt.browsergame.ranking.domain.PointsFarmedView;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PointsFarmed entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PointsFarmedRepository extends JpaRepository<PointsFarmed, Long> {
    @Query(value = "SELECT pf.user_name as username, pf.points as points, RANK() OVER(ORDER BY pf.points DESC) as rank from points_farmed pf WHERE pf.time_limit = :time ORDER BY rank ASC", countQuery = "SELECT count(user_name) FROM points_farmed WHERE time_limit = :time GROUP BY user_name", nativeQuery = true)
    Page<PointsFarmedView> getRankingPointsMax(Pageable pageable, @Param("time") Double time);

    @Query(value = "WITH tbl as (SELECT pf.user_name as userName, pf.points as points, RANK() OVER(ORDER BY pf.points DESC) as rank FROM points_farmed pf WHERE pf.time_limit = :time) Select userName, points, rank from tbl WHERE tbl.userName = :username", nativeQuery = true)
    PointsFarmedView getMaxPointsFarmed(@Param("time") Double time, @Param("username") String userName);
}
