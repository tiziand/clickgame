package bt.browsergame.ranking.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PointsFarmed.
 */
@Entity
@Table(name = "points_farmed")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PointsFarmed implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "user_name", nullable = false)
    private String userName;

    @NotNull
    @Column(name = "points", nullable = false)
    private Long points;

    @NotNull
    @Column(name = "creation_time", nullable = false)
    private Instant creationTime;

    @Column(name = "time_limit")
    private Double timeLimit;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PointsFarmed id(Long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public PointsFarmed userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getPoints() {
        return this.points;
    }

    public PointsFarmed points(Long points) {
        this.points = points;
        return this;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public Instant getCreationTime() {
        return this.creationTime;
    }

    public PointsFarmed creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Double getTimeLimit() {
        return this.timeLimit;
    }

    public PointsFarmed timeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public void setTimeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PointsFarmed)) {
            return false;
        }
        return id != null && id.equals(((PointsFarmed) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PointsFarmed{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", points=" + getPoints() +
            ", creationTime='" + getCreationTime() + "'" +
            ", timeLimit=" + getTimeLimit() +
            "}";
    }
}
