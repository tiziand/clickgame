package bt.browsergame.mtxstore.kafka.entities;

import java.io.Serializable;

/**
 * A ButtonColorMtx.
 */
public class ButtonColorMtxKafka implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String mtxColor;

    private Integer price;

    private Boolean available;

    private Double timeLimit;

    private Long pointsRequired;

    private Boolean locked;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ButtonColorMtxKafka id(Long id) {
        this.id = id;
        return this;
    }

    public String getMtxColor() {
        return this.mtxColor;
    }

    public ButtonColorMtxKafka mtxColor(String mtxColor) {
        this.mtxColor = mtxColor;
        return this;
    }

    public void setMtxColor(String mtxColor) {
        this.mtxColor = mtxColor;
    }

    public Integer getPrice() {
        return this.price;
    }

    public ButtonColorMtxKafka price(Integer price) {
        this.price = price;
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    public ButtonColorMtxKafka available(Boolean available) {
        this.available = available;
        return this;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Double getTimeLimit() {
        return this.timeLimit;
    }

    public ButtonColorMtxKafka timeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public void setTimeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Long getPointsRequired() {
        return this.pointsRequired;
    }

    public ButtonColorMtxKafka pointsRequired(Long pointsRequired) {
        this.pointsRequired = pointsRequired;
        return this;
    }

    public void setPointsRequired(Long pointsRequired) {
        this.pointsRequired = pointsRequired;
    }

    public Boolean getLocked() {
        return this.locked;
    }

    public ButtonColorMtxKafka locked(Boolean locked) {
        this.locked = locked;
        return this;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ButtonColorMtxKafka)) {
            return false;
        }
        return id != null && id.equals(((ButtonColorMtxKafka) o).id);
    }

    @Override
    public int hashCode() {
        // see
        // https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ButtonColorMtx{" + "id=" + getId() + ", mtxColor='" + getMtxColor() + "'" + ", price=" + getPrice()
                + ", available='" + getAvailable() + "'" + ", timeLimit=" + getTimeLimit() + ", pointsRequired="
                + getPointsRequired() + ", locked='" + getLocked() + "'" + "}";
    }
}
