package bt.browsergame.mtxstore.repository;

import bt.browsergame.mtxstore.domain.ButtonColorMtx;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ButtonColorMtx entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ButtonColorMtxRepository extends JpaRepository<ButtonColorMtx, Long> {}
