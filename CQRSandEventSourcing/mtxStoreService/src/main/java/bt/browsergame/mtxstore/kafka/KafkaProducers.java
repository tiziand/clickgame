package bt.browsergame.mtxstore.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;

import bt.browsergame.mtxstore.domain.ButtonColorMtx;
import bt.browsergame.mtxstore.kafka.serialization.JsonSerializationFactory;

@Configuration
@ConfigurationProperties(prefix = "kafka")
public class KafkaProducers {

    private String bootStrapServers = "localhost:9092";

    public String getBootStrapServers() {
        return bootStrapServers;
    }

    public void setBootStrapServers(String bootStrapServers) {
        this.bootStrapServers = bootStrapServers;
    }

    @Bean(name = "buttonColorMtxProducer")
    public Producer<Long, ButtonColorMtx> buttonColorMtxProducer() {
        Properties config = new Properties();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        config.put(ProducerConfig.ACKS_CONFIG, "all");
        config.put(ProducerConfig.RETRIES_CONFIG, 10);
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);

        final Serde<ButtonColorMtx> buttonColorMtxSerde = JsonSerializationFactory
                .<ButtonColorMtx>getSerde(ButtonColorMtx.class);

        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, buttonColorMtxSerde.serializer().getClass());

        Producer<Long, ButtonColorMtx> producer = new KafkaProducer<>(config);
        return producer;
    }
}
