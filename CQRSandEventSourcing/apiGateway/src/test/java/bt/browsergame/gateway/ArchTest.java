package bt.browsergame.gateway;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("bt.browsergame.gateway");

        noClasses()
            .that()
            .resideInAnyPackage("bt.browsergame.gateway.service..")
            .or()
            .resideInAnyPackage("bt.browsergame.gateway.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..bt.browsergame.gateway.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
