export interface IBasketItem {
  id?: number;
  userName?: string;
  mtxId?: number;
}

export const defaultValue: Readonly<IBasketItem> = {};
