import dayjs from 'dayjs';

export interface IPointsFarmed {
  id?: number;
  userName?: string;
  points?: number;
  creationTime?: string;
  timeLimit?: number | null;
}

export const defaultValue: Readonly<IPointsFarmed> = {};
