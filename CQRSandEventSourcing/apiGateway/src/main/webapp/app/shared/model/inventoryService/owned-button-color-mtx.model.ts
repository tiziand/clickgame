export interface IOwnedButtonColorMtx {
  id?: number;
  userName?: string;
  mtxId?: number;
}

export const defaultValue: Readonly<IOwnedButtonColorMtx> = {};
