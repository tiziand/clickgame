export interface IEquippedButtonColorMtx {
  id?: number;
  userName?: string;
  mtxId?: number | null;
  timeSlot?: number;
}

export const defaultValue: Readonly<IEquippedButtonColorMtx> = {};
