import dayjs from 'dayjs';

export interface IClickPerMin {
  id?: number;
  creationTime?: string;
  clickCount?: number;
}

export const defaultValue: Readonly<IClickPerMin> = {};
