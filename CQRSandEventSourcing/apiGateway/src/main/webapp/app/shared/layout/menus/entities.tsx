import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';

import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown icon="th-list" name="Entities" id="entity-menu" data-cy="entity" style={{ maxHeight: '80vh', overflow: 'auto' }}>
    <MenuItem icon="asterisk" to="/button-color-mtx">
      Button Color Mtx
    </MenuItem>
    <MenuItem icon="asterisk" to="/user-count">
      User Count
    </MenuItem>
    <MenuItem icon="asterisk" to="/equipped-button-color-mtx">
      Equipped Button Color Mtx
    </MenuItem>
    <MenuItem icon="asterisk" to="/purchases-mtx-points">
      Purchases Mtx Points
    </MenuItem>
    <MenuItem icon="asterisk" to="/purchases-button-color-mtx">
      Purchases Button Color Mtx
    </MenuItem>
    <MenuItem icon="asterisk" to="/points-farmed">
      Points Farmed
    </MenuItem>
    <MenuItem icon="asterisk" to="/owned-button-color-mtx">
      Owned Button Color Mtx
    </MenuItem>
    <MenuItem icon="asterisk" to="/payment-info">
      Payment Info
    </MenuItem>
    <MenuItem icon="asterisk" to="/point-option">
      Point Option
    </MenuItem>
    <MenuItem icon="asterisk" to="/click-per-min">
      Click Per Min
    </MenuItem>
    <MenuItem icon="asterisk" to="/owned-mtx-points">
      Owned Mtx Points
    </MenuItem>
    <MenuItem icon="asterisk" to="/basket-item">
      Basket Item
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
