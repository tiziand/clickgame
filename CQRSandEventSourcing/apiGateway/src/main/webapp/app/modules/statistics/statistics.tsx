import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';
import { getEntity as getTotalMoneySpent } from 'app/entities/statisticService/moneySpent/moneySpent.reducer';
import { getEntity as getTotalClicks } from 'app/entities/statisticService/clicks/clicks.reducer';
import { getEntity as getButtonColorMtxPurchasesTotal } from 'app/entities/statisticService/buttonColorMtxPurchases/buttonColorMtxPurchases.reducer';
import { getEntities as getButtonColorMtxPurchasesList } from 'app/entities/statisticService/buttonColorMtxPurchases/buttonColorMtxPurchasesList.reducer';
import { getEntities as getButtonColorMtxList } from 'app/entities/mtxStoreService/button-color-mtx/button-color-mtx.reducer';

export interface IStatisticProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Statistics = (props: IStatisticProps) => {
  const [startDate, setStartDate] = useState('2020-01-01');
  const [endDate, setEndDate] = useState(new Date().toLocaleDateString('en-Ca'));
  useEffect(() => {
    props.getTotalMoneySpent(startDate, endDate);
    props.getTotalClicks(startDate, endDate);
    props.getButtonColorMtxPurchasesTotal(startDate, endDate);
    props.getButtonColorMtxPurchasesList(startDate, endDate);
  }, [startDate, endDate]);

  useEffect(() => {
    props.getButtonColorMtxList();
  }, []);

  const getPurchases = (mtxId: number): number => {
    const foundMTX = props.buttonColorMtxPurchasesList.find(element => {
      return element.mtxId === mtxId;
    });
    if (foundMTX === undefined) return 0;
    return foundMTX.sumPurchases;
  };

  const { buttonColorMtxList, bCMloading } = props;
  return (
    <div>
      <h2 id="ranking-heading">Statistics</h2>
      Start: <input type="date" id="start" name="startTime" value={startDate} onChange={event => setStartDate(event.target.value)}></input>
      End: <input type="date" id="end" name="endTime" value={endDate} onChange={event => setEndDate(event.target.value)}></input>
      <h3>Total money spent: {props.moneySpent.sumMoneySpent}</h3>
      <h3>Total clicks by all users: {props.clicksTotal.clicks}</h3>
      <h3>Total button color mtx purchases by all users: {props.purchasesButtonColorMtxTotal.sumPurchases}</h3>
      <div className="table-responsive">
        {buttonColorMtxList && buttonColorMtxList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>Mtx Id</th>
                <th>Color Sample</th>
                <th>Price</th>
                <th>Requirment</th>
                <th>GameModeLock</th>
                <th>Purchases</th>
              </tr>
            </thead>
            <tbody>
              {buttonColorMtxList.map((buttonColorMtx, i) => (
                <tr key={`entity-${i}`}>
                  {buttonColorMtx.available && (
                    <>
                      <td>{buttonColorMtx.id}</td>
                      <td>{<Button style={{ backgroundColor: buttonColorMtx.mtxColor }}>Color</Button>}</td>
                      <td>{buttonColorMtx.price}</td>
                      <td>
                        {buttonColorMtx.pointsRequired !== 0
                          ? 'Points: ' +
                            buttonColorMtx.pointsRequired +
                            ' TimeLimit: ' +
                            (buttonColorMtx.timeLimit !== 0 ? buttonColorMtx.timeLimit : 'infinte')
                          : 'none'}
                      </td>
                      <td>
                        {buttonColorMtx.locked
                          ? buttonColorMtx.timeLimit === 0
                            ? 'Locked: infinite'
                            : 'Locked: ' + buttonColorMtx.timeLimit + 's'
                          : 'none'}
                      </td>
                      <td>{getPurchases(buttonColorMtx.id)}</td>
                    </>
                  )}
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !bCMloading && <div className="alert alert-warning">No items in the store</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({
  moneySpent,
  clicksTotal,
  purchasesButtonColorMtxTotal,
  buttonColorMtx,
  purchasesButtonColorMtxTotalPer,
}: IRootState) => ({
  moneySpent: moneySpent.entity,
  mSloading: moneySpent.loading,
  clicksTotal: clicksTotal.entity,
  cTloading: clicksTotal.loading,
  purchasesButtonColorMtxTotal: purchasesButtonColorMtxTotal.entity,
  pBCMTloading: purchasesButtonColorMtxTotal.loading,
  buttonColorMtxList: buttonColorMtx.entities,
  bCMloading: buttonColorMtx.loading,
  buttonColorMtxPurchasesList: purchasesButtonColorMtxTotalPer.entities,
  bCMPLloading: purchasesButtonColorMtxTotalPer.loading,
});

const mapDispatchToProps = {
  getTotalClicks,
  getTotalMoneySpent,
  getButtonColorMtxPurchasesTotal,
  getButtonColorMtxList,
  getButtonColorMtxPurchasesList,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Statistics);
