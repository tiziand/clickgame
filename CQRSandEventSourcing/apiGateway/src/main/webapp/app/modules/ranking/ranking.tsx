import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table, ButtonGroup, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';

import { IRootState } from 'app/shared/reducers';
import {
  getMaxTimeRanking,
  getSumRanking,
  getMaxTimeUserName,
  getSumUserName,
} from 'app/entities/rankingService/points-farmed-view/points-farmed-view.reducer';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';

export interface IRankingProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Ranking = (props: IRankingProps) => {
  const [timeLimit, setTimeLimit] = useState('0');

  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 0), props.location.search)
  );

  const getAllEntitiesSum = () => {
    props.getSumRanking(paginationState.activePage - 1, paginationState.itemsPerPage);
  };

  const getAllEntitiesMax = () => {
    props.getMaxTimeRanking(paginationState.activePage - 1, paginationState.itemsPerPage, timeLimit);
  };

  const updateEntities = () => {
    if (timeLimit === '0') getAllEntitiesSum();
    else getAllEntitiesMax();
    const endURL = `?page=${paginationState.activePage}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    setPaginationState({ ...paginationState, activePage: 1 });
  }, [timeLimit]);

  useEffect(() => {
    updateEntities();
  }, [paginationState.activePage, paginationState.order, timeLimit]);

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const search = (event, errors, values) => {
    if (timeLimit === '0') props.getSumUserName(values.userName);
    else props.getMaxTimeUserName(timeLimit, values.userName);
  };

  const { rankingList, match, loading, totalItems } = props;
  return (
    <div>
      <h2 id="ranking-heading">
        Ranking{' '}
        <ButtonGroup style={{ paddingLeft: 15, paddingRight: 15 }}>
          <Button
            color="info"
            onClick={() => {
              setTimeLimit('0');
            }}
          >
            Infinite
          </Button>
          <Button
            color="success"
            onClick={() => {
              setTimeLimit('6');
            }}
          >
            6s
          </Button>
          <Button
            color="warning"
            onClick={() => {
              setTimeLimit('4');
            }}
          >
            4s
          </Button>
          <Button
            color="danger"
            onClick={() => {
              setTimeLimit('2');
            }}
          >
            2s
          </Button>
        </ButtonGroup>
      </h2>
      <div className="table-responsive">
        {rankingList && rankingList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand">Rank</th>
                <th className="hand">User Name</th>
                <th className="hand">Points</th>
              </tr>
            </thead>
            <tbody>
              {rankingList.map((ranking, i) => (
                <tr key={`entity-${i}`}>
                  <td>{ranking.rank}</td>
                  <td>{ranking.userName}</td>
                  <td>{ranking.points}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Points Farmeds found</div>
        )}
      </div>
      {props.totalItems ? (
        <div className={rankingList && rankingList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={paginationState.activePage} total={totalItems} itemsPerPage={paginationState.itemsPerPage} />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={paginationState.activePage}
              onSelect={handlePagination}
              maxButtons={5}
              itemsPerPage={paginationState.itemsPerPage}
              totalItems={props.totalItems}
            />
          </Row>
        </div>
      ) : (
        ''
      )}
      <b>Search:</b>
      <AvForm onSubmit={search}>
        <AvGroup>
          <Label id="userNameLabel" for="userName">
            User Name
          </Label>
          <AvField
            id="userName"
            type="string"
            className="form-control"
            name="userName"
            validate={{
              required: { value: true, errorMessage: 'This field is required.' },
            }}
          />
        </AvGroup>
        <Button color="primary" id="buy-points" type="submit">
          Search
        </Button>
      </AvForm>
      <p></p>
      <b>
        {props.foundEntity.userName !== undefined &&
          props.foundEntity.userName + ' / Rank: ' + props.foundEntity.rank + ' / Points: ' + props.foundEntity.points}
      </b>
    </div>
  );
};

const mapStateToProps = ({ pointsFarmedView }: IRootState) => ({
  rankingList: pointsFarmedView.entities,
  loading: pointsFarmedView.loading,
  updated: pointsFarmedView.updateSuccess,
  totalItems: pointsFarmedView.totalItems,
  foundEntity: pointsFarmedView.entity,
});

const mapDispatchToProps = {
  getMaxTimeRanking,
  getSumRanking,
  getMaxTimeUserName,
  getSumUserName,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Ranking);
