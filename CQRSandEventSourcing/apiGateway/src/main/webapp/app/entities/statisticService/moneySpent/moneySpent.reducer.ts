import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IMoneySpent, defaultValue } from 'app/shared/model/statisticService/moneySpent.model';

export const ACTION_TYPES = {
  FETCH_MONEYSPENT: 'purchasesMtxPoints/FETCH_MONEYSPENT',
  RESET: 'purchasesMtxPoints/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IMoneySpent>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type MoneySpentState = Readonly<typeof initialState>;

// Reducer

export default (state: MoneySpentState = initialState, action): MoneySpentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_MONEYSPENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_MONEYSPENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_MONEYSPENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/purchases-mtx-points';

// Actions

export const getEntity = (startDate: string, endDate: string) => {
  const requestUrl = `${apiUrl}/moneySpentSum/${startDate}/${endDate}`;
  return {
    type: ACTION_TYPES.FETCH_MONEYSPENT,
    payload: axios.get<IMoneySpent>(requestUrl),
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
