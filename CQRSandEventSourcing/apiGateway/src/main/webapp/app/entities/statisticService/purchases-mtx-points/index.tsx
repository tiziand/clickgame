import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PurchasesMtxPoints from './purchases-mtx-points';
import PurchasesMtxPointsDetail from './purchases-mtx-points-detail';
import PurchasesMtxPointsUpdate from './purchases-mtx-points-update';
import PurchasesMtxPointsDeleteDialog from './purchases-mtx-points-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PurchasesMtxPointsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PurchasesMtxPointsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PurchasesMtxPointsDetail} />
      <ErrorBoundaryRoute path={match.url} component={PurchasesMtxPoints} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PurchasesMtxPointsDeleteDialog} />
  </>
);

export default Routes;
