import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './purchases-button-color-mtx.reducer';

export interface IPurchasesButtonColorMtxDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PurchasesButtonColorMtxDeleteDialog = (props: IPurchasesButtonColorMtxDeleteDialogProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const handleClose = () => {
    props.history.push('/purchases-button-color-mtx');
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const confirmDelete = () => {
    props.deleteEntity(props.purchasesButtonColorMtxEntity.id);
  };

  const { purchasesButtonColorMtxEntity } = props;
  return (
    <Modal isOpen toggle={handleClose}>
      <ModalHeader toggle={handleClose} data-cy="purchasesButtonColorMtxDeleteDialogHeading">
        Confirm delete operation
      </ModalHeader>
      <ModalBody id="apiGatewayApp.statisticServicePurchasesButtonColorMtx.delete.question">
        Are you sure you want to delete this PurchasesButtonColorMtx?
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={handleClose}>
          <FontAwesomeIcon icon="ban" />
          &nbsp; Cancel
        </Button>
        <Button id="jhi-confirm-delete-purchasesButtonColorMtx" data-cy="entityConfirmDeleteButton" color="danger" onClick={confirmDelete}>
          <FontAwesomeIcon icon="trash" />
          &nbsp; Delete
        </Button>
      </ModalFooter>
    </Modal>
  );
};

const mapStateToProps = ({ purchasesButtonColorMtx }: IRootState) => ({
  purchasesButtonColorMtxEntity: purchasesButtonColorMtx.entity,
  updateSuccess: purchasesButtonColorMtx.updateSuccess,
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PurchasesButtonColorMtxDeleteDialog);
