import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserCount from './user-count';
import UserCountDetail from './user-count-detail';
import UserCountUpdate from './user-count-update';
import UserCountDeleteDialog from './user-count-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserCountUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserCountUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserCountDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserCount} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={UserCountDeleteDialog} />
  </>
);

export default Routes;
