import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './click-per-min.reducer';
import { IClickPerMin } from 'app/shared/model/statisticService/click-per-min.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IClickPerMinUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ClickPerMinUpdate = (props: IClickPerMinUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { clickPerMinEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/click-per-min');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.creationTime = convertDateTimeToServer(values.creationTime);

    if (errors.length === 0) {
      const entity = {
        ...clickPerMinEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="apiGatewayApp.statisticServiceClickPerMin.home.createOrEditLabel" data-cy="ClickPerMinCreateUpdateHeading">
            Create or edit a ClickPerMin
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : clickPerMinEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="click-per-min-id">ID</Label>
                  <AvInput id="click-per-min-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="creationTimeLabel" for="click-per-min-creationTime">
                  Creation Time
                </Label>
                <AvInput
                  id="click-per-min-creationTime"
                  data-cy="creationTime"
                  type="datetime-local"
                  className="form-control"
                  name="creationTime"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.clickPerMinEntity.creationTime)}
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="clickCountLabel" for="click-per-min-clickCount">
                  Click Count
                </Label>
                <AvField
                  id="click-per-min-clickCount"
                  data-cy="clickCount"
                  type="string"
                  className="form-control"
                  name="clickCount"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/click-per-min" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  clickPerMinEntity: storeState.clickPerMin.entity,
  loading: storeState.clickPerMin.loading,
  updating: storeState.clickPerMin.updating,
  updateSuccess: storeState.clickPerMin.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ClickPerMinUpdate);
