import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IOwnedButtonColorMtx, defaultValue } from 'app/shared/model/inventoryService/owned-button-color-mtx.model';

export const ACTION_TYPES = {
  FETCH_OWNEDBUTTONCOLORMTX_LIST: 'ownedButtonColorMtx/FETCH_OWNEDBUTTONCOLORMTX_LIST',
  FETCH_OWNEDBUTTONCOLORMTX: 'ownedButtonColorMtx/FETCH_OWNEDBUTTONCOLORMTX',
  CREATE_OWNEDBUTTONCOLORMTX: 'ownedButtonColorMtx/CREATE_OWNEDBUTTONCOLORMTX',
  UPDATE_OWNEDBUTTONCOLORMTX: 'ownedButtonColorMtx/UPDATE_OWNEDBUTTONCOLORMTX',
  PARTIAL_UPDATE_OWNEDBUTTONCOLORMTX: 'ownedButtonColorMtx/PARTIAL_UPDATE_OWNEDBUTTONCOLORMTX',
  DELETE_OWNEDBUTTONCOLORMTX: 'ownedButtonColorMtx/DELETE_OWNEDBUTTONCOLORMTX',
  RESET: 'ownedButtonColorMtx/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IOwnedButtonColorMtx>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type OwnedButtonColorMtxState = Readonly<typeof initialState>;

// Reducer

export default (state: OwnedButtonColorMtxState = initialState, action): OwnedButtonColorMtxState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX_LIST):
    case REQUEST(ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_OWNEDBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.UPDATE_OWNEDBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.DELETE_OWNEDBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_OWNEDBUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX_LIST):
    case FAILURE(ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.CREATE_OWNEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.UPDATE_OWNEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_OWNEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.DELETE_OWNEDBUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_OWNEDBUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.UPDATE_OWNEDBUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_OWNEDBUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_OWNEDBUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/inventoryservice/api/owned-button-color-mtxes';

// Actions

export const getEntities: ICrudGetAllAction<IOwnedButtonColorMtx> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX_LIST,
  payload: axios.get<IOwnedButtonColorMtx>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IOwnedButtonColorMtx> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX,
    payload: axios.get<IOwnedButtonColorMtx>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IOwnedButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_OWNEDBUTTONCOLORMTX,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IOwnedButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_OWNEDBUTTONCOLORMTX,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IOwnedButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_OWNEDBUTTONCOLORMTX,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IOwnedButtonColorMtx> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_OWNEDBUTTONCOLORMTX,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const getEntitiesOfUser: ICrudGetAllAction<IOwnedButtonColorMtx> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_OWNEDBUTTONCOLORMTX_LIST,
  payload: axios.get<IOwnedButtonColorMtx>(`${apiUrl}/userOwnedButtonColorMtx?cacheBuster=${new Date().getTime()}`),
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
