import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './owned-mtx-points.reducer';
import { IOwnedMtxPoints } from 'app/shared/model/inventoryService/owned-mtx-points.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOwnedMtxPointsProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const OwnedMtxPoints = (props: IOwnedMtxPointsProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { ownedMtxPointsList, match, loading } = props;
  return (
    <div>
      <h2 id="owned-mtx-points-heading" data-cy="OwnedMtxPointsHeading">
        Owned Mtx Points
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Owned Mtx Points
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {ownedMtxPointsList && ownedMtxPointsList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Mtx Points</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {ownedMtxPointsList.map((ownedMtxPoints, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${ownedMtxPoints.id}`} color="link" size="sm">
                      {ownedMtxPoints.id}
                    </Button>
                  </td>
                  <td>{ownedMtxPoints.userName}</td>
                  <td>{ownedMtxPoints.mtxPoints}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${ownedMtxPoints.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${ownedMtxPoints.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${ownedMtxPoints.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Owned Mtx Points found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ ownedMtxPoints }: IRootState) => ({
  ownedMtxPointsList: ownedMtxPoints.entities,
  loading: ownedMtxPoints.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OwnedMtxPoints);
