import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './point-option.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPointOptionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PointOptionDetail = (props: IPointOptionDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { pointOptionEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="pointOptionDetailsHeading">PointOption</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{pointOptionEntity.id}</dd>
          <dt>
            <span id="pointAmount">Point Amount</span>
          </dt>
          <dd>{pointOptionEntity.pointAmount}</dd>
          <dt>
            <span id="price">Price</span>
          </dt>
          <dd>{pointOptionEntity.price}</dd>
          <dt>
            <span id="available">Available</span>
          </dt>
          <dd>{pointOptionEntity.available ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/point-option" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/point-option/${pointOptionEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ pointOption }: IRootState) => ({
  pointOptionEntity: pointOption.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PointOptionDetail);
