import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IPaymentInfo, defaultValue } from 'app/shared/model/paymentService/payment-info.model';

export const ACTION_TYPES = {
  FETCH_PAYMENTINFO_LIST: 'paymentInfo/FETCH_PAYMENTINFO_LIST',
  FETCH_PAYMENTINFO: 'paymentInfo/FETCH_PAYMENTINFO',
  CREATE_PAYMENTINFO: 'paymentInfo/CREATE_PAYMENTINFO',
  UPDATE_PAYMENTINFO: 'paymentInfo/UPDATE_PAYMENTINFO',
  PARTIAL_UPDATE_PAYMENTINFO: 'paymentInfo/PARTIAL_UPDATE_PAYMENTINFO',
  DELETE_PAYMENTINFO: 'paymentInfo/DELETE_PAYMENTINFO',
  RESET: 'paymentInfo/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPaymentInfo>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PaymentInfoState = Readonly<typeof initialState>;

// Reducer

export default (state: PaymentInfoState = initialState, action): PaymentInfoState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PAYMENTINFO_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PAYMENTINFO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PAYMENTINFO):
    case REQUEST(ACTION_TYPES.UPDATE_PAYMENTINFO):
    case REQUEST(ACTION_TYPES.DELETE_PAYMENTINFO):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_PAYMENTINFO):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PAYMENTINFO_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PAYMENTINFO):
    case FAILURE(ACTION_TYPES.CREATE_PAYMENTINFO):
    case FAILURE(ACTION_TYPES.UPDATE_PAYMENTINFO):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_PAYMENTINFO):
    case FAILURE(ACTION_TYPES.DELETE_PAYMENTINFO):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PAYMENTINFO_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PAYMENTINFO):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PAYMENTINFO):
    case SUCCESS(ACTION_TYPES.UPDATE_PAYMENTINFO):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_PAYMENTINFO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PAYMENTINFO):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/paymentservice/api/payment-infos';

// Actions

export const getEntities: ICrudGetAllAction<IPaymentInfo> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PAYMENTINFO_LIST,
  payload: axios.get<IPaymentInfo>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPaymentInfo> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PAYMENTINFO,
    payload: axios.get<IPaymentInfo>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPaymentInfo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PAYMENTINFO,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const updateEntity: ICrudPutAction<IPaymentInfo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PAYMENTINFO,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IPaymentInfo> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_PAYMENTINFO,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPaymentInfo> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PAYMENTINFO,
    payload: axios.delete(requestUrl),
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
