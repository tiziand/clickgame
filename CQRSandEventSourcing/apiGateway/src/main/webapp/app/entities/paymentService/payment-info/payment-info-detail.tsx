import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './payment-info.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPaymentInfoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PaymentInfoDetail = (props: IPaymentInfoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { paymentInfoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="paymentInfoDetailsHeading">PaymentInfo</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{paymentInfoEntity.id}</dd>
          <dt>
            <span id="userName">User Name</span>
          </dt>
          <dd>{paymentInfoEntity.userName}</dd>
          <dt>
            <span id="paymentInfo">Payment Info</span>
          </dt>
          <dd>{paymentInfoEntity.paymentInfo}</dd>
          <dt>Point Option</dt>
          <dd>{paymentInfoEntity.pointOption ? paymentInfoEntity.pointOption.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/payment-info" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/payment-info/${paymentInfoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ paymentInfo }: IRootState) => ({
  paymentInfoEntity: paymentInfo.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PaymentInfoDetail);
