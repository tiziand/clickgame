import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IPointOption, defaultValue } from 'app/shared/model/paymentService/point-option.model';

export const ACTION_TYPES = {
  FETCH_POINTOPTION_LIST: 'pointOption/FETCH_POINTOPTION_LIST',
  FETCH_POINTOPTION: 'pointOption/FETCH_POINTOPTION',
  CREATE_POINTOPTION: 'pointOption/CREATE_POINTOPTION',
  UPDATE_POINTOPTION: 'pointOption/UPDATE_POINTOPTION',
  PARTIAL_UPDATE_POINTOPTION: 'pointOption/PARTIAL_UPDATE_POINTOPTION',
  DELETE_POINTOPTION: 'pointOption/DELETE_POINTOPTION',
  RESET: 'pointOption/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPointOption>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PointOptionState = Readonly<typeof initialState>;

// Reducer

export default (state: PointOptionState = initialState, action): PointOptionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_POINTOPTION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_POINTOPTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_POINTOPTION):
    case REQUEST(ACTION_TYPES.UPDATE_POINTOPTION):
    case REQUEST(ACTION_TYPES.DELETE_POINTOPTION):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_POINTOPTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_POINTOPTION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_POINTOPTION):
    case FAILURE(ACTION_TYPES.CREATE_POINTOPTION):
    case FAILURE(ACTION_TYPES.UPDATE_POINTOPTION):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_POINTOPTION):
    case FAILURE(ACTION_TYPES.DELETE_POINTOPTION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_POINTOPTION_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_POINTOPTION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_POINTOPTION):
    case SUCCESS(ACTION_TYPES.UPDATE_POINTOPTION):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_POINTOPTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_POINTOPTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/paymentservice/api/point-options';

// Actions

export const getEntities: ICrudGetAllAction<IPointOption> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_POINTOPTION_LIST,
  payload: axios.get<IPointOption>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPointOption> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_POINTOPTION,
    payload: axios.get<IPointOption>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPointOption> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_POINTOPTION,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPointOption> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_POINTOPTION,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IPointOption> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_POINTOPTION,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPointOption> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_POINTOPTION,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
