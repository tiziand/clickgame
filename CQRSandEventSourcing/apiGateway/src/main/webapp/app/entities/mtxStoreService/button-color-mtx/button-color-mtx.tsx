import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './button-color-mtx.reducer';
import { IButtonColorMtx } from 'app/shared/model/mtxStoreService/button-color-mtx.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IButtonColorMtxProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const ButtonColorMtx = (props: IButtonColorMtxProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { buttonColorMtxList, match, loading } = props;
  return (
    <div>
      <h2 id="button-color-mtx-heading" data-cy="ButtonColorMtxHeading">
        Button Color Mtxes
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Button Color Mtx
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {buttonColorMtxList && buttonColorMtxList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Mtx Color</th>
                <th>Price</th>
                <th>Available</th>
                <th>Time Limit</th>
                <th>Points Required</th>
                <th>Locked</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {buttonColorMtxList.map((buttonColorMtx, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${buttonColorMtx.id}`} color="link" size="sm">
                      {buttonColorMtx.id}
                    </Button>
                  </td>
                  <td>{buttonColorMtx.mtxColor}</td>
                  <td>{buttonColorMtx.price}</td>
                  <td>{buttonColorMtx.available ? 'true' : 'false'}</td>
                  <td>{buttonColorMtx.timeLimit}</td>
                  <td>{buttonColorMtx.pointsRequired}</td>
                  <td>{buttonColorMtx.locked ? 'true' : 'false'}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${buttonColorMtx.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${buttonColorMtx.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${buttonColorMtx.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Button Color Mtxes found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ buttonColorMtx }: IRootState) => ({
  buttonColorMtxList: buttonColorMtx.entities,
  loading: buttonColorMtx.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ButtonColorMtx);
