/**
 * View Models used by Spring MVC REST controllers.
 */
package bt.browsergame.gateway.web.rest.vm;
