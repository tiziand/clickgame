#!/bin/bash

(cd rankingService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag rankingservice tiziand/btkafka:rankingservice; docker push tiziand/btkafka:rankingservice)
(cd apiGateway; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag apigateway tiziand/btkafka:apigateway; docker push tiziand/btkafka:apigateway)
(cd inventoryService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag inventoryservice tiziand/btkafka:inventoryservice; docker push tiziand/btkafka:inventoryservice)
(cd basketService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag basketservice tiziand/btkafka:basketservice; docker push tiziand/btkafka:basketservice)
(cd mtxStoreService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag mtxstoreservice tiziand/btkafka:mtxstoreservice; docker push tiziand/btkafka:mtxstoreservice)
(cd paymentService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag paymentservice tiziand/btkafka:paymentservice; docker push tiziand/btkafka:paymentservice)
(cd statisticService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag statisticservice tiziand/btkafka:statisticservice; docker push tiziand/btkafka:statisticservice)
