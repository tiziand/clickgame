/**
 * View Models used by Spring MVC REST controllers.
 */
package bt.browsergame.payment.web.rest.vm;
