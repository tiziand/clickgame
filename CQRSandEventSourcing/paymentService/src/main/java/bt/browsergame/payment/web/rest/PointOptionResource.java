package bt.browsergame.payment.web.rest;

import bt.browsergame.payment.domain.PointOption;
import bt.browsergame.payment.repository.PointOptionRepository;
import bt.browsergame.payment.service.PointOptionService;
import bt.browsergame.payment.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bt.browsergame.payment.domain.PointOption}.
 */
@RestController
@RequestMapping("/api")
public class PointOptionResource {

    private final Logger log = LoggerFactory.getLogger(PointOptionResource.class);

    private static final String ENTITY_NAME = "paymentServicePointOption";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PointOptionService pointOptionService;

    private final PointOptionRepository pointOptionRepository;

    public PointOptionResource(PointOptionService pointOptionService, PointOptionRepository pointOptionRepository) {
        this.pointOptionService = pointOptionService;
        this.pointOptionRepository = pointOptionRepository;
    }

    /**
     * {@code POST  /point-options} : Create a new pointOption.
     *
     * @param pointOption the pointOption to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new pointOption, or with status {@code 400 (Bad Request)} if the pointOption has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/point-options")
    public ResponseEntity<PointOption> createPointOption(@Valid @RequestBody PointOption pointOption) throws URISyntaxException {
        log.debug("REST request to save PointOption : {}", pointOption);
        if (pointOption.getId() != null) {
            throw new BadRequestAlertException("A new pointOption cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PointOption result = pointOptionService.save(pointOption);
        return ResponseEntity
            .created(new URI("/api/point-options/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /point-options/:id} : Updates an existing pointOption.
     *
     * @param id the id of the pointOption to save.
     * @param pointOption the pointOption to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pointOption,
     * or with status {@code 400 (Bad Request)} if the pointOption is not valid,
     * or with status {@code 500 (Internal Server Error)} if the pointOption couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/point-options/{id}")
    public ResponseEntity<PointOption> updatePointOption(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PointOption pointOption
    ) throws URISyntaxException {
        log.debug("REST request to update PointOption : {}, {}", id, pointOption);
        if (pointOption.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pointOption.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pointOptionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PointOption result = pointOptionService.save(pointOption);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pointOption.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /point-options/:id} : Partial updates given fields of an existing pointOption, field will ignore if it is null
     *
     * @param id the id of the pointOption to save.
     * @param pointOption the pointOption to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated pointOption,
     * or with status {@code 400 (Bad Request)} if the pointOption is not valid,
     * or with status {@code 404 (Not Found)} if the pointOption is not found,
     * or with status {@code 500 (Internal Server Error)} if the pointOption couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/point-options/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PointOption> partialUpdatePointOption(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PointOption pointOption
    ) throws URISyntaxException {
        log.debug("REST request to partial update PointOption partially : {}, {}", id, pointOption);
        if (pointOption.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pointOption.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pointOptionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PointOption> result = pointOptionService.partialUpdate(pointOption);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pointOption.getId().toString())
        );
    }

    /**
     * {@code GET  /point-options} : get all the pointOptions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pointOptions in body.
     */
    @GetMapping("/point-options")
    public List<PointOption> getAllPointOptions() {
        log.debug("REST request to get all PointOptions");
        return pointOptionService.findAll();
    }

    /**
     * {@code GET  /point-options/:id} : get the "id" pointOption.
     *
     * @param id the id of the pointOption to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the pointOption, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/point-options/{id}")
    public ResponseEntity<PointOption> getPointOption(@PathVariable Long id) {
        log.debug("REST request to get PointOption : {}", id);
        Optional<PointOption> pointOption = pointOptionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pointOption);
    }

    /**
     * {@code DELETE  /point-options/:id} : delete the "id" pointOption.
     *
     * @param id the id of the pointOption to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/point-options/{id}")
    public ResponseEntity<Void> deletePointOption(@PathVariable Long id) {
        log.debug("REST request to delete PointOption : {}", id);
        pointOptionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
