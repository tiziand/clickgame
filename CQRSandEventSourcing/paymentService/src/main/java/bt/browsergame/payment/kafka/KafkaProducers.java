package bt.browsergame.payment.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.DoubleSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;

import bt.browsergame.payment.kafka.entities.OwnedMtxPoints;
import bt.browsergame.payment.kafka.serialization.JsonSerializationFactory;

@Configuration
@ConfigurationProperties(prefix = "kafka")
public class KafkaProducers {

    private String bootStrapServers = "localhost:9092";

    public String getBootStrapServers() {
        return bootStrapServers;
    }

    public void setBootStrapServers(String bootStrapServers) {
        this.bootStrapServers = bootStrapServers;
    }

    @Bean(name = "ownedMtxPointsProducer")
    public Producer<String, OwnedMtxPoints> ownedMtxPointsProducer() {
        Properties config = new Properties();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        config.put(ProducerConfig.ACKS_CONFIG, "all");
        config.put(ProducerConfig.RETRIES_CONFIG, 10);
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        final Serde<OwnedMtxPoints> ownedMtxPointsSerde = JsonSerializationFactory
                .<OwnedMtxPoints>getSerde(OwnedMtxPoints.class);

        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ownedMtxPointsSerde.serializer().getClass());

        Producer<String, OwnedMtxPoints> producer = new KafkaProducer<>(config);
        return producer;
    }

    @Bean(name = "purchasesProducer")
    public Producer<String, Double> purchasesProducer() {
        Properties config = new Properties();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        config.put(ProducerConfig.ACKS_CONFIG, "all");
        config.put(ProducerConfig.RETRIES_CONFIG, 10);
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, DoubleSerializer.class);

        Producer<String, Double> producer = new KafkaProducer<>(config);
        return producer;
    }
}
