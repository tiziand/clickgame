package bt.browsergame.payment.repository;

import bt.browsergame.payment.domain.PointOption;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PointOption entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PointOptionRepository extends JpaRepository<PointOption, Long> {}
