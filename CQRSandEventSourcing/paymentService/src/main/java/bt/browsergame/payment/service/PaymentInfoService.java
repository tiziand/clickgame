package bt.browsergame.payment.service;

import bt.browsergame.payment.domain.PaymentInfo;
import bt.browsergame.payment.kafka.entities.OwnedMtxPoints;
import bt.browsergame.payment.repository.PaymentInfoRepository;
import bt.browsergame.payment.security.SecurityUtils;

import java.util.List;
import java.util.Optional;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PaymentInfo}.
 */
@Service
@Transactional
public class PaymentInfoService {

    private final Logger log = LoggerFactory.getLogger(PaymentInfoService.class);

    private final PaymentInfoRepository paymentInfoRepository;

    @Autowired
    private ApplicationContext context;

    public PaymentInfoService(PaymentInfoRepository paymentInfoRepository) {
        this.paymentInfoRepository = paymentInfoRepository;
    }

    /**
     * Save a paymentInfo.
     *
     * @param paymentInfo the entity to save.
     * @return the persisted entity.
     */
    public PaymentInfo save(PaymentInfo paymentInfo) {
        log.debug("Request to save PaymentInfo : {}", paymentInfo);
        // publish purchase to kafka
        OwnedMtxPoints ownedMtxPoints = new OwnedMtxPoints();
        ownedMtxPoints.setMtxPoints(paymentInfo.getPointOption().getPointAmount());
        ownedMtxPoints.setUserName(SecurityUtils.getCurrentUserLogin().get());

        // publish purchase for owned mtx points
        Producer<String, OwnedMtxPoints> producerOwnedMtxPoints = context.getBean("ownedMtxPointsProducer",
                Producer.class);
        producerOwnedMtxPoints.send(new ProducerRecord<String, OwnedMtxPoints>("mtx-points-update",
                ownedMtxPoints.getUserName(), ownedMtxPoints));

        // publish money spent
        Producer<String, Double> producer = context.getBean("purchasesProducer", Producer.class);
        producer.send(new ProducerRecord<String, Double>("purchases", ownedMtxPoints.getUserName(),
                paymentInfo.getPointOption().getPrice()));

        return paymentInfoRepository.save(paymentInfo);
    }

    /**
     * Partially update a paymentInfo.
     *
     * @param paymentInfo the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PaymentInfo> partialUpdate(PaymentInfo paymentInfo) {
        log.debug("Request to partially update PaymentInfo : {}", paymentInfo);

        return paymentInfoRepository.findById(paymentInfo.getId()).map(existingPaymentInfo -> {
            if (paymentInfo.getUserName() != null) {
                existingPaymentInfo.setUserName(paymentInfo.getUserName());
            }
            if (paymentInfo.getPaymentInfo() != null) {
                existingPaymentInfo.setPaymentInfo(paymentInfo.getPaymentInfo());
            }

            return existingPaymentInfo;
        }).map(paymentInfoRepository::save);
    }

    /**
     * Get all the paymentInfos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PaymentInfo> findAll() {
        log.debug("Request to get all PaymentInfos");
        return paymentInfoRepository.findAll();
    }

    /**
     * Get one paymentInfo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PaymentInfo> findOne(Long id) {
        log.debug("Request to get PaymentInfo : {}", id);
        return paymentInfoRepository.findById(id);
    }

    /**
     * Delete the paymentInfo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentInfo : {}", id);
        paymentInfoRepository.deleteById(id);
    }
}
