package bt.browsergame.payment.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.payment.IntegrationTest;
import bt.browsergame.payment.domain.PaymentInfo;
import bt.browsergame.payment.domain.PointOption;
import bt.browsergame.payment.repository.PaymentInfoRepository;
import bt.browsergame.payment.repository.PointOptionRepository;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PaymentInfoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PaymentInfoResourceIT {

    private static final String DEFAULT_USER_NAME = "user";
    private static final String UPDATED_USER_NAME = "user";

    private static final String DEFAULT_PAYMENT_INFO = "AAAAAAAAAA";
    private static final String UPDATED_PAYMENT_INFO = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/payment-infos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PaymentInfoRepository paymentInfoRepository;

    @Autowired
    private PointOptionRepository pointOptionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPaymentInfoMockMvc;

    private PaymentInfo paymentInfo;

    /**
     * Create an entity for this test.
     */
    public PaymentInfo createEntity(EntityManager em) {
        PointOption pointOption = new PointOption().available(true).pointAmount(1000).price(10.0);
        pointOptionRepository.saveAndFlush(pointOption);
        PaymentInfo paymentInfo = new PaymentInfo().userName(DEFAULT_USER_NAME).paymentInfo(DEFAULT_PAYMENT_INFO)
                .pointOption(pointOption);
        return paymentInfo;
    }

    /**
     * Create an updated entity for this test.
     */
    public PaymentInfo createUpdatedEntity(EntityManager em) {
        PointOption pointOption = new PointOption().available(true).pointAmount(1000).price(10.0);
        pointOptionRepository.saveAndFlush(pointOption);
        PaymentInfo paymentInfo = new PaymentInfo().userName(UPDATED_USER_NAME).paymentInfo(UPDATED_PAYMENT_INFO)
                .pointOption(pointOption);
        return paymentInfo;
    }

    @BeforeEach
    public void initTest() {
        paymentInfo = createEntity(em);
    }

    @Test
    @Transactional
    void createPaymentInfo() throws Exception {
        int databaseSizeBeforeCreate = paymentInfoRepository.findAll().size();
        // Create the PaymentInfo
        restPaymentInfoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentInfo)))
            .andExpect(status().isCreated());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeCreate + 1);
        PaymentInfo testPaymentInfo = paymentInfoList.get(paymentInfoList.size() - 1);
        assertThat(testPaymentInfo.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testPaymentInfo.getPaymentInfo()).isEqualTo(DEFAULT_PAYMENT_INFO);
    }

    @Test
    @Transactional
    void createPaymentInfoWithExistingId() throws Exception {
        // Create the PaymentInfo with an existing ID
        paymentInfo.setId(1L);

        int databaseSizeBeforeCreate = paymentInfoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentInfoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentInfo)))
            .andExpect(status().isBadRequest());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkUserNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentInfoRepository.findAll().size();
        // set the field null
        paymentInfo.setUserName(null);

        // Create the PaymentInfo, which fails.

        restPaymentInfoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentInfo)))
            .andExpect(status().isBadRequest());

        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPaymentInfoIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentInfoRepository.findAll().size();
        // set the field null
        paymentInfo.setPaymentInfo(null);

        // Create the PaymentInfo, which fails.

        restPaymentInfoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentInfo)))
            .andExpect(status().isBadRequest());

        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPaymentInfos() throws Exception {
        // Initialize the database
        paymentInfoRepository.saveAndFlush(paymentInfo);

        // Get all the paymentInfoList
        restPaymentInfoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(paymentInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
            .andExpect(jsonPath("$.[*].paymentInfo").value(hasItem(DEFAULT_PAYMENT_INFO)));
    }

    @Test
    @Transactional
    void getPaymentInfo() throws Exception {
        // Initialize the database
        paymentInfoRepository.saveAndFlush(paymentInfo);

        // Get the paymentInfo
        restPaymentInfoMockMvc
            .perform(get(ENTITY_API_URL_ID, paymentInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(paymentInfo.getId().intValue()))
            .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
            .andExpect(jsonPath("$.paymentInfo").value(DEFAULT_PAYMENT_INFO));
    }

    @Test
    @Transactional
    void getNonExistingPaymentInfo() throws Exception {
        // Get the paymentInfo
        restPaymentInfoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPaymentInfo() throws Exception {
        // Initialize the database
        paymentInfoRepository.saveAndFlush(paymentInfo);

        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();

        // Update the paymentInfo
        PaymentInfo updatedPaymentInfo = paymentInfoRepository.findById(paymentInfo.getId()).get();
        // Disconnect from session so that the updates on updatedPaymentInfo are not directly saved in db
        em.detach(updatedPaymentInfo);
        updatedPaymentInfo.userName(UPDATED_USER_NAME).paymentInfo(UPDATED_PAYMENT_INFO);

        restPaymentInfoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPaymentInfo.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPaymentInfo))
            )
            .andExpect(status().isOk());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
        PaymentInfo testPaymentInfo = paymentInfoList.get(paymentInfoList.size() - 1);
        assertThat(testPaymentInfo.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testPaymentInfo.getPaymentInfo()).isEqualTo(UPDATED_PAYMENT_INFO);
    }

    @Test
    @Transactional
    void putNonExistingPaymentInfo() throws Exception {
        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();
        paymentInfo.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentInfoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentInfo.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentInfo))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPaymentInfo() throws Exception {
        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();
        paymentInfo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentInfoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentInfo))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPaymentInfo() throws Exception {
        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();
        paymentInfo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentInfoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentInfo)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePaymentInfoWithPatch() throws Exception {
        // Initialize the database
        paymentInfoRepository.saveAndFlush(paymentInfo);

        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();

        // Update the paymentInfo using partial update
        PaymentInfo partialUpdatedPaymentInfo = new PaymentInfo();
        partialUpdatedPaymentInfo.setId(paymentInfo.getId());

        partialUpdatedPaymentInfo.userName(UPDATED_USER_NAME);

        restPaymentInfoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentInfo.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentInfo))
            )
            .andExpect(status().isOk());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
        PaymentInfo testPaymentInfo = paymentInfoList.get(paymentInfoList.size() - 1);
        assertThat(testPaymentInfo.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testPaymentInfo.getPaymentInfo()).isEqualTo(DEFAULT_PAYMENT_INFO);
    }

    @Test
    @Transactional
    void fullUpdatePaymentInfoWithPatch() throws Exception {
        // Initialize the database
        paymentInfoRepository.saveAndFlush(paymentInfo);

        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();

        // Update the paymentInfo using partial update
        PaymentInfo partialUpdatedPaymentInfo = new PaymentInfo();
        partialUpdatedPaymentInfo.setId(paymentInfo.getId());

        partialUpdatedPaymentInfo.userName(UPDATED_USER_NAME).paymentInfo(UPDATED_PAYMENT_INFO);

        restPaymentInfoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPaymentInfo.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPaymentInfo))
            )
            .andExpect(status().isOk());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
        PaymentInfo testPaymentInfo = paymentInfoList.get(paymentInfoList.size() - 1);
        assertThat(testPaymentInfo.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testPaymentInfo.getPaymentInfo()).isEqualTo(UPDATED_PAYMENT_INFO);
    }

    @Test
    @Transactional
    void patchNonExistingPaymentInfo() throws Exception {
        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();
        paymentInfo.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentInfoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, paymentInfo.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentInfo))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPaymentInfo() throws Exception {
        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();
        paymentInfo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentInfoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentInfo))
            )
            .andExpect(status().isBadRequest());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPaymentInfo() throws Exception {
        int databaseSizeBeforeUpdate = paymentInfoRepository.findAll().size();
        paymentInfo.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentInfoMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(paymentInfo))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PaymentInfo in the database
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePaymentInfo() throws Exception {
        // Initialize the database
        paymentInfoRepository.saveAndFlush(paymentInfo);

        int databaseSizeBeforeDelete = paymentInfoRepository.findAll().size();

        // Delete the paymentInfo
        restPaymentInfoMockMvc
            .perform(delete(ENTITY_API_URL_ID, paymentInfo.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PaymentInfo> paymentInfoList = paymentInfoRepository.findAll();
        assertThat(paymentInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
