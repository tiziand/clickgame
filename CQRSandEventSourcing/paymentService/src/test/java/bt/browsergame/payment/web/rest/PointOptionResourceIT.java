package bt.browsergame.payment.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.payment.IntegrationTest;
import bt.browsergame.payment.domain.PointOption;
import bt.browsergame.payment.repository.PointOptionRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PointOptionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PointOptionResourceIT {

    private static final Integer DEFAULT_POINT_AMOUNT = 1;
    private static final Integer UPDATED_POINT_AMOUNT = 2;

    private static final Double DEFAULT_PRICE = 1D;
    private static final Double UPDATED_PRICE = 2D;

    private static final Boolean DEFAULT_AVAILABLE = false;
    private static final Boolean UPDATED_AVAILABLE = true;

    private static final String ENTITY_API_URL = "/api/point-options";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PointOptionRepository pointOptionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPointOptionMockMvc;

    private PointOption pointOption;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointOption createEntity(EntityManager em) {
        PointOption pointOption = new PointOption().pointAmount(DEFAULT_POINT_AMOUNT).price(DEFAULT_PRICE).available(DEFAULT_AVAILABLE);
        return pointOption;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PointOption createUpdatedEntity(EntityManager em) {
        PointOption pointOption = new PointOption().pointAmount(UPDATED_POINT_AMOUNT).price(UPDATED_PRICE).available(UPDATED_AVAILABLE);
        return pointOption;
    }

    @BeforeEach
    public void initTest() {
        pointOption = createEntity(em);
    }

    @Test
    @Transactional
    void createPointOption() throws Exception {
        int databaseSizeBeforeCreate = pointOptionRepository.findAll().size();
        // Create the PointOption
        restPointOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointOption)))
            .andExpect(status().isCreated());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeCreate + 1);
        PointOption testPointOption = pointOptionList.get(pointOptionList.size() - 1);
        assertThat(testPointOption.getPointAmount()).isEqualTo(DEFAULT_POINT_AMOUNT);
        assertThat(testPointOption.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testPointOption.getAvailable()).isEqualTo(DEFAULT_AVAILABLE);
    }

    @Test
    @Transactional
    void createPointOptionWithExistingId() throws Exception {
        // Create the PointOption with an existing ID
        pointOption.setId(1L);

        int databaseSizeBeforeCreate = pointOptionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPointOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointOption)))
            .andExpect(status().isBadRequest());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkPointAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = pointOptionRepository.findAll().size();
        // set the field null
        pointOption.setPointAmount(null);

        // Create the PointOption, which fails.

        restPointOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointOption)))
            .andExpect(status().isBadRequest());

        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = pointOptionRepository.findAll().size();
        // set the field null
        pointOption.setPrice(null);

        // Create the PointOption, which fails.

        restPointOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointOption)))
            .andExpect(status().isBadRequest());

        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAvailableIsRequired() throws Exception {
        int databaseSizeBeforeTest = pointOptionRepository.findAll().size();
        // set the field null
        pointOption.setAvailable(null);

        // Create the PointOption, which fails.

        restPointOptionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointOption)))
            .andExpect(status().isBadRequest());

        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPointOptions() throws Exception {
        // Initialize the database
        pointOptionRepository.saveAndFlush(pointOption);

        // Get all the pointOptionList
        restPointOptionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pointOption.getId().intValue())))
            .andExpect(jsonPath("$.[*].pointAmount").value(hasItem(DEFAULT_POINT_AMOUNT)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].available").value(hasItem(DEFAULT_AVAILABLE.booleanValue())));
    }

    @Test
    @Transactional
    void getPointOption() throws Exception {
        // Initialize the database
        pointOptionRepository.saveAndFlush(pointOption);

        // Get the pointOption
        restPointOptionMockMvc
            .perform(get(ENTITY_API_URL_ID, pointOption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pointOption.getId().intValue()))
            .andExpect(jsonPath("$.pointAmount").value(DEFAULT_POINT_AMOUNT))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.available").value(DEFAULT_AVAILABLE.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingPointOption() throws Exception {
        // Get the pointOption
        restPointOptionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPointOption() throws Exception {
        // Initialize the database
        pointOptionRepository.saveAndFlush(pointOption);

        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();

        // Update the pointOption
        PointOption updatedPointOption = pointOptionRepository.findById(pointOption.getId()).get();
        // Disconnect from session so that the updates on updatedPointOption are not directly saved in db
        em.detach(updatedPointOption);
        updatedPointOption.pointAmount(UPDATED_POINT_AMOUNT).price(UPDATED_PRICE).available(UPDATED_AVAILABLE);

        restPointOptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPointOption.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPointOption))
            )
            .andExpect(status().isOk());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
        PointOption testPointOption = pointOptionList.get(pointOptionList.size() - 1);
        assertThat(testPointOption.getPointAmount()).isEqualTo(UPDATED_POINT_AMOUNT);
        assertThat(testPointOption.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testPointOption.getAvailable()).isEqualTo(UPDATED_AVAILABLE);
    }

    @Test
    @Transactional
    void putNonExistingPointOption() throws Exception {
        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();
        pointOption.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPointOptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, pointOption.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pointOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPointOption() throws Exception {
        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();
        pointOption.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointOptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(pointOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPointOption() throws Exception {
        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();
        pointOption.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointOptionMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(pointOption)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePointOptionWithPatch() throws Exception {
        // Initialize the database
        pointOptionRepository.saveAndFlush(pointOption);

        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();

        // Update the pointOption using partial update
        PointOption partialUpdatedPointOption = new PointOption();
        partialUpdatedPointOption.setId(pointOption.getId());

        partialUpdatedPointOption.pointAmount(UPDATED_POINT_AMOUNT);

        restPointOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPointOption.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPointOption))
            )
            .andExpect(status().isOk());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
        PointOption testPointOption = pointOptionList.get(pointOptionList.size() - 1);
        assertThat(testPointOption.getPointAmount()).isEqualTo(UPDATED_POINT_AMOUNT);
        assertThat(testPointOption.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testPointOption.getAvailable()).isEqualTo(DEFAULT_AVAILABLE);
    }

    @Test
    @Transactional
    void fullUpdatePointOptionWithPatch() throws Exception {
        // Initialize the database
        pointOptionRepository.saveAndFlush(pointOption);

        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();

        // Update the pointOption using partial update
        PointOption partialUpdatedPointOption = new PointOption();
        partialUpdatedPointOption.setId(pointOption.getId());

        partialUpdatedPointOption.pointAmount(UPDATED_POINT_AMOUNT).price(UPDATED_PRICE).available(UPDATED_AVAILABLE);

        restPointOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPointOption.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPointOption))
            )
            .andExpect(status().isOk());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
        PointOption testPointOption = pointOptionList.get(pointOptionList.size() - 1);
        assertThat(testPointOption.getPointAmount()).isEqualTo(UPDATED_POINT_AMOUNT);
        assertThat(testPointOption.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testPointOption.getAvailable()).isEqualTo(UPDATED_AVAILABLE);
    }

    @Test
    @Transactional
    void patchNonExistingPointOption() throws Exception {
        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();
        pointOption.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPointOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, pointOption.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pointOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPointOption() throws Exception {
        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();
        pointOption.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointOptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(pointOption))
            )
            .andExpect(status().isBadRequest());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPointOption() throws Exception {
        int databaseSizeBeforeUpdate = pointOptionRepository.findAll().size();
        pointOption.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPointOptionMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(pointOption))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PointOption in the database
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePointOption() throws Exception {
        // Initialize the database
        pointOptionRepository.saveAndFlush(pointOption);

        int databaseSizeBeforeDelete = pointOptionRepository.findAll().size();

        // Delete the pointOption
        restPointOptionMockMvc
            .perform(delete(ENTITY_API_URL_ID, pointOption.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PointOption> pointOptionList = pointOptionRepository.findAll();
        assertThat(pointOptionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
