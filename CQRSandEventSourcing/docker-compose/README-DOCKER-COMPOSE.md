# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:

- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:

- apiGateway (gateway application)
- apiGateway's postgresql database
- statisticService (microservice application)
- statisticService's postgresql database
- inventoryService (microservice application)
- inventoryService's postgresql database
- paymentService (microservice application)
- paymentService's postgresql database
- mtxStoreService (microservice application)
- mtxStoreService's postgresql database
- basketService (microservice application)
- basketService's postgresql database
- rankingService (microservice application)
- rankingService's postgresql database

### Additional Services:

- Kafka
- Zookeeper
