package bt.browsergame.statistic.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.statistic.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PurchasesButtonColorMtxTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PurchasesButtonColorMtx.class);
        PurchasesButtonColorMtx purchasesButtonColorMtx1 = new PurchasesButtonColorMtx();
        purchasesButtonColorMtx1.setId(1L);
        PurchasesButtonColorMtx purchasesButtonColorMtx2 = new PurchasesButtonColorMtx();
        purchasesButtonColorMtx2.setId(purchasesButtonColorMtx1.getId());
        assertThat(purchasesButtonColorMtx1).isEqualTo(purchasesButtonColorMtx2);
        purchasesButtonColorMtx2.setId(2L);
        assertThat(purchasesButtonColorMtx1).isNotEqualTo(purchasesButtonColorMtx2);
        purchasesButtonColorMtx1.setId(null);
        assertThat(purchasesButtonColorMtx1).isNotEqualTo(purchasesButtonColorMtx2);
    }
}
