package bt.browsergame.statistic.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.statistic.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ClickPerMinTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClickPerMin.class);
        ClickPerMin clickPerMin1 = new ClickPerMin();
        clickPerMin1.setId(1L);
        ClickPerMin clickPerMin2 = new ClickPerMin();
        clickPerMin2.setId(clickPerMin1.getId());
        assertThat(clickPerMin1).isEqualTo(clickPerMin2);
        clickPerMin2.setId(2L);
        assertThat(clickPerMin1).isNotEqualTo(clickPerMin2);
        clickPerMin1.setId(null);
        assertThat(clickPerMin1).isNotEqualTo(clickPerMin2);
    }
}
