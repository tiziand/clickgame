package bt.browsergame.statistic.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.statistic.IntegrationTest;
import bt.browsergame.statistic.domain.PurchasesMtxPoints;
import bt.browsergame.statistic.repository.PurchasesMtxPointsRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PurchasesMtxPointsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PurchasesMtxPointsResourceIT {

    private static final Instant DEFAULT_CREATION_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATION_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_MONEY_SPENT = 1D;
    private static final Double UPDATED_MONEY_SPENT = 2D;

    private static final String ENTITY_API_URL = "/api/purchases-mtx-points";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PurchasesMtxPointsRepository purchasesMtxPointsRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPurchasesMtxPointsMockMvc;

    private PurchasesMtxPoints purchasesMtxPoints;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchasesMtxPoints createEntity(EntityManager em) {
        PurchasesMtxPoints purchasesMtxPoints = new PurchasesMtxPoints()
            .creationTime(DEFAULT_CREATION_TIME)
            .moneySpent(DEFAULT_MONEY_SPENT);
        return purchasesMtxPoints;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchasesMtxPoints createUpdatedEntity(EntityManager em) {
        PurchasesMtxPoints purchasesMtxPoints = new PurchasesMtxPoints()
            .creationTime(UPDATED_CREATION_TIME)
            .moneySpent(UPDATED_MONEY_SPENT);
        return purchasesMtxPoints;
    }

    @BeforeEach
    public void initTest() {
        purchasesMtxPoints = createEntity(em);
    }

    @Test
    @Transactional
    void createPurchasesMtxPoints() throws Exception {
        int databaseSizeBeforeCreate = purchasesMtxPointsRepository.findAll().size();
        // Create the PurchasesMtxPoints
        restPurchasesMtxPointsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isCreated());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeCreate + 1);
        PurchasesMtxPoints testPurchasesMtxPoints = purchasesMtxPointsList.get(purchasesMtxPointsList.size() - 1);
        assertThat(testPurchasesMtxPoints.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testPurchasesMtxPoints.getMoneySpent()).isEqualTo(DEFAULT_MONEY_SPENT);
    }

    @Test
    @Transactional
    void createPurchasesMtxPointsWithExistingId() throws Exception {
        // Create the PurchasesMtxPoints with an existing ID
        purchasesMtxPoints.setId(1L);

        int databaseSizeBeforeCreate = purchasesMtxPointsRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPurchasesMtxPointsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCreationTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchasesMtxPointsRepository.findAll().size();
        // set the field null
        purchasesMtxPoints.setCreationTime(null);

        // Create the PurchasesMtxPoints, which fails.

        restPurchasesMtxPointsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isBadRequest());

        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMoneySpentIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchasesMtxPointsRepository.findAll().size();
        // set the field null
        purchasesMtxPoints.setMoneySpent(null);

        // Create the PurchasesMtxPoints, which fails.

        restPurchasesMtxPointsMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isBadRequest());

        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPurchasesMtxPoints() throws Exception {
        // Initialize the database
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);

        // Get all the purchasesMtxPointsList
        restPurchasesMtxPointsMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(purchasesMtxPoints.getId().intValue())))
            .andExpect(jsonPath("$.[*].creationTime").value(hasItem(DEFAULT_CREATION_TIME.toString())))
            .andExpect(jsonPath("$.[*].moneySpent").value(hasItem(DEFAULT_MONEY_SPENT.doubleValue())));
    }

    @Test
    @Transactional
    void getPurchasesMtxPoints() throws Exception {
        // Initialize the database
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);

        // Get the purchasesMtxPoints
        restPurchasesMtxPointsMockMvc
            .perform(get(ENTITY_API_URL_ID, purchasesMtxPoints.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(purchasesMtxPoints.getId().intValue()))
            .andExpect(jsonPath("$.creationTime").value(DEFAULT_CREATION_TIME.toString()))
            .andExpect(jsonPath("$.moneySpent").value(DEFAULT_MONEY_SPENT.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingPurchasesMtxPoints() throws Exception {
        // Get the purchasesMtxPoints
        restPurchasesMtxPointsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPurchasesMtxPoints() throws Exception {
        // Initialize the database
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);

        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();

        // Update the purchasesMtxPoints
        PurchasesMtxPoints updatedPurchasesMtxPoints = purchasesMtxPointsRepository.findById(purchasesMtxPoints.getId()).get();
        // Disconnect from session so that the updates on updatedPurchasesMtxPoints are not directly saved in db
        em.detach(updatedPurchasesMtxPoints);
        updatedPurchasesMtxPoints.creationTime(UPDATED_CREATION_TIME).moneySpent(UPDATED_MONEY_SPENT);

        restPurchasesMtxPointsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPurchasesMtxPoints.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPurchasesMtxPoints))
            )
            .andExpect(status().isOk());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        PurchasesMtxPoints testPurchasesMtxPoints = purchasesMtxPointsList.get(purchasesMtxPointsList.size() - 1);
        assertThat(testPurchasesMtxPoints.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testPurchasesMtxPoints.getMoneySpent()).isEqualTo(UPDATED_MONEY_SPENT);
    }

    @Test
    @Transactional
    void putNonExistingPurchasesMtxPoints() throws Exception {
        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();
        purchasesMtxPoints.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPurchasesMtxPointsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, purchasesMtxPoints.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPurchasesMtxPoints() throws Exception {
        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();
        purchasesMtxPoints.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesMtxPointsMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPurchasesMtxPoints() throws Exception {
        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();
        purchasesMtxPoints.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesMtxPointsMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePurchasesMtxPointsWithPatch() throws Exception {
        // Initialize the database
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);

        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();

        // Update the purchasesMtxPoints using partial update
        PurchasesMtxPoints partialUpdatedPurchasesMtxPoints = new PurchasesMtxPoints();
        partialUpdatedPurchasesMtxPoints.setId(purchasesMtxPoints.getId());

        partialUpdatedPurchasesMtxPoints.creationTime(UPDATED_CREATION_TIME);

        restPurchasesMtxPointsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPurchasesMtxPoints.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchasesMtxPoints))
            )
            .andExpect(status().isOk());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        PurchasesMtxPoints testPurchasesMtxPoints = purchasesMtxPointsList.get(purchasesMtxPointsList.size() - 1);
        assertThat(testPurchasesMtxPoints.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testPurchasesMtxPoints.getMoneySpent()).isEqualTo(DEFAULT_MONEY_SPENT);
    }

    @Test
    @Transactional
    void fullUpdatePurchasesMtxPointsWithPatch() throws Exception {
        // Initialize the database
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);

        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();

        // Update the purchasesMtxPoints using partial update
        PurchasesMtxPoints partialUpdatedPurchasesMtxPoints = new PurchasesMtxPoints();
        partialUpdatedPurchasesMtxPoints.setId(purchasesMtxPoints.getId());

        partialUpdatedPurchasesMtxPoints.creationTime(UPDATED_CREATION_TIME).moneySpent(UPDATED_MONEY_SPENT);

        restPurchasesMtxPointsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPurchasesMtxPoints.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchasesMtxPoints))
            )
            .andExpect(status().isOk());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        PurchasesMtxPoints testPurchasesMtxPoints = purchasesMtxPointsList.get(purchasesMtxPointsList.size() - 1);
        assertThat(testPurchasesMtxPoints.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testPurchasesMtxPoints.getMoneySpent()).isEqualTo(UPDATED_MONEY_SPENT);
    }

    @Test
    @Transactional
    void patchNonExistingPurchasesMtxPoints() throws Exception {
        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();
        purchasesMtxPoints.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPurchasesMtxPointsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, purchasesMtxPoints.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPurchasesMtxPoints() throws Exception {
        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();
        purchasesMtxPoints.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesMtxPointsMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPurchasesMtxPoints() throws Exception {
        int databaseSizeBeforeUpdate = purchasesMtxPointsRepository.findAll().size();
        purchasesMtxPoints.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesMtxPointsMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purchasesMtxPoints))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PurchasesMtxPoints in the database
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePurchasesMtxPoints() throws Exception {
        // Initialize the database
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);

        int databaseSizeBeforeDelete = purchasesMtxPointsRepository.findAll().size();

        // Delete the purchasesMtxPoints
        restPurchasesMtxPointsMockMvc
                .perform(delete(ENTITY_API_URL_ID, purchasesMtxPoints.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PurchasesMtxPoints> purchasesMtxPointsList = purchasesMtxPointsRepository.findAll();
        assertThat(purchasesMtxPointsList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
    @Transactional
    void getMoneySpentSum() throws Exception {
        // Initialize the database
        Instant today = Instant.now();
        String todayString = today.toString().substring(0, 10);
        Instant yesterday = Instant.now().minus(1, ChronoUnit.DAYS);
        String yesterdayString = yesterday.toString().substring(0, 10);
        PurchasesMtxPoints purchasesMtxPoints = new PurchasesMtxPoints().creationTime(yesterday).moneySpent(100D);
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);
        purchasesMtxPoints = new PurchasesMtxPoints().creationTime(today).moneySpent(100D);
        purchasesMtxPointsRepository.saveAndFlush(purchasesMtxPoints);

        // Get the purchasesMtxPoints two date versions
        restPurchasesMtxPointsMockMvc
                .perform(get(ENTITY_API_URL + "/moneySpentSum/" + yesterdayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.sumMoneySpent").value(200));

        restPurchasesMtxPointsMockMvc.perform(get(ENTITY_API_URL + "/moneySpentSum/" + todayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.sumMoneySpent").value(100));
    }
}
