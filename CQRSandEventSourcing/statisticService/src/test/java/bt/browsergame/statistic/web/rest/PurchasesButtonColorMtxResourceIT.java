package bt.browsergame.statistic.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.statistic.IntegrationTest;
import bt.browsergame.statistic.domain.PurchasesButtonColorMtx;
import bt.browsergame.statistic.repository.PurchasesButtonColorMtxRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PurchasesButtonColorMtxResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PurchasesButtonColorMtxResourceIT {

    private static final Long DEFAULT_MTX_ID = 1L;
    private static final Long UPDATED_MTX_ID = 2L;

    private static final Instant DEFAULT_CREATION_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATION_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_AMOUNT = 1L;
    private static final Long UPDATED_AMOUNT = 2L;

    private static final String ENTITY_API_URL = "/api/purchases-button-color-mtxes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PurchasesButtonColorMtxRepository purchasesButtonColorMtxRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPurchasesButtonColorMtxMockMvc;

    private PurchasesButtonColorMtx purchasesButtonColorMtx;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchasesButtonColorMtx createEntity(EntityManager em) {
        PurchasesButtonColorMtx purchasesButtonColorMtx = new PurchasesButtonColorMtx()
            .mtxId(DEFAULT_MTX_ID)
            .creationTime(DEFAULT_CREATION_TIME)
            .amount(DEFAULT_AMOUNT);
        return purchasesButtonColorMtx;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchasesButtonColorMtx createUpdatedEntity(EntityManager em) {
        PurchasesButtonColorMtx purchasesButtonColorMtx = new PurchasesButtonColorMtx()
            .mtxId(UPDATED_MTX_ID)
            .creationTime(UPDATED_CREATION_TIME)
            .amount(UPDATED_AMOUNT);
        return purchasesButtonColorMtx;
    }

    @BeforeEach
    public void initTest() {
        purchasesButtonColorMtx = createEntity(em);
    }

    @Test
    @Transactional
    void createPurchasesButtonColorMtx() throws Exception {
        int databaseSizeBeforeCreate = purchasesButtonColorMtxRepository.findAll().size();
        // Create the PurchasesButtonColorMtx
        restPurchasesButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isCreated());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeCreate + 1);
        PurchasesButtonColorMtx testPurchasesButtonColorMtx = purchasesButtonColorMtxList.get(purchasesButtonColorMtxList.size() - 1);
        assertThat(testPurchasesButtonColorMtx.getMtxId()).isEqualTo(DEFAULT_MTX_ID);
        assertThat(testPurchasesButtonColorMtx.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testPurchasesButtonColorMtx.getAmount()).isEqualTo(DEFAULT_AMOUNT);
    }

    @Test
    @Transactional
    void createPurchasesButtonColorMtxWithExistingId() throws Exception {
        // Create the PurchasesButtonColorMtx with an existing ID
        purchasesButtonColorMtx.setId(1L);

        int databaseSizeBeforeCreate = purchasesButtonColorMtxRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPurchasesButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMtxIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchasesButtonColorMtxRepository.findAll().size();
        // set the field null
        purchasesButtonColorMtx.setMtxId(null);

        // Create the PurchasesButtonColorMtx, which fails.

        restPurchasesButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreationTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchasesButtonColorMtxRepository.findAll().size();
        // set the field null
        purchasesButtonColorMtx.setCreationTime(null);

        // Create the PurchasesButtonColorMtx, which fails.

        restPurchasesButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = purchasesButtonColorMtxRepository.findAll().size();
        // set the field null
        purchasesButtonColorMtx.setAmount(null);

        // Create the PurchasesButtonColorMtx, which fails.

        restPurchasesButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPurchasesButtonColorMtxes() throws Exception {
        // Initialize the database
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        // Get all the purchasesButtonColorMtxList
        restPurchasesButtonColorMtxMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(purchasesButtonColorMtx.getId().intValue())))
            .andExpect(jsonPath("$.[*].mtxId").value(hasItem(DEFAULT_MTX_ID.intValue())))
            .andExpect(jsonPath("$.[*].creationTime").value(hasItem(DEFAULT_CREATION_TIME.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())));
    }

    @Test
    @Transactional
    void getPurchasesButtonColorMtx() throws Exception {
        // Initialize the database
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        // Get the purchasesButtonColorMtx
        restPurchasesButtonColorMtxMockMvc
            .perform(get(ENTITY_API_URL_ID, purchasesButtonColorMtx.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(purchasesButtonColorMtx.getId().intValue()))
            .andExpect(jsonPath("$.mtxId").value(DEFAULT_MTX_ID.intValue()))
            .andExpect(jsonPath("$.creationTime").value(DEFAULT_CREATION_TIME.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingPurchasesButtonColorMtx() throws Exception {
        // Get the purchasesButtonColorMtx
        restPurchasesButtonColorMtxMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPurchasesButtonColorMtx() throws Exception {
        // Initialize the database
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();

        // Update the purchasesButtonColorMtx
        PurchasesButtonColorMtx updatedPurchasesButtonColorMtx = purchasesButtonColorMtxRepository
            .findById(purchasesButtonColorMtx.getId())
            .get();
        // Disconnect from session so that the updates on updatedPurchasesButtonColorMtx are not directly saved in db
        em.detach(updatedPurchasesButtonColorMtx);
        updatedPurchasesButtonColorMtx.mtxId(UPDATED_MTX_ID).creationTime(UPDATED_CREATION_TIME).amount(UPDATED_AMOUNT);

        restPurchasesButtonColorMtxMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPurchasesButtonColorMtx.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPurchasesButtonColorMtx))
            )
            .andExpect(status().isOk());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        PurchasesButtonColorMtx testPurchasesButtonColorMtx = purchasesButtonColorMtxList.get(purchasesButtonColorMtxList.size() - 1);
        assertThat(testPurchasesButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
        assertThat(testPurchasesButtonColorMtx.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testPurchasesButtonColorMtx.getAmount()).isEqualTo(UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void putNonExistingPurchasesButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();
        purchasesButtonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPurchasesButtonColorMtxMockMvc
            .perform(
                put(ENTITY_API_URL_ID, purchasesButtonColorMtx.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPurchasesButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();
        purchasesButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesButtonColorMtxMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPurchasesButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();
        purchasesButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesButtonColorMtxMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePurchasesButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();

        // Update the purchasesButtonColorMtx using partial update
        PurchasesButtonColorMtx partialUpdatedPurchasesButtonColorMtx = new PurchasesButtonColorMtx();
        partialUpdatedPurchasesButtonColorMtx.setId(purchasesButtonColorMtx.getId());

        partialUpdatedPurchasesButtonColorMtx.mtxId(UPDATED_MTX_ID).creationTime(UPDATED_CREATION_TIME).amount(UPDATED_AMOUNT);

        restPurchasesButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPurchasesButtonColorMtx.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchasesButtonColorMtx))
            )
            .andExpect(status().isOk());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        PurchasesButtonColorMtx testPurchasesButtonColorMtx = purchasesButtonColorMtxList.get(purchasesButtonColorMtxList.size() - 1);
        assertThat(testPurchasesButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
        assertThat(testPurchasesButtonColorMtx.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testPurchasesButtonColorMtx.getAmount()).isEqualTo(UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void fullUpdatePurchasesButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();

        // Update the purchasesButtonColorMtx using partial update
        PurchasesButtonColorMtx partialUpdatedPurchasesButtonColorMtx = new PurchasesButtonColorMtx();
        partialUpdatedPurchasesButtonColorMtx.setId(purchasesButtonColorMtx.getId());

        partialUpdatedPurchasesButtonColorMtx.mtxId(UPDATED_MTX_ID).creationTime(UPDATED_CREATION_TIME).amount(UPDATED_AMOUNT);

        restPurchasesButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPurchasesButtonColorMtx.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPurchasesButtonColorMtx))
            )
            .andExpect(status().isOk());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        PurchasesButtonColorMtx testPurchasesButtonColorMtx = purchasesButtonColorMtxList.get(purchasesButtonColorMtxList.size() - 1);
        assertThat(testPurchasesButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
        assertThat(testPurchasesButtonColorMtx.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testPurchasesButtonColorMtx.getAmount()).isEqualTo(UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void patchNonExistingPurchasesButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();
        purchasesButtonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPurchasesButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, purchasesButtonColorMtx.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPurchasesButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();
        purchasesButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPurchasesButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = purchasesButtonColorMtxRepository.findAll().size();
        purchasesButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPurchasesButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(purchasesButtonColorMtx))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PurchasesButtonColorMtx in the database
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePurchasesButtonColorMtx() throws Exception {
        // Initialize the database
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        int databaseSizeBeforeDelete = purchasesButtonColorMtxRepository.findAll().size();

        // Delete the purchasesButtonColorMtx
        restPurchasesButtonColorMtxMockMvc
                .perform(delete(ENTITY_API_URL_ID, purchasesButtonColorMtx.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PurchasesButtonColorMtx> purchasesButtonColorMtxList = purchasesButtonColorMtxRepository.findAll();
        assertThat(purchasesButtonColorMtxList).hasSize(databaseSizeBeforeDelete - 1);
    }
    

    @Test
    @Transactional
    void getPurchasesSum() throws Exception {
        // Initialize the database
        Instant today = Instant.now();
        String todayString = today.toString().substring(0, 10);
        Instant yesterday = Instant.now().minus(1, ChronoUnit.DAYS);
        String yesterdayString = yesterday.toString().substring(0, 10);
        PurchasesButtonColorMtx purchasesButtonColorMtx = new PurchasesButtonColorMtx().creationTime(yesterday)
                .mtxId(1L).amount(1L);
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);
        purchasesButtonColorMtx = new PurchasesButtonColorMtx().creationTime(today).mtxId(1L).amount(1L);
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        // Get the purchasesMtxPoints two date versions
        restPurchasesButtonColorMtxMockMvc
                .perform(get(ENTITY_API_URL + "/purchasesSum/" + yesterdayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.sumPurchases").value(2));

        restPurchasesButtonColorMtxMockMvc
                .perform(get(ENTITY_API_URL + "/purchasesSum/" + todayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.sumPurchases").value(1));
    }

    @Test
    @Transactional
    void getAllPurchasesSumList() throws Exception {
        // Initialize the database
        Instant today = Instant.now();
        String todayString = today.toString().substring(0, 10);
        Instant yesterday = Instant.now().minus(1, ChronoUnit.DAYS);
        String yesterdayString = yesterday.toString().substring(0, 10);
        PurchasesButtonColorMtx purchasesButtonColorMtx = new PurchasesButtonColorMtx().creationTime(yesterday)
                .mtxId(1L).amount(1L);
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);
        purchasesButtonColorMtx = new PurchasesButtonColorMtx().creationTime(today).mtxId(1L).amount(1L);
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);
        purchasesButtonColorMtx = new PurchasesButtonColorMtx().creationTime(today).mtxId(2L).amount(1L);
        purchasesButtonColorMtxRepository.saveAndFlush(purchasesButtonColorMtx);

        // Get the purchasesMtxPoints two date versions
        restPurchasesButtonColorMtxMockMvc
                .perform(get(ENTITY_API_URL + "/pruchasesSumList/" + yesterdayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].sumPurchases").value(hasItem(1)))
                .andExpect(jsonPath("$.[*].sumPurchases").value(hasItem(2)))
                .andExpect(jsonPath("$.[*].mtxId").value((hasItem(1))))
                .andExpect(jsonPath("$.[*].mtxId").value((hasItem(2))));

        restPurchasesButtonColorMtxMockMvc
                .perform(get(ENTITY_API_URL + "/pruchasesSumList/" + todayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].sumPurchases").value((hasItem(1))))
                .andExpect(jsonPath("$.[*].sumPurchases").value(not(hasItem(2))))
                .andExpect(jsonPath("$.[*].mtxId").value((hasItem(1))))
                .andExpect(jsonPath("$.[*].mtxId").value((hasItem(2))));
    }
}
