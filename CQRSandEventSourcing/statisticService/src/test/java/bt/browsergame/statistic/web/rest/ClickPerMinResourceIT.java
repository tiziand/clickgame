package bt.browsergame.statistic.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.statistic.IntegrationTest;
import bt.browsergame.statistic.domain.ClickPerMin;
import bt.browsergame.statistic.repository.ClickPerMinRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ClickPerMinResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ClickPerMinResourceIT {

    private static final Instant DEFAULT_CREATION_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATION_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_CLICK_COUNT = 1L;
    private static final Long UPDATED_CLICK_COUNT = 2L;

    private static final String ENTITY_API_URL = "/api/click-per-mins";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ClickPerMinRepository clickPerMinRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restClickPerMinMockMvc;

    private ClickPerMin clickPerMin;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClickPerMin createEntity(EntityManager em) {
        ClickPerMin clickPerMin = new ClickPerMin().creationTime(DEFAULT_CREATION_TIME).clickCount(DEFAULT_CLICK_COUNT);
        return clickPerMin;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClickPerMin createUpdatedEntity(EntityManager em) {
        ClickPerMin clickPerMin = new ClickPerMin().creationTime(UPDATED_CREATION_TIME).clickCount(UPDATED_CLICK_COUNT);
        return clickPerMin;
    }

    @BeforeEach
    public void initTest() {
        clickPerMin = createEntity(em);
    }

    @Test
    @Transactional
    void createClickPerMin() throws Exception {
        int databaseSizeBeforeCreate = clickPerMinRepository.findAll().size();
        // Create the ClickPerMin
        restClickPerMinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clickPerMin)))
            .andExpect(status().isCreated());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeCreate + 1);
        ClickPerMin testClickPerMin = clickPerMinList.get(clickPerMinList.size() - 1);
        assertThat(testClickPerMin.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testClickPerMin.getClickCount()).isEqualTo(DEFAULT_CLICK_COUNT);
    }

    @Test
    @Transactional
    void createClickPerMinWithExistingId() throws Exception {
        // Create the ClickPerMin with an existing ID
        clickPerMin.setId(1L);

        int databaseSizeBeforeCreate = clickPerMinRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restClickPerMinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clickPerMin)))
            .andExpect(status().isBadRequest());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCreationTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = clickPerMinRepository.findAll().size();
        // set the field null
        clickPerMin.setCreationTime(null);

        // Create the ClickPerMin, which fails.

        restClickPerMinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clickPerMin)))
            .andExpect(status().isBadRequest());

        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkClickCountIsRequired() throws Exception {
        int databaseSizeBeforeTest = clickPerMinRepository.findAll().size();
        // set the field null
        clickPerMin.setClickCount(null);

        // Create the ClickPerMin, which fails.

        restClickPerMinMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clickPerMin)))
            .andExpect(status().isBadRequest());

        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllClickPerMins() throws Exception {
        // Initialize the database
        clickPerMinRepository.saveAndFlush(clickPerMin);

        // Get all the clickPerMinList
        restClickPerMinMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clickPerMin.getId().intValue())))
            .andExpect(jsonPath("$.[*].creationTime").value(hasItem(DEFAULT_CREATION_TIME.toString())))
            .andExpect(jsonPath("$.[*].clickCount").value(hasItem(DEFAULT_CLICK_COUNT.intValue())));
    }

    @Test
    @Transactional
    void getClickPerMin() throws Exception {
        // Initialize the database
        clickPerMinRepository.saveAndFlush(clickPerMin);

        // Get the clickPerMin
        restClickPerMinMockMvc
            .perform(get(ENTITY_API_URL_ID, clickPerMin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(clickPerMin.getId().intValue()))
            .andExpect(jsonPath("$.creationTime").value(DEFAULT_CREATION_TIME.toString()))
            .andExpect(jsonPath("$.clickCount").value(DEFAULT_CLICK_COUNT.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingClickPerMin() throws Exception {
        // Get the clickPerMin
        restClickPerMinMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewClickPerMin() throws Exception {
        // Initialize the database
        clickPerMinRepository.saveAndFlush(clickPerMin);

        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();

        // Update the clickPerMin
        ClickPerMin updatedClickPerMin = clickPerMinRepository.findById(clickPerMin.getId()).get();
        // Disconnect from session so that the updates on updatedClickPerMin are not directly saved in db
        em.detach(updatedClickPerMin);
        updatedClickPerMin.creationTime(UPDATED_CREATION_TIME).clickCount(UPDATED_CLICK_COUNT);

        restClickPerMinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedClickPerMin.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedClickPerMin))
            )
            .andExpect(status().isOk());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
        ClickPerMin testClickPerMin = clickPerMinList.get(clickPerMinList.size() - 1);
        assertThat(testClickPerMin.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testClickPerMin.getClickCount()).isEqualTo(UPDATED_CLICK_COUNT);
    }

    @Test
    @Transactional
    void putNonExistingClickPerMin() throws Exception {
        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();
        clickPerMin.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClickPerMinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, clickPerMin.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clickPerMin))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchClickPerMin() throws Exception {
        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();
        clickPerMin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClickPerMinMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(clickPerMin))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamClickPerMin() throws Exception {
        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();
        clickPerMin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClickPerMinMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(clickPerMin)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateClickPerMinWithPatch() throws Exception {
        // Initialize the database
        clickPerMinRepository.saveAndFlush(clickPerMin);

        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();

        // Update the clickPerMin using partial update
        ClickPerMin partialUpdatedClickPerMin = new ClickPerMin();
        partialUpdatedClickPerMin.setId(clickPerMin.getId());

        partialUpdatedClickPerMin.clickCount(UPDATED_CLICK_COUNT);

        restClickPerMinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClickPerMin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClickPerMin))
            )
            .andExpect(status().isOk());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
        ClickPerMin testClickPerMin = clickPerMinList.get(clickPerMinList.size() - 1);
        assertThat(testClickPerMin.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testClickPerMin.getClickCount()).isEqualTo(UPDATED_CLICK_COUNT);
    }

    @Test
    @Transactional
    void fullUpdateClickPerMinWithPatch() throws Exception {
        // Initialize the database
        clickPerMinRepository.saveAndFlush(clickPerMin);

        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();

        // Update the clickPerMin using partial update
        ClickPerMin partialUpdatedClickPerMin = new ClickPerMin();
        partialUpdatedClickPerMin.setId(clickPerMin.getId());

        partialUpdatedClickPerMin.creationTime(UPDATED_CREATION_TIME).clickCount(UPDATED_CLICK_COUNT);

        restClickPerMinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedClickPerMin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedClickPerMin))
            )
            .andExpect(status().isOk());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
        ClickPerMin testClickPerMin = clickPerMinList.get(clickPerMinList.size() - 1);
        assertThat(testClickPerMin.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testClickPerMin.getClickCount()).isEqualTo(UPDATED_CLICK_COUNT);
    }

    @Test
    @Transactional
    void patchNonExistingClickPerMin() throws Exception {
        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();
        clickPerMin.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClickPerMinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, clickPerMin.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clickPerMin))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchClickPerMin() throws Exception {
        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();
        clickPerMin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClickPerMinMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(clickPerMin))
            )
            .andExpect(status().isBadRequest());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamClickPerMin() throws Exception {
        int databaseSizeBeforeUpdate = clickPerMinRepository.findAll().size();
        clickPerMin.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restClickPerMinMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(clickPerMin))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ClickPerMin in the database
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteClickPerMin() throws Exception {
        // Initialize the database
        clickPerMinRepository.saveAndFlush(clickPerMin);

        int databaseSizeBeforeDelete = clickPerMinRepository.findAll().size();

        // Delete the clickPerMin
        restClickPerMinMockMvc
                .perform(delete(ENTITY_API_URL_ID, clickPerMin.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ClickPerMin> clickPerMinList = clickPerMinRepository.findAll();
        assertThat(clickPerMinList).hasSize(databaseSizeBeforeDelete - 1);
    }
    
    @Test
    @Transactional
    void getClicksSum() throws Exception {
        // Initialize the database
        Instant today = Instant.now();
        String todayString = today.toString().substring(0, 10);
        Instant yesterday = Instant.now().minus(1, ChronoUnit.DAYS);
        String yesterdayString = yesterday.toString().substring(0, 10);
        ClickPerMin clickPerMin = new ClickPerMin().creationTime(yesterday).clickCount(10L);
        clickPerMinRepository.saveAndFlush(clickPerMin);
        clickPerMin = new ClickPerMin().creationTime(today).clickCount(12L);
        clickPerMinRepository.saveAndFlush(clickPerMin);

        // Get the purchasesMtxPoints two date versions
        restClickPerMinMockMvc.perform(get(ENTITY_API_URL + "/clicksSum/" + yesterdayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.clicks").value(22));

        restClickPerMinMockMvc.perform(get(ENTITY_API_URL + "/clicksSum/" + todayString + "/" + todayString))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.clicks").value(12));
    }
}
