package bt.browsergame.statistic.web.rest;

import bt.browsergame.statistic.domain.ClickPerMin;
import bt.browsergame.statistic.repository.ClickPerMinRepository;
import bt.browsergame.statistic.service.ClickPerMinService;
import bt.browsergame.statistic.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import bt.browsergame.statistic.domain.views.ClickPerMinView;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Calendar;

/**
 * REST controller for managing
 * {@link bt.browsergame.statistic.domain.ClickPerMin}.
 */
@RestController
@RequestMapping("/api")
public class ClickPerMinResource {

    private final Logger log = LoggerFactory.getLogger(ClickPerMinResource.class);

    private static final String ENTITY_NAME = "statisticServiceClickPerMin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClickPerMinService clickPerMinService;

    private final ClickPerMinRepository clickPerMinRepository;

    public ClickPerMinResource(ClickPerMinService clickPerMinService, ClickPerMinRepository clickPerMinRepository) {
        this.clickPerMinService = clickPerMinService;
        this.clickPerMinRepository = clickPerMinRepository;
    }

    /**
     * {@code POST  /click-per-mins} : Create a new clickPerMin.
     *
     * @param clickPerMin the clickPerMin to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new clickPerMin, or with status {@code 400 (Bad Request)} if
     *         the clickPerMin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/click-per-mins")
    public ResponseEntity<ClickPerMin> createClickPerMin(@Valid @RequestBody ClickPerMin clickPerMin)
            throws URISyntaxException {
        log.debug("REST request to save ClickPerMin : {}", clickPerMin);
        if (clickPerMin.getId() != null) {
            throw new BadRequestAlertException("A new clickPerMin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClickPerMin result = clickPerMinService.save(clickPerMin);
        return ResponseEntity
                .created(new URI("/api/click-per-mins/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /click-per-mins/:id} : Updates an existing clickPerMin.
     *
     * @param id          the id of the clickPerMin to save.
     * @param clickPerMin the clickPerMin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated clickPerMin, or with status {@code 400 (Bad Request)} if
     *         the clickPerMin is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the clickPerMin couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/click-per-mins/{id}")
    public ResponseEntity<ClickPerMin> updateClickPerMin(@PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody ClickPerMin clickPerMin) throws URISyntaxException {
        log.debug("REST request to update ClickPerMin : {}, {}", id, clickPerMin);
        if (clickPerMin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, clickPerMin.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!clickPerMinRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ClickPerMin result = clickPerMinService.save(clickPerMin);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clickPerMin.getId().toString()))
                .body(result);
    }

    /**
     * {@code PATCH  /click-per-mins/:id} : Partial updates given fields of an
     * existing clickPerMin, field will ignore if it is null
     *
     * @param id          the id of the clickPerMin to save.
     * @param clickPerMin the clickPerMin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated clickPerMin, or with status {@code 400 (Bad Request)} if
     *         the clickPerMin is not valid, or with status {@code 404 (Not Found)}
     *         if the clickPerMin is not found, or with status
     *         {@code 500 (Internal Server Error)} if the clickPerMin couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/click-per-mins/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ClickPerMin> partialUpdateClickPerMin(
            @PathVariable(value = "id", required = false) final Long id, @NotNull @RequestBody ClickPerMin clickPerMin)
            throws URISyntaxException {
        log.debug("REST request to partial update ClickPerMin partially : {}, {}", id, clickPerMin);
        if (clickPerMin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, clickPerMin.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!clickPerMinRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ClickPerMin> result = clickPerMinService.partialUpdate(clickPerMin);

        return ResponseUtil.wrapOrNotFound(result,
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clickPerMin.getId().toString()));
    }

    /**
     * {@code GET  /click-per-mins} : get all the clickPerMins.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of clickPerMins in body.
     */
    @GetMapping("/click-per-mins")
    public List<ClickPerMin> getAllClickPerMins() {
        log.debug("REST request to get all ClickPerMins");
        return clickPerMinService.findAll();
    }

    /**
     * {@code GET  /click-per-mins/:id} : get the "id" clickPerMin.
     *
     * @param id the id of the clickPerMin to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the clickPerMin, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/click-per-mins/{id}")
    public ResponseEntity<ClickPerMin> getClickPerMin(@PathVariable Long id) {
        log.debug("REST request to get ClickPerMin : {}", id);
        Optional<ClickPerMin> clickPerMin = clickPerMinService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clickPerMin);
    }

    /**
     * {@code DELETE  /click-per-mins/:id} : delete the "id" clickPerMin.
     *
     * @param id the id of the clickPerMin to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/click-per-mins/{id}")
    public ResponseEntity<Void> deleteClickPerMin(@PathVariable Long id) {
        log.debug("REST request to delete ClickPerMin : {}", id);
        clickPerMinService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code GET  /click-per-mins/clickSum/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get the sum of clicks
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     */
    @GetMapping("/click-per-mins/clicksSum/{startDate}/{endDate}")
    public ResponseEntity<ClickPerMinView> getClicksSum(
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate) {
        log.debug("REST request to get getClicksSum : {}");
        return ResponseEntity.ok(clickPerMinService.getClicksSum(startDate, endDate));
    }
}
