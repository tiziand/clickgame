package bt.browsergame.statistic.kafka;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.DoubleDeserializer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import bt.browsergame.statistic.domain.ClickPerMin;
import bt.browsergame.statistic.domain.PurchasesButtonColorMtx;
import bt.browsergame.statistic.domain.PurchasesMtxPoints;
import bt.browsergame.statistic.service.ClickPerMinService;
import bt.browsergame.statistic.service.PurchasesButtonColorMtxService;
import bt.browsergame.statistic.service.PurchasesMtxPointsService;

@Configuration
@ConfigurationProperties(prefix = "kafka")
public class KafkaConsumerConfig {

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private String bootStrapServers = "localhost:9092";

    public String getBootStrapServers() {
        return bootStrapServers;
    }

    public void setBootStrapServers(String bootStrapServers) {
        this.bootStrapServers = bootStrapServers;
    }

    @Autowired
    ClickPerMinService clickPerMinService;

    @Autowired
    PurchasesMtxPointsService purchasesMtxPointsService;

    @Autowired
    PurchasesButtonColorMtxService purchasesButtonColorMtxService;

    @Bean(name = "statsConsumer")
    public void consumer() {
        executorService.execute(() -> {
            Properties config = new Properties();
            config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
            config.put(ConsumerConfig.GROUP_ID_CONFIG, "statisticService");
            config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
            config.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
            config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

            config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class);

            KafkaConsumer<String, Long> kafkaConsumer = new KafkaConsumer<>(config);

            kafkaConsumer.subscribe(List.of("points-farmed-all"));

            try {
                while (true) {
                    ConsumerRecords<String, Long> records = kafkaConsumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, Long> record : records) {
                        ClickPerMin clickPerMin = new ClickPerMin();
                        clickPerMin.setCreationTime(Instant.now());
                        clickPerMin.setClickCount(record.value());
                        clickPerMinService.save(clickPerMin);
                    }
                }
            } finally {
                kafkaConsumer.close();
            }
        });

        executorService.execute(() -> {
            Properties config = new Properties();
            config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
            config.put(ConsumerConfig.GROUP_ID_CONFIG, "statisticService");
            config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
            config.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
            config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, DoubleDeserializer.class);

            KafkaConsumer<String, Double> kafkaConsumer = new KafkaConsumer<>(config);

            kafkaConsumer.subscribe(List.of("purchases"));

            try {
                while (true) {
                    ConsumerRecords<String, Double> records = kafkaConsumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, Double> record : records) {
                        PurchasesMtxPoints purchasesMtxPoints = new PurchasesMtxPoints();
                        purchasesMtxPoints.creationTime(Instant.now());
                        purchasesMtxPoints.setMoneySpent(record.value());
                        purchasesMtxPointsService.save(purchasesMtxPoints);
                    }
                }
            } finally {
                kafkaConsumer.close();
            }
        });

        executorService.execute(() -> {
            Properties config = new Properties();
            config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
            config.put(ConsumerConfig.GROUP_ID_CONFIG, "statisticService");
            config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
            config.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
            config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);

            config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class);

            KafkaConsumer<String, Long> kafkaConsumer = new KafkaConsumer<>(config);

            kafkaConsumer.subscribe(List.of("owned-button-color-mtx"));
            try {
                while (true) {
                    ConsumerRecords<String, Long> records = kafkaConsumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, Long> record : records) {
                        PurchasesButtonColorMtx purchasesButtonColorMtx = new PurchasesButtonColorMtx();
                        purchasesButtonColorMtx.creationTime(Instant.now());
                        purchasesButtonColorMtx.setAmount(Long.valueOf(1));
                        purchasesButtonColorMtx.setMtxId(record.value());
                        purchasesButtonColorMtxService.save(purchasesButtonColorMtx);
                    }
                }
            } finally {
                kafkaConsumer.close();
            }
        });
    }
}
