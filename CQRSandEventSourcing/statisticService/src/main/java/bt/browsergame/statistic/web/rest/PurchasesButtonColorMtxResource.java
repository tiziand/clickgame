package bt.browsergame.statistic.web.rest;

import bt.browsergame.statistic.domain.PurchasesButtonColorMtx;
import bt.browsergame.statistic.repository.PurchasesButtonColorMtxRepository;
import bt.browsergame.statistic.service.PurchasesButtonColorMtxService;
import bt.browsergame.statistic.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxAllView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxListView;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Calendar;

/**
 * REST controller for managing
 * {@link bt.browsergame.statistic.domain.PurchasesButtonColorMtx}.
 */
@RestController
@RequestMapping("/api")
public class PurchasesButtonColorMtxResource {

    private final Logger log = LoggerFactory.getLogger(PurchasesButtonColorMtxResource.class);

    private static final String ENTITY_NAME = "statisticServicePurchasesButtonColorMtx";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PurchasesButtonColorMtxService purchasesButtonColorMtxService;

    private final PurchasesButtonColorMtxRepository purchasesButtonColorMtxRepository;

    public PurchasesButtonColorMtxResource(PurchasesButtonColorMtxService purchasesButtonColorMtxService,
            PurchasesButtonColorMtxRepository purchasesButtonColorMtxRepository) {
        this.purchasesButtonColorMtxService = purchasesButtonColorMtxService;
        this.purchasesButtonColorMtxRepository = purchasesButtonColorMtxRepository;
    }

    /**
     * {@code POST  /purchases-button-color-mtxes} : Create a new
     * purchasesButtonColorMtx.
     *
     * @param purchasesButtonColorMtx the purchasesButtonColorMtx to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new purchasesButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the purchasesButtonColorMtx has already
     *         an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/purchases-button-color-mtxes")
    public ResponseEntity<PurchasesButtonColorMtx> createPurchasesButtonColorMtx(
            @Valid @RequestBody PurchasesButtonColorMtx purchasesButtonColorMtx) throws URISyntaxException {
        log.debug("REST request to save PurchasesButtonColorMtx : {}", purchasesButtonColorMtx);
        if (purchasesButtonColorMtx.getId() != null) {
            throw new BadRequestAlertException("A new purchasesButtonColorMtx cannot already have an ID", ENTITY_NAME,
                    "idexists");
        }
        PurchasesButtonColorMtx result = purchasesButtonColorMtxService.save(purchasesButtonColorMtx);
        return ResponseEntity
                .created(new URI("/api/purchases-button-color-mtxes/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /purchases-button-color-mtxes/:id} : Updates an existing
     * purchasesButtonColorMtx.
     *
     * @param id                      the id of the purchasesButtonColorMtx to save.
     * @param purchasesButtonColorMtx the purchasesButtonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated purchasesButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the purchasesButtonColorMtx is not
     *         valid, or with status {@code 500 (Internal Server Error)} if the
     *         purchasesButtonColorMtx couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/purchases-button-color-mtxes/{id}")
    public ResponseEntity<PurchasesButtonColorMtx> updatePurchasesButtonColorMtx(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody PurchasesButtonColorMtx purchasesButtonColorMtx) throws URISyntaxException {
        log.debug("REST request to update PurchasesButtonColorMtx : {}, {}", id, purchasesButtonColorMtx);
        if (purchasesButtonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purchasesButtonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purchasesButtonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PurchasesButtonColorMtx result = purchasesButtonColorMtxService.save(purchasesButtonColorMtx);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME,
                purchasesButtonColorMtx.getId().toString())).body(result);
    }

    /**
     * {@code PATCH  /purchases-button-color-mtxes/:id} : Partial updates given
     * fields of an existing purchasesButtonColorMtx, field will ignore if it is
     * null
     *
     * @param id                      the id of the purchasesButtonColorMtx to save.
     * @param purchasesButtonColorMtx the purchasesButtonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated purchasesButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the purchasesButtonColorMtx is not
     *         valid, or with status {@code 404 (Not Found)} if the
     *         purchasesButtonColorMtx is not found, or with status
     *         {@code 500 (Internal Server Error)} if the purchasesButtonColorMtx
     *         couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/purchases-button-color-mtxes/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PurchasesButtonColorMtx> partialUpdatePurchasesButtonColorMtx(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody PurchasesButtonColorMtx purchasesButtonColorMtx) throws URISyntaxException {
        log.debug("REST request to partial update PurchasesButtonColorMtx partially : {}, {}", id,
                purchasesButtonColorMtx);
        if (purchasesButtonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purchasesButtonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purchasesButtonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PurchasesButtonColorMtx> result = purchasesButtonColorMtxService
                .partialUpdate(purchasesButtonColorMtx);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ENTITY_NAME, purchasesButtonColorMtx.getId().toString()));
    }

    /**
     * {@code GET  /purchases-button-color-mtxes} : get all the
     * purchasesButtonColorMtxes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of purchasesButtonColorMtxes in body.
     */
    @GetMapping("/purchases-button-color-mtxes")
    public List<PurchasesButtonColorMtx> getAllPurchasesButtonColorMtxes() {
        log.debug("REST request to get all PurchasesButtonColorMtxes");
        return purchasesButtonColorMtxService.findAll();
    }

    /**
     * {@code GET  /purchases-button-color-mtxes/:id} : get the "id"
     * purchasesButtonColorMtx.
     *
     * @param id the id of the purchasesButtonColorMtx to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the purchasesButtonColorMtx, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/purchases-button-color-mtxes/{id}")
    public ResponseEntity<PurchasesButtonColorMtx> getPurchasesButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to get PurchasesButtonColorMtx : {}", id);
        Optional<PurchasesButtonColorMtx> purchasesButtonColorMtx = purchasesButtonColorMtxService.findOne(id);
        return ResponseUtil.wrapOrNotFound(purchasesButtonColorMtx);
    }

    /**
     * {@code DELETE  /purchases-button-color-mtxes/:id} : delete the "id"
     * purchasesButtonColorMtx.
     *
     * @param id the id of the purchasesButtonColorMtx to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/purchases-button-color-mtxes/{id}")
    public ResponseEntity<Void> deletePurchasesButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to delete PurchasesButtonColorMtx : {}", id);
        purchasesButtonColorMtxService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code GET  /purchases-button-color-mtxes/purchasesSum/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get the sum of button color mtx purchases
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     */
    @GetMapping("/purchases-button-color-mtxes/purchasesSum/{startDate}/{endDate}")
    public ResponseEntity<PurchasesButtonColorMtxAllView> getPurchasesSum(
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate) {
        log.debug("REST request to get PurchasesButtonColorMtx : {}");
        return ResponseEntity.ok(purchasesButtonColorMtxService.getPurchasesSum(startDate, endDate));
    }

    /**
     * {@code GET  /purchases-button-color-mtxes/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get all the purchasesButtonColorMtxes summed up.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of purchasesButtonColorMtxes in body.
     */
    @GetMapping("/purchases-button-color-mtxes/pruchasesSumList/{startDate}/{endDate}")
    public List<PurchasesButtonColorMtxListView> getAllPurchasesSumList(
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate) {
        log.debug("REST request to get all PurchasesButtonColorMtxes");
        return purchasesButtonColorMtxService.getPurchsesSumList(startDate, endDate);
    }
}
