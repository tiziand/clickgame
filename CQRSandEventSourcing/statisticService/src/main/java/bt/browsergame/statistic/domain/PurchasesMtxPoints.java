package bt.browsergame.statistic.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PurchasesMtxPoints.
 */
@Entity
@Table(name = "purchases_mtx_points")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PurchasesMtxPoints implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "creation_time", nullable = false)
    private Instant creationTime;

    @NotNull
    @Column(name = "money_spent", nullable = false)
    private Double moneySpent;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PurchasesMtxPoints id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getCreationTime() {
        return this.creationTime;
    }

    public PurchasesMtxPoints creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Double getMoneySpent() {
        return this.moneySpent;
    }

    public PurchasesMtxPoints moneySpent(Double moneySpent) {
        this.moneySpent = moneySpent;
        return this;
    }

    public void setMoneySpent(Double moneySpent) {
        this.moneySpent = moneySpent;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchasesMtxPoints)) {
            return false;
        }
        return id != null && id.equals(((PurchasesMtxPoints) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PurchasesMtxPoints{" +
            "id=" + getId() +
            ", creationTime='" + getCreationTime() + "'" +
            ", moneySpent=" + getMoneySpent() +
            "}";
    }
}
