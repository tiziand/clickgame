package bt.browsergame.statistic.domain.views;

public interface PurchasesButtonColorMtxAllView {
    Long getSumPurchases();
}
