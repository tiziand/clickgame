package bt.browsergame.statistic.repository;

import bt.browsergame.statistic.domain.PurchasesMtxPoints;
import bt.browsergame.statistic.domain.views.PurchasesMtxPointsView;

import java.time.Instant;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the PurchasesMtxPoints entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PurchasesMtxPointsRepository extends JpaRepository<PurchasesMtxPoints, Long> {
    @Query("SELECT SUM(pmp.moneySpent) as sumMoneySpent FROM PurchasesMtxPoints pmp WHERE pmp.creationTime >= :startdate AND pmp.creationTime <= :enddate")
    PurchasesMtxPointsView getSumMoneySpentSum(@Param("startdate") Instant startDate,
            @Param("enddate") Instant endDate);
}
