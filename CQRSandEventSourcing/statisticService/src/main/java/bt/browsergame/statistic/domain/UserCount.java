package bt.browsergame.statistic.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A UserCount.
 */
@Entity
@Table(name = "user_count")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserCount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "creation_time", nullable = false)
    private Instant creationTime;

    @NotNull
    @Column(name = "user_count", nullable = false)
    private Long userCount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserCount id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getCreationTime() {
        return this.creationTime;
    }

    public UserCount creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Long getUserCount() {
        return this.userCount;
    }

    public UserCount userCount(Long userCount) {
        this.userCount = userCount;
        return this;
    }

    public void setUserCount(Long userCount) {
        this.userCount = userCount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserCount)) {
            return false;
        }
        return id != null && id.equals(((UserCount) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserCount{" +
            "id=" + getId() +
            ", creationTime='" + getCreationTime() + "'" +
            ", userCount=" + getUserCount() +
            "}";
    }
}
