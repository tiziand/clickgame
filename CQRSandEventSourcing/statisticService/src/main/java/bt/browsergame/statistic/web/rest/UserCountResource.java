package bt.browsergame.statistic.web.rest;

import bt.browsergame.statistic.domain.UserCount;
import bt.browsergame.statistic.repository.UserCountRepository;
import bt.browsergame.statistic.service.UserCountService;
import bt.browsergame.statistic.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bt.browsergame.statistic.domain.UserCount}.
 */
@RestController
@RequestMapping("/api")
public class UserCountResource {

    private final Logger log = LoggerFactory.getLogger(UserCountResource.class);

    private static final String ENTITY_NAME = "statisticServiceUserCount";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserCountService userCountService;

    private final UserCountRepository userCountRepository;

    public UserCountResource(UserCountService userCountService, UserCountRepository userCountRepository) {
        this.userCountService = userCountService;
        this.userCountRepository = userCountRepository;
    }

    /**
     * {@code POST  /user-counts} : Create a new userCount.
     *
     * @param userCount the userCount to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userCount, or with status {@code 400 (Bad Request)} if the userCount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-counts")
    public ResponseEntity<UserCount> createUserCount(@Valid @RequestBody UserCount userCount) throws URISyntaxException {
        log.debug("REST request to save UserCount : {}", userCount);
        if (userCount.getId() != null) {
            throw new BadRequestAlertException("A new userCount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserCount result = userCountService.save(userCount);
        return ResponseEntity
            .created(new URI("/api/user-counts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-counts/:id} : Updates an existing userCount.
     *
     * @param id the id of the userCount to save.
     * @param userCount the userCount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userCount,
     * or with status {@code 400 (Bad Request)} if the userCount is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userCount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-counts/{id}")
    public ResponseEntity<UserCount> updateUserCount(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody UserCount userCount
    ) throws URISyntaxException {
        log.debug("REST request to update UserCount : {}, {}", id, userCount);
        if (userCount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userCount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userCountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserCount result = userCountService.save(userCount);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userCount.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-counts/:id} : Partial updates given fields of an existing userCount, field will ignore if it is null
     *
     * @param id the id of the userCount to save.
     * @param userCount the userCount to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userCount,
     * or with status {@code 400 (Bad Request)} if the userCount is not valid,
     * or with status {@code 404 (Not Found)} if the userCount is not found,
     * or with status {@code 500 (Internal Server Error)} if the userCount couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-counts/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<UserCount> partialUpdateUserCount(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody UserCount userCount
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserCount partially : {}, {}", id, userCount);
        if (userCount.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userCount.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userCountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserCount> result = userCountService.partialUpdate(userCount);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userCount.getId().toString())
        );
    }

    /**
     * {@code GET  /user-counts} : get all the userCounts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userCounts in body.
     */
    @GetMapping("/user-counts")
    public List<UserCount> getAllUserCounts() {
        log.debug("REST request to get all UserCounts");
        return userCountService.findAll();
    }

    /**
     * {@code GET  /user-counts/:id} : get the "id" userCount.
     *
     * @param id the id of the userCount to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userCount, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-counts/{id}")
    public ResponseEntity<UserCount> getUserCount(@PathVariable Long id) {
        log.debug("REST request to get UserCount : {}", id);
        Optional<UserCount> userCount = userCountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userCount);
    }

    /**
     * {@code DELETE  /user-counts/:id} : delete the "id" userCount.
     *
     * @param id the id of the userCount to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-counts/{id}")
    public ResponseEntity<Void> deleteUserCount(@PathVariable Long id) {
        log.debug("REST request to delete UserCount : {}", id);
        userCountService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
