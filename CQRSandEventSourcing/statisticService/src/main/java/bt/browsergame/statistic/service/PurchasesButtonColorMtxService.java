package bt.browsergame.statistic.service;

import bt.browsergame.statistic.domain.PurchasesButtonColorMtx;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxAllView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxListView;
import bt.browsergame.statistic.repository.PurchasesButtonColorMtxRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PurchasesButtonColorMtx}.
 */
@Service
@Transactional
public class PurchasesButtonColorMtxService {

    private final Logger log = LoggerFactory.getLogger(PurchasesButtonColorMtxService.class);

    private final PurchasesButtonColorMtxRepository purchasesButtonColorMtxRepository;

    public PurchasesButtonColorMtxService(PurchasesButtonColorMtxRepository purchasesButtonColorMtxRepository) {
        this.purchasesButtonColorMtxRepository = purchasesButtonColorMtxRepository;
    }

    /**
     * Save a purchasesButtonColorMtx.
     *
     * @param purchasesButtonColorMtx the entity to save.
     * @return the persisted entity.
     */
    public PurchasesButtonColorMtx save(PurchasesButtonColorMtx purchasesButtonColorMtx) {
        log.debug("Request to save PurchasesButtonColorMtx : {}", purchasesButtonColorMtx);
        return purchasesButtonColorMtxRepository.save(purchasesButtonColorMtx);
    }

    /**
     * Partially update a purchasesButtonColorMtx.
     *
     * @param purchasesButtonColorMtx the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PurchasesButtonColorMtx> partialUpdate(PurchasesButtonColorMtx purchasesButtonColorMtx) {
        log.debug("Request to partially update PurchasesButtonColorMtx : {}", purchasesButtonColorMtx);

        return purchasesButtonColorMtxRepository.findById(purchasesButtonColorMtx.getId())
                .map(existingPurchasesButtonColorMtx -> {
                    if (purchasesButtonColorMtx.getMtxId() != null) {
                        existingPurchasesButtonColorMtx.setMtxId(purchasesButtonColorMtx.getMtxId());
                    }
                    if (purchasesButtonColorMtx.getCreationTime() != null) {
                        existingPurchasesButtonColorMtx.setCreationTime(purchasesButtonColorMtx.getCreationTime());
                    }
                    if (purchasesButtonColorMtx.getAmount() != null) {
                        existingPurchasesButtonColorMtx.setAmount(purchasesButtonColorMtx.getAmount());
                    }

                    return existingPurchasesButtonColorMtx;
                }).map(purchasesButtonColorMtxRepository::save);
    }

    /**
     * Get all the purchasesButtonColorMtxes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PurchasesButtonColorMtx> findAll() {
        log.debug("Request to get all PurchasesButtonColorMtxes");
        return purchasesButtonColorMtxRepository.findAll();
    }

    /**
     * Get one purchasesButtonColorMtx by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PurchasesButtonColorMtx> findOne(Long id) {
        log.debug("Request to get PurchasesButtonColorMtx : {}", id);
        return purchasesButtonColorMtxRepository.findById(id);
    }

    /**
     * Delete the purchasesButtonColorMtx by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PurchasesButtonColorMtx : {}", id);
        purchasesButtonColorMtxRepository.deleteById(id);
    }

    /**
     * Get sum of button color mtx purchases.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public PurchasesButtonColorMtxAllView getPurchasesSum(Calendar startDate, Calendar endDate) {
        log.debug("Request to get PurchasesButtonColorMtx : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return purchasesButtonColorMtxRepository.getSumPurchases(startInstant, endInstant);
    }

    /**
     * Get sum of button color mtx purchases.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public List<PurchasesButtonColorMtxListView> getPurchsesSumList(Calendar startDate, Calendar endDate) {
        log.debug("Request to get PurchasesButtonColorMtx : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return purchasesButtonColorMtxRepository.getSumPurchasesList(startInstant, endInstant);
    }
}
