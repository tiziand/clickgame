package bt.browsergame.statistic.service;

import bt.browsergame.statistic.domain.PurchasesMtxPoints;
import bt.browsergame.statistic.domain.views.PurchasesMtxPointsView;
import bt.browsergame.statistic.repository.PurchasesMtxPointsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PurchasesMtxPoints}.
 */
@Service
@Transactional
public class PurchasesMtxPointsService {

    private final Logger log = LoggerFactory.getLogger(PurchasesMtxPointsService.class);

    private final PurchasesMtxPointsRepository purchasesMtxPointsRepository;

    public PurchasesMtxPointsService(PurchasesMtxPointsRepository purchasesMtxPointsRepository) {
        this.purchasesMtxPointsRepository = purchasesMtxPointsRepository;
    }

    /**
     * Save a purchasesMtxPoints.
     *
     * @param purchasesMtxPoints the entity to save.
     * @return the persisted entity.
     */
    public PurchasesMtxPoints save(PurchasesMtxPoints purchasesMtxPoints) {
        log.debug("Request to save PurchasesMtxPoints : {}", purchasesMtxPoints);
        return purchasesMtxPointsRepository.save(purchasesMtxPoints);
    }

    /**
     * Partially update a purchasesMtxPoints.
     *
     * @param purchasesMtxPoints the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PurchasesMtxPoints> partialUpdate(PurchasesMtxPoints purchasesMtxPoints) {
        log.debug("Request to partially update PurchasesMtxPoints : {}", purchasesMtxPoints);

        return purchasesMtxPointsRepository.findById(purchasesMtxPoints.getId()).map(existingPurchasesMtxPoints -> {
            if (purchasesMtxPoints.getCreationTime() != null) {
                existingPurchasesMtxPoints.setCreationTime(purchasesMtxPoints.getCreationTime());
            }
            if (purchasesMtxPoints.getMoneySpent() != null) {
                existingPurchasesMtxPoints.setMoneySpent(purchasesMtxPoints.getMoneySpent());
            }

            return existingPurchasesMtxPoints;
        }).map(purchasesMtxPointsRepository::save);
    }

    /**
     * Get all the purchasesMtxPoints.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PurchasesMtxPoints> findAll() {
        log.debug("Request to get all PurchasesMtxPoints");
        return purchasesMtxPointsRepository.findAll();
    }

    /**
     * Get one purchasesMtxPoints by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PurchasesMtxPoints> findOne(Long id) {
        log.debug("Request to get PurchasesMtxPoints : {}", id);
        return purchasesMtxPointsRepository.findById(id);
    }

    /**
     * Delete the purchasesMtxPoints by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PurchasesMtxPoints : {}", id);
        purchasesMtxPointsRepository.deleteById(id);
    }

    /**
     * Get sum of money Spent.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public PurchasesMtxPointsView getMoneySpentSum(Calendar startDate, Calendar endDate) {
        log.debug("Request to get PurchasesMtxPoints : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return purchasesMtxPointsRepository.getSumMoneySpentSum(startInstant, endInstant);
    }
}
