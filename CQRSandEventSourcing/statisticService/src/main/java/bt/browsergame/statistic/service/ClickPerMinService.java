package bt.browsergame.statistic.service;

import bt.browsergame.statistic.domain.ClickPerMin;
import bt.browsergame.statistic.domain.views.ClickPerMinView;
import bt.browsergame.statistic.repository.ClickPerMinRepository;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ClickPerMin}.
 */
@Service
@Transactional
public class ClickPerMinService {

    private final Logger log = LoggerFactory.getLogger(ClickPerMinService.class);

    private final ClickPerMinRepository clickPerMinRepository;

    public ClickPerMinService(ClickPerMinRepository clickPerMinRepository) {
        this.clickPerMinRepository = clickPerMinRepository;
    }

    /**
     * Save a clickPerMin.
     *
     * @param clickPerMin the entity to save.
     * @return the persisted entity.
     */
    public ClickPerMin save(ClickPerMin clickPerMin) {
        log.debug("Request to save ClickPerMin : {}", clickPerMin);
        return clickPerMinRepository.save(clickPerMin);
    }

    /**
     * Partially update a clickPerMin.
     *
     * @param clickPerMin the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ClickPerMin> partialUpdate(ClickPerMin clickPerMin) {
        log.debug("Request to partially update ClickPerMin : {}", clickPerMin);

        return clickPerMinRepository.findById(clickPerMin.getId()).map(existingClickPerMin -> {
            if (clickPerMin.getCreationTime() != null) {
                existingClickPerMin.setCreationTime(clickPerMin.getCreationTime());
            }
            if (clickPerMin.getClickCount() != null) {
                existingClickPerMin.setClickCount(clickPerMin.getClickCount());
            }

            return existingClickPerMin;
        }).map(clickPerMinRepository::save);
    }

    /**
     * Get all the clickPerMins.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ClickPerMin> findAll() {
        log.debug("Request to get all ClickPerMins");
        return clickPerMinRepository.findAll();
    }

    /**
     * Get one clickPerMin by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ClickPerMin> findOne(Long id) {
        log.debug("Request to get ClickPerMin : {}", id);
        return clickPerMinRepository.findById(id);
    }

    /**
     * Delete the clickPerMin by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ClickPerMin : {}", id);
        clickPerMinRepository.deleteById(id);
    }

    /**
     * Get sum of clicks.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public ClickPerMinView getClicksSum(Calendar startDate, Calendar endDate) {
        log.debug("Request to get ClickPerMin : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return clickPerMinRepository.getClicksSum(startInstant, endInstant);
    }
}
