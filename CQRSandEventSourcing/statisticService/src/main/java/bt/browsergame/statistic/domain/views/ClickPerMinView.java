package bt.browsergame.statistic.domain.views;

public interface ClickPerMinView {
    Long getClicks();
}
