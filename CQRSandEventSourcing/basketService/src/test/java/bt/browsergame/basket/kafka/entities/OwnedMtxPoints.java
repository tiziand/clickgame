package bt.browsergame.basket.kafka.entities;

import java.io.Serializable;

/**
 * A OwnedMtxPoints.
 */
public class OwnedMtxPoints implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String userName;

    private Integer mtxPoints;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OwnedMtxPoints id(Long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public OwnedMtxPoints userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getMtxPoints() {
        return this.mtxPoints;
    }

    public OwnedMtxPoints mtxPoints(Integer mtxPoints) {
        this.mtxPoints = mtxPoints;
        return this;
    }

    public void setMtxPoints(Integer mtxPoints) {
        this.mtxPoints = mtxPoints;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OwnedMtxPoints)) {
            return false;
        }
        return id != null && id.equals(((OwnedMtxPoints) o).id);
    }

    @Override
    public int hashCode() {
        // see
        // https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OwnedMtxPoints{" + "id=" + getId() + ", userName='" + getUserName() + "'" + ", mtxPoints="
                + getMtxPoints() + "}";
    }
}
