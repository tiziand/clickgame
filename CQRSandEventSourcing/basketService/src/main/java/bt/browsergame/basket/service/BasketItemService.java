package bt.browsergame.basket.service;

import bt.browsergame.basket.domain.BasketItem;
import bt.browsergame.basket.kafka.entities.ButtonColorMtx;
import bt.browsergame.basket.kafka.entities.OwnedMtxPoints;
import bt.browsergame.basket.repository.BasketItemRepository;
import bt.browsergame.basket.security.SecurityUtils;
import bt.browsergame.basket.web.rest.errors.BadRequestAlertException;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;

/**
 * Service Implementation for managing {@link BasketItem}.
 */
@Service
@Transactional
public class BasketItemService {

    private final Logger log = LoggerFactory.getLogger(BasketItemService.class);

    private final BasketItemRepository basketItemRepository;

    @Autowired
    private ApplicationContext context;

    public BasketItemService(BasketItemRepository basketItemRepository) {
        this.basketItemRepository = basketItemRepository;
    }

    /**
     * Save a basketItem.
     *
     * @param basketItem the entity to save.
     * @return the persisted entity.
     */
    public BasketItem save(BasketItem basketItem) {
        basketItem.setUserName(SecurityUtils.getCurrentUserLogin().get());
        // get mtx from kafka-store
        KafkaStreams streams = context.getBean("stream", KafkaStreams.class);
        ReadOnlyKeyValueStore<Long, ButtonColorMtx> buttonColorMtxStore = streams.store(
                StoreQueryParameters.fromNameAndType("button-color-mtx-store", QueryableStoreTypes.keyValueStore()));
        ButtonColorMtx buttonColorMtx = buttonColorMtxStore.get(basketItem.getMtxId());

        // check requirments before adding to basket
        if (!buttonColorMtx.isAvailable())
            throw new BadRequestAlertException("", "", "Mtx not avaliable");
        if (buttonColorMtx.getPointsRequired() != 0) {
            Long pointsFarmed;
            ReadOnlyKeyValueStore<String, Long> pointsFarmedStore;
            if (buttonColorMtx.getTimeLimit() == 0) {
                pointsFarmedStore = streams.store(StoreQueryParameters.fromNameAndType("points-farmed-all-sum-store",
                        QueryableStoreTypes.keyValueStore()));
            } else {
                pointsFarmedStore = streams.store(StoreQueryParameters.fromNameAndType(
                        "points-farmed-" + buttonColorMtx.getTimeLimit() + "-max-store",
                        QueryableStoreTypes.keyValueStore()));
            }
            if (pointsFarmedStore == null) {
                throw new BadRequestAlertException("", "", "Requirments not met");
            }
            pointsFarmed = pointsFarmedStore.get(SecurityUtils.getCurrentUserLogin().get());
            if (pointsFarmed == null || pointsFarmed < buttonColorMtx.getPointsRequired())
                throw new BadRequestAlertException("", "", "Requirments not met");
        }
        return basketItemRepository.save(basketItem);
    }

    /**
     * Partially update a basketItem.
     *
     * @param basketItem the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<BasketItem> partialUpdate(BasketItem basketItem) {
        log.debug("Request to partially update BasketItem : {}", basketItem);

        return basketItemRepository.findById(basketItem.getId()).map(existingBasketItem -> {
            if (basketItem.getUserName() != null) {
                existingBasketItem.setUserName(basketItem.getUserName());
            }
            if (basketItem.getMtxId() != null) {
                existingBasketItem.setMtxId(basketItem.getMtxId());
            }

            return existingBasketItem;
        }).map(basketItemRepository::save);
    }

    /**
     * Get all the basketItems.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BasketItem> findAll() {
        log.debug("Request to get all BasketItems");
        return basketItemRepository.findAll();
    }

    /**
     * Get one basketItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BasketItem> findOne(Long id) {
        log.debug("Request to get BasketItem : {}", id);
        return basketItemRepository.findById(id);
    }

    /**
     * Delete the basketItem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BasketItem : {}", id);
        basketItemRepository.deleteById(id);
    }

    /**
     * Get the basketItems of the logged in user.
     *
     */
    public List<BasketItem> getUserBasket() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxId");
        BasketItem exBasketItem = new BasketItem();
        exBasketItem.setUserName(SecurityUtils.getCurrentUserLogin().get());
        Example<BasketItem> example = Example.of(exBasketItem, usernameMatcher);
        return basketItemRepository.findAll(example);
    }

    /**
     * Purchases basketItems of the logged in user.
     *
     */
    public List<BasketItem> purchaseBasket() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        List<BasketItem> basketItems = getUserBasket();

        // calculate cost of the basket and remove mtxPoints if enough are available
        int sum = 0;
        KafkaStreams streams = context.getBean("stream", KafkaStreams.class);
        ReadOnlyKeyValueStore<Long, ButtonColorMtx> buttonColorMtxStore = streams.store(
                StoreQueryParameters.fromNameAndType("button-color-mtx-store", QueryableStoreTypes.keyValueStore()));
        for (BasketItem basketItem : basketItems) {
            ButtonColorMtx buttonColorMtx = buttonColorMtxStore.get(basketItem.getMtxId());
            sum += buttonColorMtx.getPrice();
        }
        ReadOnlyKeyValueStore<String, Integer> keyValueStore = streams
                .store(StoreQueryParameters.fromNameAndType("mtx-points-store", QueryableStoreTypes.keyValueStore()));
        Integer points = keyValueStore.get(SecurityUtils.getCurrentUserLogin().get());

        if (points < sum)
            return basketItems;

        // add mtx to inventory and update statstics
        Producer<String, Long> ownedButtonColorMtxProducer = context.getBean("ownedButtonColorMtxProducer",
                Producer.class);
        for (BasketItem basketItem : basketItems) {
            // publish owned to kafka
            ownedButtonColorMtxProducer.send(new ProducerRecord<String, Long>("owned-button-color-mtx",
                    SecurityUtils.getCurrentUserLogin().get(), basketItem.getMtxId()));

            // update statistics
            delete(basketItem.getId());
        }

        // subtract mtx points via kafka
        Producer<String, OwnedMtxPoints> ownedMtxPointsProducer = context.getBean("ownedMtxPointsProducer",
                Producer.class);
        OwnedMtxPoints ownedMtxPoints = new OwnedMtxPoints();
        ownedMtxPoints.setMtxPoints(sum * -1);
        ownedMtxPoints.setUserName(SecurityUtils.getCurrentUserLogin().get());
        ownedMtxPointsProducer.send(new ProducerRecord<String, OwnedMtxPoints>("mtx-points-update",
                ownedMtxPoints.getUserName(), ownedMtxPoints));

        basketItems.clear();
        return basketItems;
    }
}
