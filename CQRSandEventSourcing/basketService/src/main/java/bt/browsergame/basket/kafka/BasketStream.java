package bt.browsergame.basket.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.Initializer;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import bt.browsergame.basket.kafka.entities.ButtonColorMtx;
import bt.browsergame.basket.kafka.entities.OwnedMtxPoints;
import bt.browsergame.basket.kafka.serialization.JsonSerializationFactory;

import java.util.Properties;

//inspired by https://kafka.apache.org/27/documentation/streams/tutorial
@Configuration
@ConfigurationProperties(prefix = "kafka")
public class BasketStream {

        private final Logger log = LoggerFactory.getLogger(BasketStream.class);
        private String bootStrapServers = "localhost:9092";

        public String getBootStrapServers() {
                return bootStrapServers;
        }

        public void setBootStrapServers(String bootStrapServers) {
                this.bootStrapServers = bootStrapServers;
        }

        private void maxAggregation(StreamsBuilder builder, String topic) {

                builder.stream(topic, Consumed.with(Serdes.String(), Serdes.Long())).groupByKey()
                                .aggregate(new Initializer<Long>() {
                                        @Override
                                        public Long apply() {
                                                return Long.valueOf(0);
                                        }
                                }, (key, value, aggregate) -> {
                                        if (aggregate < value)
                                                return value;
                                        return aggregate;
                                }, Materialized.<String, Long, KeyValueStore<Bytes, byte[]>>as(topic + "-state")
                                                .withValueSerde(Serdes.Long()).withKeySerde(Serdes.String()))
                                .toStream().to(topic + "-max");

                builder.globalTable(topic + "-max", Consumed.with(Serdes.String(), Serdes.Long()),
                                Materialized.<String, Long, KeyValueStore<Bytes, byte[]>>as(topic + "-max-store"));
        }

        @Bean(name = "stream")
        public KafkaStreams kafkaStream() {

                // config
                Properties props = new Properties();
                props.put(StreamsConfig.APPLICATION_ID_CONFIG, "basketService");
                props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
                props.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                                LogAndContinueExceptionHandler.class);
                props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, "1000");

                final Serde<ButtonColorMtx> buttonColorMtxSerde = JsonSerializationFactory
                                .<ButtonColorMtx>getSerde(ButtonColorMtx.class);

                final StreamsBuilder builder = new StreamsBuilder();

                // build global tables of the needed data
                builder.globalTable("button-color-mtx", Consumed.with(Serdes.Long(), buttonColorMtxSerde), Materialized
                                .<Long, ButtonColorMtx, KeyValueStore<Bytes, byte[]>>as("button-color-mtx-store"));

                builder.globalTable("mtx-points-user", Consumed.with(Serdes.String(), Serdes.Integer()),
                                Materialized.<String, Integer, KeyValueStore<Bytes, byte[]>>as("mtx-points-store"));

                maxAggregation(builder, "points-farmed-2.0");
                maxAggregation(builder, "points-farmed-4.0");
                maxAggregation(builder, "points-farmed-6.0");

                KStream<String, Long> source = builder.stream("points-farmed-all",
                                Consumed.with(Serdes.String(), Serdes.Long()));

                // sum points farmed
                source.groupByKey(Grouped.with(Serdes.String(), Serdes.Long())).reduce(Long::sum).toStream()
                                .to("points-farmed-all-sum");

                builder.globalTable("points-farmed-all-sum", Consumed.with(Serdes.String(), Serdes.Long()), Materialized
                                .<String, Long, KeyValueStore<Bytes, byte[]>>as("points-farmed-all-sum-store"));

                // build topology and start stream
                final Topology topology = builder.build();
                log.debug(topology.describe().toString());
                final KafkaStreams streams = new KafkaStreams(topology, props);
                streams.start();

                return streams;

        }

        @Bean(name = "ownedButtonColorMtxProducer")
        public Producer<String, Long> ownedButtonColorMtxProducer() {
                Properties config = new Properties();
                config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
                config.put(ProducerConfig.ACKS_CONFIG, "all");
                config.put(ProducerConfig.RETRIES_CONFIG, 10);
                config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
                config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, LongSerializer.class);

                Producer<String, Long> producer = new KafkaProducer<>(config);
                return producer;
        }

        @Bean(name = "ownedMtxPointsProducer")
        public Producer<String, OwnedMtxPoints> ownedMtxPointsProducer() {
                Properties config = new Properties();
                config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
                config.put(ProducerConfig.ACKS_CONFIG, "all");
                config.put(ProducerConfig.RETRIES_CONFIG, 10);
                config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

                final Serde<OwnedMtxPoints> ownedMtxPointsSerde = JsonSerializationFactory
                                .<OwnedMtxPoints>getSerde(OwnedMtxPoints.class);

                config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ownedMtxPointsSerde.serializer().getClass());

                Producer<String, OwnedMtxPoints> producer = new KafkaProducer<>(config);
                return producer;
        }

}
