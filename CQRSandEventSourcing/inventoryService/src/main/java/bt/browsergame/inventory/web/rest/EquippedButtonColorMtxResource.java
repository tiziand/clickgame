package bt.browsergame.inventory.web.rest;

import bt.browsergame.inventory.domain.EquippedButtonColorMtx;
import bt.browsergame.inventory.repository.EquippedButtonColorMtxRepository;
import bt.browsergame.inventory.service.EquippedButtonColorMtxService;
import bt.browsergame.inventory.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing
 * {@link bt.browsergame.inventory.domain.EquippedButtonColorMtx}.
 */
@RestController
@RequestMapping("/api")
public class EquippedButtonColorMtxResource {

    private final Logger log = LoggerFactory.getLogger(EquippedButtonColorMtxResource.class);

    private static final String ENTITY_NAME = "inventoryServiceEquippedButtonColorMtx";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EquippedButtonColorMtxService equippedButtonColorMtxService;

    private final EquippedButtonColorMtxRepository equippedButtonColorMtxRepository;

    public EquippedButtonColorMtxResource(EquippedButtonColorMtxService equippedButtonColorMtxService,
            EquippedButtonColorMtxRepository equippedButtonColorMtxRepository) {
        this.equippedButtonColorMtxService = equippedButtonColorMtxService;
        this.equippedButtonColorMtxRepository = equippedButtonColorMtxRepository;
    }

    /**
     * {@code POST  /equipped-button-color-mtxes} : Create a new
     * equippedButtonColorMtx.
     *
     * @param equippedButtonColorMtx the equippedButtonColorMtx to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new equippedButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the equippedButtonColorMtx has already
     *         an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/equipped-button-color-mtxes")
    public ResponseEntity<EquippedButtonColorMtx> createEquippedButtonColorMtx(
            @Valid @RequestBody EquippedButtonColorMtx equippedButtonColorMtx) throws URISyntaxException {
        log.debug("REST request to save EquippedButtonColorMtx : {}", equippedButtonColorMtx);
        if (equippedButtonColorMtx.getId() != null) {
            throw new BadRequestAlertException("A new equippedButtonColorMtx cannot already have an ID", ENTITY_NAME,
                    "idexists");
        }
        EquippedButtonColorMtx result = equippedButtonColorMtxService.save(equippedButtonColorMtx);
        return ResponseEntity
                .created(new URI("/api/equipped-button-color-mtxes/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /equipped-button-color-mtxes/:id} : Updates an existing
     * equippedButtonColorMtx.
     *
     * @param id                     the id of the equippedButtonColorMtx to save.
     * @param equippedButtonColorMtx the equippedButtonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated equippedButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the equippedButtonColorMtx is not valid,
     *         or with status {@code 500 (Internal Server Error)} if the
     *         equippedButtonColorMtx couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/equipped-button-color-mtxes/{id}")
    public ResponseEntity<EquippedButtonColorMtx> updateEquippedButtonColorMtx(
            @PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody EquippedButtonColorMtx equippedButtonColorMtx) throws URISyntaxException {
        log.debug("REST request to update EquippedButtonColorMtx : {}, {}", id, equippedButtonColorMtx);
        if (equippedButtonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, equippedButtonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!equippedButtonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EquippedButtonColorMtx result = equippedButtonColorMtxService.save(equippedButtonColorMtx);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME,
                equippedButtonColorMtx.getId().toString())).body(result);
    }

    /**
     * {@code PATCH  /equipped-button-color-mtxes/:id} : Partial updates given
     * fields of an existing equippedButtonColorMtx, field will ignore if it is null
     *
     * @param id                     the id of the equippedButtonColorMtx to save.
     * @param equippedButtonColorMtx the equippedButtonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated equippedButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the equippedButtonColorMtx is not valid,
     *         or with status {@code 404 (Not Found)} if the equippedButtonColorMtx
     *         is not found, or with status {@code 500 (Internal Server Error)} if
     *         the equippedButtonColorMtx couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/equipped-button-color-mtxes/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<EquippedButtonColorMtx> partialUpdateEquippedButtonColorMtx(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody EquippedButtonColorMtx equippedButtonColorMtx) throws URISyntaxException {
        log.debug("REST request to partial update EquippedButtonColorMtx partially : {}, {}", id,
                equippedButtonColorMtx);
        if (equippedButtonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, equippedButtonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!equippedButtonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EquippedButtonColorMtx> result = equippedButtonColorMtxService.partialUpdate(equippedButtonColorMtx);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ENTITY_NAME, equippedButtonColorMtx.getId().toString()));
    }

    /**
     * {@code GET  /equipped-button-color-mtxes} : get all the
     * equippedButtonColorMtxes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of equippedButtonColorMtxes in body.
     */
    @GetMapping("/equipped-button-color-mtxes")
    public List<EquippedButtonColorMtx> getAllEquippedButtonColorMtxes() {
        log.debug("REST request to get all EquippedButtonColorMtxes");
        return equippedButtonColorMtxService.findAll();
    }

    /**
     * {@code GET  /equipped-button-color-mtxes/:id} : get the "id"
     * equippedButtonColorMtx.
     *
     * @param id the id of the equippedButtonColorMtx to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the equippedButtonColorMtx, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/equipped-button-color-mtxes/{id}")
    public ResponseEntity<EquippedButtonColorMtx> getEquippedButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to get EquippedButtonColorMtx : {}", id);
        Optional<EquippedButtonColorMtx> equippedButtonColorMtx = equippedButtonColorMtxService.findOne(id);
        return ResponseUtil.wrapOrNotFound(equippedButtonColorMtx);
    }

    /**
     * {@code DELETE  /equipped-button-color-mtxes/:id} : delete the "id"
     * equippedButtonColorMtx.
     *
     * @param id the id of the equippedButtonColorMtx to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/equipped-button-color-mtxes/{id}")
    public ResponseEntity<Void> deleteEquippedButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to delete EquippedButtonColorMtx : {}", id);
        equippedButtonColorMtxService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code GET  /userEquippedButtonColorMtx} : get the "username" equippedMtx.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the equipped mtx
     */
    @GetMapping("/equipped-button-color-mtxes/userEquippedButtonColorMtx/{timeSlot}")
    public ResponseEntity<EquippedButtonColorMtx> getUserEquippedButtonColorMtx(@PathVariable Double timeSlot) {
        return ResponseEntity.ok(equippedButtonColorMtxService.getUserEquippedButtonColorMtx(timeSlot));
    }

    /**
     * {@code PUT  /equippButtonColorMtx} : equippMtx on logged in User.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the equipped mtx
     */
    @PutMapping("/equipped-button-color-mtxes/equippButtonColorMtx/{id}/{timeSlot}")
    public ResponseEntity<EquippedButtonColorMtx> equippButtonColorMtx(@PathVariable Long id,
            @PathVariable Double timeSlot) {
        return ResponseEntity.ok(equippedButtonColorMtxService.equippButtonColorMtx(id, timeSlot));
    }
}
