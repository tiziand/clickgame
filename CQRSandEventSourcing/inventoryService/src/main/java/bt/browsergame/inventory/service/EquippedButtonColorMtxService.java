package bt.browsergame.inventory.service;

import bt.browsergame.inventory.domain.EquippedButtonColorMtx;
import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.kafka.entities.ButtonColorMtx;
import bt.browsergame.inventory.repository.EquippedButtonColorMtxRepository;
import bt.browsergame.inventory.security.SecurityUtils;
import bt.browsergame.inventory.web.rest.errors.BadRequestAlertException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * Service Implementation for managing {@link EquippedButtonColorMtx}.
 */
@Service
@Transactional
public class EquippedButtonColorMtxService {

    private final Logger log = LoggerFactory.getLogger(EquippedButtonColorMtxService.class);

    private final EquippedButtonColorMtxRepository equippedButtonColorMtxRepository;

    @Autowired
    private ApplicationContext context;

    public EquippedButtonColorMtxService(EquippedButtonColorMtxRepository equippedButtonColorMtxRepository) {
        this.equippedButtonColorMtxRepository = equippedButtonColorMtxRepository;
    }

    /**
     * Save a equippedButtonColorMtx.
     *
     * @param equippedButtonColorMtx the entity to save.
     * @return the persisted entity.
     */
    public EquippedButtonColorMtx save(EquippedButtonColorMtx equippedButtonColorMtx) {
        log.debug("Request to save EquippedButtonColorMtx : {}", equippedButtonColorMtx);
        return equippedButtonColorMtxRepository.save(equippedButtonColorMtx);
    }

    /**
     * Partially update a equippedButtonColorMtx.
     *
     * @param equippedButtonColorMtx the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<EquippedButtonColorMtx> partialUpdate(EquippedButtonColorMtx equippedButtonColorMtx) {
        log.debug("Request to partially update EquippedButtonColorMtx : {}", equippedButtonColorMtx);

        return equippedButtonColorMtxRepository.findById(equippedButtonColorMtx.getId())
                .map(existingEquippedButtonColorMtx -> {
                    if (equippedButtonColorMtx.getUserName() != null) {
                        existingEquippedButtonColorMtx.setUserName(equippedButtonColorMtx.getUserName());
                    }
                    if (equippedButtonColorMtx.getMtxId() != null) {
                        existingEquippedButtonColorMtx.setMtxId(equippedButtonColorMtx.getMtxId());
                    }
                    if (equippedButtonColorMtx.getTimeSlot() != null) {
                        existingEquippedButtonColorMtx.setTimeSlot(equippedButtonColorMtx.getTimeSlot());
                    }

                    return existingEquippedButtonColorMtx;
                }).map(equippedButtonColorMtxRepository::save);
    }

    /**
     * Get all the equippedButtonColorMtxes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EquippedButtonColorMtx> findAll() {
        log.debug("Request to get all EquippedButtonColorMtxes");
        return equippedButtonColorMtxRepository.findAll();
    }

    /**
     * Get one equippedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EquippedButtonColorMtx> findOne(Long id) {
        log.debug("Request to get EquippedButtonColorMtx : {}", id);
        return equippedButtonColorMtxRepository.findById(id);
    }

    /**
     * Delete the equippedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EquippedButtonColorMtx : {}", id);
        equippedButtonColorMtxRepository.deleteById(id);
    }

    /**
     * Get the equippedButtonColorMtx of the logged in user.
     *
     */
    public EquippedButtonColorMtx getUserEquippedButtonColorMtx(Double timeSlot) {
        log.debug("Request to get getUserEquippedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxId");
        EquippedButtonColorMtx exEButtonColorMtx = new EquippedButtonColorMtx();
        exEButtonColorMtx.setUserName(SecurityUtils.getCurrentUserLogin().get());
        exEButtonColorMtx.setTimeSlot(timeSlot);
        Example<EquippedButtonColorMtx> example = Example.of(exEButtonColorMtx, usernameMatcher);
        Optional<EquippedButtonColorMtx> result = equippedButtonColorMtxRepository.findOne(example);
        if (result.isPresent()) {
            return result.get();
        }
        exEButtonColorMtx.setMtxId(Long.valueOf(1));
        return equippedButtonColorMtxRepository.save(exEButtonColorMtx);
    }

    /**
     * Equipp MTX for the logged in user.
     *
     */
    public EquippedButtonColorMtx equippButtonColorMtx(Long id, Double timeSlot) {

        KafkaStreams streams = context.getBean("stream", KafkaStreams.class);

        // check owned and slot requirements with kafka if it's not the default mtx
        if (!id.equals(Long.valueOf(1))) {
            ReadOnlyKeyValueStore<Long, ButtonColorMtx> buttonColorMtxStore = streams.store(StoreQueryParameters
                    .fromNameAndType("button-color-mtx-store", QueryableStoreTypes.keyValueStore()));
            ButtonColorMtx buttonColorMtx = buttonColorMtxStore.get(id);
            if (buttonColorMtx.isLocked() && !buttonColorMtx.getTimeLimit().equals(timeSlot)) {
                throw new BadRequestAlertException("", "", "Wrong time slot!");
            }
        }
        // get owned mtx from kafka and check owned
        ObjectMapper mapper = new ObjectMapper();
        ReadOnlyKeyValueStore<String, ArrayList<OwnedButtonColorMtx>> ownedButtonColorMtxStore = streams
                .store(StoreQueryParameters.fromNameAndType("owned-button-color-mtx-store",
                        QueryableStoreTypes.keyValueStore()));
        // deserializer needs this for now
        // https://stackoverflow.com/questions/22917324/jackson-json-list-with-object-type
        List<OwnedButtonColorMtx> list = mapper.convertValue(
                ownedButtonColorMtxStore.get(SecurityUtils.getCurrentUserLogin().get()),
                new TypeReference<List<OwnedButtonColorMtx>>() {
                });
        // add default mtx
        OwnedButtonColorMtx defaultMtx = new OwnedButtonColorMtx();
        defaultMtx.setMtxId(Long.valueOf(1));
        defaultMtx.setUserName(SecurityUtils.getCurrentUserLogin().get());
        defaultMtx.setId(Long.valueOf(0));
        list.add(defaultMtx);
        OwnedButtonColorMtx result = list.stream().filter(element -> element.getMtxId().equals(id)).findAny()
                .orElse(null);
        if (result == null)
            throw new BadRequestAlertException("", "", "Mtx not owned!");
        EquippedButtonColorMtx mtx = getUserEquippedButtonColorMtx(timeSlot);
        mtx.setMtxId(id);
        return equippedButtonColorMtxRepository.save(mtx);
    }
}
