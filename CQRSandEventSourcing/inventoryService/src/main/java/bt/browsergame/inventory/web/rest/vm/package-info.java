/**
 * View Models used by Spring MVC REST controllers.
 */
package bt.browsergame.inventory.web.rest.vm;
