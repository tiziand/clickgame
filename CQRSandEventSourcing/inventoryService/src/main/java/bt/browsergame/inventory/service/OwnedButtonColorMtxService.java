package bt.browsergame.inventory.service;

import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.repository.OwnedButtonColorMtxRepository;
import bt.browsergame.inventory.security.SecurityUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link OwnedButtonColorMtx}.
 */
@Service
@Transactional
public class OwnedButtonColorMtxService {

    private final Logger log = LoggerFactory.getLogger(OwnedButtonColorMtxService.class);

    private final OwnedButtonColorMtxRepository ownedButtonColorMtxRepository;

    @Autowired
    private ApplicationContext context;

    public OwnedButtonColorMtxService(OwnedButtonColorMtxRepository ownedButtonColorMtxRepository) {
        this.ownedButtonColorMtxRepository = ownedButtonColorMtxRepository;
    }

    /**
     * Save a ownedButtonColorMtx.
     *
     * @param ownedButtonColorMtx the entity to save.
     * @return the persisted entity.
     */
    public OwnedButtonColorMtx save(OwnedButtonColorMtx ownedButtonColorMtx) {
        log.debug("Request to save OwnedButtonColorMtx : {}", ownedButtonColorMtx);
        return ownedButtonColorMtxRepository.save(ownedButtonColorMtx);
    }

    /**
     * Partially update a ownedButtonColorMtx.
     *
     * @param ownedButtonColorMtx the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OwnedButtonColorMtx> partialUpdate(OwnedButtonColorMtx ownedButtonColorMtx) {
        log.debug("Request to partially update OwnedButtonColorMtx : {}", ownedButtonColorMtx);

        return ownedButtonColorMtxRepository.findById(ownedButtonColorMtx.getId()).map(existingOwnedButtonColorMtx -> {
            if (ownedButtonColorMtx.getUserName() != null) {
                existingOwnedButtonColorMtx.setUserName(ownedButtonColorMtx.getUserName());
            }
            if (ownedButtonColorMtx.getMtxId() != null) {
                existingOwnedButtonColorMtx.setMtxId(ownedButtonColorMtx.getMtxId());
            }

            return existingOwnedButtonColorMtx;
        }).map(ownedButtonColorMtxRepository::save);
    }

    /**
     * Get all the ownedButtonColorMtxes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OwnedButtonColorMtx> findAll() {
        log.debug("Request to get all OwnedButtonColorMtxes");
        return ownedButtonColorMtxRepository.findAll();
    }

    /**
     * Get one ownedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OwnedButtonColorMtx> findOne(Long id) {
        log.debug("Request to get OwnedButtonColorMtx : {}", id);
        return ownedButtonColorMtxRepository.findById(id);
    }

    /**
     * Delete the ownedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OwnedButtonColorMtx : {}", id);
        ownedButtonColorMtxRepository.deleteById(id);
    }

    /**
     * Get the ownedButtonColorMtx of the logged in user.
     *
     */
    public List<OwnedButtonColorMtx> getUserOwnedButtonColorMtx() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        // get owned mtx from kafka
        KafkaStreams streams = context.getBean("stream", KafkaStreams.class);
        ReadOnlyKeyValueStore<String, ArrayList<OwnedButtonColorMtx>> ownedButtonColorMtxStore = streams
                .store(StoreQueryParameters.fromNameAndType("owned-button-color-mtx-store",
                        QueryableStoreTypes.keyValueStore()));

        List<OwnedButtonColorMtx> result = ownedButtonColorMtxStore.get(SecurityUtils.getCurrentUserLogin().get());
        if (result == null)
            result = new ArrayList<OwnedButtonColorMtx>();
        OwnedButtonColorMtx defaultMtx = new OwnedButtonColorMtx();
        defaultMtx.setMtxId(Long.valueOf(1));
        defaultMtx.setUserName(SecurityUtils.getCurrentUserLogin().get());
        defaultMtx.setId(Long.valueOf(0));
        result.add(defaultMtx);

        return result;
    }
}
