package bt.browsergame.inventory.kafka;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;

import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.domain.OwnedMtxPoints;
import bt.browsergame.inventory.kafka.entities.ButtonColorMtx;
import bt.browsergame.inventory.kafka.serialization.JsonSerializationFactory;

import java.util.ArrayList;
import java.util.Properties;

//inspired by https://kafka.apache.org/27/documentation/streams/tutorial
@Configuration
@ConfigurationProperties(prefix = "kafka")
public class PurchaseStream {

        private final Logger log = LoggerFactory.getLogger(PurchaseStream.class);
        private String bootStrapServers = "localhost:9092";

        public String getBootStrapServers() {
                return bootStrapServers;
        }

        public void setBootStrapServers(String bootStrapServers) {
                this.bootStrapServers = bootStrapServers;
        }

        @Bean(name = "stream")
        public KafkaStreams kafkaStream() {

                // config
                Properties props = new Properties();
                props.put(StreamsConfig.APPLICATION_ID_CONFIG, "inventoryService");
                props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
                props.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                                LogAndContinueExceptionHandler.class);
                props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, "1000");

                final Serde<OwnedMtxPoints> ownedMtxPointsSerde = JsonSerializationFactory
                                .<OwnedMtxPoints>getSerde(OwnedMtxPoints.class);

                final StreamsBuilder builder = new StreamsBuilder();

                KStream<String, OwnedMtxPoints> source = builder.stream("mtx-points-update",
                                Consumed.with(Serdes.String(), ownedMtxPointsSerde));

                // sum mtx points stream back to kafka
                source.map((key, value) -> new KeyValue<>(key, value.getMtxPoints()))
                                .groupByKey(Grouped.with(Serdes.String(), Serdes.Integer())).reduce(Integer::sum)
                                .toStream().to("mtx-points-user");

                // consume globally
                builder.globalTable("mtx-points-user", Consumed.with(Serdes.String(), Serdes.Integer()),
                                Materialized.<String, Integer, KeyValueStore<Bytes, byte[]>>as("mtx-points-store"));

                final Serde<ArrayList<OwnedButtonColorMtx>> ownedButtonColorMtxSerde = JsonSerializationFactory
                                .<ArrayList<OwnedButtonColorMtx>>getSerde(
                                                (Class<ArrayList<OwnedButtonColorMtx>>) new ArrayList<OwnedButtonColorMtx>()
                                                                .getClass());

                // aggregate owned button color mtx
                builder.stream("owned-button-color-mtx", Consumed.with(Serdes.String(), Serdes.Long())).groupByKey()
                                .aggregate(ArrayList::new, (key, value, aggregate) -> {
                                        OwnedButtonColorMtx ownedButtonColorMtx = new OwnedButtonColorMtx();
                                        ownedButtonColorMtx.setId(value);
                                        ownedButtonColorMtx.setMtxId(value);
                                        ownedButtonColorMtx.setUserName(key);
                                        aggregate.add(ownedButtonColorMtx);
                                        return aggregate;
                                }, Materialized.<String, ArrayList<OwnedButtonColorMtx>, KeyValueStore<Bytes, byte[]>>as(
                                                "owned-button-color-mtx-state").withValueSerde(ownedButtonColorMtxSerde)
                                                .withKeySerde(Serdes.String()))
                                .toStream().to("owned-button-color-mtx-agg");

                // consume aggregate globally
                builder.globalTable("owned-button-color-mtx-agg",
                                Consumed.with(Serdes.String(), ownedButtonColorMtxSerde),
                                Materialized.<String, ArrayList<OwnedButtonColorMtx>, KeyValueStore<Bytes, byte[]>>as(
                                                "owned-button-color-mtx-store").withValueSerde(ownedButtonColorMtxSerde)
                                                .withKeySerde(Serdes.String()));

                // consume button collor mtx and store in global table
                final Serde<ButtonColorMtx> buttonColorMtxSerde = JsonSerializationFactory
                                .<ButtonColorMtx>getSerde(ButtonColorMtx.class);
                builder.globalTable("button-color-mtx", Consumed.with(Serdes.Long(), buttonColorMtxSerde), Materialized
                                .<Long, ButtonColorMtx, KeyValueStore<Bytes, byte[]>>as("button-color-mtx-store"));

                // build topology and start stream
                final Topology topology = builder.build();
                log.debug(topology.describe().toString());
                final KafkaStreams streams = new KafkaStreams(topology, props);
                streams.start();

                return streams;

        }
}
