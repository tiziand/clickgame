package bt.browsergame.inventory.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.inventory.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EquippedButtonColorMtxTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EquippedButtonColorMtx.class);
        EquippedButtonColorMtx equippedButtonColorMtx1 = new EquippedButtonColorMtx();
        equippedButtonColorMtx1.setId(1L);
        EquippedButtonColorMtx equippedButtonColorMtx2 = new EquippedButtonColorMtx();
        equippedButtonColorMtx2.setId(equippedButtonColorMtx1.getId());
        assertThat(equippedButtonColorMtx1).isEqualTo(equippedButtonColorMtx2);
        equippedButtonColorMtx2.setId(2L);
        assertThat(equippedButtonColorMtx1).isNotEqualTo(equippedButtonColorMtx2);
        equippedButtonColorMtx1.setId(null);
        assertThat(equippedButtonColorMtx1).isNotEqualTo(equippedButtonColorMtx2);
    }
}
