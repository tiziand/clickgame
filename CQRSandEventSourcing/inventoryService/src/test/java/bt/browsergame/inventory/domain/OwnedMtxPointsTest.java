package bt.browsergame.inventory.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.inventory.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OwnedMtxPointsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OwnedMtxPoints.class);
        OwnedMtxPoints ownedMtxPoints1 = new OwnedMtxPoints();
        ownedMtxPoints1.setId(1L);
        OwnedMtxPoints ownedMtxPoints2 = new OwnedMtxPoints();
        ownedMtxPoints2.setId(ownedMtxPoints1.getId());
        assertThat(ownedMtxPoints1).isEqualTo(ownedMtxPoints2);
        ownedMtxPoints2.setId(2L);
        assertThat(ownedMtxPoints1).isNotEqualTo(ownedMtxPoints2);
        ownedMtxPoints1.setId(null);
        assertThat(ownedMtxPoints1).isNotEqualTo(ownedMtxPoints2);
    }
}
