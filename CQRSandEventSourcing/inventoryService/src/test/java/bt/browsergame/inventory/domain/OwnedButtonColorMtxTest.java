package bt.browsergame.inventory.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.inventory.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OwnedButtonColorMtxTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OwnedButtonColorMtx.class);
        OwnedButtonColorMtx ownedButtonColorMtx1 = new OwnedButtonColorMtx();
        ownedButtonColorMtx1.setId(1L);
        OwnedButtonColorMtx ownedButtonColorMtx2 = new OwnedButtonColorMtx();
        ownedButtonColorMtx2.setId(ownedButtonColorMtx1.getId());
        assertThat(ownedButtonColorMtx1).isEqualTo(ownedButtonColorMtx2);
        ownedButtonColorMtx2.setId(2L);
        assertThat(ownedButtonColorMtx1).isNotEqualTo(ownedButtonColorMtx2);
        ownedButtonColorMtx1.setId(null);
        assertThat(ownedButtonColorMtx1).isNotEqualTo(ownedButtonColorMtx2);
    }
}
