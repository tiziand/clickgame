import scala.concurrent.duration._
import scala.util.Random

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class MediumTestOne extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:9000")
    .inferHtmlResources()
    
  val headers_11 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_13 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYyMjEwMTA4NH0.P7nBwTUDGBTq4KWfbTurF_ftt3Dz18vbkEeAd1CcXn7gXjPENi5iABJg32XO-UIKaPVkThVipLcXFzz82mA9PQ",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_14 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYyMjA5NjI2NH0.DLxXFmYgmR8HtJQZC2ZbRbVXp_XRZl-xiPsFrsEWlWtbzBBxY_t5b1lbkUVasbp72zZK5iisYE3I8VaT9mEgQQ",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_18 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMjAzNzkyNH0.11tGEVB-5EOM8N-roIBx80Fxt4_yI9OjAh-VlsdghzHRhjAA7NXageHrsTOJdneNISQxv40WBhR-iLV6lhGxrA",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  //https://stackoverflow.com/questions/34665566/generate-random-string-letter-in-scala/34665585
  val alpha = "abcdefghijklmnopqrstuvwxyz123456789"
  def randStr(n: Int) =
    (1 to n).map(_ => alpha(Random.nextInt(alpha.length))).mkString

  //https://stackoverflow.com/questions/35348845/simple-feeder-in-gatling-without-using-a-csv-file
  def randUserInfo() =
    Map(
      "email" -> (randStr(8) + "@test.com"),
      "user" -> randStr(8)
    )

  val feeder = Iterator.continually(randUserInfo())

  val scn = scenario("MediumTestOne")
    .feed(feeder)
    .exec(
      http("register")
        .post("/api/register")
        .headers(headers_11)
        .body(
          StringBody(
            """{"login":"${user}","email":"${email}","password":"${user}","langKey":"en"}"""
          )
        )
        .asJson
    )
    .exec(
      http("getuserid")
        .get("/api/admin/users/${user}")
        .headers(headers_13)
        .check(jsonPath("$.id").saveAs("id"))
    )
    .pause(2)
    .exec(
      http("activate")
        .put("/api/admin/users")
        .headers(headers_13)
        .body(
          StringBody(
            """{"id":${id},"login":"${user}","firstName":null,"lastName":null,"email":"${email}","imageUrl":null,"activated":true,"langKey":"en","createdBy":"system","createdDate":"2021-05-17T08:30:19.074692Z","lastModifiedBy":"system","lastModifiedDate":"2021-05-17T08:30:19.074692Z","authorities":["ROLE_USER"]}"""
          )
        )
        .asJson
    )
    .pause(2)
    .exec(
      http("login")
        .post("/api/authenticate")
        .headers(headers_11)
        .body(
          StringBody(
            """{"username":"${user}","password":"${user}","rememberMe":false}"""
          )
        )
        .asJson
        .check(jsonPath("$.id_token").saveAs("token"))
    )
    .pause(2)
    .exec(
      http("Game Section")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_16")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))))
        )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0018_request.json"))
        .resources(
          http("request_19")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_20")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951530817"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_21")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_22")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0023_request.json"))
        .resources(
          http("request_24")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_25")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951538002"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_26")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/4"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_27")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0028_request.json"))
        .resources(
          http("request_29")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951543389"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_30")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("request_31")
        .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_32")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/2"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0033_request.json"))
        .resources(
          http("request_34")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951547140"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_35")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Check Ranking")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingSum?page=0&size=5&cacheBuster=1621951550728"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("Check Ranking")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingMax/6?page=0&size=5&cacheBuster=1621951552649"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_38")
            .get(
              "/services/rankingservice/api/points-farmeds/rankingMax/4?page=0&size=5&cacheBuster=1621951553402"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_39")
            .get(
              "/services/rankingservice/api/points-farmeds/rankingMax/2?page=0&size=5&cacheBuster=1621951554086"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Check statstics")
        .get(
          "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-25"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_41")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-25"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_42")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-25?cacheBuster=1621951555743"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_43")
            .get(
              "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-25"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_44")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621951555744"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Go to game section")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_46")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_47")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0048_request.json"))
        .resources(
          http("request_49")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_50")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951594537"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_51")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_52")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0053_request.json"))
        .resources(
          http("request_54")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951601816"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_55")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("request_56")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/4"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_57")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0058_request.json"))
        .resources(
          http("request_59")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951607557"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_60")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("request_61")
        .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_62")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/2"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_18  + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtestone/0063_request.json"))
        .resources(
          http("request_64")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_65")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951611204"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Check Ranking")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingSum?page=0&size=5&cacheBuster=1621951616730"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("request_67")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingMax/6?page=0&size=5&cacheBuster=1621951617970"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_68")
            .get(
              "/services/rankingservice/api/points-farmeds/rankingMax/4?page=0&size=5&cacheBuster=1621951618571"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_69")
            .get(
              "/services/rankingservice/api/points-farmeds/rankingMax/2?page=0&size=5&cacheBuster=1621951619561"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Check Statistic")
        .get(
          "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-25"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_71")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-25?cacheBuster=1621951620680"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_72")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-25"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_73")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621951620680"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_74")
            .get(
              "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-25"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Statistc change date")
        .get(
          "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-24"
        )
        .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_76")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-24"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_77")
            .get(
              "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-24"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}"))),
          http("request_78")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-24?cacheBuster=1621951624179"
            )
            .headers(headers_14  + ("authorization" -> ("Bearer ${token}")))
        )
    )

  setUp(scn.inject(atOnceUsers(2000))).protocols(httpProtocol)
}

