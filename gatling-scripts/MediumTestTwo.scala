import scala.concurrent.duration._
import scala.util.Random

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class MediumTestTwo extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:9000")
    .inferHtmlResources()

  val headers_11 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_13 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYyMjA5OTMyOH0.CgPrFPfGVonOcrWFRhACVd4Nn9dpXkNFbLB9cu8XdrS212ijUSsUhQkCNJHi5ayW9rZ-sGmnHNuirbcNAN50Ow",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_20 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMjAzODExMH0.Bbo1rT3oeFvgjhGx9bgngwRzNFoRX-FtM9qa43K0AhJiI1y08NY2_mq60i7y7waJYJbH4SFD1MZenPvA6L00kA",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_24 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMjAzODExMH0.Bbo1rT3oeFvgjhGx9bgngwRzNFoRX-FtM9qa43K0AhJiI1y08NY2_mq60i7y7waJYJbH4SFD1MZenPvA6L00kA",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_73 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMjAzODExMH0.Bbo1rT3oeFvgjhGx9bgngwRzNFoRX-FtM9qa43K0AhJiI1y08NY2_mq60i7y7waJYJbH4SFD1MZenPvA6L00kA",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )
  
  //https://stackoverflow.com/questions/34665566/generate-random-string-letter-in-scala/34665585
  val alpha = "abcdefghijklmnopqrstuvwxyz123456789"
  def randStr(n: Int) =
    (1 to n).map(_ => alpha(Random.nextInt(alpha.length))).mkString

  //https://stackoverflow.com/questions/35348845/simple-feeder-in-gatling-without-using-a-csv-file
  def randUserInfo() =
    Map(
      "email" -> (randStr(8) + "@test.com"),
      "user" -> randStr(8)
    )
    
  val feeder = Iterator.continually(randUserInfo())

  val scn = scenario("MediumTestTwo")
    .feed(feeder)
    .exec(
      http("register")
        .post("/api/register")
        .headers(headers_11)
        .body(
          StringBody(
            """{"login":"${user}","email":"${email}","password":"${user}","langKey":"en"}"""
          )
        )
        .asJson
    )
    .exec(
      http("getuserid")
        .get("/api/admin/users/${user}")
        .headers(headers_13)
        .check(jsonPath("$.id").saveAs("id"))
    )
    .pause(2)
    .exec(
      http("activate")
        .put("/api/admin/users")
        .headers(headers_13)
        .body(
          StringBody(
            """{"id":${id},"login":"${user}","firstName":null,"lastName":null,"email":"${email}","imageUrl":null,"activated":true,"langKey":"en","createdBy":"system","createdDate":"2021-05-17T08:30:19.074692Z","lastModifiedBy":"system","lastModifiedDate":"2021-05-17T08:30:19.074692Z","authorities":["ROLE_USER"]}"""
          )
        )
        .asJson
    )
    .pause(2)
    .exec(
      http("login")
        .post("/api/authenticate")
        .headers(headers_11)
        .body(
          StringBody(
            """{"username":"${user}","password":"${user}","rememberMe":false}"""
          )
        )
        .asJson
        .check(jsonPath("$.id_token").saveAs("token"))
    )
    .pause(2)
    .exec(
      http("Go to game section")
        .get("/services/rankingservice/api/points-farmeds/userSum")
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_22")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_23")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Farmpoints")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0024_request.json"))
        .resources(
          http("request_25")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951715567"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_26")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_27")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_28")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("request_29")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0029_request.json"))
        .resources(
          http("request_30")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_31")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951722702"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Check inventory")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_33")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_34")
            .get(
              "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621951725131"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Check store")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621951728970"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_36")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_37")
            .get(
              "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621951728967"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_38")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621951728973"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("Add things to basket")
        .post("/services/basketservice/api/basket-items")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0039_request.json"))
        .resources(
          http("request_40")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621951738761"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_41")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0041_request.json")),
          http("request_42")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621951739658"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_43")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0043_request.json"))
            .check(status.is(400)),
          http("request_44")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621951740248"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_45")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0045_request.json"))
            .check(status.is(400)),
          http("request_46")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0046_request.json")),
          http("request_47")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621951741437"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_48")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0048_request.json"))
            .check(status.is(400)),
          http("request_49")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0049_request.json"))
            .check(status.is(400)),
          http("request_50")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0050_request.json"))
            .check(status.is(400)),
          http("request_51")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
            .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0051_request.json"))
            .check(status.is(400))
        )
    )
    .pause(2)
    .exec(
      http("go to mtx point store")
        .get(
          "/services/paymentservice/api/point-options?cacheBuster=1621951749632"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("buy mtx points")
        .post("/services/paymentservice/api/payment-infos")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0053_request.json"))
    )
    .pause(2)
    .exec(
      http("buy mtx points")
        .post("/services/paymentservice/api/payment-infos")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0054_request.json"))
    )
    .pause(2)
    .exec(
      http("go to basket")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621951760562"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_56")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_57")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621951760563"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_58")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_59")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("request_60")
        .get(
          "/services/paymentservice/api/point-options?cacheBuster=1621951763273"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("purchase points")
        .post("/services/paymentservice/api/payment-infos")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0061_request.json"))
    )
    .pause(2)
    .exec(
      http("go to basket")
        .get(
          "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_63")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621951770195"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_64")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621951770195"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_65")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_66")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("purchase basket")
        .get(
          "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_68")
            .get(
              "/services/basketservice/api/basket-items/purchase?cacheBuster=1621951772562"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_69")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("go to inventory")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_71")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_72")
            .get(
              "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621951778578"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .put(
          "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/2/0"
        )
        .headers(headers_73 +  ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_75")
            .put(
              "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/6/6"
            )
            .headers(headers_73 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("request_76")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/4"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .put(
          "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/3/4"
        )
        .headers(headers_73 +  ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("request_78")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/2"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .put(
          "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/2/2"
        )
        .headers(headers_73 +  ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("go to game section")
        .get("/services/rankingservice/api/points-farmeds/userSum")
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_81")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_82")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0083_request.json"))
        .resources(
          http("request_84")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_85")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951798055"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_86")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_87")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_24 +  ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/mediumtesttwo/0088_request.json"))
        .resources(
          http("request_89")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951805479"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_90")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check statistics")
        .get(
          "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-25"
        )
        .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_92")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-25"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_93")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621951807964"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_94")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-25?cacheBuster=1621951807964"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}"))),
          http("request_95")
            .get(
              "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-25"
            )
            .headers(headers_20 +  ("authorization" -> ("Bearer ${token}")))
        )
    )

  setUp(scn.inject(atOnceUsers(2000))).protocols(httpProtocol)
}
