import scala.concurrent.duration._
import scala.util.Random

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class SmallTest extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:9000")
    .inferHtmlResources()
    
    
  val headers_11 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_13 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYyMjEwMTA4NH0.P7nBwTUDGBTq4KWfbTurF_ftt3Dz18vbkEeAd1CcXn7gXjPENi5iABJg32XO-UIKaPVkThVipLcXFzz82mA9PQ",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )


  val headers_20 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMjAzNzgyOX0.6czFAXT9nDoJ_Lcyj5Ec-Z16-m8HHhbEm0MEvTonH2zpQQiSTIJrObxXyDsAtMoX9iBG023jtCn219aH9_01Sg",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_24 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMjAzNzgyOX0.6czFAXT9nDoJ_Lcyj5Ec-Z16-m8HHhbEm0MEvTonH2zpQQiSTIJrObxXyDsAtMoX9iBG023jtCn219aH9_01Sg",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  //https://stackoverflow.com/questions/34665566/generate-random-string-letter-in-scala/34665585
  val alpha = "abcdefghijklmnopqrstuvwxyz123456789"
  def randStr(n: Int) =
    (1 to n).map(_ => alpha(Random.nextInt(alpha.length))).mkString

  //https://stackoverflow.com/questions/35348845/simple-feeder-in-gatling-without-using-a-csv-file
  def randUserInfo() =
    Map(
      "email" -> (randStr(8) + "@test.com"),
      "user" -> randStr(8)
    )

  val feeder = Iterator.continually(randUserInfo())

  val scn = scenario("SmallTest")
    .feed(feeder)
    .exec(
      http("register")
        .post("/api/register")
        .headers(headers_11)
        .body(
          StringBody(
            """{"login":"${user}","email":"${email}","password":"${user}","langKey":"en"}"""
          )
        )
        .asJson
    )
    .exec(
      http("getuserid")
        .get("/api/admin/users/${user}")
        .headers(headers_13)
        .check(jsonPath("$.id").saveAs("id"))
    )
    .pause(2)
    .exec(
      http("activate")
        .put("/api/admin/users")
        .headers(headers_13)
        .body(
          StringBody(
            """{"id":${id},"login":"${user}","firstName":null,"lastName":null,"email":"${email}","imageUrl":null,"activated":true,"langKey":"en","createdBy":"system","createdDate":"2021-05-17T08:30:19.074692Z","lastModifiedBy":"system","lastModifiedDate":"2021-05-17T08:30:19.074692Z","authorities":["ROLE_USER"]}"""
          )
        )
        .asJson
    )
    .pause(2)
    .exec(
      http("login")
        .post("/api/authenticate")
        .headers(headers_11)
        .body(
          StringBody(
            """{"username":"${user}","password":"${user}","rememberMe":false}"""
          )
        )
        .asJson
        .check(jsonPath("$.id_token").saveAs("token"))
    )
    .pause(2)
    .exec(
      http("click game section")
        .get("/services/rankingservice/api/points-farmeds/userSum")
        .headers(headers_20 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_22")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}"))),
          http("request_23")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
            )
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points in two modes")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_24 + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/smalltest/0024_request.json"))
        .resources(
          http("request_25")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}"))),
          http("request_26")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951442915"
            )
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}"))),
          http("request_27")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
            )
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}"))),
          http("request_28")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check ranking in two modes")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_24 + ("authorization" -> ("Bearer ${token}")))
        .body(RawFileBody("/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/smalltest/0029_request.json"))
        .resources(
          http("request_30")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}"))),
          http("request_31")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621951450330"
            )
            .headers(headers_20 + ("authorization" -> ("Bearer ${token}")))
        )
    )

  setUp(scn.inject(atOnceUsers(2000))).protocols(httpProtocol)
}

