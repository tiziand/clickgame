import scala.concurrent.duration._
import scala.util.Random

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class LargeTest extends Simulation {

  val httpProtocol = http
    .baseUrl("http://localhost:9000")
    .inferHtmlResources()
    .userAgentHeader(
      "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"
    )

  val headers_11 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_13 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYyNDM1MDA1MX0.mJUKINOHM9aYvBopm2kEqNfo0KCwcA2xdrgv839pjmEx3c2k1pFmpnva6pQVCwC_Q5jBucwQrtyHsvQNboSJpQ",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_12 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYyMTc3ODM5Mn0.GQqiDRag1mmhIvW4xOSRPhjGYq_shjt4s64f6VIEKbOPCL3D6tOcfm8eDFg4NekGf8YqOnoa8IOeIBMYyIpTfA",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_17 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Content-Type" -> "application/json;charset=UTF-8",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMTc3NDUxMX0.0j-le62FcIkVyMpLcjIu0Hv20VQWHw9EijFZn5QNX94WBvrL8rREvvTJspIURWnVtLz3wdTBSiTKQIKdRFZkAQ",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  val headers_87 = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Accept-Language" -> "en-US,en;q=0.9",
    "Origin" -> "http://localhost:9000",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "authorization" -> "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTYyMTc3NDUxMX0.0j-le62FcIkVyMpLcjIu0Hv20VQWHw9EijFZn5QNX94WBvrL8rREvvTJspIURWnVtLz3wdTBSiTKQIKdRFZkAQ",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90""",
    "sec-ch-ua-mobile" -> "?0"
  )

  //https://stackoverflow.com/questions/34665566/generate-random-string-letter-in-scala/34665585
  val alpha = "abcdefghijklmnopqrstuvwxyz123456789"
  def randStr(n: Int) =
    (1 to n).map(_ => alpha(Random.nextInt(alpha.length))).mkString

  //https://stackoverflow.com/questions/35348845/simple-feeder-in-gatling-without-using-a-csv-file
  def randUserInfo() =
    Map(
      "email" -> (randStr(8) + "@test.com"),
      "user" -> randStr(8)
    )

  val feeder = Iterator.continually(randUserInfo())

  val chain_0 = scenario("KafkaLarge")
    .feed(feeder)
    .exec(
      http("register")
        .post("/api/register")
        .headers(headers_11)
        .body(
          StringBody(
            """{"login":"${user}","email":"${email}","password":"${user}","langKey":"en"}"""
          )
        )
        .asJson
    )
    .exec(
      http("getuserid")
        .get("/api/admin/users/${user}")
        .headers(headers_13)
        .check(jsonPath("$.id").saveAs("id"))
    )
    .pause(2)
    .exec(
      http("activate")
        .put("/api/admin/users")
        .headers(headers_13)
        .body(
          StringBody(
            """{"id":${id},"login":"${user}","firstName":null,"lastName":null,"email":"${email}","imageUrl":null,"activated":true,"langKey":"en","createdBy":"system","createdDate":"2021-05-17T08:30:19.074692Z","lastModifiedBy":"system","lastModifiedDate":"2021-05-17T08:30:19.074692Z","authorities":["ROLE_USER"]}"""
          )
        )
        .asJson
    )
    .pause(2)
    .exec(
      http("login")
        .post("/api/authenticate")
        .headers(headers_11)
        .body(
          StringBody(
            """{"username":"${user}","password":"${user}","rememberMe":false}"""
          )
        )
        .asJson
        .check(jsonPath("$.id_token").saveAs("token"))
    )
    .pause(2)
    .exec(
      http("farm points infinite categorie")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0017_request.json"
          )
        )
        .resources(
          http("request_18")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_19")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688118751"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_20")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_21")
            .get("/services/mtxstoreservice/api/button-color-mtxes/1")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_22")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_23")
            .get("/services/mtxstoreservice/api/button-color-mtxes/1")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points 6 categorie")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0024_request.json"
          )
        )
        .resources(
          http("request_25")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688126230"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_26")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_27")
            .get("/services/mtxstoreservice/api/button-color-mtxes/1")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_28")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_29")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/4"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_30")
            .get("/services/mtxstoreservice/api/button-color-mtxes/1")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points 4 categorie")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0031_request.json"
          )
        )
        .resources(
          http("request_32")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688131623"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_33")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("play in 2 categorie")
        .get("/services/mtxstoreservice/api/button-color-mtxes/1")
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_35")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/2"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_36")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_37")
            .get("/services/mtxstoreservice/api/button-color-mtxes/1")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("play in 2 categorie")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0038_request.json"
          )
        )
        .resources(
          http("request_39")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688135805"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_40")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check rank")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingSum?page=0&size=5&cacheBuster=1621688138707"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("check rank")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingMax/6?page=0&size=5&cacheBuster=1621688141878"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("check rank")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingMax/4?page=0&size=5&cacheBuster=1621688143060"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("check rank")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingMax/2?page=0&size=5&cacheBuster=1621688144217"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("check store")
        .get(
          "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621688155351"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_46")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688155352"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_47")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_48")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688155352"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check points options")
        .get(
          "/services/paymentservice/api/point-options?cacheBuster=1621688156743"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("buy points")
        .post("/services/paymentservice/api/payment-infos")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0050_request.json"
          )
        )
    )
    .pause(2)
    .exec(
      http("buy points")
        .post("/services/paymentservice/api/payment-infos")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0051_request.json"
          )
        )
    )
    .pause(2)
    .exec(
      http("check store")
        .get(
          "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621688167203"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_53")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688167204"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_54")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_55")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688167207"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("add mtx to basket")
        .post("/services/basketservice/api/basket-items")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0056_request.json"
          )
        )
        .resources(
          http("request_57")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688170386"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_58")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0058_request.json"
              )
            ),
          http("request_59")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688171275"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_60")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0060_request.json"
              )
            )
            .check(status.is(400)),
          http("request_61")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0061_request.json"
              )
            )
            .check(status.is(400)),
          http("request_62")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0062_request.json"
              )
            ),
          http("request_63")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688173601"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_64")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0064_request.json"
              )
            )
            .check(status.is(400)),
          http("request_65")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0065_request.json"
              )
            )
            .check(status.is(400))
        )
    )
    .pause(2)
    .exec(
      http("add mtx to basket")
        .post("/services/basketservice/api/basket-items")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0066_request.json"
          )
        )
        .check(status.is(400))
    )
    .pause(2)
    .exec(
      http("add mtx to basket")
        .post("/services/basketservice/api/basket-items")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0067_request.json"
          )
        )
        .check(status.is(400))
    )
    .pause(2)
    .exec(
      http("add mtx to basket")
        .post("/services/basketservice/api/basket-items")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0068_request.json"
          )
        )
        .resources(
          http("request_69")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0069_request.json"
              )
            )
            .check(status.is(400))
        )
        .check(status.is(400))
    )
    .pause(2)
    .exec(
      http("check basket")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688186992"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_71")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688186994"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_72")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_73")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_74")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("purchase basket")
        .get(
          "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_76")
            .get(
              "/services/basketservice/api/basket-items/purchase?cacheBuster=1621688190016"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_77")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check statstics")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688193825"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_79")
            .get(
              "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-22"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_80")
            .get(
              "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-22"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_81")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-22"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_82")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-22?cacheBuster=1621688193825"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check inventory")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688200138"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_84")
            .get(
              "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621688200139"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_85")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_86")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .put(
          "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/3/0"
        )
        .headers(headers_87 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .put(
          "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/3/6"
        )
        .headers(headers_87 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("equipp mtx")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/4"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_91")
            .put(
              "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/3/4"
            )
            .headers(headers_87 + ("authorization" -> ("Bearer ${token}"))),
          http("request_92")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/2"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_93")
            .put(
              "/services/inventoryservice/api/equipped-button-color-mtxes/equippButtonColorMtx/2/2"
            )
            .headers(headers_87 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check mtx store")
        .get("/services/mtxstoreservice/api/button-color-mtxes/2")
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_95")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_96")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_97")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_98")
            .get("/services/mtxstoreservice/api/button-color-mtxes/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_99")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0100_request.json"
          )
        )
        .resources(
          http("request_101")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688215856"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_102")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_103")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_104")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/6"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_105")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/6")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_106")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_108")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_109")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/2"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_110")
            .get("/services/mtxstoreservice/api/button-color-mtxes/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0111_request.json"
          )
        )
        .resources(
          http("request_112")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688221534"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_113")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("buy mtx points")
        .get(
          "/services/paymentservice/api/point-options?cacheBuster=1621688224205"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("check rank")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingSum?page=0&size=5&cacheBuster=1621688226990"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("check rank")
        .get(
          "/services/rankingservice/api/points-farmeds/rankingMax/2?page=0&size=5&cacheBuster=1621688228382"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("check mtx store")
        .get(
          "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621688230273"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_118")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_119")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688230277"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_120")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688230275"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check inventory")
        .get("/services/mtxstoreservice/api/button-color-mtxes/2")
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_122")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_123")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/0"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_124")
            .get("/services/rankingservice/api/points-farmeds/userSum")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_125")
            .get("/services/mtxstoreservice/api/button-color-mtxes/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_126")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check inventory")
        .get(
          "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/4"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_128")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_129")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_130")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0131_request.json"
          )
        )
        .resources(
          http("request_132")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688243069"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_133")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/4")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_134")
            .get(
              "/services/inventoryservice/api/equipped-button-color-mtxes/userEquippedButtonColorMtx/2"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_135")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_136")
            .get("/services/mtxstoreservice/api/button-color-mtxes/3")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_137")
            .get("/services/mtxstoreservice/api/button-color-mtxes/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0138_request.json"
          )
        )
        .resources(
          http("request_139")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688246740"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_140")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("farm points")
        .post("/services/rankingservice/api/points-farmeds")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0141_request.json"
          )
        )
        .resources(
          http("request_142")
            .get(
              "/services/rankingservice/api/points-farmeds?cacheBuster=1621688251399"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_143")
            .get("/services/rankingservice/api/points-farmeds/userMaxTime/2")
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check mtx store")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688255365"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_145")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688255366"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_146")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_147")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_148")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check store")
        .get(
          "/services/inventoryservice/api/owned-button-color-mtxes/userOwnedButtonColorMtx?cacheBuster=1621688256672"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_150")
            .get(
              "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688256674"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_151")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_152")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688256676"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("add mtx")
        .post("/services/basketservice/api/basket-items")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0153_request.json"
          )
        )
        .resources(
          http("request_154")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688263567"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_155")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0155_request.json"
              )
            )
            .check(status.is(400)),
          http("request_156")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0156_request.json"
              )
            )
            .check(status.is(400)),
          http("request_157")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0157_request.json"
              )
            )
            .check(status.is(400)),
          http("request_158")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0158_request.json"
              )
            )
            .check(status.is(400)),
          http("request_159")
            .post("/services/basketservice/api/basket-items")
            .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
            .body(
              RawFileBody(
                "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0159_request.json"
              )
            )
            .check(status.is(400))
        )
    )
    .pause(2)
    .exec(
      http("check basket")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688272198"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_161")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688272200"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_162")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_163")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_164")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("buy points")
        .get(
          "/services/paymentservice/api/point-options?cacheBuster=1621688274983"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
    )
    .pause(2)
    .exec(
      http("buy points")
        .post("/services/paymentservice/api/payment-infos")
        .headers(headers_17 + ("authorization" -> ("Bearer ${token}")))
        .body(
          RawFileBody(
            "/home/tizian/Documents/gatling/gatling-charts-highcharts-bundle-3.5.1/user-files/resources/largetest/0166_request.json"
          )
        )
    )
    .pause(2)
    .exec(
      http("go to mtx store")
        .get(
          "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688279135"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_168")
            .get(
              "/services/basketservice/api/basket-items/userBasket?cacheBuster=1621688279137"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_169")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_170")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_171")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("buy basket")
        .get(
          "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_173")
            .get(
              "/services/basketservice/api/basket-items/purchase?cacheBuster=1621688280539"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_174")
            .get(
              "/services/inventoryservice/api/owned-mtx-points/userOwnedMtxPoints"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)

  val chain_1 = exec(
    http("check stats")
      .get(
        "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-22"
      )
      .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
      .resources(
        http("request_176")
          .get(
            "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-22"
          )
          .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
        http("request_177")
          .get(
            "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-22"
          )
          .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
        http("request_178")
          .get(
            "/services/mtxstoreservice/api/button-color-mtxes?cacheBuster=1621688282868"
          )
          .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
        http("request_179")
          .get(
            "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-22?cacheBuster=1621688282867"
          )
          .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
      )
  )
    .pause(2)
    .exec(
      http("check stats")
        .get(
          "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-21"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_181")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-21"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_182")
            .get(
              "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-21"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_183")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-21?cacheBuster=1621688287412"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check stats")
        .get(
          "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-22"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_185")
            .get(
              "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-22"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_186")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-22"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_187")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-22?cacheBuster=1621688289507"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )
    .pause(2)
    .exec(
      http("check stats")
        .get(
          "/services/statisticservice/api/purchases-mtx-points/moneySpentSum/2020-01-01/2021-05-21"
        )
        .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        .resources(
          http("request_189")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/purchasesSum/2020-01-01/2021-05-21"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_190")
            .get(
              "/services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList/2020-01-01/2021-05-21?cacheBuster=1621688291564"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}"))),
          http("request_191")
            .get(
              "/services/statisticservice/api/click-per-mins/clicksSum/2020-01-01/2021-05-21"
            )
            .headers(headers_12 + ("authorization" -> ("Bearer ${token}")))
        )
    )

  val scn = scenario("LargeTest").exec(chain_0, chain_1)

  setUp(scn.inject(atOnceUsers(500))).protocols(httpProtocol)
}

