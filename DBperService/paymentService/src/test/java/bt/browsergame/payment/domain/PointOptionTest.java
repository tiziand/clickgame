package bt.browsergame.payment.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.payment.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PointOptionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PointOption.class);
        PointOption pointOption1 = new PointOption();
        pointOption1.setId(1L);
        PointOption pointOption2 = new PointOption();
        pointOption2.setId(pointOption1.getId());
        assertThat(pointOption1).isEqualTo(pointOption2);
        pointOption2.setId(2L);
        assertThat(pointOption1).isNotEqualTo(pointOption2);
        pointOption1.setId(null);
        assertThat(pointOption1).isNotEqualTo(pointOption2);
    }
}
