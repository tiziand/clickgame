package bt.browsergame.payment.foreignClients;

import bt.browsergame.payment.foreignEntities.OwnedMtxPoints;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class InventoryService {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    private final Logger log = LoggerFactory.getLogger(InventoryService.class);

    public OwnedMtxPoints addMtxPoints(OwnedMtxPoints ownedMtxPoints) {
        List<ServiceInstance> instances = discoveryClient.getInstances("inventoryService");
        log.debug(instances.get(0).getUri().toString());

        ResponseEntity<OwnedMtxPoints> resp = restTemplate.postForEntity(
            instances.get(0).getUri().toString() + "/api/owned-mtx-points",
            ownedMtxPoints,
            OwnedMtxPoints.class
        );
        return resp.getBody();
    }
}
