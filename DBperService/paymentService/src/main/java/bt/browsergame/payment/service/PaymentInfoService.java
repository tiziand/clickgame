package bt.browsergame.payment.service;

import bt.browsergame.payment.domain.PaymentInfo;
import bt.browsergame.payment.foreignClients.InventoryService;
import bt.browsergame.payment.foreignClients.StatisticService;
import bt.browsergame.payment.foreignEntities.OwnedMtxPoints;
import bt.browsergame.payment.foreignEntities.PurchasesMtxPoints;
import bt.browsergame.payment.repository.PaymentInfoRepository;
import bt.browsergame.payment.security.SecurityUtils;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PaymentInfo}.
 */
@Service
@Transactional
public class PaymentInfoService {

    private final Logger log = LoggerFactory.getLogger(PaymentInfoService.class);

    private final PaymentInfoRepository paymentInfoRepository;

    private final InventoryService inventoryService;

    private final StatisticService statisticService;

    @Autowired
    public PaymentInfoService(PaymentInfoRepository paymentInfoRepository, InventoryService inventoryService,
            StatisticService statisticService) {
        this.paymentInfoRepository = paymentInfoRepository;
        this.inventoryService = inventoryService;
        this.statisticService = statisticService;
    }

    /**
     * Save a paymentInfo.
     *
     * @param paymentInfo the entity to save.
     * @return the persisted entity.
     */
    public PaymentInfo save(PaymentInfo paymentInfo) {
        log.debug("Request to save PaymentInfo : {}", paymentInfo);
        paymentInfo.setUserName(SecurityUtils.getCurrentUserLogin().get());

        // add mtx points to inventory
        OwnedMtxPoints ownedMtxPoints = new OwnedMtxPoints();
        ownedMtxPoints.setMtxPoints(paymentInfo.getPointOption().getPointAmount());
        ownedMtxPoints.setUserName(SecurityUtils.getCurrentUserLogin().get());
        inventoryService.addMtxPoints(ownedMtxPoints);

        // update purchase statistics
        PurchasesMtxPoints purchasesMtxPoints = new PurchasesMtxPoints();
        purchasesMtxPoints.setCreationTime(Instant.now());
        purchasesMtxPoints.setMoneySpent(paymentInfo.getPointOption().getPrice());
        statisticService.createPurchasesMtxPoints(purchasesMtxPoints);

        return paymentInfoRepository.save(paymentInfo);
    }

    /**
     * Partially update a paymentInfo.
     *
     * @param paymentInfo the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PaymentInfo> partialUpdate(PaymentInfo paymentInfo) {
        log.debug("Request to partially update PaymentInfo : {}", paymentInfo);

        return paymentInfoRepository.findById(paymentInfo.getId()).map(existingPaymentInfo -> {
            if (paymentInfo.getUserName() != null) {
                existingPaymentInfo.setUserName(paymentInfo.getUserName());
            }
            if (paymentInfo.getPaymentInfo() != null) {
                existingPaymentInfo.setPaymentInfo(paymentInfo.getPaymentInfo());
            }

            return existingPaymentInfo;
        }).map(paymentInfoRepository::save);
    }

    /**
     * Get all the paymentInfos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PaymentInfo> findAll() {
        log.debug("Request to get all PaymentInfos");
        return paymentInfoRepository.findAll();
    }

    /**
     * Get one paymentInfo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PaymentInfo> findOne(Long id) {
        log.debug("Request to get PaymentInfo : {}", id);
        return paymentInfoRepository.findById(id);
    }

    /**
     * Delete the paymentInfo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentInfo : {}", id);
        paymentInfoRepository.deleteById(id);
    }
}
