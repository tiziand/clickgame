package bt.browsergame.inventory.repository;

import bt.browsergame.inventory.domain.EquippedButtonColorMtx;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the EquippedButtonColorMtx entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EquippedButtonColorMtxRepository extends JpaRepository<EquippedButtonColorMtx, Long> {}
