package bt.browsergame.inventory.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SYSTEM = "system";

    private Constants() {}
}
