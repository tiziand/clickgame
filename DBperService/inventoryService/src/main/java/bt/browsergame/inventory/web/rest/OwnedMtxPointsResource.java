package bt.browsergame.inventory.web.rest;

import bt.browsergame.inventory.domain.OwnedMtxPoints;
import bt.browsergame.inventory.repository.OwnedMtxPointsRepository;
import bt.browsergame.inventory.service.OwnedMtxPointsService;
import bt.browsergame.inventory.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing
 * {@link bt.browsergame.inventory.domain.OwnedMtxPoints}.
 */
@RestController
@RequestMapping("/api")
public class OwnedMtxPointsResource {

    private final Logger log = LoggerFactory.getLogger(OwnedMtxPointsResource.class);

    private static final String ENTITY_NAME = "inventoryServiceOwnedMtxPoints";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OwnedMtxPointsService ownedMtxPointsService;

    private final OwnedMtxPointsRepository ownedMtxPointsRepository;

    public OwnedMtxPointsResource(OwnedMtxPointsService ownedMtxPointsService, OwnedMtxPointsRepository ownedMtxPointsRepository) {
        this.ownedMtxPointsService = ownedMtxPointsService;
        this.ownedMtxPointsRepository = ownedMtxPointsRepository;
    }

    /**
     * {@code POST  /owned-mtx-points} : Create a new ownedMtxPoints.
     *
     * @param ownedMtxPoints the ownedMtxPoints to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new ownedMtxPoints, or with status {@code 400 (Bad Request)}
     *         if the ownedMtxPoints has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/owned-mtx-points")
    public ResponseEntity<OwnedMtxPoints> createOwnedMtxPoints(@Valid @RequestBody OwnedMtxPoints ownedMtxPoints)
        throws URISyntaxException {
        log.debug("REST request to save OwnedMtxPoints : {}", ownedMtxPoints);
        if (ownedMtxPoints.getId() != null) {
            throw new BadRequestAlertException("A new ownedMtxPoints cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OwnedMtxPoints result = ownedMtxPointsService.save(ownedMtxPoints);
        return ResponseEntity
            .created(new URI("/api/owned-mtx-points/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /owned-mtx-points/:id} : Updates an existing ownedMtxPoints.
     *
     * @param id             the id of the ownedMtxPoints to save.
     * @param ownedMtxPoints the ownedMtxPoints to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated ownedMtxPoints, or with status {@code 400 (Bad Request)}
     *         if the ownedMtxPoints is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the ownedMtxPoints couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/owned-mtx-points/{id}")
    public ResponseEntity<OwnedMtxPoints> updateOwnedMtxPoints(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OwnedMtxPoints ownedMtxPoints
    ) throws URISyntaxException {
        log.debug("REST request to update OwnedMtxPoints : {}, {}", id, ownedMtxPoints);
        if (ownedMtxPoints.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ownedMtxPoints.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ownedMtxPointsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OwnedMtxPoints result = ownedMtxPointsService.save(ownedMtxPoints);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ownedMtxPoints.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /owned-mtx-points/:id} : Partial updates given fields of an
     * existing ownedMtxPoints, field will ignore if it is null
     *
     * @param id             the id of the ownedMtxPoints to save.
     * @param ownedMtxPoints the ownedMtxPoints to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated ownedMtxPoints, or with status {@code 400 (Bad Request)}
     *         if the ownedMtxPoints is not valid, or with status
     *         {@code 404 (Not Found)} if the ownedMtxPoints is not found, or with
     *         status {@code 500 (Internal Server Error)} if the ownedMtxPoints
     *         couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/owned-mtx-points/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<OwnedMtxPoints> partialUpdateOwnedMtxPoints(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody OwnedMtxPoints ownedMtxPoints
    ) throws URISyntaxException {
        log.debug("REST request to partial update OwnedMtxPoints partially : {}, {}", id, ownedMtxPoints);
        if (ownedMtxPoints.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ownedMtxPoints.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ownedMtxPointsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OwnedMtxPoints> result = ownedMtxPointsService.partialUpdate(ownedMtxPoints);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ownedMtxPoints.getId().toString())
        );
    }

    /**
     * {@code GET  /owned-mtx-points} : get all the ownedMtxPoints.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of ownedMtxPoints in body.
     */
    @GetMapping("/owned-mtx-points")
    public List<OwnedMtxPoints> getAllOwnedMtxPoints() {
        log.debug("REST request to get all OwnedMtxPoints");
        return ownedMtxPointsService.findAll();
    }

    /**
     * {@code GET  /owned-mtx-points/:id} : get the "id" ownedMtxPoints.
     *
     * @param id the id of the ownedMtxPoints to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the ownedMtxPoints, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/owned-mtx-points/{id}")
    public ResponseEntity<OwnedMtxPoints> getOwnedMtxPoints(@PathVariable Long id) {
        log.debug("REST request to get OwnedMtxPoints : {}", id);
        Optional<OwnedMtxPoints> ownedMtxPoints = ownedMtxPointsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ownedMtxPoints);
    }

    /**
     * {@code DELETE  /owned-mtx-points/:id} : delete the "id" ownedMtxPoints.
     *
     * @param id the id of the ownedMtxPoints to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/owned-mtx-points/{id}")
    public ResponseEntity<Void> deleteOwnedMtxPoints(@PathVariable Long id) {
        log.debug("REST request to delete OwnedMtxPoints : {}", id);
        ownedMtxPointsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /owned-mtx-points/userOwnedMtxPoints} : get the "userName"
     * OwnedMtxPoints.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the OwnedMtxPoints
     */
    @GetMapping("/owned-mtx-points/userOwnedMtxPoints")
    public ResponseEntity<OwnedMtxPoints> getOwnedMtxPointsOfUser() {
        return ResponseEntity.ok(ownedMtxPointsService.getOwnedMtxPointsOfUser());
    }

    /**
     * {@code GET /owned-mtx-points/subtractMtxPoints/:points } : subtract owned MTX
     * points.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with a
     *         boolean that implies the success of the subtract operation.
     */
    @GetMapping("/owned-mtx-points/subtractMtxPoints/{points}")
    public ResponseEntity<Boolean> subtractMtxPoints(@PathVariable Integer points) {
        return ResponseEntity.ok(ownedMtxPointsService.subtractMtxPoints(points));
    }
}
