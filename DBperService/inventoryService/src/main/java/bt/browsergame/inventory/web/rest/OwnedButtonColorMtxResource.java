package bt.browsergame.inventory.web.rest;

import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.repository.OwnedButtonColorMtxRepository;
import bt.browsergame.inventory.service.OwnedButtonColorMtxService;
import bt.browsergame.inventory.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing
 * {@link bt.browsergame.inventory.domain.OwnedButtonColorMtx}.
 */
@RestController
@RequestMapping("/api")
public class OwnedButtonColorMtxResource {

    private final Logger log = LoggerFactory.getLogger(OwnedButtonColorMtxResource.class);

    private static final String ENTITY_NAME = "inventoryServiceOwnedButtonColorMtx";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OwnedButtonColorMtxService ownedButtonColorMtxService;

    private final OwnedButtonColorMtxRepository ownedButtonColorMtxRepository;

    public OwnedButtonColorMtxResource(
        OwnedButtonColorMtxService ownedButtonColorMtxService,
        OwnedButtonColorMtxRepository ownedButtonColorMtxRepository
    ) {
        this.ownedButtonColorMtxService = ownedButtonColorMtxService;
        this.ownedButtonColorMtxRepository = ownedButtonColorMtxRepository;
    }

    /**
     * {@code POST  /owned-button-color-mtxes} : Create a new ownedButtonColorMtx.
     *
     * @param ownedButtonColorMtx the ownedButtonColorMtx to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new ownedButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the ownedButtonColorMtx has already an
     *         ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/owned-button-color-mtxes")
    public ResponseEntity<OwnedButtonColorMtx> createOwnedButtonColorMtx(@Valid @RequestBody OwnedButtonColorMtx ownedButtonColorMtx)
        throws URISyntaxException {
        log.debug("REST request to save OwnedButtonColorMtx : {}", ownedButtonColorMtx);
        if (ownedButtonColorMtx.getId() != null) {
            throw new BadRequestAlertException("A new ownedButtonColorMtx cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OwnedButtonColorMtx result = ownedButtonColorMtxService.save(ownedButtonColorMtx);
        return ResponseEntity
            .created(new URI("/api/owned-button-color-mtxes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /owned-button-color-mtxes/:id} : Updates an existing
     * ownedButtonColorMtx.
     *
     * @param id                  the id of the ownedButtonColorMtx to save.
     * @param ownedButtonColorMtx the ownedButtonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated ownedButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the ownedButtonColorMtx is not valid, or
     *         with status {@code 500 (Internal Server Error)} if the
     *         ownedButtonColorMtx couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/owned-button-color-mtxes/{id}")
    public ResponseEntity<OwnedButtonColorMtx> updateOwnedButtonColorMtx(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OwnedButtonColorMtx ownedButtonColorMtx
    ) throws URISyntaxException {
        log.debug("REST request to update OwnedButtonColorMtx : {}, {}", id, ownedButtonColorMtx);
        if (ownedButtonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ownedButtonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ownedButtonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OwnedButtonColorMtx result = ownedButtonColorMtxService.save(ownedButtonColorMtx);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ownedButtonColorMtx.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /owned-button-color-mtxes/:id} : Partial updates given fields
     * of an existing ownedButtonColorMtx, field will ignore if it is null
     *
     * @param id                  the id of the ownedButtonColorMtx to save.
     * @param ownedButtonColorMtx the ownedButtonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated ownedButtonColorMtx, or with status
     *         {@code 400 (Bad Request)} if the ownedButtonColorMtx is not valid, or
     *         with status {@code 404 (Not Found)} if the ownedButtonColorMtx is not
     *         found, or with status {@code 500 (Internal Server Error)} if the
     *         ownedButtonColorMtx couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/owned-button-color-mtxes/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<OwnedButtonColorMtx> partialUpdateOwnedButtonColorMtx(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody OwnedButtonColorMtx ownedButtonColorMtx
    ) throws URISyntaxException {
        log.debug("REST request to partial update OwnedButtonColorMtx partially : {}, {}", id, ownedButtonColorMtx);
        if (ownedButtonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ownedButtonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ownedButtonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OwnedButtonColorMtx> result = ownedButtonColorMtxService.partialUpdate(ownedButtonColorMtx);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ownedButtonColorMtx.getId().toString())
        );
    }

    /**
     * {@code GET  /owned-button-color-mtxes} : get all the ownedButtonColorMtxes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of ownedButtonColorMtxes in body.
     */
    @GetMapping("/owned-button-color-mtxes")
    public List<OwnedButtonColorMtx> getAllOwnedButtonColorMtxes() {
        log.debug("REST request to get all OwnedButtonColorMtxes");
        return ownedButtonColorMtxService.findAll();
    }

    /**
     * {@code GET  /owned-button-color-mtxes/:id} : get the "id"
     * ownedButtonColorMtx.
     *
     * @param id the id of the ownedButtonColorMtx to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the ownedButtonColorMtx, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/owned-button-color-mtxes/{id}")
    public ResponseEntity<OwnedButtonColorMtx> getOwnedButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to get OwnedButtonColorMtx : {}", id);
        Optional<OwnedButtonColorMtx> ownedButtonColorMtx = ownedButtonColorMtxService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ownedButtonColorMtx);
    }

    /**
     * {@code DELETE  /owned-button-color-mtxes/:id} : delete the "id"
     * ownedButtonColorMtx.
     *
     * @param id the id of the ownedButtonColorMtx to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/owned-button-color-mtxes/{id}")
    public ResponseEntity<Void> deleteOwnedButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to delete OwnedButtonColorMtx : {}", id);
        ownedButtonColorMtxService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /userEquippedButtonColorMtx} : get the "username" ownedMtx.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the gameData
     */
    @GetMapping("/owned-button-color-mtxes/userOwnedButtonColorMtx")
    public List<OwnedButtonColorMtx> getUserOwnedButtonColorMtx() {
        return ownedButtonColorMtxService.getUserOwnedButtonColorMtx();
    }
}
