package bt.browsergame.inventory.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.inventory.IntegrationTest;
import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.repository.OwnedButtonColorMtxRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OwnedButtonColorMtxResource} REST
 * controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OwnedButtonColorMtxResourceIT {

    private static final String DEFAULT_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USER_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_MTX_ID = 1L;
    private static final Long UPDATED_MTX_ID = 2L;

    private static final String ENTITY_API_URL = "/api/owned-button-color-mtxes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OwnedButtonColorMtxRepository ownedButtonColorMtxRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOwnedButtonColorMtxMockMvc;

    private OwnedButtonColorMtx ownedButtonColorMtx;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static OwnedButtonColorMtx createEntity(EntityManager em) {
        OwnedButtonColorMtx ownedButtonColorMtx = new OwnedButtonColorMtx().userName(DEFAULT_USER_NAME)
                .mtxId(DEFAULT_MTX_ID);
        return ownedButtonColorMtx;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static OwnedButtonColorMtx createUpdatedEntity(EntityManager em) {
        OwnedButtonColorMtx ownedButtonColorMtx = new OwnedButtonColorMtx().userName(UPDATED_USER_NAME)
                .mtxId(UPDATED_MTX_ID);
        return ownedButtonColorMtx;
    }

    @BeforeEach
    public void initTest() {
        ownedButtonColorMtx = createEntity(em);
    }

    @Test
    @Transactional
    void createOwnedButtonColorMtx() throws Exception {
        int databaseSizeBeforeCreate = ownedButtonColorMtxRepository.findAll().size();
        // Create the OwnedButtonColorMtx
        restOwnedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isCreated());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeCreate + 1);
        OwnedButtonColorMtx testOwnedButtonColorMtx = ownedButtonColorMtxList.get(ownedButtonColorMtxList.size() - 1);
        assertThat(testOwnedButtonColorMtx.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testOwnedButtonColorMtx.getMtxId()).isEqualTo(DEFAULT_MTX_ID);
    }

    @Test
    @Transactional
    void createOwnedButtonColorMtxWithExistingId() throws Exception {
        // Create the OwnedButtonColorMtx with an existing ID
        ownedButtonColorMtx.setId(1L);

        int databaseSizeBeforeCreate = ownedButtonColorMtxRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOwnedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkUserNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = ownedButtonColorMtxRepository.findAll().size();
        // set the field null
        ownedButtonColorMtx.setUserName(null);

        // Create the OwnedButtonColorMtx, which fails.

        restOwnedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMtxIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = ownedButtonColorMtxRepository.findAll().size();
        // set the field null
        ownedButtonColorMtx.setMtxId(null);

        // Create the OwnedButtonColorMtx, which fails.

        restOwnedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllOwnedButtonColorMtxes() throws Exception {
        // Initialize the database
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        // Get all the ownedButtonColorMtxList
        restOwnedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL + "?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(ownedButtonColorMtx.getId().intValue())))
                .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
                .andExpect(jsonPath("$.[*].mtxId").value(hasItem(DEFAULT_MTX_ID.intValue())));
    }

    @Test
    @Transactional
    void getOwnedButtonColorMtx() throws Exception {
        // Initialize the database
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        // Get the ownedButtonColorMtx
        restOwnedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL_ID, ownedButtonColorMtx.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(ownedButtonColorMtx.getId().intValue()))
                .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
                .andExpect(jsonPath("$.mtxId").value(DEFAULT_MTX_ID.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingOwnedButtonColorMtx() throws Exception {
        // Get the ownedButtonColorMtx
        restOwnedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOwnedButtonColorMtx() throws Exception {
        // Initialize the database
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();

        // Update the ownedButtonColorMtx
        OwnedButtonColorMtx updatedOwnedButtonColorMtx = ownedButtonColorMtxRepository
                .findById(ownedButtonColorMtx.getId()).get();
        // Disconnect from session so that the updates on updatedOwnedButtonColorMtx are
        // not directly saved in db
        em.detach(updatedOwnedButtonColorMtx);
        updatedOwnedButtonColorMtx.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID);

        restOwnedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL_ID, updatedOwnedButtonColorMtx.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(updatedOwnedButtonColorMtx)))
                .andExpect(status().isOk());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        OwnedButtonColorMtx testOwnedButtonColorMtx = ownedButtonColorMtxList.get(ownedButtonColorMtxList.size() - 1);
        assertThat(testOwnedButtonColorMtx.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testOwnedButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
    }

    @Test
    @Transactional
    void putNonExistingOwnedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();
        ownedButtonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOwnedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL_ID, ownedButtonColorMtx.getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOwnedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();
        ownedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOwnedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL_ID, count.incrementAndGet()).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOwnedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();
        ownedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOwnedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOwnedButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();

        // Update the ownedButtonColorMtx using partial update
        OwnedButtonColorMtx partialUpdatedOwnedButtonColorMtx = new OwnedButtonColorMtx();
        partialUpdatedOwnedButtonColorMtx.setId(ownedButtonColorMtx.getId());

        partialUpdatedOwnedButtonColorMtx.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID);

        restOwnedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, partialUpdatedOwnedButtonColorMtx.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOwnedButtonColorMtx)))
                .andExpect(status().isOk());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        OwnedButtonColorMtx testOwnedButtonColorMtx = ownedButtonColorMtxList.get(ownedButtonColorMtxList.size() - 1);
        assertThat(testOwnedButtonColorMtx.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testOwnedButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
    }

    @Test
    @Transactional
    void fullUpdateOwnedButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();

        // Update the ownedButtonColorMtx using partial update
        OwnedButtonColorMtx partialUpdatedOwnedButtonColorMtx = new OwnedButtonColorMtx();
        partialUpdatedOwnedButtonColorMtx.setId(ownedButtonColorMtx.getId());

        partialUpdatedOwnedButtonColorMtx.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID);

        restOwnedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, partialUpdatedOwnedButtonColorMtx.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOwnedButtonColorMtx)))
                .andExpect(status().isOk());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        OwnedButtonColorMtx testOwnedButtonColorMtx = ownedButtonColorMtxList.get(ownedButtonColorMtxList.size() - 1);
        assertThat(testOwnedButtonColorMtx.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testOwnedButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
    }

    @Test
    @Transactional
    void patchNonExistingOwnedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();
        ownedButtonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOwnedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, ownedButtonColorMtx.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOwnedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();
        ownedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOwnedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, count.incrementAndGet()).contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOwnedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = ownedButtonColorMtxRepository.findAll().size();
        ownedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOwnedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(ownedButtonColorMtx)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the OwnedButtonColorMtx in the database
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOwnedButtonColorMtx() throws Exception {
        // Initialize the database
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        int databaseSizeBeforeDelete = ownedButtonColorMtxRepository.findAll().size();

        // Delete the ownedButtonColorMtx
        restOwnedButtonColorMtxMockMvc
                .perform(delete(ENTITY_API_URL_ID, ownedButtonColorMtx.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OwnedButtonColorMtx> ownedButtonColorMtxList = ownedButtonColorMtxRepository.findAll();
        assertThat(ownedButtonColorMtxList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    void getUserOwnedButtonColorMtx() throws Exception {
        // Initialize the database
        OwnedButtonColorMtx ownedButtonColorMtx = new OwnedButtonColorMtx().mtxId(1L).userName("user");
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);
        ownedButtonColorMtx = new OwnedButtonColorMtx().mtxId(2L).userName("john");
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        // Get all the ownedButtonColorMtxList of user
        restOwnedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL + "/userOwnedButtonColorMtx"))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].userName").value(hasItem("user")))
                .andExpect(jsonPath("$.[*].mtxId").value(hasItem(1)))
                .andExpect(jsonPath("$.[*].userName").value(not(hasItem("john"))))
                .andExpect(jsonPath("$.[*].mtxId").value(not(hasItem(2))));

    }
}
