package bt.browsergame.inventory.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.inventory.IntegrationTest;
import bt.browsergame.inventory.domain.EquippedButtonColorMtx;
import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.foreignClients.MtxStoreService;
import bt.browsergame.inventory.repository.EquippedButtonColorMtxRepository;
import bt.browsergame.inventory.repository.OwnedButtonColorMtxRepository;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EquippedButtonColorMtxResource} REST
 * controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EquippedButtonColorMtxResourceIT {

    private static final String DEFAULT_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_USER_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_MTX_ID = 1L;
    private static final Long UPDATED_MTX_ID = 2L;

    private static final Double DEFAULT_TIME_SLOT = 1D;
    private static final Double UPDATED_TIME_SLOT = 2D;

    private static final String ENTITY_API_URL = "/api/equipped-button-color-mtxes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Mock
    private MtxStoreService mtxStoreService;

    @Autowired
    private EquippedButtonColorMtxRepository equippedButtonColorMtxRepository;

    @Autowired
    private OwnedButtonColorMtxRepository ownedButtonColorMtxRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEquippedButtonColorMtxMockMvc;

    private EquippedButtonColorMtx equippedButtonColorMtx;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static EquippedButtonColorMtx createEntity(EntityManager em) {
        EquippedButtonColorMtx equippedButtonColorMtx = new EquippedButtonColorMtx().userName(DEFAULT_USER_NAME)
                .mtxId(DEFAULT_MTX_ID).timeSlot(DEFAULT_TIME_SLOT);
        return equippedButtonColorMtx;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static EquippedButtonColorMtx createUpdatedEntity(EntityManager em) {
        EquippedButtonColorMtx equippedButtonColorMtx = new EquippedButtonColorMtx().userName(UPDATED_USER_NAME)
                .mtxId(UPDATED_MTX_ID).timeSlot(UPDATED_TIME_SLOT);
        return equippedButtonColorMtx;
    }

    @BeforeEach
    public void initTest() {
        equippedButtonColorMtx = createEntity(em);
    }

    @Test
    @Transactional
    void createEquippedButtonColorMtx() throws Exception {
        int databaseSizeBeforeCreate = equippedButtonColorMtxRepository.findAll().size();
        // Create the EquippedButtonColorMtx
        restEquippedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isCreated());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeCreate + 1);
        EquippedButtonColorMtx testEquippedButtonColorMtx = equippedButtonColorMtxList
                .get(equippedButtonColorMtxList.size() - 1);
        assertThat(testEquippedButtonColorMtx.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testEquippedButtonColorMtx.getMtxId()).isEqualTo(DEFAULT_MTX_ID);
        assertThat(testEquippedButtonColorMtx.getTimeSlot()).isEqualTo(DEFAULT_TIME_SLOT);
    }

    @Test
    @Transactional
    void createEquippedButtonColorMtxWithExistingId() throws Exception {
        // Create the EquippedButtonColorMtx with an existing ID
        equippedButtonColorMtx.setId(1L);

        int databaseSizeBeforeCreate = equippedButtonColorMtxRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEquippedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkUserNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = equippedButtonColorMtxRepository.findAll().size();
        // set the field null
        equippedButtonColorMtx.setUserName(null);

        // Create the EquippedButtonColorMtx, which fails.

        restEquippedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTimeSlotIsRequired() throws Exception {
        int databaseSizeBeforeTest = equippedButtonColorMtxRepository.findAll().size();
        // set the field null
        equippedButtonColorMtx.setTimeSlot(null);

        // Create the EquippedButtonColorMtx, which fails.

        restEquippedButtonColorMtxMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEquippedButtonColorMtxes() throws Exception {
        // Initialize the database
        equippedButtonColorMtxRepository.saveAndFlush(equippedButtonColorMtx);

        // Get all the equippedButtonColorMtxList
        restEquippedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL + "?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(equippedButtonColorMtx.getId().intValue())))
                .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
                .andExpect(jsonPath("$.[*].mtxId").value(hasItem(DEFAULT_MTX_ID.intValue())))
                .andExpect(jsonPath("$.[*].timeSlot").value(hasItem(DEFAULT_TIME_SLOT.doubleValue())));
    }

    @Test
    @Transactional
    void getEquippedButtonColorMtx() throws Exception {
        // Initialize the database
        equippedButtonColorMtxRepository.saveAndFlush(equippedButtonColorMtx);

        // Get the equippedButtonColorMtx
        restEquippedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL_ID, equippedButtonColorMtx.getId()))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(equippedButtonColorMtx.getId().intValue()))
                .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
                .andExpect(jsonPath("$.mtxId").value(DEFAULT_MTX_ID.intValue()))
                .andExpect(jsonPath("$.timeSlot").value(DEFAULT_TIME_SLOT.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingEquippedButtonColorMtx() throws Exception {
        // Get the equippedButtonColorMtx
        restEquippedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEquippedButtonColorMtx() throws Exception {
        // Initialize the database
        equippedButtonColorMtxRepository.saveAndFlush(equippedButtonColorMtx);

        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();

        // Update the equippedButtonColorMtx
        EquippedButtonColorMtx updatedEquippedButtonColorMtx = equippedButtonColorMtxRepository
                .findById(equippedButtonColorMtx.getId()).get();
        // Disconnect from session so that the updates on updatedEquippedButtonColorMtx
        // are not directly saved in db
        em.detach(updatedEquippedButtonColorMtx);
        updatedEquippedButtonColorMtx.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID).timeSlot(UPDATED_TIME_SLOT);

        restEquippedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL_ID, updatedEquippedButtonColorMtx.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(updatedEquippedButtonColorMtx)))
                .andExpect(status().isOk());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        EquippedButtonColorMtx testEquippedButtonColorMtx = equippedButtonColorMtxList
                .get(equippedButtonColorMtxList.size() - 1);
        assertThat(testEquippedButtonColorMtx.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testEquippedButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
        assertThat(testEquippedButtonColorMtx.getTimeSlot()).isEqualTo(UPDATED_TIME_SLOT);
    }

    @Test
    @Transactional
    void putNonExistingEquippedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();
        equippedButtonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEquippedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL_ID, equippedButtonColorMtx.getId()).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEquippedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();
        equippedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquippedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL_ID, count.incrementAndGet()).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEquippedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();
        equippedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquippedButtonColorMtxMockMvc
                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEquippedButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        equippedButtonColorMtxRepository.saveAndFlush(equippedButtonColorMtx);

        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();

        // Update the equippedButtonColorMtx using partial update
        EquippedButtonColorMtx partialUpdatedEquippedButtonColorMtx = new EquippedButtonColorMtx();
        partialUpdatedEquippedButtonColorMtx.setId(equippedButtonColorMtx.getId());

        partialUpdatedEquippedButtonColorMtx.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID);

        restEquippedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, partialUpdatedEquippedButtonColorMtx.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEquippedButtonColorMtx)))
                .andExpect(status().isOk());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        EquippedButtonColorMtx testEquippedButtonColorMtx = equippedButtonColorMtxList
                .get(equippedButtonColorMtxList.size() - 1);
        assertThat(testEquippedButtonColorMtx.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testEquippedButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
        assertThat(testEquippedButtonColorMtx.getTimeSlot()).isEqualTo(DEFAULT_TIME_SLOT);
    }

    @Test
    @Transactional
    void fullUpdateEquippedButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        equippedButtonColorMtxRepository.saveAndFlush(equippedButtonColorMtx);

        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();

        // Update the equippedButtonColorMtx using partial update
        EquippedButtonColorMtx partialUpdatedEquippedButtonColorMtx = new EquippedButtonColorMtx();
        partialUpdatedEquippedButtonColorMtx.setId(equippedButtonColorMtx.getId());

        partialUpdatedEquippedButtonColorMtx.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID)
                .timeSlot(UPDATED_TIME_SLOT);

        restEquippedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, partialUpdatedEquippedButtonColorMtx.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEquippedButtonColorMtx)))
                .andExpect(status().isOk());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        EquippedButtonColorMtx testEquippedButtonColorMtx = equippedButtonColorMtxList
                .get(equippedButtonColorMtxList.size() - 1);
        assertThat(testEquippedButtonColorMtx.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testEquippedButtonColorMtx.getMtxId()).isEqualTo(UPDATED_MTX_ID);
        assertThat(testEquippedButtonColorMtx.getTimeSlot()).isEqualTo(UPDATED_TIME_SLOT);
    }

    @Test
    @Transactional
    void patchNonExistingEquippedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();
        equippedButtonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEquippedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, equippedButtonColorMtx.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEquippedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();
        equippedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquippedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL_ID, count.incrementAndGet()).contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isBadRequest());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEquippedButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = equippedButtonColorMtxRepository.findAll().size();
        equippedButtonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEquippedButtonColorMtxMockMvc
                .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(equippedButtonColorMtx)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the EquippedButtonColorMtx in the database
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEquippedButtonColorMtx() throws Exception {
        // Initialize the database
        equippedButtonColorMtxRepository.saveAndFlush(equippedButtonColorMtx);

        int databaseSizeBeforeDelete = equippedButtonColorMtxRepository.findAll().size();

        // Delete the equippedButtonColorMtx
        restEquippedButtonColorMtxMockMvc
                .perform(delete(ENTITY_API_URL_ID, equippedButtonColorMtx.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EquippedButtonColorMtx> equippedButtonColorMtxList = equippedButtonColorMtxRepository.findAll();
        assertThat(equippedButtonColorMtxList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    void getUserEquippedButtonColorMtx() throws Exception {
        // Get the user equippedButtonColorMtx, default will be created
        restEquippedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL + "/userEquippedButtonColorMtx/0.0"))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.userName").value("user"))
                .andExpect(jsonPath("$.mtxId").value(DEFAULT_MTX_ID.intValue()))
                .andExpect(jsonPath("$.timeSlot").value(0D));
    }

    @Test
    @Transactional
    void equippButtonColorMtx() throws Exception {
        // add owned buttoncolormtx
        OwnedButtonColorMtx ownedButtonColorMtx = new OwnedButtonColorMtx().mtxId(2L).userName("user");
        ownedButtonColorMtxRepository.saveAndFlush(ownedButtonColorMtx);

        // equipp mtx not owned mtx
        restEquippedButtonColorMtxMockMvc.perform(put(ENTITY_API_URL + "/equippButtonColorMtx/3/0.0"))
                .andExpect(status().isBadRequest());

        // equipp mtx owned mtx
        restEquippedButtonColorMtxMockMvc.perform(put(ENTITY_API_URL + "/equippButtonColorMtx/2/0.0"))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.userName").value("user")).andExpect(jsonPath("$.mtxId").value(2L))
                .andExpect(jsonPath("$.timeSlot").value(0D));

        // Get the user equippedButtonColorMtx, default will be created
        restEquippedButtonColorMtxMockMvc.perform(get(ENTITY_API_URL + "/userEquippedButtonColorMtx/0.0"))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.userName").value("user")).andExpect(jsonPath("$.mtxId").value(2L))
                .andExpect(jsonPath("$.timeSlot").value(0D));
    }
}
