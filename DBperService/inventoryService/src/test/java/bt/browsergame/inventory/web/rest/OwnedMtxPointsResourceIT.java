package bt.browsergame.inventory.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.inventory.IntegrationTest;
import bt.browsergame.inventory.domain.OwnedMtxPoints;
import bt.browsergame.inventory.repository.OwnedMtxPointsRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OwnedMtxPointsResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OwnedMtxPointsResourceIT {

        private static final String DEFAULT_USER_NAME = "user";
        private static final String UPDATED_USER_NAME = "user";

        private static final Integer DEFAULT_MTX_POINTS = 1000;
        private static final Integer UPDATED_MTX_POINTS = 2;

        private static final String ENTITY_API_URL = "/api/owned-mtx-points";
        private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

        private static Random random = new Random();
        private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

        @Autowired
        private OwnedMtxPointsRepository ownedMtxPointsRepository;

        @Autowired
        private EntityManager em;

        @Autowired
        private MockMvc restOwnedMtxPointsMockMvc;

        private OwnedMtxPoints ownedMtxPoints;

        /**
         * Create an entity for this test.
         *
         * This is a static method, as tests for other entities might also need it, if
         * they test an entity which requires the current entity.
         */
        public static OwnedMtxPoints createEntity(EntityManager em) {
                OwnedMtxPoints ownedMtxPoints = new OwnedMtxPoints().userName(DEFAULT_USER_NAME)
                                .mtxPoints(DEFAULT_MTX_POINTS);
                return ownedMtxPoints;
        }

        /**
         * Create an updated entity for this test.
         *
         * This is a static method, as tests for other entities might also need it, if
         * they test an entity which requires the current entity.
         */
        public static OwnedMtxPoints createUpdatedEntity(EntityManager em) {
                OwnedMtxPoints ownedMtxPoints = new OwnedMtxPoints().userName(UPDATED_USER_NAME)
                                .mtxPoints(UPDATED_MTX_POINTS);
                return ownedMtxPoints;
        }

        @BeforeEach
        public void initTest() {
                ownedMtxPoints = createEntity(em);
        }

        @Test
        @Transactional
        void createOwnedMtxPoints() throws Exception {
                int databaseSizeBeforeCreate = ownedMtxPointsRepository.findAll().size();
                // Create the OwnedMtxPoints
                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isCreated());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeCreate + 1);
                OwnedMtxPoints testOwnedMtxPoints = ownedMtxPointsList.get(ownedMtxPointsList.size() - 1);
                assertThat(testOwnedMtxPoints.getUserName()).isEqualTo(DEFAULT_USER_NAME);
                assertThat(testOwnedMtxPoints.getMtxPoints()).isEqualTo(DEFAULT_MTX_POINTS);
        }

        @Test
        @Transactional
        void createOwnedMtxPointsWithExistingId() throws Exception {
                // Create the OwnedMtxPoints with an existing ID
                ownedMtxPoints.setId(1L);

                int databaseSizeBeforeCreate = ownedMtxPointsRepository.findAll().size();

                // An entity with an existing ID cannot be created, so this API call must fail
                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isBadRequest());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeCreate);
        }

        @Test
        @Transactional
        void checkUserNameIsRequired() throws Exception {
                int databaseSizeBeforeTest = ownedMtxPointsRepository.findAll().size();
                // set the field null
                ownedMtxPoints.setUserName(null);

                // Create the OwnedMtxPoints, which fails.

                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isBadRequest());

                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeTest);
        }

        @Test
        @Transactional
        void checkMtxPointsIsRequired() throws Exception {
                int databaseSizeBeforeTest = ownedMtxPointsRepository.findAll().size();
                // set the field null
                ownedMtxPoints.setMtxPoints(null);

                // Create the OwnedMtxPoints, which fails.

                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isBadRequest());

                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeTest);
        }

        @Test
        @Transactional
        void getAllOwnedMtxPoints() throws Exception {
                // Initialize the database
                ownedMtxPointsRepository.saveAndFlush(ownedMtxPoints);

                // Get all the ownedMtxPointsList
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL + "?sort=id,desc")).andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                                .andExpect(jsonPath("$.[*].id").value(hasItem(ownedMtxPoints.getId().intValue())))
                                .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
                                .andExpect(jsonPath("$.[*].mtxPoints").value(hasItem(DEFAULT_MTX_POINTS)));
        }

        @Test
        @Transactional
        void getOwnedMtxPoints() throws Exception {
                // Initialize the database
                ownedMtxPointsRepository.saveAndFlush(ownedMtxPoints);

                // Get the ownedMtxPoints
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL_ID, ownedMtxPoints.getId()))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                                .andExpect(jsonPath("$.id").value(ownedMtxPoints.getId().intValue()))
                                .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
                                .andExpect(jsonPath("$.mtxPoints").value(DEFAULT_MTX_POINTS));
        }

        @Test
        @Transactional
        void getNonExistingOwnedMtxPoints() throws Exception {
                // Get the ownedMtxPoints
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE))
                                .andExpect(status().isNotFound());
        }

        @Test
        @Transactional
        void putNewOwnedMtxPoints() throws Exception {
                // Initialize the database
                ownedMtxPointsRepository.saveAndFlush(ownedMtxPoints);

                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();

                // Update the ownedMtxPoints
                OwnedMtxPoints updatedOwnedMtxPoints = ownedMtxPointsRepository.findById(ownedMtxPoints.getId()).get();
                // Disconnect from session so that the updates on updatedOwnedMtxPoints are not
                // directly saved in db
                em.detach(updatedOwnedMtxPoints);
                updatedOwnedMtxPoints.userName(UPDATED_USER_NAME).mtxPoints(UPDATED_MTX_POINTS);

                restOwnedMtxPointsMockMvc
                                .perform(put(ENTITY_API_URL_ID, updatedOwnedMtxPoints.getId())
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(updatedOwnedMtxPoints)))
                                .andExpect(status().isOk());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
                OwnedMtxPoints testOwnedMtxPoints = ownedMtxPointsList.get(ownedMtxPointsList.size() - 1);
                assertThat(testOwnedMtxPoints.getUserName()).isEqualTo(UPDATED_USER_NAME);
                assertThat(testOwnedMtxPoints.getMtxPoints()).isEqualTo(1002);
        }

        @Test
        @Transactional
        void putNonExistingOwnedMtxPoints() throws Exception {
                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();
                ownedMtxPoints.setId(count.incrementAndGet());

                // If the entity doesn't have an ID, it will throw BadRequestAlertException
                restOwnedMtxPointsMockMvc
                                .perform(put(ENTITY_API_URL_ID, ownedMtxPoints.getId())
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isBadRequest());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        }

        @Test
        @Transactional
        void putWithIdMismatchOwnedMtxPoints() throws Exception {
                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();
                ownedMtxPoints.setId(count.incrementAndGet());

                // If url ID doesn't match entity ID, it will throw BadRequestAlertException
                restOwnedMtxPointsMockMvc
                                .perform(put(ENTITY_API_URL_ID, count.incrementAndGet())
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isBadRequest());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        }

        @Test
        @Transactional
        void putWithMissingIdPathParamOwnedMtxPoints() throws Exception {
                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();
                ownedMtxPoints.setId(count.incrementAndGet());

                // If url ID doesn't match entity ID, it will throw BadRequestAlertException
                restOwnedMtxPointsMockMvc
                                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isMethodNotAllowed());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        }

        @Test
        @Transactional
        void partialUpdateOwnedMtxPointsWithPatch() throws Exception {
                // Initialize the database
                ownedMtxPointsRepository.saveAndFlush(ownedMtxPoints);

                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();

                // Update the ownedMtxPoints using partial update
                OwnedMtxPoints partialUpdatedOwnedMtxPoints = new OwnedMtxPoints();
                partialUpdatedOwnedMtxPoints.setId(ownedMtxPoints.getId());

                restOwnedMtxPointsMockMvc
                                .perform(patch(ENTITY_API_URL_ID, partialUpdatedOwnedMtxPoints.getId())
                                                .contentType("application/merge-patch+json")
                                                .content(TestUtil.convertObjectToJsonBytes(
                                                                partialUpdatedOwnedMtxPoints)))
                                .andExpect(status().isOk());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
                OwnedMtxPoints testOwnedMtxPoints = ownedMtxPointsList.get(ownedMtxPointsList.size() - 1);
                assertThat(testOwnedMtxPoints.getUserName()).isEqualTo(DEFAULT_USER_NAME);
                assertThat(testOwnedMtxPoints.getMtxPoints()).isEqualTo(DEFAULT_MTX_POINTS);
        }

        @Test
        @Transactional
        void fullUpdateOwnedMtxPointsWithPatch() throws Exception {
                // Initialize the database
                ownedMtxPointsRepository.saveAndFlush(ownedMtxPoints);

                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();

                // Update the ownedMtxPoints using partial update
                OwnedMtxPoints partialUpdatedOwnedMtxPoints = new OwnedMtxPoints();
                partialUpdatedOwnedMtxPoints.setId(ownedMtxPoints.getId());

                partialUpdatedOwnedMtxPoints.userName(UPDATED_USER_NAME).mtxPoints(UPDATED_MTX_POINTS);

                restOwnedMtxPointsMockMvc
                                .perform(patch(ENTITY_API_URL_ID, partialUpdatedOwnedMtxPoints.getId())
                                                .contentType("application/merge-patch+json")
                                                .content(TestUtil.convertObjectToJsonBytes(
                                                                partialUpdatedOwnedMtxPoints)))
                                .andExpect(status().isOk());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
                OwnedMtxPoints testOwnedMtxPoints = ownedMtxPointsList.get(ownedMtxPointsList.size() - 1);
                assertThat(testOwnedMtxPoints.getUserName()).isEqualTo(UPDATED_USER_NAME);
                assertThat(testOwnedMtxPoints.getMtxPoints()).isEqualTo(UPDATED_MTX_POINTS);
        }

        @Test
        @Transactional
        void patchNonExistingOwnedMtxPoints() throws Exception {
                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();
                ownedMtxPoints.setId(count.incrementAndGet());

                // If the entity doesn't have an ID, it will throw BadRequestAlertException
                restOwnedMtxPointsMockMvc
                                .perform(patch(ENTITY_API_URL_ID, ownedMtxPoints.getId())
                                                .contentType("application/merge-patch+json")
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isBadRequest());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        }

        @Test
        @Transactional
        void patchWithIdMismatchOwnedMtxPoints() throws Exception {
                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();
                ownedMtxPoints.setId(count.incrementAndGet());

                // If url ID doesn't match entity ID, it will throw BadRequestAlertException
                restOwnedMtxPointsMockMvc
                                .perform(patch(ENTITY_API_URL_ID, count.incrementAndGet())
                                                .contentType("application/merge-patch+json")
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isBadRequest());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        }

        @Test
        @Transactional
        void patchWithMissingIdPathParamOwnedMtxPoints() throws Exception {
                int databaseSizeBeforeUpdate = ownedMtxPointsRepository.findAll().size();
                ownedMtxPoints.setId(count.incrementAndGet());

                // If url ID doesn't match entity ID, it will throw BadRequestAlertException
                restOwnedMtxPointsMockMvc
                                .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json")
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isMethodNotAllowed());

                // Validate the OwnedMtxPoints in the database
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeUpdate);
        }

        @Test
        @Transactional
        void deleteOwnedMtxPoints() throws Exception {
                // Initialize the database
                ownedMtxPointsRepository.saveAndFlush(ownedMtxPoints);

                int databaseSizeBeforeDelete = ownedMtxPointsRepository.findAll().size();

                // Delete the ownedMtxPoints
                restOwnedMtxPointsMockMvc.perform(
                                delete(ENTITY_API_URL_ID, ownedMtxPoints.getId()).accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNoContent());

                // Validate the database contains one less item
                List<OwnedMtxPoints> ownedMtxPointsList = ownedMtxPointsRepository.findAll();
                assertThat(ownedMtxPointsList).hasSize(databaseSizeBeforeDelete - 1);
        }

        @Test
        @Transactional
        void getOwnedMtxPointsOfUser() throws Exception {
                // Create the OwnedMtxPoints
                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isCreated());
                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isCreated());

                // Get the ownedMtxPoints
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL + "/userOwnedMtxPoints"))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                                .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
                                .andExpect(jsonPath("$.mtxPoints").value(DEFAULT_MTX_POINTS * 2));

        }

        @Test
        @Transactional
        void subtractMtxPoints() throws Exception {
                // Create the OwnedMtxPoints
                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isCreated());
                restOwnedMtxPointsMockMvc
                                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                                                .content(TestUtil.convertObjectToJsonBytes(ownedMtxPoints)))
                                .andExpect(status().isCreated());

                // subtract to much points
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL + "/subtractMtxPoints/3000"))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                                .andExpect(jsonPath("$").value(false));

                // check no subtraction
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL + "/userOwnedMtxPoints"))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                                .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
                                .andExpect(jsonPath("$.mtxPoints").value(DEFAULT_MTX_POINTS * 2));

                // subtract to much points
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL + "/subtractMtxPoints/1500"))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                                .andExpect(jsonPath("$").value(true));

                // check subtraction
                restOwnedMtxPointsMockMvc.perform(get(ENTITY_API_URL + "/userOwnedMtxPoints"))
                                .andExpect(status().isOk())
                                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                                .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
                                .andExpect(jsonPath("$.mtxPoints").value((DEFAULT_MTX_POINTS * 2) - 1500));

        }
}
