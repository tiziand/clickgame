package bt.browsergame.statistic.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.statistic.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PurchasesMtxPointsTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PurchasesMtxPoints.class);
        PurchasesMtxPoints purchasesMtxPoints1 = new PurchasesMtxPoints();
        purchasesMtxPoints1.setId(1L);
        PurchasesMtxPoints purchasesMtxPoints2 = new PurchasesMtxPoints();
        purchasesMtxPoints2.setId(purchasesMtxPoints1.getId());
        assertThat(purchasesMtxPoints1).isEqualTo(purchasesMtxPoints2);
        purchasesMtxPoints2.setId(2L);
        assertThat(purchasesMtxPoints1).isNotEqualTo(purchasesMtxPoints2);
        purchasesMtxPoints1.setId(null);
        assertThat(purchasesMtxPoints1).isNotEqualTo(purchasesMtxPoints2);
    }
}
