package bt.browsergame.statistic.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.statistic.IntegrationTest;
import bt.browsergame.statistic.domain.UserCount;
import bt.browsergame.statistic.repository.UserCountRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UserCountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserCountResourceIT {

    private static final Instant DEFAULT_CREATION_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATION_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_USER_COUNT = 1L;
    private static final Long UPDATED_USER_COUNT = 2L;

    private static final String ENTITY_API_URL = "/api/user-counts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserCountRepository userCountRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserCountMockMvc;

    private UserCount userCount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserCount createEntity(EntityManager em) {
        UserCount userCount = new UserCount().creationTime(DEFAULT_CREATION_TIME).userCount(DEFAULT_USER_COUNT);
        return userCount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserCount createUpdatedEntity(EntityManager em) {
        UserCount userCount = new UserCount().creationTime(UPDATED_CREATION_TIME).userCount(UPDATED_USER_COUNT);
        return userCount;
    }

    @BeforeEach
    public void initTest() {
        userCount = createEntity(em);
    }

    @Test
    @Transactional
    void createUserCount() throws Exception {
        int databaseSizeBeforeCreate = userCountRepository.findAll().size();
        // Create the UserCount
        restUserCountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCount)))
            .andExpect(status().isCreated());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeCreate + 1);
        UserCount testUserCount = userCountList.get(userCountList.size() - 1);
        assertThat(testUserCount.getCreationTime()).isEqualTo(DEFAULT_CREATION_TIME);
        assertThat(testUserCount.getUserCount()).isEqualTo(DEFAULT_USER_COUNT);
    }

    @Test
    @Transactional
    void createUserCountWithExistingId() throws Exception {
        // Create the UserCount with an existing ID
        userCount.setId(1L);

        int databaseSizeBeforeCreate = userCountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserCountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCount)))
            .andExpect(status().isBadRequest());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCreationTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userCountRepository.findAll().size();
        // set the field null
        userCount.setCreationTime(null);

        // Create the UserCount, which fails.

        restUserCountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCount)))
            .andExpect(status().isBadRequest());

        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUserCountIsRequired() throws Exception {
        int databaseSizeBeforeTest = userCountRepository.findAll().size();
        // set the field null
        userCount.setUserCount(null);

        // Create the UserCount, which fails.

        restUserCountMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCount)))
            .andExpect(status().isBadRequest());

        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllUserCounts() throws Exception {
        // Initialize the database
        userCountRepository.saveAndFlush(userCount);

        // Get all the userCountList
        restUserCountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userCount.getId().intValue())))
            .andExpect(jsonPath("$.[*].creationTime").value(hasItem(DEFAULT_CREATION_TIME.toString())))
            .andExpect(jsonPath("$.[*].userCount").value(hasItem(DEFAULT_USER_COUNT.intValue())));
    }

    @Test
    @Transactional
    void getUserCount() throws Exception {
        // Initialize the database
        userCountRepository.saveAndFlush(userCount);

        // Get the userCount
        restUserCountMockMvc
            .perform(get(ENTITY_API_URL_ID, userCount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userCount.getId().intValue()))
            .andExpect(jsonPath("$.creationTime").value(DEFAULT_CREATION_TIME.toString()))
            .andExpect(jsonPath("$.userCount").value(DEFAULT_USER_COUNT.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingUserCount() throws Exception {
        // Get the userCount
        restUserCountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewUserCount() throws Exception {
        // Initialize the database
        userCountRepository.saveAndFlush(userCount);

        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();

        // Update the userCount
        UserCount updatedUserCount = userCountRepository.findById(userCount.getId()).get();
        // Disconnect from session so that the updates on updatedUserCount are not directly saved in db
        em.detach(updatedUserCount);
        updatedUserCount.creationTime(UPDATED_CREATION_TIME).userCount(UPDATED_USER_COUNT);

        restUserCountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedUserCount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedUserCount))
            )
            .andExpect(status().isOk());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
        UserCount testUserCount = userCountList.get(userCountList.size() - 1);
        assertThat(testUserCount.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testUserCount.getUserCount()).isEqualTo(UPDATED_USER_COUNT);
    }

    @Test
    @Transactional
    void putNonExistingUserCount() throws Exception {
        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();
        userCount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserCountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userCount.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userCount))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserCount() throws Exception {
        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();
        userCount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userCount))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserCount() throws Exception {
        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();
        userCount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCountMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCount)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserCountWithPatch() throws Exception {
        // Initialize the database
        userCountRepository.saveAndFlush(userCount);

        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();

        // Update the userCount using partial update
        UserCount partialUpdatedUserCount = new UserCount();
        partialUpdatedUserCount.setId(userCount.getId());

        partialUpdatedUserCount.creationTime(UPDATED_CREATION_TIME).userCount(UPDATED_USER_COUNT);

        restUserCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserCount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserCount))
            )
            .andExpect(status().isOk());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
        UserCount testUserCount = userCountList.get(userCountList.size() - 1);
        assertThat(testUserCount.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testUserCount.getUserCount()).isEqualTo(UPDATED_USER_COUNT);
    }

    @Test
    @Transactional
    void fullUpdateUserCountWithPatch() throws Exception {
        // Initialize the database
        userCountRepository.saveAndFlush(userCount);

        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();

        // Update the userCount using partial update
        UserCount partialUpdatedUserCount = new UserCount();
        partialUpdatedUserCount.setId(userCount.getId());

        partialUpdatedUserCount.creationTime(UPDATED_CREATION_TIME).userCount(UPDATED_USER_COUNT);

        restUserCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserCount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserCount))
            )
            .andExpect(status().isOk());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
        UserCount testUserCount = userCountList.get(userCountList.size() - 1);
        assertThat(testUserCount.getCreationTime()).isEqualTo(UPDATED_CREATION_TIME);
        assertThat(testUserCount.getUserCount()).isEqualTo(UPDATED_USER_COUNT);
    }

    @Test
    @Transactional
    void patchNonExistingUserCount() throws Exception {
        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();
        userCount.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userCount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userCount))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserCount() throws Exception {
        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();
        userCount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userCount))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserCount() throws Exception {
        int databaseSizeBeforeUpdate = userCountRepository.findAll().size();
        userCount.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCountMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(userCount))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserCount in the database
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserCount() throws Exception {
        // Initialize the database
        userCountRepository.saveAndFlush(userCount);

        int databaseSizeBeforeDelete = userCountRepository.findAll().size();

        // Delete the userCount
        restUserCountMockMvc
            .perform(delete(ENTITY_API_URL_ID, userCount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserCount> userCountList = userCountRepository.findAll();
        assertThat(userCountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
