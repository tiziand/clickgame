package bt.browsergame.statistic.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.statistic.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserCountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserCount.class);
        UserCount userCount1 = new UserCount();
        userCount1.setId(1L);
        UserCount userCount2 = new UserCount();
        userCount2.setId(userCount1.getId());
        assertThat(userCount1).isEqualTo(userCount2);
        userCount2.setId(2L);
        assertThat(userCount1).isNotEqualTo(userCount2);
        userCount1.setId(null);
        assertThat(userCount1).isNotEqualTo(userCount2);
    }
}
