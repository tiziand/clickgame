package bt.browsergame.statistic.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PurchasesButtonColorMtx.
 */
@Entity
@Table(name = "purchases_button_color_mtx")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PurchasesButtonColorMtx implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "mtx_id", nullable = false)
    private Long mtxId;

    @NotNull
    @Column(name = "creation_time", nullable = false)
    private Instant creationTime;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Long amount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PurchasesButtonColorMtx id(Long id) {
        this.id = id;
        return this;
    }

    public Long getMtxId() {
        return this.mtxId;
    }

    public PurchasesButtonColorMtx mtxId(Long mtxId) {
        this.mtxId = mtxId;
        return this;
    }

    public void setMtxId(Long mtxId) {
        this.mtxId = mtxId;
    }

    public Instant getCreationTime() {
        return this.creationTime;
    }

    public PurchasesButtonColorMtx creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Long getAmount() {
        return this.amount;
    }

    public PurchasesButtonColorMtx amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchasesButtonColorMtx)) {
            return false;
        }
        return id != null && id.equals(((PurchasesButtonColorMtx) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PurchasesButtonColorMtx{" +
            "id=" + getId() +
            ", mtxId=" + getMtxId() +
            ", creationTime='" + getCreationTime() + "'" +
            ", amount=" + getAmount() +
            "}";
    }
}
