package bt.browsergame.statistic.domain.views;

public interface PurchasesButtonColorMtxListView {
    Long getSumPurchases();

    Long getMtxId();
}
