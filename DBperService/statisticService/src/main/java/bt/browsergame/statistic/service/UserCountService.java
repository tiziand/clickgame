package bt.browsergame.statistic.service;

import bt.browsergame.statistic.domain.UserCount;
import bt.browsergame.statistic.repository.UserCountRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserCount}.
 */
@Service
@Transactional
public class UserCountService {

    private final Logger log = LoggerFactory.getLogger(UserCountService.class);

    private final UserCountRepository userCountRepository;

    public UserCountService(UserCountRepository userCountRepository) {
        this.userCountRepository = userCountRepository;
    }

    /**
     * Save a userCount.
     *
     * @param userCount the entity to save.
     * @return the persisted entity.
     */
    public UserCount save(UserCount userCount) {
        log.debug("Request to save UserCount : {}", userCount);
        return userCountRepository.save(userCount);
    }

    /**
     * Partially update a userCount.
     *
     * @param userCount the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<UserCount> partialUpdate(UserCount userCount) {
        log.debug("Request to partially update UserCount : {}", userCount);

        return userCountRepository
            .findById(userCount.getId())
            .map(
                existingUserCount -> {
                    if (userCount.getCreationTime() != null) {
                        existingUserCount.setCreationTime(userCount.getCreationTime());
                    }
                    if (userCount.getUserCount() != null) {
                        existingUserCount.setUserCount(userCount.getUserCount());
                    }

                    return existingUserCount;
                }
            )
            .map(userCountRepository::save);
    }

    /**
     * Get all the userCounts.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UserCount> findAll() {
        log.debug("Request to get all UserCounts");
        return userCountRepository.findAll();
    }

    /**
     * Get one userCount by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UserCount> findOne(Long id) {
        log.debug("Request to get UserCount : {}", id);
        return userCountRepository.findById(id);
    }

    /**
     * Delete the userCount by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UserCount : {}", id);
        userCountRepository.deleteById(id);
    }
}
