package bt.browsergame.statistic.repository;

import bt.browsergame.statistic.domain.UserCount;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the UserCount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserCountRepository extends JpaRepository<UserCount, Long> {}
