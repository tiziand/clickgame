package bt.browsergame.statistic.web.rest;

import bt.browsergame.statistic.domain.PurchasesMtxPoints;
import bt.browsergame.statistic.domain.views.PurchasesMtxPointsView;
import bt.browsergame.statistic.repository.PurchasesMtxPointsRepository;
import bt.browsergame.statistic.service.PurchasesMtxPointsService;
import bt.browsergame.statistic.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing
 * {@link bt.browsergame.statistic.domain.PurchasesMtxPoints}.
 */
@RestController
@RequestMapping("/api")
public class PurchasesMtxPointsResource {

    private final Logger log = LoggerFactory.getLogger(PurchasesMtxPointsResource.class);

    private static final String ENTITY_NAME = "statisticServicePurchasesMtxPoints";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PurchasesMtxPointsService purchasesMtxPointsService;

    private final PurchasesMtxPointsRepository purchasesMtxPointsRepository;

    public PurchasesMtxPointsResource(
        PurchasesMtxPointsService purchasesMtxPointsService,
        PurchasesMtxPointsRepository purchasesMtxPointsRepository
    ) {
        this.purchasesMtxPointsService = purchasesMtxPointsService;
        this.purchasesMtxPointsRepository = purchasesMtxPointsRepository;
    }

    /**
     * {@code POST  /purchases-mtx-points} : Create a new purchasesMtxPoints.
     *
     * @param purchasesMtxPoints the purchasesMtxPoints to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new purchasesMtxPoints, or with status
     *         {@code 400 (Bad Request)} if the purchasesMtxPoints has already an
     *         ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/purchases-mtx-points")
    public ResponseEntity<PurchasesMtxPoints> createPurchasesMtxPoints(@Valid @RequestBody PurchasesMtxPoints purchasesMtxPoints)
        throws URISyntaxException {
        log.debug("REST request to save PurchasesMtxPoints : {}", purchasesMtxPoints);
        if (purchasesMtxPoints.getId() != null) {
            throw new BadRequestAlertException("A new purchasesMtxPoints cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PurchasesMtxPoints result = purchasesMtxPointsService.save(purchasesMtxPoints);
        return ResponseEntity
            .created(new URI("/api/purchases-mtx-points/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /purchases-mtx-points/:id} : Updates an existing
     * purchasesMtxPoints.
     *
     * @param id                 the id of the purchasesMtxPoints to save.
     * @param purchasesMtxPoints the purchasesMtxPoints to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated purchasesMtxPoints, or with status
     *         {@code 400 (Bad Request)} if the purchasesMtxPoints is not valid, or
     *         with status {@code 500 (Internal Server Error)} if the
     *         purchasesMtxPoints couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/purchases-mtx-points/{id}")
    public ResponseEntity<PurchasesMtxPoints> updatePurchasesMtxPoints(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PurchasesMtxPoints purchasesMtxPoints
    ) throws URISyntaxException {
        log.debug("REST request to update PurchasesMtxPoints : {}, {}", id, purchasesMtxPoints);
        if (purchasesMtxPoints.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purchasesMtxPoints.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purchasesMtxPointsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PurchasesMtxPoints result = purchasesMtxPointsService.save(purchasesMtxPoints);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, purchasesMtxPoints.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /purchases-mtx-points/:id} : Partial updates given fields of an
     * existing purchasesMtxPoints, field will ignore if it is null
     *
     * @param id                 the id of the purchasesMtxPoints to save.
     * @param purchasesMtxPoints the purchasesMtxPoints to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated purchasesMtxPoints, or with status
     *         {@code 400 (Bad Request)} if the purchasesMtxPoints is not valid, or
     *         with status {@code 404 (Not Found)} if the purchasesMtxPoints is not
     *         found, or with status {@code 500 (Internal Server Error)} if the
     *         purchasesMtxPoints couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/purchases-mtx-points/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PurchasesMtxPoints> partialUpdatePurchasesMtxPoints(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PurchasesMtxPoints purchasesMtxPoints
    ) throws URISyntaxException {
        log.debug("REST request to partial update PurchasesMtxPoints partially : {}, {}", id, purchasesMtxPoints);
        if (purchasesMtxPoints.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, purchasesMtxPoints.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!purchasesMtxPointsRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PurchasesMtxPoints> result = purchasesMtxPointsService.partialUpdate(purchasesMtxPoints);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, purchasesMtxPoints.getId().toString())
        );
    }

    /**
     * {@code GET  /purchases-mtx-points} : get all the purchasesMtxPoints.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of purchasesMtxPoints in body.
     */
    @GetMapping("/purchases-mtx-points")
    public List<PurchasesMtxPoints> getAllPurchasesMtxPoints() {
        log.debug("REST request to get all PurchasesMtxPoints");
        return purchasesMtxPointsService.findAll();
    }

    /**
     * {@code GET  /purchases-mtx-points/:id} : get the "id" purchasesMtxPoints.
     *
     * @param id the id of the purchasesMtxPoints to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the purchasesMtxPoints, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/purchases-mtx-points/{id}")
    public ResponseEntity<PurchasesMtxPoints> getPurchasesMtxPoints(@PathVariable Long id) {
        log.debug("REST request to get PurchasesMtxPoints : {}", id);
        Optional<PurchasesMtxPoints> purchasesMtxPoints = purchasesMtxPointsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(purchasesMtxPoints);
    }

    /**
     * {@code DELETE  /purchases-mtx-points/:id} : delete the "id"
     * purchasesMtxPoints.
     *
     * @param id the id of the purchasesMtxPoints to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/purchases-mtx-points/{id}")
    public ResponseEntity<Void> deletePurchasesMtxPoints(@PathVariable Long id) {
        log.debug("REST request to delete PurchasesMtxPoints : {}", id);
        purchasesMtxPointsService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /purchases-mtx-points/moneySpentSum/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get the sum of money spent
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     */
    @GetMapping("/purchases-mtx-points/moneySpentSum/{startDate}/{endDate}")
    public ResponseEntity<PurchasesMtxPointsView> getMoneySpentSum(
        @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
        @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate
    ) {
        log.debug("REST request to get getMoneySpentSum : {}");
        return ResponseEntity.ok(purchasesMtxPointsService.getMoneySpentSum(startDate, endDate));
    }
}
