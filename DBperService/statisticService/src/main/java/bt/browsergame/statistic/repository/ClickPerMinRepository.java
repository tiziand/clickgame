package bt.browsergame.statistic.repository;

import bt.browsergame.statistic.domain.ClickPerMin;
import bt.browsergame.statistic.domain.views.ClickPerMinView;
import java.time.Instant;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ClickPerMin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClickPerMinRepository extends JpaRepository<ClickPerMin, Long> {
    @Query(
        "SELECT SUM(cpm.clickCount) as clicks FROM ClickPerMin cpm WHERE cpm.creationTime >= :startdate AND cpm.creationTime <= :enddate"
    )
    ClickPerMinView getClicksSum(@Param("startdate") Instant startDate, @Param("enddate") Instant endDate);
}
