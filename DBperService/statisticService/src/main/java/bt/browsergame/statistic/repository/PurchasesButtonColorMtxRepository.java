package bt.browsergame.statistic.repository;

import bt.browsergame.statistic.domain.PurchasesButtonColorMtx;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxAllView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxListView;
import java.time.Instant;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PurchasesButtonColorMtx entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PurchasesButtonColorMtxRepository extends JpaRepository<PurchasesButtonColorMtx, Long> {
    @Query(
        "SELECT SUM(pbcm.amount) as sumPurchases FROM PurchasesButtonColorMtx pbcm WHERE pbcm.creationTime >= :startdate AND pbcm.creationTime <= :enddate"
    )
    PurchasesButtonColorMtxAllView getSumPurchases(@Param("startdate") Instant startDate, @Param("enddate") Instant endDate);

    @Query(
        "SELECT SUM(pbcm.amount) as sumPurchases, pbcm.mtxId as mtxId FROM PurchasesButtonColorMtx pbcm WHERE pbcm.creationTime >= :startdate AND pbcm.creationTime <= :enddate GROUP BY pbcm.mtxId"
    )
    List<PurchasesButtonColorMtxListView> getSumPurchasesList(@Param("startdate") Instant startDate, @Param("enddate") Instant endDate);
}
