package bt.browsergame.statistic.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ClickPerMin.
 */
@Entity
@Table(name = "click_per_min")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ClickPerMin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "creation_time", nullable = false)
    private Instant creationTime;

    @NotNull
    @Column(name = "click_count", nullable = false)
    private Long clickCount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClickPerMin id(Long id) {
        this.id = id;
        return this;
    }

    public Instant getCreationTime() {
        return this.creationTime;
    }

    public ClickPerMin creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Long getClickCount() {
        return this.clickCount;
    }

    public ClickPerMin clickCount(Long clickCount) {
        this.clickCount = clickCount;
        return this;
    }

    public void setClickCount(Long clickCount) {
        this.clickCount = clickCount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClickPerMin)) {
            return false;
        }
        return id != null && id.equals(((ClickPerMin) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClickPerMin{" +
            "id=" + getId() +
            ", creationTime='" + getCreationTime() + "'" +
            ", clickCount=" + getClickCount() +
            "}";
    }
}
