package bt.browsergame.basket.foreignClients;

import org.springframework.stereotype.Component;

import bt.browsergame.basket.foreignEntities.ButtonColorMtx;

@Component
public class MtxStoreService {
    public ButtonColorMtx getButtonColorMtx(Long id) {
        ButtonColorMtx buttonColorMtx = new ButtonColorMtx();
        buttonColorMtx.setAvailable(true);
        buttonColorMtx.setId(id);
        buttonColorMtx.setLocked(false);
        buttonColorMtx.setMtxColor("#12423");
        buttonColorMtx.setPointsRequired(0L);
        buttonColorMtx.setPrice(1000);
        buttonColorMtx.setTimeLimit(0.0);
        return buttonColorMtx;
    }
}
