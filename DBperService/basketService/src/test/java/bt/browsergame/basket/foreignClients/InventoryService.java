package bt.browsergame.basket.foreignClients;

import bt.browsergame.basket.foreignEntities.OwnedButtonColorMtx;
import org.springframework.stereotype.Component;

@Component
public class InventoryService {

    public Boolean subtractMtxPoints(Integer points) {
        return points <= 1000;
    }

    public OwnedButtonColorMtx createOwnedButtonColorMtx(OwnedButtonColorMtx ownedButtonColorMtx) {
        return ownedButtonColorMtx;
    }
}
