package bt.browsergame.basket.foreignClients;

import bt.browsergame.basket.foreignEntities.PurchasesButtonColorMtx;
import org.springframework.stereotype.Component;
@Component
public class StatisticService {

    public PurchasesButtonColorMtx createPurchasesButtonColorMtx(PurchasesButtonColorMtx purchasesButtonColorMtx) {
        return purchasesButtonColorMtx;
    }
}
