package bt.browsergame.basket.foreignClients;

import bt.browsergame.basket.foreignEntities.PointsFarmed;
import org.springframework.stereotype.Component;

@Component
public class RankingService {

    public PointsFarmed getUserSum() {
        PointsFarmed pointsFarmed = new PointsFarmed();
        pointsFarmed.setPoints(0L);
        pointsFarmed.setTimeLimit(0L);
        pointsFarmed.setUserName("user");
        return pointsFarmed;
    }

    public PointsFarmed getUserMaxTime(double timeLimit) {
        PointsFarmed pointsFarmed = new PointsFarmed();
        pointsFarmed.setPoints(0L);
        pointsFarmed.setTimeLimit(0L);
        pointsFarmed.setUserName("user");
        return pointsFarmed;
    }
}
