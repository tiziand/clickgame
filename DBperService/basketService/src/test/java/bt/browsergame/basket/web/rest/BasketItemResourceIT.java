package bt.browsergame.basket.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.basket.IntegrationTest;
import bt.browsergame.basket.domain.BasketItem;
import bt.browsergame.basket.foreignClients.MtxStoreService;
import bt.browsergame.basket.foreignClients.RankingService;
import bt.browsergame.basket.repository.BasketItemRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BasketItemResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BasketItemResourceIT {

    private static final String DEFAULT_USER_NAME = "user";
    private static final String UPDATED_USER_NAME = "password";

    private static final Long DEFAULT_MTX_ID = 1L;
    private static final Long UPDATED_MTX_ID = 2L;

    private static final String ENTITY_API_URL = "/api/basket-items";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BasketItemRepository basketItemRepository;

    @Autowired
    private EntityManager em;

    @Mock
    private MtxStoreService mtxStoreService;

    @Mock
    private RankingService rankingService;

    @Autowired
    private MockMvc restBasketItemMockMvc;

    private BasketItem basketItem;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static BasketItem createEntity(EntityManager em) {
        BasketItem basketItem = new BasketItem().userName(DEFAULT_USER_NAME).mtxId(DEFAULT_MTX_ID);
        return basketItem;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static BasketItem createUpdatedEntity(EntityManager em) {
        BasketItem basketItem = new BasketItem().userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID);
        return basketItem;
    }

    @BeforeEach
    public void initTest() {
        basketItem = createEntity(em);
    }

    @Test
    @Transactional
    void createBasketItem() throws Exception {
        int databaseSizeBeforeCreate = basketItemRepository.findAll().size();
        // Create the BasketItem
        restBasketItemMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(basketItem))).andExpect(status().isCreated());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeCreate + 1);
        BasketItem testBasketItem = basketItemList.get(basketItemList.size() - 1);
        assertThat(testBasketItem.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testBasketItem.getMtxId()).isEqualTo(DEFAULT_MTX_ID);
    }

    @Test
    @Transactional
    void createBasketItemWithExistingId() throws Exception {
        // Create the BasketItem with an existing ID
        basketItem.setId(1L);

        int databaseSizeBeforeCreate = basketItemRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBasketItemMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(basketItem))).andExpect(status().isBadRequest());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkUserNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = basketItemRepository.findAll().size();
        // set the field null
        basketItem.setUserName(null);

        // Create the BasketItem, which fails.

        restBasketItemMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(basketItem))).andExpect(status().isBadRequest());

        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkMtxIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = basketItemRepository.findAll().size();
        // set the field null
        basketItem.setMtxId(null);

        // Create the BasketItem, which fails.

        restBasketItemMockMvc.perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(basketItem))).andExpect(status().isBadRequest());

        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllBasketItems() throws Exception {
        // Initialize the database
        basketItemRepository.saveAndFlush(basketItem);

        // Get all the basketItemList
        restBasketItemMockMvc.perform(get(ENTITY_API_URL + "?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(basketItem.getId().intValue())))
                .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
                .andExpect(jsonPath("$.[*].mtxId").value(hasItem(DEFAULT_MTX_ID.intValue())));
    }

    @Test
    @Transactional
    void getBasketItem() throws Exception {
        // Initialize the database
        basketItemRepository.saveAndFlush(basketItem);

        // Get the basketItem
        restBasketItemMockMvc.perform(get(ENTITY_API_URL_ID, basketItem.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.id").value(basketItem.getId().intValue()))
                .andExpect(jsonPath("$.userName").value(DEFAULT_USER_NAME))
                .andExpect(jsonPath("$.mtxId").value(DEFAULT_MTX_ID.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingBasketItem() throws Exception {
        // Get the basketItem
        restBasketItemMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewBasketItem() throws Exception {
        // Initialize the database
        basketItemRepository.saveAndFlush(basketItem);

        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();

        // Update the basketItem
        BasketItem updatedBasketItem = basketItemRepository.findById(basketItem.getId()).get();
        // Disconnect from session so that the updates on updatedBasketItem are not
        // directly saved in db
        em.detach(updatedBasketItem);
        updatedBasketItem.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID);

        restBasketItemMockMvc.perform(put(ENTITY_API_URL_ID, updatedBasketItem.getId())
                .contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(updatedBasketItem)))
                .andExpect(status().isOk());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
        BasketItem testBasketItem = basketItemList.get(basketItemList.size() - 1);
        assertThat(testBasketItem.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testBasketItem.getMtxId()).isEqualTo(UPDATED_MTX_ID);
    }

    @Test
    @Transactional
    void putNonExistingBasketItem() throws Exception {
        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();
        basketItem.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBasketItemMockMvc.perform(put(ENTITY_API_URL_ID, basketItem.getId()).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.convertObjectToJsonBytes(basketItem))).andExpect(status().isBadRequest());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBasketItem() throws Exception {
        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();
        basketItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBasketItemMockMvc.perform(put(ENTITY_API_URL_ID, count.incrementAndGet())
                .contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(basketItem)))
                .andExpect(status().isBadRequest());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBasketItem() throws Exception {
        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();
        basketItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBasketItemMockMvc
                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON)
                        .content(TestUtil.convertObjectToJsonBytes(basketItem)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBasketItemWithPatch() throws Exception {
        // Initialize the database
        basketItemRepository.saveAndFlush(basketItem);

        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();

        // Update the basketItem using partial update
        BasketItem partialUpdatedBasketItem = new BasketItem();
        partialUpdatedBasketItem.setId(basketItem.getId());

        restBasketItemMockMvc
                .perform(patch(ENTITY_API_URL_ID, partialUpdatedBasketItem.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBasketItem)))
                .andExpect(status().isOk());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
        BasketItem testBasketItem = basketItemList.get(basketItemList.size() - 1);
        assertThat(testBasketItem.getUserName()).isEqualTo(DEFAULT_USER_NAME);
        assertThat(testBasketItem.getMtxId()).isEqualTo(DEFAULT_MTX_ID);
    }

    @Test
    @Transactional
    void fullUpdateBasketItemWithPatch() throws Exception {
        // Initialize the database
        basketItemRepository.saveAndFlush(basketItem);

        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();

        // Update the basketItem using partial update
        BasketItem partialUpdatedBasketItem = new BasketItem();
        partialUpdatedBasketItem.setId(basketItem.getId());

        partialUpdatedBasketItem.userName(UPDATED_USER_NAME).mtxId(UPDATED_MTX_ID);

        restBasketItemMockMvc
                .perform(patch(ENTITY_API_URL_ID, partialUpdatedBasketItem.getId())
                        .contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBasketItem)))
                .andExpect(status().isOk());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
        BasketItem testBasketItem = basketItemList.get(basketItemList.size() - 1);
        assertThat(testBasketItem.getUserName()).isEqualTo(UPDATED_USER_NAME);
        assertThat(testBasketItem.getMtxId()).isEqualTo(UPDATED_MTX_ID);
    }

    @Test
    @Transactional
    void patchNonExistingBasketItem() throws Exception {
        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();
        basketItem.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBasketItemMockMvc.perform(patch(ENTITY_API_URL_ID, basketItem.getId())
                .contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(basketItem)))
                .andExpect(status().isBadRequest());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBasketItem() throws Exception {
        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();
        basketItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBasketItemMockMvc.perform(patch(ENTITY_API_URL_ID, count.incrementAndGet())
                .contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(basketItem)))
                .andExpect(status().isBadRequest());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBasketItem() throws Exception {
        int databaseSizeBeforeUpdate = basketItemRepository.findAll().size();
        basketItem.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBasketItemMockMvc
                .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json")
                        .content(TestUtil.convertObjectToJsonBytes(basketItem)))
                .andExpect(status().isMethodNotAllowed());

        // Validate the BasketItem in the database
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBasketItem() throws Exception {
        // Initialize the database
        basketItemRepository.saveAndFlush(basketItem);

        int databaseSizeBeforeDelete = basketItemRepository.findAll().size();

        // Delete the basketItem
        restBasketItemMockMvc.perform(delete(ENTITY_API_URL_ID, basketItem.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BasketItem> basketItemList = basketItemRepository.findAll();
        assertThat(basketItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    void getUserBasket() throws Exception {
        // Initialize the database
        BasketItem basketItem = new BasketItem().id(null).mtxId(DEFAULT_MTX_ID).userName(DEFAULT_USER_NAME);
        basketItemRepository.saveAndFlush(basketItem);

        basketItem = new BasketItem().id(null).mtxId(DEFAULT_MTX_ID).userName("john");
        basketItemRepository.saveAndFlush(basketItem);

        // get user basket
        restBasketItemMockMvc.perform(get(ENTITY_API_URL + "/userBasket")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
                .andExpect(jsonPath("$.[*].userName").value(not(hasItem("john"))));
    }

    @Test
    @Transactional
    void purchaseBasket() throws Exception {
        // Initialize the database
        BasketItem basketItem = new BasketItem().id(null).mtxId(DEFAULT_MTX_ID).userName(DEFAULT_USER_NAME);
        basketItemRepository.saveAndFlush(basketItem);
        basketItem = new BasketItem().id(null).mtxId(DEFAULT_MTX_ID).userName(DEFAULT_USER_NAME);
        basketItemRepository.saveAndFlush(basketItem);

        // try to purchase to expensive basket assuming 1000 points owned
        restBasketItemMockMvc.perform(get(ENTITY_API_URL + "/purchase")).andExpect(status().isOk());

        restBasketItemMockMvc.perform(get(ENTITY_API_URL + "/userBasket")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].userName").value(hasItem(DEFAULT_USER_NAME)))
                .andExpect(jsonPath("$.[*].userName").value(not(hasItem("john"))));

        // remove item
        restBasketItemMockMvc.perform(delete(ENTITY_API_URL_ID, basketItem.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        // purchase viable basket
        restBasketItemMockMvc.perform(get(ENTITY_API_URL + "/purchase")).andExpect(status().isOk());

        restBasketItemMockMvc.perform(get(ENTITY_API_URL + "/userBasket")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.[*].userName").value(not(hasItem(DEFAULT_USER_NAME))))
                .andExpect(jsonPath("$.[*].userName").value(not(hasItem("john"))));
    }
}
