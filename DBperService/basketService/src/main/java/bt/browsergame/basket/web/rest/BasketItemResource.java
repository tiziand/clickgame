package bt.browsergame.basket.web.rest;

import bt.browsergame.basket.domain.BasketItem;
import bt.browsergame.basket.repository.BasketItemRepository;
import bt.browsergame.basket.service.BasketItemService;
import bt.browsergame.basket.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bt.browsergame.basket.domain.BasketItem}.
 */
@RestController
@RequestMapping("/api")
public class BasketItemResource {

    private final Logger log = LoggerFactory.getLogger(BasketItemResource.class);

    private static final String ENTITY_NAME = "basketServiceBasketItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BasketItemService basketItemService;

    private final BasketItemRepository basketItemRepository;

    public BasketItemResource(BasketItemService basketItemService, BasketItemRepository basketItemRepository) {
        this.basketItemService = basketItemService;
        this.basketItemRepository = basketItemRepository;
    }

    /**
     * {@code POST  /basket-items} : Create a new basketItem.
     *
     * @param basketItem the basketItem to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new basketItem, or with status {@code 400 (Bad Request)} if
     *         the basketItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/basket-items")
    public ResponseEntity<BasketItem> createBasketItem(@Valid @RequestBody BasketItem basketItem) throws URISyntaxException {
        log.debug("REST request to save BasketItem : {}", basketItem);
        if (basketItem.getId() != null) {
            throw new BadRequestAlertException("A new basketItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BasketItem result = basketItemService.save(basketItem);
        return ResponseEntity
            .created(new URI("/api/basket-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /basket-items/:id} : Updates an existing basketItem.
     *
     * @param id         the id of the basketItem to save.
     * @param basketItem the basketItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated basketItem, or with status {@code 400 (Bad Request)} if
     *         the basketItem is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the basketItem couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/basket-items/{id}")
    public ResponseEntity<BasketItem> updateBasketItem(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody BasketItem basketItem
    ) throws URISyntaxException {
        log.debug("REST request to update BasketItem : {}, {}", id, basketItem);
        if (basketItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, basketItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!basketItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        BasketItem result = basketItemService.save(basketItem);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, basketItem.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /basket-items/:id} : Partial updates given fields of an
     * existing basketItem, field will ignore if it is null
     *
     * @param id         the id of the basketItem to save.
     * @param basketItem the basketItem to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated basketItem, or with status {@code 400 (Bad Request)} if
     *         the basketItem is not valid, or with status {@code 404 (Not Found)}
     *         if the basketItem is not found, or with status
     *         {@code 500 (Internal Server Error)} if the basketItem couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/basket-items/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<BasketItem> partialUpdateBasketItem(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody BasketItem basketItem
    ) throws URISyntaxException {
        log.debug("REST request to partial update BasketItem partially : {}, {}", id, basketItem);
        if (basketItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, basketItem.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!basketItemRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BasketItem> result = basketItemService.partialUpdate(basketItem);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, basketItem.getId().toString())
        );
    }

    /**
     * {@code GET  /basket-items} : get all the basketItems.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of basketItems in body.
     */
    @GetMapping("/basket-items")
    public List<BasketItem> getAllBasketItems() {
        log.debug("REST request to get all BasketItems");
        return basketItemService.findAll();
    }

    /**
     * {@code GET  /basket-items/:id} : get the "id" basketItem.
     *
     * @param id the id of the basketItem to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the basketItem, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/basket-items/{id}")
    public ResponseEntity<BasketItem> getBasketItem(@PathVariable Long id) {
        log.debug("REST request to get BasketItem : {}", id);
        Optional<BasketItem> basketItem = basketItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(basketItem);
    }

    /**
     * {@code DELETE  /basket-items/:id} : delete the "id" basketItem.
     *
     * @param id the id of the basketItem to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/basket-items/{id}")
    public ResponseEntity<Void> deleteBasketItem(@PathVariable Long id) {
        log.debug("REST request to delete BasketItem : {}", id);
        basketItemService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /basket-items/userBasket} : get the "username" items in the
     * basket.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the basketItems
     */
    @GetMapping("/basket-items/userBasket")
    public List<BasketItem> getUserBasket() {
        log.debug("REST request to get basket items of user : {}");
        return basketItemService.getUserBasket();
    }

    /**
     * {@code GET  /basket-items/purchase} : purchase basket items.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of basketItems in body.
     */
    @GetMapping("/basket-items/purchase")
    public List<BasketItem> purchaseBasket() {
        log.debug("REST request to get all BasketItems");
        return basketItemService.purchaseBasket();
    }
}
