package bt.browsergame.basket.foreignClients;

import bt.browsergame.basket.foreignEntities.PointsFarmed;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RankingService {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    private final Logger log = LoggerFactory.getLogger(InventoryService.class);

    public PointsFarmed getUserSum() {
        List<ServiceInstance> instances = discoveryClient.getInstances("rankingService");
        log.debug(instances.get(0).getUri().toString());

        ResponseEntity<PointsFarmed> resp = restTemplate.getForEntity(
            instances.get(0).getUri().toString() + "/api/points-farmeds/userSum",
            PointsFarmed.class
        );
        return resp.getBody();
    }

    public PointsFarmed getUserMaxTime(double timeLimit) {
        List<ServiceInstance> instances = discoveryClient.getInstances("rankingService");
        log.debug(instances.get(0).getUri().toString());

        ResponseEntity<PointsFarmed> resp = restTemplate.getForEntity(
            instances.get(0).getUri().toString() + "/api/points-farmeds/userMaxTime/" + timeLimit,
            PointsFarmed.class
        );
        return resp.getBody();
    }
}
