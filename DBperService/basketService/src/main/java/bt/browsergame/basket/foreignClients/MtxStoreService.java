package bt.browsergame.basket.foreignClients;

import bt.browsergame.basket.foreignEntities.ButtonColorMtx;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MtxStoreService {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    private final Logger log = LoggerFactory.getLogger(MtxStoreService.class);

    public ButtonColorMtx getButtonColorMtx(Long id) {
        List<ServiceInstance> instances = discoveryClient.getInstances("mtxStoreService");
        log.debug(instances.get(0).getUri().toString());

        ResponseEntity<ButtonColorMtx> resp = restTemplate.getForEntity(
            instances.get(0).getUri().toString() + "/api/button-color-mtxes/" + id,
            ButtonColorMtx.class
        );
        return resp.getBody();
    }
}
