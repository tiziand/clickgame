package bt.browsergame.basket.foreignEntities;

import java.io.Serializable;
import java.time.Instant;

/**
 * A PurchasesButtonColorMtx.
 */
public class PurchasesButtonColorMtx implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long mtxId;

    private Instant creationTime;

    private Long amount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMtxId() {
        return mtxId;
    }

    public PurchasesButtonColorMtx mtxId(Long mtxId) {
        this.mtxId = mtxId;
        return this;
    }

    public void setMtxId(Long mtxId) {
        this.mtxId = mtxId;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public PurchasesButtonColorMtx creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Long getAmount() {
        return amount;
    }

    public PurchasesButtonColorMtx amount(Long amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchasesButtonColorMtx)) {
            return false;
        }
        return id != null && id.equals(((PurchasesButtonColorMtx) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PurchasesButtonColorMtx{" + "id=" + getId() + ", mtxId=" + getMtxId() + ", creationTime='"
                + getCreationTime() + "'" + ", amount=" + getAmount() + "}";
    }
}
