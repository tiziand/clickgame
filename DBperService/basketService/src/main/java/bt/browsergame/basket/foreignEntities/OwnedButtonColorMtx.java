package bt.browsergame.basket.foreignEntities;

import java.io.Serializable;

/**
 * A OwnedButtonColorMtx.
 */
public class OwnedButtonColorMtx implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String userName;

    private Long mtxId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public OwnedButtonColorMtx userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getMtxId() {
        return mtxId;
    }

    public OwnedButtonColorMtx mtxId(Long mtxId) {
        this.mtxId = mtxId;
        return this;
    }

    public void setMtxId(Long mtxId) {
        this.mtxId = mtxId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OwnedButtonColorMtx)) {
            return false;
        }
        return id != null && id.equals(((OwnedButtonColorMtx) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OwnedButtonColorMtx{" + "id=" + getId() + ", userName='" + getUserName() + "'" + ", mtxId=" + getMtxId()
                + "}";
    }
}
