package bt.browsergame.basket.foreignEntities;

import java.io.Serializable;

/**
 * A ButtonColorMtx.
 */
public class ButtonColorMtx implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String mtxColor;

    private Integer price;

    private Boolean available;

    private Double timeLimit;

    private Long pointsRequired;

    private Boolean locked;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMtxColor() {
        return mtxColor;
    }

    public ButtonColorMtx mtxColor(String mtxColor) {
        this.mtxColor = mtxColor;
        return this;
    }

    public void setMtxColor(String mtxColor) {
        this.mtxColor = mtxColor;
    }

    public Integer getPrice() {
        return price;
    }

    public ButtonColorMtx price(Integer price) {
        this.price = price;
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean isAvailable() {
        return available;
    }

    public ButtonColorMtx available(Boolean available) {
        this.available = available;
        return this;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Double getTimeLimit() {
        return timeLimit;
    }

    public ButtonColorMtx timeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public void setTimeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Long getPointsRequired() {
        return pointsRequired;
    }

    public ButtonColorMtx pointsRequired(Long pointsRequired) {
        this.pointsRequired = pointsRequired;
        return this;
    }

    public void setPointsRequired(Long pointsRequired) {
        this.pointsRequired = pointsRequired;
    }

    public Boolean isLocked() {
        return locked;
    }

    public ButtonColorMtx locked(Boolean locked) {
        this.locked = locked;
        return this;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ButtonColorMtx)) {
            return false;
        }
        return id != null && id.equals(((ButtonColorMtx) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ButtonColorMtx{" + "id=" + getId() + ", mtxColor='" + getMtxColor() + "'" + ", price=" + getPrice()
                + ", available='" + isAvailable() + "'" + ", timeLimit=" + getTimeLimit() + ", pointsRequired="
                + getPointsRequired() + ", locked='" + isLocked() + "'" + "}";
    }
}
