package bt.browsergame.basket.client;

import bt.browsergame.basket.security.SecurityUtils;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

//based on https://www.baeldung.com/spring-rest-template-interceptor
public class RestTemplateHeaderModifierInterceptor implements ClientHttpRequestInterceptor {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String BEARER = "Bearer ";

    private final Logger log = LoggerFactory.getLogger(RestTemplateHeaderModifierInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        log.debug("called");
        log.debug(SecurityUtils.getCurrentUserJWT().get());
        request.getHeaders().add(AUTHORIZATION_HEADER, BEARER + SecurityUtils.getCurrentUserJWT().get());
        ClientHttpResponse response = execution.execute(request, body);
        return response;
    }
}
