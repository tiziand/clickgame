package bt.browsergame.basket.foreignEntities;

import java.io.Serializable;
import java.time.Instant;

/**
 * A PurchasesMtxPoints.
 */
public class PurchasesMtxPoints implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Instant creationTime;

    private Double moneySpent;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public PurchasesMtxPoints creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Double getMoneySpent() {
        return moneySpent;
    }

    public PurchasesMtxPoints moneySpent(Double moneySpent) {
        this.moneySpent = moneySpent;
        return this;
    }

    public void setMoneySpent(Double moneySpent) {
        this.moneySpent = moneySpent;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchasesMtxPoints)) {
            return false;
        }
        return id != null && id.equals(((PurchasesMtxPoints) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PurchasesMtxPoints{" + "id=" + getId() + ", creationTime='" + getCreationTime() + "'" + ", moneySpent="
                + getMoneySpent() + "}";
    }
}
