package bt.browsergame.basket.foreignEntities;

import java.io.Serializable;

/**
 * A PointsFarmed.
 */
public class PointsFarmed implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userName;

    private Long points;

    private Long ranking;

    public String getUserName() {
        return userName;
    }

    public PointsFarmed userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getPoints() {
        return points;
    }

    public PointsFarmed points(Long points) {
        this.points = points;
        return this;
    }

    public void setPoints(Long points) {
        this.points = points;
    }

    public Long getRanking() {
        return ranking;
    }

    public PointsFarmed ranking(Long ranking) {
        this.ranking = ranking;
        return this;
    }

    public void setTimeLimit(Long ranking) {
        this.ranking = ranking;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PointsFarmed)) {
            return false;
        }
        return userName != null && userName.equals(((PointsFarmed) o).userName);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PointsFarmed{" + "userName='" + getUserName() + "'" + ", points=" + getPoints() + "}";
    }
}
