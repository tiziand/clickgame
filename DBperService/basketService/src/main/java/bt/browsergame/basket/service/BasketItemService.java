package bt.browsergame.basket.service;

import bt.browsergame.basket.domain.BasketItem;
import bt.browsergame.basket.foreignClients.InventoryService;
import bt.browsergame.basket.foreignClients.MtxStoreService;
import bt.browsergame.basket.foreignClients.RankingService;
import bt.browsergame.basket.foreignClients.StatisticService;
import bt.browsergame.basket.foreignEntities.ButtonColorMtx;
import bt.browsergame.basket.foreignEntities.OwnedButtonColorMtx;
import bt.browsergame.basket.foreignEntities.PointsFarmed;
import bt.browsergame.basket.foreignEntities.PurchasesButtonColorMtx;
import bt.browsergame.basket.repository.BasketItemRepository;
import bt.browsergame.basket.security.SecurityUtils;
import bt.browsergame.basket.web.rest.errors.BadRequestAlertException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link BasketItem}.
 */
@Service
@Transactional
public class BasketItemService {

    private final Logger log = LoggerFactory.getLogger(BasketItemService.class);

    private final BasketItemRepository basketItemRepository;

    private final InventoryService inventoryService;

    private final StatisticService statisticService;

    private final MtxStoreService mtxStoreService;

    private final RankingService rankingService;

    public BasketItemService(BasketItemRepository basketItemRepository, InventoryService inventoryService,
            StatisticService statisticService, MtxStoreService mtxStoreService, RankingService rankingService) {
        this.basketItemRepository = basketItemRepository;
        this.inventoryService = inventoryService;
        this.statisticService = statisticService;
        this.mtxStoreService = mtxStoreService;
        this.rankingService = rankingService;
    }

    /**
     * Save a basketItem.
     *
     * @param basketItem the entity to save.
     * @return the persisted entity.
     */
    public BasketItem save(BasketItem basketItem) {
        log.debug("Request to save BasketItem : {}", basketItem);
        basketItem.setUserName(SecurityUtils.getCurrentUserLogin().get());
        ButtonColorMtx buttonColorMtx = mtxStoreService.getButtonColorMtx(basketItem.getMtxId());

        // check requirments before adding to basket
        if (!buttonColorMtx.isAvailable())
            throw new BadRequestAlertException("", "", "Mtx not avaliable!");
        if (buttonColorMtx.getPointsRequired() != 0) {
            PointsFarmed pointsFarmed;
            if (buttonColorMtx.getTimeLimit() == 0)
                pointsFarmed = rankingService.getUserSum();
            else
                pointsFarmed = rankingService.getUserMaxTime(buttonColorMtx.getTimeLimit());
            if (pointsFarmed.getPoints() < buttonColorMtx.getPointsRequired())
                throw new BadRequestAlertException("", "", "Requirments not fulfilled!");
        }
        return basketItemRepository.save(basketItem);
    }

    /**
     * Partially update a basketItem.
     *
     * @param basketItem the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<BasketItem> partialUpdate(BasketItem basketItem) {
        log.debug("Request to partially update BasketItem : {}", basketItem);

        return basketItemRepository.findById(basketItem.getId()).map(existingBasketItem -> {
            if (basketItem.getUserName() != null) {
                existingBasketItem.setUserName(basketItem.getUserName());
            }
            if (basketItem.getMtxId() != null) {
                existingBasketItem.setMtxId(basketItem.getMtxId());
            }

            return existingBasketItem;
        }).map(basketItemRepository::save);
    }

    /**
     * Get all the basketItems.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BasketItem> findAll() {
        log.debug("Request to get all BasketItems");
        return basketItemRepository.findAll();
    }

    /**
     * Get one basketItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BasketItem> findOne(Long id) {
        log.debug("Request to get BasketItem : {}", id);
        return basketItemRepository.findById(id);
    }

    /**
     * Delete the basketItem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BasketItem : {}", id);
        basketItemRepository.deleteById(id);
    }

    /**
     * Get the basketItems of the logged in user.
     *
     */
    public List<BasketItem> getUserBasket() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxId");
        BasketItem exBasketItem = new BasketItem();
        exBasketItem.setUserName(SecurityUtils.getCurrentUserLogin().get());
        Example<BasketItem> example = Example.of(exBasketItem, usernameMatcher);
        return basketItemRepository.findAll(example);
    }

    /**
     * Purchases basketItems of the logged in user.
     *
     */
    public List<BasketItem> purchaseBasket() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        List<BasketItem> basketItems = getUserBasket();

        // calculate cost of the basket and remove mtxPoints if enough are available
        int sum = 0;
        for (BasketItem basketItem : basketItems) {
            ButtonColorMtx buttonColorMtx = mtxStoreService.getButtonColorMtx(basketItem.getMtxId());
            sum += buttonColorMtx.getPrice();
        }
        Boolean enoughPoints = inventoryService.subtractMtxPoints(sum);
        if (!enoughPoints)
            return basketItems;

        // add mtx to inventory and update statstics
        for (BasketItem basketItem : basketItems) {
            // add to inventory
            OwnedButtonColorMtx ownedButtonColorMtx = new OwnedButtonColorMtx();
            ownedButtonColorMtx.setMtxId(basketItem.getMtxId());
            ownedButtonColorMtx.setUserName(basketItem.getUserName());
            inventoryService.createOwnedButtonColorMtx(ownedButtonColorMtx);

            // update statistics
            PurchasesButtonColorMtx purchasesButtonColorMtx = new PurchasesButtonColorMtx();
            purchasesButtonColorMtx.setAmount(Long.valueOf(1));
            purchasesButtonColorMtx.setCreationTime(Instant.now());
            purchasesButtonColorMtx.setMtxId(basketItem.getMtxId());
            statisticService.createPurchasesButtonColorMtx(purchasesButtonColorMtx);

            delete(basketItem.getId());
        }
        basketItems.clear();
        return basketItems;
    }
}
