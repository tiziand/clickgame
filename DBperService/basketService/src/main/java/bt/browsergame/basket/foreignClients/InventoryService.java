package bt.browsergame.basket.foreignClients;

import bt.browsergame.basket.foreignEntities.OwnedButtonColorMtx;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class InventoryService {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    private final Logger log = LoggerFactory.getLogger(InventoryService.class);

    public Boolean subtractMtxPoints(Integer points) {
        List<ServiceInstance> instances = discoveryClient.getInstances("inventoryService");
        log.debug(instances.get(0).getUri().toString());

        ResponseEntity<Boolean> resp = restTemplate.getForEntity(
            instances.get(0).getUri().toString() + "/api/owned-mtx-points/subtractMtxPoints/" + points,
            Boolean.class
        );
        return resp.getBody();
    }

    public OwnedButtonColorMtx createOwnedButtonColorMtx(OwnedButtonColorMtx ownedButtonColorMtx) {
        List<ServiceInstance> instances = discoveryClient.getInstances("inventoryService");
        log.debug(instances.get(0).getUri().toString());

        ResponseEntity<OwnedButtonColorMtx> resp = restTemplate.postForEntity(
            instances.get(0).getUri().toString() + "/api/owned-button-color-mtxes",
            ownedButtonColorMtx,
            OwnedButtonColorMtx.class
        );
        return resp.getBody();
    }
}
