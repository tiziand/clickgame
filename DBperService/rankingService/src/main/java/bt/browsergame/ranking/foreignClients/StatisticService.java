package bt.browsergame.ranking.foreignClients;

import bt.browsergame.ranking.foreignEntities.ClickPerMin;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class StatisticService {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    private final Logger log = LoggerFactory.getLogger(StatisticService.class);

    public ClickPerMin createClickPerMin(ClickPerMin clickPerMin) {
        List<ServiceInstance> instances = discoveryClient.getInstances("statisticService");
        log.debug(instances.get(0).getUri().toString());

        ResponseEntity<ClickPerMin> resp = restTemplate.postForEntity(
            instances.get(0).getUri().toString() + "/api/click-per-mins",
            clickPerMin,
            ClickPerMin.class
        );
        return resp.getBody();
    }
}
