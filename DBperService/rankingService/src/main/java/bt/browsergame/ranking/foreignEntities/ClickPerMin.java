package bt.browsergame.ranking.foreignEntities;

import java.io.Serializable;
import java.time.Instant;

/**
 * A ClickPerMin.
 */
public class ClickPerMin implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Instant creationTime;

    private Long clickCount;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public ClickPerMin creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public Long getClickCount() {
        return clickCount;
    }

    public ClickPerMin clickCount(Long clickCount) {
        this.clickCount = clickCount;
        return this;
    }

    public void setClickCount(Long clickCount) {
        this.clickCount = clickCount;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and
    // setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClickPerMin)) {
            return false;
        }
        return id != null && id.equals(((ClickPerMin) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ClickPerMin{" + "id=" + getId() + ", creationTime='" + getCreationTime() + "'" + ", clickCount="
                + getClickCount() + "}";
    }
}
