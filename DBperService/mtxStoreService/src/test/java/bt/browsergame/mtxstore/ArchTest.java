package bt.browsergame.mtxstore;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("bt.browsergame.mtxstore");

        noClasses()
            .that()
            .resideInAnyPackage("bt.browsergame.mtxstore.service..")
            .or()
            .resideInAnyPackage("bt.browsergame.mtxstore.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..bt.browsergame.mtxstore.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
