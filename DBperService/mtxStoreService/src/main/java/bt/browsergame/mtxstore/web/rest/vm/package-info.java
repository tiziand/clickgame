/**
 * View Models used by Spring MVC REST controllers.
 */
package bt.browsergame.mtxstore.web.rest.vm;
