import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IOwnedMtxPoints, defaultValue } from 'app/shared/model/inventoryService/owned-mtx-points.model';

export const ACTION_TYPES = {
  FETCH_OWNEDMTXPOINTS_LIST: 'ownedMtxPoints/FETCH_OWNEDMTXPOINTS_LIST',
  FETCH_OWNEDMTXPOINTS: 'ownedMtxPoints/FETCH_OWNEDMTXPOINTS',
  CREATE_OWNEDMTXPOINTS: 'ownedMtxPoints/CREATE_OWNEDMTXPOINTS',
  UPDATE_OWNEDMTXPOINTS: 'ownedMtxPoints/UPDATE_OWNEDMTXPOINTS',
  PARTIAL_UPDATE_OWNEDMTXPOINTS: 'ownedMtxPoints/PARTIAL_UPDATE_OWNEDMTXPOINTS',
  DELETE_OWNEDMTXPOINTS: 'ownedMtxPoints/DELETE_OWNEDMTXPOINTS',
  RESET: 'ownedMtxPoints/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IOwnedMtxPoints>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type OwnedMtxPointsState = Readonly<typeof initialState>;

// Reducer

export default (state: OwnedMtxPointsState = initialState, action): OwnedMtxPointsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_OWNEDMTXPOINTS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_OWNEDMTXPOINTS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_OWNEDMTXPOINTS):
    case REQUEST(ACTION_TYPES.UPDATE_OWNEDMTXPOINTS):
    case REQUEST(ACTION_TYPES.DELETE_OWNEDMTXPOINTS):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_OWNEDMTXPOINTS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_OWNEDMTXPOINTS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_OWNEDMTXPOINTS):
    case FAILURE(ACTION_TYPES.CREATE_OWNEDMTXPOINTS):
    case FAILURE(ACTION_TYPES.UPDATE_OWNEDMTXPOINTS):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_OWNEDMTXPOINTS):
    case FAILURE(ACTION_TYPES.DELETE_OWNEDMTXPOINTS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_OWNEDMTXPOINTS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_OWNEDMTXPOINTS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_OWNEDMTXPOINTS):
    case SUCCESS(ACTION_TYPES.UPDATE_OWNEDMTXPOINTS):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_OWNEDMTXPOINTS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_OWNEDMTXPOINTS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/inventoryservice/api/owned-mtx-points';

// Actions

export const getEntities: ICrudGetAllAction<IOwnedMtxPoints> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_OWNEDMTXPOINTS_LIST,
  payload: axios.get<IOwnedMtxPoints>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IOwnedMtxPoints> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_OWNEDMTXPOINTS,
    payload: axios.get<IOwnedMtxPoints>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IOwnedMtxPoints> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_OWNEDMTXPOINTS,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IOwnedMtxPoints> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_OWNEDMTXPOINTS,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IOwnedMtxPoints> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_OWNEDMTXPOINTS,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IOwnedMtxPoints> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_OWNEDMTXPOINTS,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const getEntityOfUser: ICrudGetAction<IOwnedMtxPoints> = () => {
  const requestUrl = `${apiUrl}/userOwnedMtxPoints`;
  return {
    type: ACTION_TYPES.FETCH_OWNEDMTXPOINTS,
    payload: axios.get<IOwnedMtxPoints>(requestUrl),
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
