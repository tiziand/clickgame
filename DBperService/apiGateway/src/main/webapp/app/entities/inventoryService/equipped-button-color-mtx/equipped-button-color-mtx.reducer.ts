import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IEquippedButtonColorMtx, defaultValue } from 'app/shared/model/inventoryService/equipped-button-color-mtx.model';

export const ACTION_TYPES = {
  FETCH_EQUIPPEDBUTTONCOLORMTX_LIST: 'equippedButtonColorMtx/FETCH_EQUIPPEDBUTTONCOLORMTX_LIST',
  FETCH_EQUIPPEDBUTTONCOLORMTX: 'equippedButtonColorMtx/FETCH_EQUIPPEDBUTTONCOLORMTX',
  CREATE_EQUIPPEDBUTTONCOLORMTX: 'equippedButtonColorMtx/CREATE_EQUIPPEDBUTTONCOLORMTX',
  UPDATE_EQUIPPEDBUTTONCOLORMTX: 'equippedButtonColorMtx/UPDATE_EQUIPPEDBUTTONCOLORMTX',
  PARTIAL_UPDATE_EQUIPPEDBUTTONCOLORMTX: 'equippedButtonColorMtx/PARTIAL_UPDATE_EQUIPPEDBUTTONCOLORMTX',
  DELETE_EQUIPPEDBUTTONCOLORMTX: 'equippedButtonColorMtx/DELETE_EQUIPPEDBUTTONCOLORMTX',
  RESET: 'equippedButtonColorMtx/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEquippedButtonColorMtx>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type EquippedButtonColorMtxState = Readonly<typeof initialState>;

// Reducer

export default (state: EquippedButtonColorMtxState = initialState, action): EquippedButtonColorMtxState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX_LIST):
    case REQUEST(ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_EQUIPPEDBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.UPDATE_EQUIPPEDBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.DELETE_EQUIPPEDBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_EQUIPPEDBUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX_LIST):
    case FAILURE(ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.CREATE_EQUIPPEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.UPDATE_EQUIPPEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_EQUIPPEDBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.DELETE_EQUIPPEDBUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_EQUIPPEDBUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.UPDATE_EQUIPPEDBUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_EQUIPPEDBUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_EQUIPPEDBUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/inventoryservice/api/equipped-button-color-mtxes';

// Actions

export const getEntities: ICrudGetAllAction<IEquippedButtonColorMtx> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX_LIST,
  payload: axios.get<IEquippedButtonColorMtx>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IEquippedButtonColorMtx> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX,
    payload: axios.get<IEquippedButtonColorMtx>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IEquippedButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_EQUIPPEDBUTTONCOLORMTX,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IEquippedButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_EQUIPPEDBUTTONCOLORMTX,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IEquippedButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_EQUIPPEDBUTTONCOLORMTX,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEquippedButtonColorMtx> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_EQUIPPEDBUTTONCOLORMTX,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const getEntityOfUser: ICrudGetAction<IEquippedButtonColorMtx> = (timeSlot: number) => {
  const requestUrl = `${apiUrl}/userEquippedButtonColorMtx/${timeSlot}`;
  return {
    type: ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX,
    payload: axios.get<IEquippedButtonColorMtx>(requestUrl),
  };
};

export const equippMTX = (id: number, timeSlot: number) => {
  const requestUrl = `${apiUrl}/equippButtonColorMtx/${id}/${timeSlot}`;
  return {
    type: ACTION_TYPES.FETCH_EQUIPPEDBUTTONCOLORMTX,
    payload: axios.put<IEquippedButtonColorMtx>(requestUrl),
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
