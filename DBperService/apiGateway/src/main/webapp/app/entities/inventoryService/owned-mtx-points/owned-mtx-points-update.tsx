import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './owned-mtx-points.reducer';
import { IOwnedMtxPoints } from 'app/shared/model/inventoryService/owned-mtx-points.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IOwnedMtxPointsUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const OwnedMtxPointsUpdate = (props: IOwnedMtxPointsUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { ownedMtxPointsEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/owned-mtx-points');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...ownedMtxPointsEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="apiGatewayApp.inventoryServiceOwnedMtxPoints.home.createOrEditLabel" data-cy="OwnedMtxPointsCreateUpdateHeading">
            Create or edit a OwnedMtxPoints
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : ownedMtxPointsEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="owned-mtx-points-id">ID</Label>
                  <AvInput id="owned-mtx-points-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="userNameLabel" for="owned-mtx-points-userName">
                  User Name
                </Label>
                <AvField
                  id="owned-mtx-points-userName"
                  data-cy="userName"
                  type="text"
                  name="userName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="mtxPointsLabel" for="owned-mtx-points-mtxPoints">
                  Mtx Points
                </Label>
                <AvField
                  id="owned-mtx-points-mtxPoints"
                  data-cy="mtxPoints"
                  type="string"
                  className="form-control"
                  name="mtxPoints"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/owned-mtx-points" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  ownedMtxPointsEntity: storeState.ownedMtxPoints.entity,
  loading: storeState.ownedMtxPoints.loading,
  updating: storeState.ownedMtxPoints.updating,
  updateSuccess: storeState.ownedMtxPoints.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OwnedMtxPointsUpdate);
