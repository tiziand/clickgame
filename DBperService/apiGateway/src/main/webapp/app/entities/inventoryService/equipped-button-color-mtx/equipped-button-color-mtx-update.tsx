import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './equipped-button-color-mtx.reducer';
import { IEquippedButtonColorMtx } from 'app/shared/model/inventoryService/equipped-button-color-mtx.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IEquippedButtonColorMtxUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const EquippedButtonColorMtxUpdate = (props: IEquippedButtonColorMtxUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { equippedButtonColorMtxEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/equipped-button-color-mtx');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...equippedButtonColorMtxEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2
            id="apiGatewayApp.inventoryServiceEquippedButtonColorMtx.home.createOrEditLabel"
            data-cy="EquippedButtonColorMtxCreateUpdateHeading"
          >
            Create or edit a EquippedButtonColorMtx
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : equippedButtonColorMtxEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="equipped-button-color-mtx-id">ID</Label>
                  <AvInput id="equipped-button-color-mtx-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="userNameLabel" for="equipped-button-color-mtx-userName">
                  User Name
                </Label>
                <AvField
                  id="equipped-button-color-mtx-userName"
                  data-cy="userName"
                  type="text"
                  name="userName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="mtxIdLabel" for="equipped-button-color-mtx-mtxId">
                  Mtx Id
                </Label>
                <AvField id="equipped-button-color-mtx-mtxId" data-cy="mtxId" type="string" className="form-control" name="mtxId" />
              </AvGroup>
              <AvGroup>
                <Label id="timeSlotLabel" for="equipped-button-color-mtx-timeSlot">
                  Time Slot
                </Label>
                <AvField
                  id="equipped-button-color-mtx-timeSlot"
                  data-cy="timeSlot"
                  type="string"
                  className="form-control"
                  name="timeSlot"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/equipped-button-color-mtx" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  equippedButtonColorMtxEntity: storeState.equippedButtonColorMtx.entity,
  loading: storeState.equippedButtonColorMtx.loading,
  updating: storeState.equippedButtonColorMtx.updating,
  updateSuccess: storeState.equippedButtonColorMtx.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EquippedButtonColorMtxUpdate);
