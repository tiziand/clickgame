import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './point-option.reducer';
import { IPointOption } from 'app/shared/model/paymentService/point-option.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPointOptionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PointOptionUpdate = (props: IPointOptionUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { pointOptionEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/point-option');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...pointOptionEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="apiGatewayApp.paymentServicePointOption.home.createOrEditLabel" data-cy="PointOptionCreateUpdateHeading">
            Create or edit a PointOption
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : pointOptionEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="point-option-id">ID</Label>
                  <AvInput id="point-option-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="pointAmountLabel" for="point-option-pointAmount">
                  Point Amount
                </Label>
                <AvField
                  id="point-option-pointAmount"
                  data-cy="pointAmount"
                  type="string"
                  className="form-control"
                  name="pointAmount"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="priceLabel" for="point-option-price">
                  Price
                </Label>
                <AvField
                  id="point-option-price"
                  data-cy="price"
                  type="string"
                  className="form-control"
                  name="price"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="availableLabel">
                  <AvInput id="point-option-available" data-cy="available" type="checkbox" className="form-check-input" name="available" />
                  Available
                </Label>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/point-option" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  pointOptionEntity: storeState.pointOption.entity,
  loading: storeState.pointOption.loading,
  updating: storeState.pointOption.updating,
  updateSuccess: storeState.pointOption.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PointOptionUpdate);
