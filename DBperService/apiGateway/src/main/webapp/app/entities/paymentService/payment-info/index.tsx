import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PaymentInfo from './payment-info';
import PaymentInfoDetail from './payment-info-detail';
import PaymentInfoUpdate from './payment-info-update';
import PaymentInfoDeleteDialog from './payment-info-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PaymentInfoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PaymentInfoUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PaymentInfoDetail} />
      <ErrorBoundaryRoute path={match.url} component={PaymentInfo} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PaymentInfoDeleteDialog} />
  </>
);

export default Routes;
