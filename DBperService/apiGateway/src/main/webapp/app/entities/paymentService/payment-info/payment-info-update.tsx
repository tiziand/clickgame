import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IPointOption } from 'app/shared/model/paymentService/point-option.model';
import { getEntities as getPointOptions } from 'app/entities/paymentService/point-option/point-option.reducer';
import { getEntity, updateEntity, createEntity, reset } from './payment-info.reducer';
import { IPaymentInfo } from 'app/shared/model/paymentService/payment-info.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPaymentInfoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PaymentInfoUpdate = (props: IPaymentInfoUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { paymentInfoEntity, pointOptions, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/payment-info');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getPointOptions();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...paymentInfoEntity,
        ...values,
        pointOption: pointOptions.find(it => it.id.toString() === values.pointOptionId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="apiGatewayApp.paymentServicePaymentInfo.home.createOrEditLabel" data-cy="PaymentInfoCreateUpdateHeading">
            Create or edit a PaymentInfo
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : paymentInfoEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="payment-info-id">ID</Label>
                  <AvInput id="payment-info-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="userNameLabel" for="payment-info-userName">
                  User Name
                </Label>
                <AvField
                  id="payment-info-userName"
                  data-cy="userName"
                  type="text"
                  name="userName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="paymentInfoLabel" for="payment-info-paymentInfo">
                  Payment Info
                </Label>
                <AvField
                  id="payment-info-paymentInfo"
                  data-cy="paymentInfo"
                  type="text"
                  name="paymentInfo"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="payment-info-pointOption">Point Option</Label>
                <AvInput id="payment-info-pointOption" data-cy="pointOption" type="select" className="form-control" name="pointOptionId">
                  <option value="" key="0" />
                  {pointOptions
                    ? pointOptions.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/payment-info" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  pointOptions: storeState.pointOption.entities,
  paymentInfoEntity: storeState.paymentInfo.entity,
  loading: storeState.paymentInfo.loading,
  updating: storeState.paymentInfo.updating,
  updateSuccess: storeState.paymentInfo.updateSuccess,
});

const mapDispatchToProps = {
  getPointOptions,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PaymentInfoUpdate);
