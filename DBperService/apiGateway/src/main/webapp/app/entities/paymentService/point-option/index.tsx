import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PointOption from './point-option';
import PointOptionDetail from './point-option-detail';
import PointOptionUpdate from './point-option-update';
import PointOptionDeleteDialog from './point-option-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PointOptionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PointOptionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PointOptionDetail} />
      <ErrorBoundaryRoute path={match.url} component={PointOption} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PointOptionDeleteDialog} />
  </>
);

export default Routes;
