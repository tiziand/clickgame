import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IClickPerMin, defaultValue } from 'app/shared/model/statisticService/click-per-min.model';

export const ACTION_TYPES = {
  FETCH_CLICKPERMIN_LIST: 'clickPerMin/FETCH_CLICKPERMIN_LIST',
  FETCH_CLICKPERMIN: 'clickPerMin/FETCH_CLICKPERMIN',
  CREATE_CLICKPERMIN: 'clickPerMin/CREATE_CLICKPERMIN',
  UPDATE_CLICKPERMIN: 'clickPerMin/UPDATE_CLICKPERMIN',
  PARTIAL_UPDATE_CLICKPERMIN: 'clickPerMin/PARTIAL_UPDATE_CLICKPERMIN',
  DELETE_CLICKPERMIN: 'clickPerMin/DELETE_CLICKPERMIN',
  RESET: 'clickPerMin/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IClickPerMin>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ClickPerMinState = Readonly<typeof initialState>;

// Reducer

export default (state: ClickPerMinState = initialState, action): ClickPerMinState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CLICKPERMIN_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CLICKPERMIN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CLICKPERMIN):
    case REQUEST(ACTION_TYPES.UPDATE_CLICKPERMIN):
    case REQUEST(ACTION_TYPES.DELETE_CLICKPERMIN):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_CLICKPERMIN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CLICKPERMIN_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CLICKPERMIN):
    case FAILURE(ACTION_TYPES.CREATE_CLICKPERMIN):
    case FAILURE(ACTION_TYPES.UPDATE_CLICKPERMIN):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_CLICKPERMIN):
    case FAILURE(ACTION_TYPES.DELETE_CLICKPERMIN):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLICKPERMIN_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLICKPERMIN):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CLICKPERMIN):
    case SUCCESS(ACTION_TYPES.UPDATE_CLICKPERMIN):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_CLICKPERMIN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CLICKPERMIN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/click-per-mins';

// Actions

export const getEntities: ICrudGetAllAction<IClickPerMin> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_CLICKPERMIN_LIST,
  payload: axios.get<IClickPerMin>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IClickPerMin> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CLICKPERMIN,
    payload: axios.get<IClickPerMin>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IClickPerMin> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CLICKPERMIN,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IClickPerMin> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CLICKPERMIN,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IClickPerMin> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_CLICKPERMIN,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IClickPerMin> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CLICKPERMIN,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
