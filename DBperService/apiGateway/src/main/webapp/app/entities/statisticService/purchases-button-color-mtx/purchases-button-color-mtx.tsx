import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './purchases-button-color-mtx.reducer';
import { IPurchasesButtonColorMtx } from 'app/shared/model/statisticService/purchases-button-color-mtx.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPurchasesButtonColorMtxProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const PurchasesButtonColorMtx = (props: IPurchasesButtonColorMtxProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { purchasesButtonColorMtxList, match, loading } = props;
  return (
    <div>
      <h2 id="purchases-button-color-mtx-heading" data-cy="PurchasesButtonColorMtxHeading">
        Purchases Button Color Mtxes
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Purchases Button Color Mtx
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {purchasesButtonColorMtxList && purchasesButtonColorMtxList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Mtx Id</th>
                <th>Creation Time</th>
                <th>Amount</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {purchasesButtonColorMtxList.map((purchasesButtonColorMtx, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${purchasesButtonColorMtx.id}`} color="link" size="sm">
                      {purchasesButtonColorMtx.id}
                    </Button>
                  </td>
                  <td>{purchasesButtonColorMtx.mtxId}</td>
                  <td>
                    {purchasesButtonColorMtx.creationTime ? (
                      <TextFormat type="date" value={purchasesButtonColorMtx.creationTime} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>{purchasesButtonColorMtx.amount}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button
                        tag={Link}
                        to={`${match.url}/${purchasesButtonColorMtx.id}`}
                        color="info"
                        size="sm"
                        data-cy="entityDetailsButton"
                      >
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${purchasesButtonColorMtx.id}/edit`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${purchasesButtonColorMtx.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Purchases Button Color Mtxes found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ purchasesButtonColorMtx }: IRootState) => ({
  purchasesButtonColorMtxList: purchasesButtonColorMtx.entities,
  loading: purchasesButtonColorMtx.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PurchasesButtonColorMtx);
