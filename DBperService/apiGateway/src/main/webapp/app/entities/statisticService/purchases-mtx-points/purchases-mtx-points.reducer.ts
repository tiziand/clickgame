import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPurchasesMtxPoints, defaultValue } from 'app/shared/model/statisticService/purchases-mtx-points.model';

export const ACTION_TYPES = {
  FETCH_PURCHASESMTXPOINTS_LIST: 'purchasesMtxPoints/FETCH_PURCHASESMTXPOINTS_LIST',
  FETCH_PURCHASESMTXPOINTS: 'purchasesMtxPoints/FETCH_PURCHASESMTXPOINTS',
  CREATE_PURCHASESMTXPOINTS: 'purchasesMtxPoints/CREATE_PURCHASESMTXPOINTS',
  UPDATE_PURCHASESMTXPOINTS: 'purchasesMtxPoints/UPDATE_PURCHASESMTXPOINTS',
  PARTIAL_UPDATE_PURCHASESMTXPOINTS: 'purchasesMtxPoints/PARTIAL_UPDATE_PURCHASESMTXPOINTS',
  DELETE_PURCHASESMTXPOINTS: 'purchasesMtxPoints/DELETE_PURCHASESMTXPOINTS',
  RESET: 'purchasesMtxPoints/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPurchasesMtxPoints>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PurchasesMtxPointsState = Readonly<typeof initialState>;

// Reducer

export default (state: PurchasesMtxPointsState = initialState, action): PurchasesMtxPointsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PURCHASESMTXPOINTS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PURCHASESMTXPOINTS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PURCHASESMTXPOINTS):
    case REQUEST(ACTION_TYPES.UPDATE_PURCHASESMTXPOINTS):
    case REQUEST(ACTION_TYPES.DELETE_PURCHASESMTXPOINTS):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_PURCHASESMTXPOINTS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PURCHASESMTXPOINTS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PURCHASESMTXPOINTS):
    case FAILURE(ACTION_TYPES.CREATE_PURCHASESMTXPOINTS):
    case FAILURE(ACTION_TYPES.UPDATE_PURCHASESMTXPOINTS):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_PURCHASESMTXPOINTS):
    case FAILURE(ACTION_TYPES.DELETE_PURCHASESMTXPOINTS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PURCHASESMTXPOINTS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PURCHASESMTXPOINTS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PURCHASESMTXPOINTS):
    case SUCCESS(ACTION_TYPES.UPDATE_PURCHASESMTXPOINTS):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_PURCHASESMTXPOINTS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PURCHASESMTXPOINTS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/purchases-mtx-points';

// Actions

export const getEntities: ICrudGetAllAction<IPurchasesMtxPoints> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PURCHASESMTXPOINTS_LIST,
  payload: axios.get<IPurchasesMtxPoints>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPurchasesMtxPoints> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PURCHASESMTXPOINTS,
    payload: axios.get<IPurchasesMtxPoints>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPurchasesMtxPoints> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PURCHASESMTXPOINTS,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPurchasesMtxPoints> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PURCHASESMTXPOINTS,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IPurchasesMtxPoints> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_PURCHASESMTXPOINTS,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPurchasesMtxPoints> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PURCHASESMTXPOINTS,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
