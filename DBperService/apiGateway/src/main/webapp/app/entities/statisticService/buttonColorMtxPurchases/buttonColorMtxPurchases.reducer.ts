import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPurchasesButtonColorMtxTotal, defaultValue } from 'app/shared/model/statisticService/purchasesButtonColorMtxTotal';

export const ACTION_TYPES = {
  FETCH_PURCHASESBUTTONCOLORMTXTOTAL: 'purchasesButtonColorMtx/FETCH_PURCHASESBUTTONCOLORMTXTOTAL',
  RESET: 'purchasesButtonColorMtx/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPurchasesButtonColorMtxTotal>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PurchasesButtonColorMtxTotalState = Readonly<typeof initialState>;

// Reducer

export default (state: PurchasesButtonColorMtxTotalState = initialState, action): PurchasesButtonColorMtxTotalState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTXTOTAL):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTXTOTAL):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTXTOTAL):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/purchases-button-color-mtxes';

// Actions

export const getEntity = (startDate: string, endDate: string) => {
  const requestUrl = `${apiUrl}/purchasesSum/${startDate}/${endDate}`;
  return {
    type: ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTXTOTAL,
    payload: axios.get<IPurchasesButtonColorMtxTotal>(requestUrl),
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
