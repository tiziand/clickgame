import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClickPerMin from './click-per-min';
import ClickPerMinDetail from './click-per-min-detail';
import ClickPerMinUpdate from './click-per-min-update';
import ClickPerMinDeleteDialog from './click-per-min-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ClickPerMinUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ClickPerMinUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClickPerMinDetail} />
      <ErrorBoundaryRoute path={match.url} component={ClickPerMin} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ClickPerMinDeleteDialog} />
  </>
);

export default Routes;
