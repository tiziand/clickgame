import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './button-color-mtx.reducer';
import { IButtonColorMtx } from 'app/shared/model/mtxStoreService/button-color-mtx.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IButtonColorMtxUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ButtonColorMtxUpdate = (props: IButtonColorMtxUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { buttonColorMtxEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/button-color-mtx');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...buttonColorMtxEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="apiGatewayApp.mtxStoreServiceButtonColorMtx.home.createOrEditLabel" data-cy="ButtonColorMtxCreateUpdateHeading">
            Create or edit a ButtonColorMtx
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : buttonColorMtxEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="button-color-mtx-id">ID</Label>
                  <AvInput id="button-color-mtx-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="mtxColorLabel" for="button-color-mtx-mtxColor">
                  Mtx Color
                </Label>
                <AvField
                  id="button-color-mtx-mtxColor"
                  data-cy="mtxColor"
                  type="text"
                  name="mtxColor"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="priceLabel" for="button-color-mtx-price">
                  Price
                </Label>
                <AvField
                  id="button-color-mtx-price"
                  data-cy="price"
                  type="string"
                  className="form-control"
                  name="price"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup check>
                <Label id="availableLabel">
                  <AvInput
                    id="button-color-mtx-available"
                    data-cy="available"
                    type="checkbox"
                    className="form-check-input"
                    name="available"
                  />
                  Available
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="timeLimitLabel" for="button-color-mtx-timeLimit">
                  Time Limit
                </Label>
                <AvField id="button-color-mtx-timeLimit" data-cy="timeLimit" type="string" className="form-control" name="timeLimit" />
              </AvGroup>
              <AvGroup>
                <Label id="pointsRequiredLabel" for="button-color-mtx-pointsRequired">
                  Points Required
                </Label>
                <AvField
                  id="button-color-mtx-pointsRequired"
                  data-cy="pointsRequired"
                  type="string"
                  className="form-control"
                  name="pointsRequired"
                />
              </AvGroup>
              <AvGroup check>
                <Label id="lockedLabel">
                  <AvInput id="button-color-mtx-locked" data-cy="locked" type="checkbox" className="form-check-input" name="locked" />
                  Locked
                </Label>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/button-color-mtx" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  buttonColorMtxEntity: storeState.buttonColorMtx.entity,
  loading: storeState.buttonColorMtx.loading,
  updating: storeState.buttonColorMtx.updating,
  updateSuccess: storeState.buttonColorMtx.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ButtonColorMtxUpdate);
