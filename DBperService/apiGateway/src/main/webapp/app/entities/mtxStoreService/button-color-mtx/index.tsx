import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ButtonColorMtx from './button-color-mtx';
import ButtonColorMtxDetail from './button-color-mtx-detail';
import ButtonColorMtxUpdate from './button-color-mtx-update';
import ButtonColorMtxDeleteDialog from './button-color-mtx-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ButtonColorMtxDetail} />
      <ErrorBoundaryRoute path={match.url} component={ButtonColorMtx} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ButtonColorMtxDeleteDialog} />
  </>
);

export default Routes;
