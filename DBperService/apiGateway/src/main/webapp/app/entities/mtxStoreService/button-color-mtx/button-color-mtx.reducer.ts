import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IButtonColorMtx, defaultValue } from 'app/shared/model/mtxStoreService/button-color-mtx.model';

export const ACTION_TYPES = {
  FETCH_BUTTONCOLORMTX_LIST: 'buttonColorMtx/FETCH_BUTTONCOLORMTX_LIST',
  FETCH_BUTTONCOLORMTX: 'buttonColorMtx/FETCH_BUTTONCOLORMTX',
  CREATE_BUTTONCOLORMTX: 'buttonColorMtx/CREATE_BUTTONCOLORMTX',
  UPDATE_BUTTONCOLORMTX: 'buttonColorMtx/UPDATE_BUTTONCOLORMTX',
  PARTIAL_UPDATE_BUTTONCOLORMTX: 'buttonColorMtx/PARTIAL_UPDATE_BUTTONCOLORMTX',
  DELETE_BUTTONCOLORMTX: 'buttonColorMtx/DELETE_BUTTONCOLORMTX',
  RESET: 'buttonColorMtx/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IButtonColorMtx>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ButtonColorMtxState = Readonly<typeof initialState>;

// Reducer

export default (state: ButtonColorMtxState = initialState, action): ButtonColorMtxState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_BUTTONCOLORMTX_LIST):
    case REQUEST(ACTION_TYPES.FETCH_BUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_BUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.UPDATE_BUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.DELETE_BUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_BUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_BUTTONCOLORMTX_LIST):
    case FAILURE(ACTION_TYPES.FETCH_BUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.CREATE_BUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.UPDATE_BUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_BUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.DELETE_BUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_BUTTONCOLORMTX_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_BUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_BUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.UPDATE_BUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_BUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_BUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/mtxstoreservice/api/button-color-mtxes';

// Actions

export const getEntities: ICrudGetAllAction<IButtonColorMtx> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_BUTTONCOLORMTX_LIST,
  payload: axios.get<IButtonColorMtx>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IButtonColorMtx> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_BUTTONCOLORMTX,
    payload: axios.get<IButtonColorMtx>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_BUTTONCOLORMTX,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_BUTTONCOLORMTX,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_BUTTONCOLORMTX,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IButtonColorMtx> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_BUTTONCOLORMTX,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
