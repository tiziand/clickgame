import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ButtonColorMtx from './mtxStoreService/button-color-mtx';
import UserCount from './statisticService/user-count';
import EquippedButtonColorMtx from './inventoryService/equipped-button-color-mtx';
import PurchasesMtxPoints from './statisticService/purchases-mtx-points';
import PurchasesButtonColorMtx from './statisticService/purchases-button-color-mtx';
import PointsFarmed from './rankingService/points-farmed';
import OwnedButtonColorMtx from './inventoryService/owned-button-color-mtx';
import PaymentInfo from './paymentService/payment-info';
import PointOption from './paymentService/point-option';
import ClickPerMin from './statisticService/click-per-min';
import OwnedMtxPoints from './inventoryService/owned-mtx-points';
import BasketItem from './basketService/basket-item';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}button-color-mtx`} component={ButtonColorMtx} />
      <ErrorBoundaryRoute path={`${match.url}user-count`} component={UserCount} />
      <ErrorBoundaryRoute path={`${match.url}equipped-button-color-mtx`} component={EquippedButtonColorMtx} />
      <ErrorBoundaryRoute path={`${match.url}purchases-mtx-points`} component={PurchasesMtxPoints} />
      <ErrorBoundaryRoute path={`${match.url}purchases-button-color-mtx`} component={PurchasesButtonColorMtx} />
      <ErrorBoundaryRoute path={`${match.url}points-farmed`} component={PointsFarmed} />
      <ErrorBoundaryRoute path={`${match.url}owned-button-color-mtx`} component={OwnedButtonColorMtx} />
      <ErrorBoundaryRoute path={`${match.url}payment-info`} component={PaymentInfo} />
      <ErrorBoundaryRoute path={`${match.url}point-option`} component={PointOption} />
      <ErrorBoundaryRoute path={`${match.url}click-per-min`} component={ClickPerMin} />
      <ErrorBoundaryRoute path={`${match.url}owned-mtx-points`} component={OwnedMtxPoints} />
      <ErrorBoundaryRoute path={`${match.url}basket-item`} component={BasketItem} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
