import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './points-farmed.reducer';
import { IPointsFarmed } from 'app/shared/model/rankingService/points-farmed.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPointsFarmedUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PointsFarmedUpdate = (props: IPointsFarmedUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { pointsFarmedEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/points-farmed' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.creationTime = convertDateTimeToServer(values.creationTime);

    if (errors.length === 0) {
      const entity = {
        ...pointsFarmedEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="apiGatewayApp.rankingServicePointsFarmed.home.createOrEditLabel" data-cy="PointsFarmedCreateUpdateHeading">
            Create or edit a PointsFarmed
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : pointsFarmedEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="points-farmed-id">ID</Label>
                  <AvInput id="points-farmed-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="userNameLabel" for="points-farmed-userName">
                  User Name
                </Label>
                <AvField
                  id="points-farmed-userName"
                  data-cy="userName"
                  type="text"
                  name="userName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="pointsLabel" for="points-farmed-points">
                  Points
                </Label>
                <AvField
                  id="points-farmed-points"
                  data-cy="points"
                  type="string"
                  className="form-control"
                  name="points"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="creationTimeLabel" for="points-farmed-creationTime">
                  Creation Time
                </Label>
                <AvInput
                  id="points-farmed-creationTime"
                  data-cy="creationTime"
                  type="datetime-local"
                  className="form-control"
                  name="creationTime"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.pointsFarmedEntity.creationTime)}
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="timeLimitLabel" for="points-farmed-timeLimit">
                  Time Limit
                </Label>
                <AvField id="points-farmed-timeLimit" data-cy="timeLimit" type="string" className="form-control" name="timeLimit" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/points-farmed" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  pointsFarmedEntity: storeState.pointsFarmed.entity,
  loading: storeState.pointsFarmed.loading,
  updating: storeState.pointsFarmed.updating,
  updateSuccess: storeState.pointsFarmed.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PointsFarmedUpdate);
