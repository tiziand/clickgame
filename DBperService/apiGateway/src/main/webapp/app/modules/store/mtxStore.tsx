import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntitiesOfUser as getOwnedButtonEntities } from 'app/entities/inventoryService/owned-button-color-mtx/owned-button-color-mtx.reducer';
import { getEntities as getEntitiesMTX } from 'app/entities/mtxStoreService/button-color-mtx/button-color-mtx.reducer';
import { getEntityOfUser as getOwnedMtxPoints } from 'app/entities/inventoryService/owned-mtx-points/owned-mtx-points.reducer';
import { getEntitiesUser as getUserBasket, createEntity, deleteEntity } from 'app/entities/basketService/basket-item/basket-item.reducer';
import { IBasketItem } from 'app/shared/model/basketService/basket-item.model';

export interface IMtxStoreProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const MtxStore = (props: IMtxStoreProps) => {
  useEffect(() => {
    props.getOwnedButtonEntities();
    props.getEntitiesMTX();
    props.getOwnedMtxPoints(null);
    props.getUserBasket();
  }, []);

  const alreadyOwned = (id: number): boolean => {
    const foundMTX = props.ownedButtonColorMtxList.find(element => {
      return element.mtxId === id;
    });
    return foundMTX !== undefined;
  };

  const alreadyInBasket = (id: number): boolean => {
    const foundMTX = props.basketItems.find(element => {
      return element.mtxId === id;
    });
    return foundMTX !== undefined;
  };

  const addToBasket = (id: number) => {
    const newBasketItem: IBasketItem = {
      mtxId: id,
      userName: '',
    };
    props.createEntity(newBasketItem);
  };

  const removeFromBasket = (id: number) => {
    const foundMTX = props.basketItems.find(element => {
      return element.mtxId === id;
    });
    props.deleteEntity(foundMTX.id);
  };

  const { buttonColorMtxList, match, bCMloading } = props;
  return (
    <div>
      <h2>Owned MTX Points: {props.ownedMtxPoints.mtxPoints}</h2>
      <h2 id="owned-button-color-mtx-heading">MTX in Store:</h2>
      <div className="table-responsive">
        {buttonColorMtxList && buttonColorMtxList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>Mtx Id</th>
                <th>Color Sample</th>
                <th>Price</th>
                <th>Requirment</th>
                <th>GameModeLock</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {buttonColorMtxList.map((buttonColorMtx, i) => (
                <tr key={`entity-${i}`}>
                  {buttonColorMtx.available && (
                    <>
                      <td>{buttonColorMtx.id}</td>
                      <td>{<Button style={{ backgroundColor: buttonColorMtx.mtxColor }}>Color</Button>}</td>
                      <td>{buttonColorMtx.price}</td>
                      <td>
                        {buttonColorMtx.pointsRequired !== 0
                          ? 'Points: ' +
                            buttonColorMtx.pointsRequired +
                            ' TimeLimit: ' +
                            (buttonColorMtx.timeLimit !== 0 ? buttonColorMtx.timeLimit : 'infinte')
                          : 'none'}
                      </td>
                      <td>
                        {buttonColorMtx.locked
                          ? buttonColorMtx.timeLimit === 0
                            ? 'Locked: infinite'
                            : 'Locked: ' + buttonColorMtx.timeLimit + 's'
                          : 'none'}
                      </td>
                      <td className="text-right">
                        {alreadyOwned(buttonColorMtx.id) ? (
                          <div>already owned</div>
                        ) : alreadyInBasket(buttonColorMtx.id) ? (
                          <div className="btn-group flex-btn-group-container">
                            <Button onClick={() => removeFromBasket(buttonColorMtx.id)} color="danger" size="sm">
                              <span className="d-none d-md-inline">remove from basket</span>
                            </Button>
                          </div>
                        ) : (
                          <div className="btn-group flex-btn-group-container">
                            <Button onClick={() => addToBasket(buttonColorMtx.id)} color="info" size="sm">
                              <span className="d-none d-md-inline">add to Cart</span>
                            </Button>
                          </div>
                        )}
                      </td>
                    </>
                  )}
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !bCMloading && <div className="alert alert-warning">No items in the store</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ ownedButtonColorMtx, buttonColorMtx, ownedMtxPoints, basketItem }: IRootState) => ({
  ownedButtonColorMtxList: ownedButtonColorMtx.entities,
  oBCMloading: ownedButtonColorMtx.loading,
  buttonColorMtxList: buttonColorMtx.entities,
  bCMloading: buttonColorMtx.loading,
  ownedMtxPoints: ownedMtxPoints.entity,
  basketItems: basketItem.entities,
  basketItemsLoading: basketItem.loading,
});

const mapDispatchToProps = {
  getOwnedButtonEntities,
  getEntitiesMTX,
  getOwnedMtxPoints,
  getUserBasket,
  createEntity,
  deleteEntity,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(MtxStore);
