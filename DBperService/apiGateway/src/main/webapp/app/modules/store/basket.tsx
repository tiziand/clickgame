import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table, Alert } from 'reactstrap';

import { IRootState } from 'app/shared/reducers';
import { getEntities as getEntitiesMTX } from 'app/entities/mtxStoreService/button-color-mtx/button-color-mtx.reducer';
import { getEntityOfUser as getOwnedMtxPoints } from 'app/entities/inventoryService/owned-mtx-points/owned-mtx-points.reducer';
import {
  getEntitiesUser as getUserBasket,
  deleteEntity as deleteBasketEntity,
  purchaseBasket,
} from 'app/entities/basketService/basket-item/basket-item.reducer';

export interface IBasketProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Basket = (props: IBasketProps) => {
  let sum = 0;
  const [notEnoughMtxPoints, setNotEnoughMtxPoints] = useState(false);

  useEffect(() => {
    props.getEntitiesMTX();
    props.getUserBasket();
  }, []);

  useEffect(() => {
    props.getOwnedMtxPoints(null);
  }, [props.basketItemsLoading]);

  const findColor = (id: number): string => {
    const foundMTX = props.buttonColorMtxList.find(element => {
      return element.id === id;
    });
    if (foundMTX === undefined) return '';
    return foundMTX.mtxColor;
  };

  const findPrice = (id: number): number => {
    const foundMTX = props.buttonColorMtxList.find(element => {
      return element.id === id;
    });
    if (foundMTX === undefined) return 0;
    sum += foundMTX.price;
    return foundMTX.price;
  };

  const purchase = () => {
    if (props.ownedMtxPoints.mtxPoints < sum) {
      setNotEnoughMtxPoints(true);
      return;
    }
    props.purchaseBasket();
  };

  const { basketItems, match, basketItemsLoading } = props;
  return (
    <div>
      <h2>Owned MTX Points: {props.ownedMtxPoints.mtxPoints}</h2>
      <h2 id="owned-button-color-mtx-heading">MTX in Basket:</h2>
      <div className="table-responsive">
        {basketItems && basketItems.length > 0 ? (
          <>
            <Table responsive>
              <thead>
                <tr>
                  <th>Mtx Id</th>
                  <th>Color Sample</th>
                  <th>Price</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {basketItems.map((buttonColorMtx, i) => (
                  <tr key={`entity-${i}`}>
                    <td>{buttonColorMtx.mtxId}</td>
                    <td>{<Button style={{ backgroundColor: findColor(buttonColorMtx.mtxId) }}>Color</Button>}</td>
                    <td>{findPrice(buttonColorMtx.mtxId)}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button onClick={() => props.deleteBasketEntity(buttonColorMtx.id)} color="danger" size="sm">
                          <span className="d-none d-md-inline">remove from basket</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <h2>Sum: {sum}</h2>
            <Button
              onClick={() => {
                purchase();
              }}
              color="primary"
              size="big"
            >
              <span className="d-none d-md-inline">Purchase</span>
            </Button>
            {notEnoughMtxPoints && <Alert color="danger">Not Enough Points</Alert>}
          </>
        ) : (
          !basketItemsLoading && <div className="alert alert-warning">No items in the basket</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ buttonColorMtx, ownedMtxPoints, basketItem }: IRootState) => ({
  buttonColorMtxList: buttonColorMtx.entities,
  bCMloading: buttonColorMtx.loading,
  ownedMtxPoints: ownedMtxPoints.entity,
  basketItems: basketItem.entities,
  basketItemsLoading: basketItem.loading,
});

const mapDispatchToProps = {
  getEntitiesMTX,
  getOwnedMtxPoints,
  getUserBasket,
  deleteBasketEntity,
  purchaseBasket,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Basket);
