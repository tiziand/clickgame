import React, { useEffect, useState } from 'react';
import { getEntityOfUser as eBCMgetEntityOfUser } from 'app/entities/inventoryService/equipped-button-color-mtx/equipped-button-color-mtx.reducer';
import { getEntity as bCMgetEntity } from 'app/entities/mtxStoreService/button-color-mtx/button-color-mtx.reducer';
import { getSum, getMaxTime } from 'app/entities/rankingService/points-farmed-view/points-farmed-view.reducer';
import { createEntity } from 'app/entities/rankingService/points-farmed/points-farmed.reducer';
import { connect } from 'react-redux';
import { Button, ButtonGroup } from 'reactstrap';
import { IRootState } from 'app/shared/reducers';
import Countdown from 'react-countdown';
import { IPointsFarmed } from 'app/shared/model/rankingService/points-farmed.model';

export interface IGameProps extends StateProps, DispatchProps {}

export const Game = (props: IGameProps) => {
  const [points, setPoints] = useState(0);
  const [timeLimit, setTimeLimit] = useState(0);
  const [timeRunning, setTimeRunning] = useState(false);
  const [countdown, setCountdown] = useState(Date.now());
  const [buttonDisabled, setButtonDisabled] = useState(false);

  useEffect(() => {
    if (props.equippedButtonColorMtx.mtxId !== undefined) {
      props.bCMgetEntity(props.equippedButtonColorMtx.mtxId);
    }
  }, [props.eBCMloading]);

  useEffect(() => {
    if (!props.pFupdating) {
      timeLimit === 0 ? props.getSum(null) : props.getMaxTime(timeLimit);
    }
  }, [props.pFupdating]);

  useEffect(() => {
    if (timeLimit === 0) props.getSum(null);
    else props.getMaxTime(timeLimit);
    setTimeRunning(false);
    setPoints(0);
    setButtonDisabled(false);
    props.eBCMgetEntityOfUser(timeLimit);
  }, [timeLimit]);

  const savePoints = () => {
    const entity: IPointsFarmed = {
      userName: '',
      creationTime: Date.now().toFixed(),
      points,
      timeLimit,
    };
    props.createEntity(entity);
    if (timeLimit === 0) setPoints(0);
  };

  // from https://www.npmjs.com/package/react-countdown
  const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
      // Render a complete state
      return <h2>Time over!</h2>;
    } else {
      // Render a countdown
      return <h2>{seconds}s</h2>;
    }
  };

  return (
    <div>
      <b>TimeLimit:</b>
      <ButtonGroup style={{ paddingLeft: 15, paddingRight: 15 }}>
        <Button
          color="info"
          onClick={() => {
            setTimeLimit(0);
          }}
        >
          Infinite
        </Button>
        <Button
          color="success"
          onClick={() => {
            setTimeLimit(6);
          }}
        >
          6s
        </Button>
        <Button
          color="warning"
          onClick={() => {
            setTimeLimit(4);
          }}
        >
          4s
        </Button>
        <Button
          color="danger"
          onClick={() => {
            setTimeLimit(2);
          }}
        >
          2s
        </Button>
      </ButtonGroup>
      <b>
        Record {timeLimit === 0 ? 'Infinite' : timeLimit + 's'}:{' '}
        {props.pointsFarmedView.points !== undefined && props.pointsFarmedView.points + ' '}
        (Rank: {props.pointsFarmedView.rank !== undefined && props.pointsFarmedView.rank})
      </b>
      {timeLimit === 0 ? (
        // infinite clicking
        <>
          <h1 style={{ paddingLeft: 40 }}>{points}</h1>
          <Button
            style={{ backgroundColor: props.buttonColorMtx.mtxColor }}
            onClick={() => {
              setPoints(points + 1);
            }}
            size="lg"
          >
            ClickMe!
          </Button>
          <a style={{ paddingLeft: 10 }}>
            <Button
              onClick={() => {
                savePoints();
              }}
              color="primary"
              size="sm"
            >
              save
            </Button>
          </a>
        </>
      ) : (
        // timed clicking
        <div>
          {timeRunning ? (
            // time running
            <>
              <Countdown
                date={countdown}
                renderer={renderer}
                onComplete={() => {
                  savePoints();
                  setButtonDisabled(true);
                }}
              />
              <h1 style={{ paddingLeft: 40 }}>{points}</h1>
              <Button
                style={{ backgroundColor: props.buttonColorMtx.mtxColor }}
                onClick={() => {
                  setPoints(points + 1);
                }}
                size="lg"
                disabled={buttonDisabled}
              >
                ClickMe!
              </Button>
              <a style={{ paddingLeft: 10 }}>
                <Button
                  color="primary"
                  size="sm"
                  onClick={() => {
                    setTimeRunning(false);
                    setButtonDisabled(false);
                    setPoints(0);
                  }}
                >
                  Reset!
                </Button>
              </a>
            </>
          ) : (
            // start section
            <>
              <h2>{timeLimit}s</h2>
              <Button
                color="primary"
                onClick={() => {
                  setTimeRunning(true);
                  setCountdown(Date.now() + timeLimit * 1000);
                }}
              >
                Start!
              </Button>
            </>
          )}
        </div>
      )}
    </div>
  );
};

const mapStateToProps = ({ equippedButtonColorMtx, buttonColorMtx, pointsFarmedView, pointsFarmed }: IRootState) => ({
  equippedButtonColorMtx: equippedButtonColorMtx.entity,
  buttonColorMtx: buttonColorMtx.entity,
  eBCMloading: equippedButtonColorMtx.loading,
  bCMloading: buttonColorMtx.loading,
  pointsFarmedView: pointsFarmedView.entity,
  pFVloading: pointsFarmedView.loading,
  pointsFarmed: pointsFarmed.entity,
  pFloading: pointsFarmed.loading,
  pFupdating: pointsFarmed.updating,
});

const mapDispatchToProps = {
  eBCMgetEntityOfUser,
  bCMgetEntity,
  getSum,
  createEntity,
  getMaxTime,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Game);
