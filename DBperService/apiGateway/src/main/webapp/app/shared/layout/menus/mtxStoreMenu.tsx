import React from 'react';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const MtxStoreMenu = props => (
  <NavItem>
    <NavLink tag={Link} to="/mtxstore" className="d-flex align-items-center">
      <span>MTX Store</span>
    </NavLink>
  </NavItem>
);
