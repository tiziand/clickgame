import React from 'react';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const BasketMenu = props => (
  <NavItem>
    <NavLink tag={Link} to="/basket" className="d-flex align-items-center">
      <span>Basket</span>
    </NavLink>
  </NavItem>
);
