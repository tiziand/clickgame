import React from 'react';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const BuyPointsMenu = props => (
  <NavItem>
    <NavLink tag={Link} to="/buypoints" className="d-flex align-items-center">
      <span>Buy MTX Points</span>
    </NavLink>
  </NavItem>
);
