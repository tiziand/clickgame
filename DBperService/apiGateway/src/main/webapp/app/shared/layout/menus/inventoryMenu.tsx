import React from 'react';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const InventoryMenu = props => (
  <NavItem>
    <NavLink tag={Link} to="/inventory" className="d-flex align-items-center">
      <span>Inventory</span>
    </NavLink>
  </NavItem>
);
