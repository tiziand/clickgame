import dayjs from 'dayjs';

export interface IPurchasesButtonColorMtx {
  id?: number;
  mtxId?: number;
  creationTime?: string;
  amount?: number;
}

export const defaultValue: Readonly<IPurchasesButtonColorMtx> = {};
