export interface IMoneySpent {
  sumMoneySpent?: number;
}

export const defaultValue: Readonly<IMoneySpent> = {};
