export interface IButtonColorMtx {
  id?: number;
  mtxColor?: string;
  price?: number;
  available?: boolean;
  timeLimit?: number | null;
  pointsRequired?: number | null;
  locked?: boolean | null;
}

export const defaultValue: Readonly<IButtonColorMtx> = {
  available: false,
  locked: false,
};
