import { Moment } from 'moment';

export interface IPurchasesButtonColorMtxTotal {
  sumPurchases?: number;
}

export const defaultValue: Readonly<IPurchasesButtonColorMtxTotal> = {};
