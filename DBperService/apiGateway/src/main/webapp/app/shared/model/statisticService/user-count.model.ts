import dayjs from 'dayjs';

export interface IUserCount {
  id?: number;
  creationTime?: string;
  userCount?: number;
}

export const defaultValue: Readonly<IUserCount> = {};
