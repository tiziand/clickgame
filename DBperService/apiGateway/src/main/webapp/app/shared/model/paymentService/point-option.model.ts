import { IPaymentInfo } from 'app/shared/model/paymentService/payment-info.model';

export interface IPointOption {
  id?: number;
  pointAmount?: number;
  price?: number;
  available?: boolean;
  paymentInfos?: IPaymentInfo[] | null;
}

export const defaultValue: Readonly<IPointOption> = {
  available: false,
};
