import { Moment } from 'moment';

export interface IPurchasesButtonColorMtxTotalPer {
  mtxId?: number;
  sumPurchases?: number;
}

export const defaultValue: Readonly<IPurchasesButtonColorMtxTotalPer> = {};
