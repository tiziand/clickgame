import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import buttonColorMtx, {
  ButtonColorMtxState
} from 'app/entities/mtxStoreService/button-color-mtx/button-color-mtx.reducer';
// prettier-ignore
import userCount, {
  UserCountState
} from 'app/entities/statisticService/user-count/user-count.reducer';
// prettier-ignore
import equippedButtonColorMtx, {
  EquippedButtonColorMtxState
} from 'app/entities/inventoryService/equipped-button-color-mtx/equipped-button-color-mtx.reducer';
// prettier-ignore
import purchasesMtxPoints, {
  PurchasesMtxPointsState
} from 'app/entities/statisticService/purchases-mtx-points/purchases-mtx-points.reducer';
// prettier-ignore
import purchasesButtonColorMtx, {
  PurchasesButtonColorMtxState
} from 'app/entities/statisticService/purchases-button-color-mtx/purchases-button-color-mtx.reducer';
// prettier-ignore
import pointsFarmed, {
  PointsFarmedState
} from 'app/entities/rankingService/points-farmed/points-farmed.reducer';
// prettier-ignore
import ownedButtonColorMtx, {
  OwnedButtonColorMtxState
} from 'app/entities/inventoryService/owned-button-color-mtx/owned-button-color-mtx.reducer';
// prettier-ignore
import paymentInfo, {
  PaymentInfoState
} from 'app/entities/paymentService/payment-info/payment-info.reducer';
// prettier-ignore
import pointOption, {
  PointOptionState
} from 'app/entities/paymentService/point-option/point-option.reducer';
// prettier-ignore
import clickPerMin, {
  ClickPerMinState
} from 'app/entities/statisticService/click-per-min/click-per-min.reducer';
// prettier-ignore
import ownedMtxPoints, {
  OwnedMtxPointsState
} from 'app/entities/inventoryService/owned-mtx-points/owned-mtx-points.reducer';
// prettier-ignore
import basketItem, {
  BasketItemState
} from 'app/entities/basketService/basket-item/basket-item.reducer';
import moneySpent, { MoneySpentState } from './../../entities/statisticService/moneySpent/moneySpent.reducer';
import clicksTotal, { ClicksTotalState } from './../../entities/statisticService/clicks/clicks.reducer';
import purchasesButtonColorMtxTotal, {
  PurchasesButtonColorMtxTotalState,
} from './../../entities/statisticService/buttonColorMtxPurchases/buttonColorMtxPurchases.reducer';

import purchasesButtonColorMtxTotalPer, {
  PurchasesButtonColorMtxTotalPerState,
} from './../../entities/statisticService/buttonColorMtxPurchases/buttonColorMtxPurchasesList.reducer';

import pointsFarmedView, { PointsFarmedViewState } from './../../entities/rankingService/points-farmed-view/points-farmed-view.reducer';

/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly buttonColorMtx: ButtonColorMtxState;
  readonly userCount: UserCountState;
  readonly equippedButtonColorMtx: EquippedButtonColorMtxState;
  readonly purchasesMtxPoints: PurchasesMtxPointsState;
  readonly purchasesButtonColorMtx: PurchasesButtonColorMtxState;
  readonly pointsFarmed: PointsFarmedState;
  readonly ownedButtonColorMtx: OwnedButtonColorMtxState;
  readonly paymentInfo: PaymentInfoState;
  readonly pointOption: PointOptionState;
  readonly clickPerMin: ClickPerMinState;
  readonly ownedMtxPoints: OwnedMtxPointsState;
  readonly basketItem: BasketItemState;
  readonly pointsFarmedView: PointsFarmedViewState;
  readonly moneySpent: MoneySpentState;
  readonly clicksTotal: ClicksTotalState;
  readonly purchasesButtonColorMtxTotal: PurchasesButtonColorMtxTotalState;
  readonly purchasesButtonColorMtxTotalPer: PurchasesButtonColorMtxTotalPerState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  buttonColorMtx,
  userCount,
  equippedButtonColorMtx,
  purchasesMtxPoints,
  purchasesButtonColorMtx,
  pointsFarmed,
  ownedButtonColorMtx,
  paymentInfo,
  pointOption,
  clickPerMin,
  ownedMtxPoints,
  basketItem,
  pointsFarmedView,
  moneySpent,
  clicksTotal,
  purchasesButtonColorMtxTotal,
  purchasesButtonColorMtxTotalPer,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;
