package bt.browsergame.statistic.repository;

import bt.browsergame.statistic.domain.PaymentInfo;
import bt.browsergame.statistic.domain.views.PurchasesMtxPointsView;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import java.time.Instant;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data SQL repository for the PaymentInfo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentInfoRepository extends JpaRepository<PaymentInfo, Long> {
    @Query("SELECT SUM(pi.pointOption.price) as sumMoneySpent FROM PaymentInfo pi WHERE pi.creationTime >= :startdate AND pi.creationTime <= :enddate")
    PurchasesMtxPointsView getSumMoneySpentSum(@Param("startdate") Instant startDate,
            @Param("enddate") Instant endDate);
}
