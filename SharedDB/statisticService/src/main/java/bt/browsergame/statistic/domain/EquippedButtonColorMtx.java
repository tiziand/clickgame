package bt.browsergame.statistic.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A EquippedButtonColorMtx.
 */
@Entity
@Table(name = "equipped_button_color_mtx")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EquippedButtonColorMtx implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "user_name", nullable = false)
    private String userName;

    @NotNull
    @Column(name = "time_slot", nullable = false)
    private Double timeSlot;

    @ManyToOne
    @JsonIgnoreProperties(value = { "equippedButtonColorMtxes", "ownedButtonColorMtxes", "basketItems" }, allowSetters = true)
    private ButtonColorMtx buttonColorMtx;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EquippedButtonColorMtx id(Long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public EquippedButtonColorMtx userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getTimeSlot() {
        return this.timeSlot;
    }

    public EquippedButtonColorMtx timeSlot(Double timeSlot) {
        this.timeSlot = timeSlot;
        return this;
    }

    public void setTimeSlot(Double timeSlot) {
        this.timeSlot = timeSlot;
    }

    public ButtonColorMtx getButtonColorMtx() {
        return this.buttonColorMtx;
    }

    public EquippedButtonColorMtx buttonColorMtx(ButtonColorMtx buttonColorMtx) {
        this.setButtonColorMtx(buttonColorMtx);
        return this;
    }

    public void setButtonColorMtx(ButtonColorMtx buttonColorMtx) {
        this.buttonColorMtx = buttonColorMtx;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EquippedButtonColorMtx)) {
            return false;
        }
        return id != null && id.equals(((EquippedButtonColorMtx) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EquippedButtonColorMtx{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", timeSlot=" + getTimeSlot() +
            "}";
    }
}
