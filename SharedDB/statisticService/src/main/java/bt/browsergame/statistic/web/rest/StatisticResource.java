package bt.browsergame.statistic.web.rest;

import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bt.browsergame.statistic.domain.views.ClickPerMinView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxAllView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxListView;
import bt.browsergame.statistic.domain.views.PurchasesMtxPointsView;
import bt.browsergame.statistic.service.StatisticService;

@RestController
@RequestMapping("/api")
public class StatisticResource {

    private final Logger log = LoggerFactory.getLogger(StatisticResource.class);

    private final StatisticService statisticService;

    public StatisticResource(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    /**
     * {@code GET  /click-per-mins/clickSum/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get the sum of clicks
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     */
    @GetMapping("/click-per-mins/clicksSum/{startDate}/{endDate}")
    public ResponseEntity<ClickPerMinView> getClicksSum(
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate) {
        log.debug("REST request to get getClicksSum : {}");
        return ResponseEntity.ok(statisticService.getClicksSum(startDate, endDate));
    }

    /**
     * {@code GET  /purchases-button-color-mtxes/purchasesSum/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get the sum of button color mtx purchases
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     */
    @GetMapping("/purchases-button-color-mtxes/purchasesSum/{startDate}/{endDate}")
    public ResponseEntity<PurchasesButtonColorMtxAllView> getPurchasesSum(
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate) {
        log.debug("REST request to get PurchasesButtonColorMtx : {}");
        return ResponseEntity.ok(statisticService.getPurchasesSum(startDate, endDate));
    }

    /**
     * {@code GET  /purchases-button-color-mtxes/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get all the purchasesButtonColorMtxes summed up.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of purchasesButtonColorMtxes in body.
     */
    @GetMapping("/purchases-button-color-mtxes/pruchasesSumList/{startDate}/{endDate}")
    public List<PurchasesButtonColorMtxListView> getAllPurchasesSumList(
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate) {
        log.debug("REST request to get all PurchasesButtonColorMtxes");
        return statisticService.getPurchsesSumList(startDate, endDate);
    }

    /**
     * {@code GET  /purchases-mtx-points/moneySpentSum/:startDate(yyyy-MM-dd)/:endDate(yyyy-MM-dd)}
     * : get the sum of money spent
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     */
    @GetMapping("/purchases-mtx-points/moneySpentSum/{startDate}/{endDate}")
    public ResponseEntity<PurchasesMtxPointsView> getMoneySpentSum(
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar startDate,
            @PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar endDate) {
        log.debug("REST request to get getMoneySpentSum : {}");
        return ResponseEntity.ok(statisticService.getMoneySpentSum(startDate, endDate));
    }
}
