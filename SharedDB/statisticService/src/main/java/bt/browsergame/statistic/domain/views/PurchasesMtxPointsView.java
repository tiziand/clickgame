package bt.browsergame.statistic.domain.views;

public interface PurchasesMtxPointsView {
    Double getSumMoneySpent();
}
