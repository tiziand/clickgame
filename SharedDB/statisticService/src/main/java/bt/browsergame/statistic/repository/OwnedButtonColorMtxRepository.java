package bt.browsergame.statistic.repository;

import bt.browsergame.statistic.domain.OwnedButtonColorMtx;

import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxAllView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxListView;

import java.time.Instant;
import java.util.List;
import org.springframework.data.repository.query.Param;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the OwnedButtonColorMtx entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OwnedButtonColorMtxRepository extends JpaRepository<OwnedButtonColorMtx, Long> {
    @Query("SELECT COUNT(obcm.id) as sumPurchases FROM OwnedButtonColorMtx obcm WHERE obcm.creationTime >= :startdate AND obcm.creationTime <= :enddate AND obcm.mtxId != 1")
    PurchasesButtonColorMtxAllView getSumPurchases(@Param("startdate") Instant startDate,
            @Param("enddate") Instant endDate);

    @Query("SELECT COUNT(obcm.id) as sumPurchases, obcm.mtxId as mtxId FROM OwnedButtonColorMtx obcm WHERE obcm.creationTime >= :startdate AND obcm.creationTime <= :enddate AND obcm.mtxId != 1 GROUP BY obcm.mtxId")
    List<PurchasesButtonColorMtxListView> getSumPurchasesList(@Param("startdate") Instant startDate,
            @Param("enddate") Instant endDate);
}
