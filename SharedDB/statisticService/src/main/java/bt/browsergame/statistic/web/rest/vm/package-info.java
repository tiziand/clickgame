/**
 * View Models used by Spring MVC REST controllers.
 */
package bt.browsergame.statistic.web.rest.vm;
