package bt.browsergame.statistic.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bt.browsergame.statistic.domain.views.ClickPerMinView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxAllView;
import bt.browsergame.statistic.domain.views.PurchasesButtonColorMtxListView;
import bt.browsergame.statistic.domain.views.PurchasesMtxPointsView;
import bt.browsergame.statistic.repository.ClickPerMinRepository;
import bt.browsergame.statistic.repository.OwnedButtonColorMtxRepository;
import bt.browsergame.statistic.repository.PaymentInfoRepository;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class StatisticService {
    private final Logger log = LoggerFactory.getLogger(StatisticService.class);

    private final PaymentInfoRepository paymentInfoRepository;

    private final OwnedButtonColorMtxRepository ownedButtonColorMtxRepository;

    private final ClickPerMinRepository clickPerMinRepository;

    public StatisticService(PaymentInfoRepository paymentInfoRepository,
            OwnedButtonColorMtxRepository ownedButtonColorMtxRepository, ClickPerMinRepository clickPerMinRepository) {
        this.paymentInfoRepository = paymentInfoRepository;
        this.ownedButtonColorMtxRepository = ownedButtonColorMtxRepository;
        this.clickPerMinRepository = clickPerMinRepository;
    }

    /**
     * Get sum of clicks.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public ClickPerMinView getClicksSum(Calendar startDate, Calendar endDate) {
        log.debug("Request to get ClickPerMin : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return clickPerMinRepository.getClicksSum(startInstant, endInstant);
    }

    /**
     * Get sum of button color mtx purchases.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public PurchasesButtonColorMtxAllView getPurchasesSum(Calendar startDate, Calendar endDate) {
        log.debug("Request to get PurchasesButtonColorMtx : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return ownedButtonColorMtxRepository.getSumPurchases(startInstant, endInstant);
    }

    /**
     * Get sum of button color mtx purchases.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public List<PurchasesButtonColorMtxListView> getPurchsesSumList(Calendar startDate, Calendar endDate) {
        log.debug("Request to get PurchasesButtonColorMtx : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return ownedButtonColorMtxRepository.getSumPurchasesList(startInstant, endInstant);
    }

    /**
     * Get sum of money Spent.
     *
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public PurchasesMtxPointsView getMoneySpentSum(Calendar startDate, Calendar endDate) {
        log.debug("Request to get PurchasesMtxPoints : {}");
        Instant startInstant = Instant.ofEpochMilli(startDate.getTimeInMillis());
        Instant endInstant = Instant.ofEpochMilli(endDate.getTimeInMillis());
        endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        return paymentInfoRepository.getSumMoneySpentSum(startInstant, endInstant);
    }
}
