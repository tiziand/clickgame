package bt.browsergame.statistic.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A BasketItem.
 */
@Entity
@Table(name = "basket_item")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class BasketItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "user_name", nullable = false)
    private String userName;

    @NotNull
    @Column(name = "mtx_id", nullable = false)
    private Long mtxId;

    @ManyToOne
    @JsonIgnoreProperties(value = { "equippedButtonColorMtxes", "ownedButtonColorMtxes", "basketItems" }, allowSetters = true)
    private ButtonColorMtx buttonColorMtx;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BasketItem id(Long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public BasketItem userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getMtxId() {
        return this.mtxId;
    }

    public BasketItem mtxId(Long mtxId) {
        this.mtxId = mtxId;
        return this;
    }

    public void setMtxId(Long mtxId) {
        this.mtxId = mtxId;
    }

    public ButtonColorMtx getButtonColorMtx() {
        return this.buttonColorMtx;
    }

    public BasketItem buttonColorMtx(ButtonColorMtx buttonColorMtx) {
        this.setButtonColorMtx(buttonColorMtx);
        return this;
    }

    public void setButtonColorMtx(ButtonColorMtx buttonColorMtx) {
        this.buttonColorMtx = buttonColorMtx;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BasketItem)) {
            return false;
        }
        return id != null && id.equals(((BasketItem) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BasketItem{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", mtxId=" + getMtxId() +
            "}";
    }
}
