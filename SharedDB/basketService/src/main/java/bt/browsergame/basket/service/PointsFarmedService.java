package bt.browsergame.basket.service;

import bt.browsergame.basket.domain.PointsFarmed;
import bt.browsergame.basket.domain.PointsFarmedView;
import bt.browsergame.basket.repository.PointsFarmedRepository;
import bt.browsergame.basket.security.SecurityUtils;
import bt.browsergame.basket.web.rest.errors.BadRequestAlertException;

import java.time.Instant;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PointsFarmed}.
 */
@Service
@Transactional
public class PointsFarmedService {

    private final Logger log = LoggerFactory.getLogger(PointsFarmedService.class);

    private final PointsFarmedRepository pointsFarmedRepository;

    public PointsFarmedService(PointsFarmedRepository pointsFarmedRepository) {
        this.pointsFarmedRepository = pointsFarmedRepository;
    }

    // create if it doesn't exist
    private boolean pointsFarmedViewCheck(PointsFarmedView pointsFarmedView, Double timeLimit) {
        if (pointsFarmedView == null || pointsFarmedView.getPoints() == null || pointsFarmedView.getUserName() == null
                || !pointsFarmedView.getUserName().equals(SecurityUtils.getCurrentUserLogin().get())) {
            PointsFarmed pointsFarmed = new PointsFarmed();
            pointsFarmed.setCreationTime(Instant.now());
            pointsFarmed.setPoints(Long.valueOf(0));
            pointsFarmed.setTimeLimit(timeLimit);
            pointsFarmed.setUserName(SecurityUtils.getCurrentUserLogin().get());
            save(pointsFarmed);
            return false;
        }
        return true;
    }

    private Optional<PointsFarmedView> pointsFarmedOptionalCreator(PointsFarmedView pointsFarmedView) {
        if (pointsFarmedView == null || pointsFarmedView.getPoints() == null || pointsFarmedView.getUserName() == null
                || pointsFarmedView.getRank() == null)
            return Optional.empty();
        return Optional.of(pointsFarmedView);
    }

    /**
     * Save a pointsFarmed.
     *
     * @param pointsFarmed the entity to save.
     * @return the persisted entity.
     */
    public PointsFarmed save(PointsFarmed pointsFarmed) {
        log.debug("Request to save PointsFarmed : {}", pointsFarmed);
        pointsFarmed.setUserName(SecurityUtils.getCurrentUserLogin().get());
        pointsFarmed.setCreationTime(Instant.now());
        if (pointsFarmed.getPoints() > 100 && pointsFarmed.getTimeLimit() != 0 || pointsFarmed.getPoints() > 10000)
            throw new BadRequestAlertException("", "", "Unrealistic amount of points!");
        return pointsFarmedRepository.save(pointsFarmed);
    }

    /**
     * Partially update a pointsFarmed.
     *
     * @param pointsFarmed the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PointsFarmed> partialUpdate(PointsFarmed pointsFarmed) {
        log.debug("Request to partially update PointsFarmed : {}", pointsFarmed);

        return pointsFarmedRepository.findById(pointsFarmed.getId()).map(existingPointsFarmed -> {
            if (pointsFarmed.getUserName() != null) {
                existingPointsFarmed.setUserName(pointsFarmed.getUserName());
            }
            if (pointsFarmed.getPoints() != null) {
                existingPointsFarmed.setPoints(pointsFarmed.getPoints());
            }
            if (pointsFarmed.getCreationTime() != null) {
                existingPointsFarmed.setCreationTime(pointsFarmed.getCreationTime());
            }
            if (pointsFarmed.getTimeLimit() != null) {
                existingPointsFarmed.setTimeLimit(pointsFarmed.getTimeLimit());
            }

            return existingPointsFarmed;
        }).map(pointsFarmedRepository::save);
    }

    /**
     * Get all the pointsFarmeds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PointsFarmed> findAll(Pageable pageable) {
        log.debug("Request to get all PointsFarmeds");
        return pointsFarmedRepository.findAll(pageable);
    }

    /**
     * Get one pointsFarmed by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PointsFarmed> findOne(Long id) {
        log.debug("Request to get PointsFarmed : {}", id);
        return pointsFarmedRepository.findById(id);
    }

    /**
     * Delete the pointsFarmed by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PointsFarmed : {}", id);
        pointsFarmedRepository.deleteById(id);
    }

    /**
     * Get a ranking list of PointsFarmed with sum.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PointsFarmedView> getRankingListSum(Pageable pageable) {
        log.debug("Request to get a Ranking list of points farmed");
        return pointsFarmedRepository.getRankingPointsSum(pageable);
    }

    /**
     * Get a ranking list of PointsFarmed with max based of time.
     *
     * @param pageable  the pagination information.
     * @param timeLimit time limit for the ranking
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PointsFarmedView> getRankingListMax(Pageable pageable, Double timeLimit) {
        log.debug("Request to get a Ranking list of points farmed max and time");
        return pointsFarmedRepository.getRankingPointsMax(pageable, timeLimit);
    }

    /**
     * Get a sum of PointsFarmed from the logged in user.
     *
     * @return return view of the entity.
     */
    public PointsFarmedView getSumPointsFarmed(String userName) {
        log.debug("Request to get sum of points farmed " + userName);
        PointsFarmedView pointsFarmedView = pointsFarmedRepository.getSumPointsFarmed(userName);
        if (pointsFarmedViewCheck(pointsFarmedView, 0.0))
            return pointsFarmedView;
        return pointsFarmedRepository.getSumPointsFarmed(userName);
    }

    /**
     * Get a max of PointsFarmed from the logged in user with given timelimit.
     *
     * @return view of the entity.
     */
    public PointsFarmedView getMaxPointsFarmed(Double timeLimit, String userName) {
        log.debug("Request to get max of points farmed " + userName);
        PointsFarmedView pointsFarmedView = pointsFarmedRepository.getMaxPointsFarmed(timeLimit, userName);
        if (pointsFarmedViewCheck(pointsFarmedView, timeLimit))
            return pointsFarmedView;
        return pointsFarmedRepository.getMaxPointsFarmed(timeLimit, userName);
    }

    /**
     * Get a sum of PointsFarmed from the logged in user.
     *
     * @return return view of the entity.
     */
    public Optional<PointsFarmedView> findSumPointsFarmed(String userName) {
        log.debug("Request to get sum of points farmed " + userName);
        PointsFarmedView pointsFarmedView = pointsFarmedRepository.getSumPointsFarmed(userName);
        return pointsFarmedOptionalCreator(pointsFarmedView);
    }

    /**
     * Get a max of PointsFarmed from the logged in user with given timelimit.
     *
     * @return view of the entity.
     */
    public Optional<PointsFarmedView> findMaxPointsFarmed(Double timeLimit, String userName) {
        log.debug("Request to get max of points farmed " + userName);
        PointsFarmedView pointsFarmedView = pointsFarmedRepository.getMaxPointsFarmed(timeLimit, userName);
        return pointsFarmedOptionalCreator(pointsFarmedView);
    }
}
