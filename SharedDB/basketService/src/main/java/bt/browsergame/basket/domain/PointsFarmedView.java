package bt.browsergame.basket.domain;

public interface PointsFarmedView {
    String getUserName();

    Long getPoints();

    Long getRank();
}
