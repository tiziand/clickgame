/**
 * View Models used by Spring MVC REST controllers.
 */
package bt.browsergame.basket.web.rest.vm;
