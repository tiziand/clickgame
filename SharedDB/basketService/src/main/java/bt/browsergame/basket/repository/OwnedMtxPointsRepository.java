package bt.browsergame.basket.repository;

import bt.browsergame.basket.domain.OwnedMtxPoints;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the OwnedMtxPoints entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OwnedMtxPointsRepository extends JpaRepository<OwnedMtxPoints, Long> {}
