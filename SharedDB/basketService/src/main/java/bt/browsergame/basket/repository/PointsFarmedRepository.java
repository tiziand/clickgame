package bt.browsergame.basket.repository;

import bt.browsergame.basket.domain.PointsFarmed;
import bt.browsergame.basket.domain.PointsFarmedView;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
/**
 * Spring Data SQL repository for the PointsFarmed entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PointsFarmedRepository extends JpaRepository<PointsFarmed, Long> {
    @Query(value = "WITH tbl as (SELECT pf.user_name as userName, SUM(pf.points) as points FROM points_farmed pf GROUP BY userName) SELECT username, points, RANK() OVER(ORDER BY points DESC) as rank from tbl ORDER BY rank ASC", countQuery = "SELECT count(user_name) FROM points_farmed GROUP BY user_name", nativeQuery = true)
    Page<PointsFarmedView> getRankingPointsSum(Pageable pageable);

    @Query(value = "WITH tbl as (SELECT pf.user_name as userName, MAX(pf.points) as points FROM points_farmed pf WHERE pf.time_limit = :time GROUP BY userName) SELECT username, points, RANK() OVER(ORDER BY points DESC) as rank from tbl ORDER BY rank ASC", countQuery = "SELECT count(user_name) FROM points_farmed GROUP BY user_name", nativeQuery = true)
    Page<PointsFarmedView> getRankingPointsMax(Pageable pageable, @Param("time") Double time);

    @Query(value = "WITH tbl_outer as (WITH tbl as (SELECT pf.user_name as userName, MAX(pf.points) as points FROM points_farmed pf WHERE pf.time_limit = :time GROUP BY userName) SELECT userName, points, RANK() OVER(ORDER BY points DESC) as rank from tbl) Select userName, points, rank from tbl_outer WHERE tbl_outer.userName = :username", nativeQuery = true)
    PointsFarmedView getMaxPointsFarmed(@Param("time") Double time, @Param("username") String userName);

    @Query(value = "WITH tbl_outer as (WITH tbl as (SELECT pf.user_name as userName, SUM(pf.points) as points FROM points_farmed pf GROUP BY userName) SELECT userName, points, RANK() OVER(ORDER BY points DESC) as rank from tbl) Select userName, points, rank from tbl_outer WHERE tbl_outer.userName = :username", nativeQuery = true)
    PointsFarmedView getSumPointsFarmed(@Param("username") String userName);
}
