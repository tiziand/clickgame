package bt.browsergame.basket.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bt.browsergame.basket.domain.BasketItem;
import bt.browsergame.basket.domain.OwnedButtonColorMtx;
import bt.browsergame.basket.domain.PointsFarmedView;
import bt.browsergame.basket.repository.BasketItemRepository;
import bt.browsergame.basket.repository.OwnedButtonColorMtxRepository;
import bt.browsergame.basket.security.SecurityUtils;
import bt.browsergame.basket.web.rest.errors.BadRequestAlertException;

/**
 * Service Implementation for managing {@link BasketItem}.
 */
@Service
@Transactional
public class BasketItemService {

    private final Logger log = LoggerFactory.getLogger(BasketItemService.class);

    private final BasketItemRepository basketItemRepository;

    private final OwnedMtxPointsService ownedMtxPointsService;

    private final OwnedButtonColorMtxRepository ownedButtonColorMtxRepository;

    private final PointsFarmedService pointsFarmedService;

    @Autowired
    public BasketItemService(BasketItemRepository basketItemRepository, OwnedMtxPointsService ownedMtxPointsService,
            OwnedButtonColorMtxRepository ownedButtonColorMtxRepository, PointsFarmedService pointsFarmedService) {
        this.basketItemRepository = basketItemRepository;
        this.ownedMtxPointsService = ownedMtxPointsService;
        this.ownedButtonColorMtxRepository = ownedButtonColorMtxRepository;
        this.pointsFarmedService = pointsFarmedService;
    }

    /**
     * Save a basketItem.
     *
     * @param basketItem the entity to save.
     * @return the persisted entity.
     */
    public BasketItem save(BasketItem basketItem) {
        log.debug("Request to save BasketItem : {}", basketItem);

        basketItem.setUserName(SecurityUtils.getCurrentUserLogin().get());

        // check requirments before adding to basket
        if (!basketItem.getButtonColorMtx().getAvailable())
            throw new BadRequestAlertException("", "", "not available");
        if (basketItem.getButtonColorMtx().getPointsRequired() != 0) {
            PointsFarmedView pointsFarmed;
            if (basketItem.getButtonColorMtx().getTimeLimit() == 0)
                pointsFarmed = pointsFarmedService.getSumPointsFarmed(SecurityUtils.getCurrentUserLogin().get());
            else
                pointsFarmed = pointsFarmedService.getMaxPointsFarmed(basketItem.getButtonColorMtx().getTimeLimit(),
                        SecurityUtils.getCurrentUserLogin().get());
            if (pointsFarmed.getPoints() < basketItem.getButtonColorMtx().getPointsRequired())
                throw new BadRequestAlertException("", "", "Requirements not met!");
        }
        return basketItemRepository.save(basketItem);
    }

    /**
     * Partially update a basketItem.
     *
     * @param basketItem the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<BasketItem> partialUpdate(BasketItem basketItem) {
        log.debug("Request to partially update BasketItem : {}", basketItem);

        return basketItemRepository.findById(basketItem.getId()).map(existingBasketItem -> {
            if (basketItem.getUserName() != null) {
                existingBasketItem.setUserName(basketItem.getUserName());
            }
            if (basketItem.getMtxId() != null) {
                existingBasketItem.setMtxId(basketItem.getMtxId());
            }

            return existingBasketItem;
        }).map(basketItemRepository::save);
    }

    /**
     * Get all the basketItems.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BasketItem> findAll() {
        log.debug("Request to get all BasketItems");
        return basketItemRepository.findAll();
    }

    /**
     * Get one basketItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BasketItem> findOne(Long id) {
        log.debug("Request to get BasketItem : {}", id);
        return basketItemRepository.findById(id);
    }

    /**
     * Delete the basketItem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BasketItem : {}", id);
        basketItemRepository.deleteById(id);
    }

    /**
     * Get the basketItems of the logged in user.
     *
     */
    public List<BasketItem> getUserBasket() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxId", "buttonColorMtx");
        BasketItem exBasketItem = new BasketItem();
        exBasketItem.setUserName(SecurityUtils.getCurrentUserLogin().get());
        Example<BasketItem> example = Example.of(exBasketItem, usernameMatcher);
        return basketItemRepository.findAll(example);
    }

    /**
     * Purchases basketItems of the logged in user.
     *
     */
    public List<BasketItem> purchaseBasket() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        List<BasketItem> basketItems = getUserBasket();

        // calculate cost of the basket and remove mtxPoints if enough are available
        int sum = 0;
        for (BasketItem basketItem : basketItems) {
            sum += basketItem.getButtonColorMtx().getPrice();
        }
        if (ownedMtxPointsService.getOwnedMtxPointsOfUser().getMtxPoints() < sum)
            return basketItems;

        // add mtx to inventory
        for (BasketItem basketItem : basketItems) {
            OwnedButtonColorMtx ownedButtonColorMtx = new OwnedButtonColorMtx();
            ownedButtonColorMtx.setButtonColorMtx(basketItem.getButtonColorMtx());
            ownedButtonColorMtx.setCreationTime(Instant.now());
            ownedButtonColorMtx.setMtxId(basketItem.getMtxId());
            ownedButtonColorMtx.setUserName(SecurityUtils.getCurrentUserLogin().get());
            ownedButtonColorMtxRepository.save(ownedButtonColorMtx);
            delete(basketItem.getId());
        }

        // subtract mtx points
        ownedMtxPointsService.subtractMtxPoints(sum);

        basketItems.clear();
        return basketItems;
    }
}
