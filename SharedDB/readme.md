# Running the application:

## starting in production mode:

### needed application

- docker

### instructions

- published on docker https://hub.docker.com/u/tiziand
- navagiate to docker-compose folder
- run the following command:
  `docker-compose up`
- if there are timeout issues on slow cpus increase jhipster-sleep values incrementally in the docker compose start the services one after another
- wait for them to finish starting
- application can be found on localhost:9000

## to compile the applications:

### needed application

- docker
- java
- node

### instructions

- navigate to docker-compose/postgres
- start the shared db with:
  `docker-compose up -d`
- navagiate to docker-compose folder
- run the following to start the jhipster registry:
  `docker-compose -f jhipster-registry.yml up`
- Navigate to all the microservice folders and run the follwoing to compile and start them:
  `./mvnw`
- application can be found on localhost:9000
