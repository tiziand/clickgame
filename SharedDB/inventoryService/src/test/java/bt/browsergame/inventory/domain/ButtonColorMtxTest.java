package bt.browsergame.inventory.domain;

import static org.assertj.core.api.Assertions.assertThat;

import bt.browsergame.inventory.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ButtonColorMtxTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ButtonColorMtx.class);
        ButtonColorMtx buttonColorMtx1 = new ButtonColorMtx();
        buttonColorMtx1.setId(1L);
        ButtonColorMtx buttonColorMtx2 = new ButtonColorMtx();
        buttonColorMtx2.setId(buttonColorMtx1.getId());
        assertThat(buttonColorMtx1).isEqualTo(buttonColorMtx2);
        buttonColorMtx2.setId(2L);
        assertThat(buttonColorMtx1).isNotEqualTo(buttonColorMtx2);
        buttonColorMtx1.setId(null);
        assertThat(buttonColorMtx1).isNotEqualTo(buttonColorMtx2);
    }
}
