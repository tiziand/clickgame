package bt.browsergame.inventory.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import bt.browsergame.inventory.IntegrationTest;
import bt.browsergame.inventory.domain.ButtonColorMtx;
import bt.browsergame.inventory.repository.ButtonColorMtxRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ButtonColorMtxResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ButtonColorMtxResourceIT {

    private static final String DEFAULT_MTX_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_MTX_COLOR = "BBBBBBBBBB";

    private static final Integer DEFAULT_PRICE = 1;
    private static final Integer UPDATED_PRICE = 2;

    private static final Boolean DEFAULT_AVAILABLE = false;
    private static final Boolean UPDATED_AVAILABLE = true;

    private static final Double DEFAULT_TIME_LIMIT = 1D;
    private static final Double UPDATED_TIME_LIMIT = 2D;

    private static final Long DEFAULT_POINTS_REQUIRED = 1L;
    private static final Long UPDATED_POINTS_REQUIRED = 2L;

    private static final Boolean DEFAULT_LOCKED = false;
    private static final Boolean UPDATED_LOCKED = true;

    private static final String ENTITY_API_URL = "/api/button-color-mtxes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ButtonColorMtxRepository buttonColorMtxRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restButtonColorMtxMockMvc;

    private ButtonColorMtx buttonColorMtx;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ButtonColorMtx createEntity(EntityManager em) {
        ButtonColorMtx buttonColorMtx = new ButtonColorMtx()
            .mtxColor(DEFAULT_MTX_COLOR)
            .price(DEFAULT_PRICE)
            .available(DEFAULT_AVAILABLE)
            .timeLimit(DEFAULT_TIME_LIMIT)
            .pointsRequired(DEFAULT_POINTS_REQUIRED)
            .locked(DEFAULT_LOCKED);
        return buttonColorMtx;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ButtonColorMtx createUpdatedEntity(EntityManager em) {
        ButtonColorMtx buttonColorMtx = new ButtonColorMtx()
            .mtxColor(UPDATED_MTX_COLOR)
            .price(UPDATED_PRICE)
            .available(UPDATED_AVAILABLE)
            .timeLimit(UPDATED_TIME_LIMIT)
            .pointsRequired(UPDATED_POINTS_REQUIRED)
            .locked(UPDATED_LOCKED);
        return buttonColorMtx;
    }

    @BeforeEach
    public void initTest() {
        buttonColorMtx = createEntity(em);
    }

    @Test
    @Transactional
    void createButtonColorMtx() throws Exception {
        int databaseSizeBeforeCreate = buttonColorMtxRepository.findAll().size();
        // Create the ButtonColorMtx
        restButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isCreated());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeCreate + 1);
        ButtonColorMtx testButtonColorMtx = buttonColorMtxList.get(buttonColorMtxList.size() - 1);
        assertThat(testButtonColorMtx.getMtxColor()).isEqualTo(DEFAULT_MTX_COLOR);
        assertThat(testButtonColorMtx.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testButtonColorMtx.getAvailable()).isEqualTo(DEFAULT_AVAILABLE);
        assertThat(testButtonColorMtx.getTimeLimit()).isEqualTo(DEFAULT_TIME_LIMIT);
        assertThat(testButtonColorMtx.getPointsRequired()).isEqualTo(DEFAULT_POINTS_REQUIRED);
        assertThat(testButtonColorMtx.getLocked()).isEqualTo(DEFAULT_LOCKED);
    }

    @Test
    @Transactional
    void createButtonColorMtxWithExistingId() throws Exception {
        // Create the ButtonColorMtx with an existing ID
        buttonColorMtx.setId(1L);

        int databaseSizeBeforeCreate = buttonColorMtxRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMtxColorIsRequired() throws Exception {
        int databaseSizeBeforeTest = buttonColorMtxRepository.findAll().size();
        // set the field null
        buttonColorMtx.setMtxColor(null);

        // Create the ButtonColorMtx, which fails.

        restButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = buttonColorMtxRepository.findAll().size();
        // set the field null
        buttonColorMtx.setPrice(null);

        // Create the ButtonColorMtx, which fails.

        restButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAvailableIsRequired() throws Exception {
        int databaseSizeBeforeTest = buttonColorMtxRepository.findAll().size();
        // set the field null
        buttonColorMtx.setAvailable(null);

        // Create the ButtonColorMtx, which fails.

        restButtonColorMtxMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllButtonColorMtxes() throws Exception {
        // Initialize the database
        buttonColorMtxRepository.saveAndFlush(buttonColorMtx);

        // Get all the buttonColorMtxList
        restButtonColorMtxMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(buttonColorMtx.getId().intValue())))
            .andExpect(jsonPath("$.[*].mtxColor").value(hasItem(DEFAULT_MTX_COLOR)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].available").value(hasItem(DEFAULT_AVAILABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].timeLimit").value(hasItem(DEFAULT_TIME_LIMIT.doubleValue())))
            .andExpect(jsonPath("$.[*].pointsRequired").value(hasItem(DEFAULT_POINTS_REQUIRED.intValue())))
            .andExpect(jsonPath("$.[*].locked").value(hasItem(DEFAULT_LOCKED.booleanValue())));
    }

    @Test
    @Transactional
    void getButtonColorMtx() throws Exception {
        // Initialize the database
        buttonColorMtxRepository.saveAndFlush(buttonColorMtx);

        // Get the buttonColorMtx
        restButtonColorMtxMockMvc
            .perform(get(ENTITY_API_URL_ID, buttonColorMtx.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(buttonColorMtx.getId().intValue()))
            .andExpect(jsonPath("$.mtxColor").value(DEFAULT_MTX_COLOR))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE))
            .andExpect(jsonPath("$.available").value(DEFAULT_AVAILABLE.booleanValue()))
            .andExpect(jsonPath("$.timeLimit").value(DEFAULT_TIME_LIMIT.doubleValue()))
            .andExpect(jsonPath("$.pointsRequired").value(DEFAULT_POINTS_REQUIRED.intValue()))
            .andExpect(jsonPath("$.locked").value(DEFAULT_LOCKED.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingButtonColorMtx() throws Exception {
        // Get the buttonColorMtx
        restButtonColorMtxMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewButtonColorMtx() throws Exception {
        // Initialize the database
        buttonColorMtxRepository.saveAndFlush(buttonColorMtx);

        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();

        // Update the buttonColorMtx
        ButtonColorMtx updatedButtonColorMtx = buttonColorMtxRepository.findById(buttonColorMtx.getId()).get();
        // Disconnect from session so that the updates on updatedButtonColorMtx are not directly saved in db
        em.detach(updatedButtonColorMtx);
        updatedButtonColorMtx
            .mtxColor(UPDATED_MTX_COLOR)
            .price(UPDATED_PRICE)
            .available(UPDATED_AVAILABLE)
            .timeLimit(UPDATED_TIME_LIMIT)
            .pointsRequired(UPDATED_POINTS_REQUIRED)
            .locked(UPDATED_LOCKED);

        restButtonColorMtxMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedButtonColorMtx.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedButtonColorMtx))
            )
            .andExpect(status().isOk());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        ButtonColorMtx testButtonColorMtx = buttonColorMtxList.get(buttonColorMtxList.size() - 1);
        assertThat(testButtonColorMtx.getMtxColor()).isEqualTo(UPDATED_MTX_COLOR);
        assertThat(testButtonColorMtx.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testButtonColorMtx.getAvailable()).isEqualTo(UPDATED_AVAILABLE);
        assertThat(testButtonColorMtx.getTimeLimit()).isEqualTo(UPDATED_TIME_LIMIT);
        assertThat(testButtonColorMtx.getPointsRequired()).isEqualTo(UPDATED_POINTS_REQUIRED);
        assertThat(testButtonColorMtx.getLocked()).isEqualTo(UPDATED_LOCKED);
    }

    @Test
    @Transactional
    void putNonExistingButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();
        buttonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restButtonColorMtxMockMvc
            .perform(
                put(ENTITY_API_URL_ID, buttonColorMtx.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();
        buttonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restButtonColorMtxMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();
        buttonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restButtonColorMtxMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(buttonColorMtx)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        buttonColorMtxRepository.saveAndFlush(buttonColorMtx);

        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();

        // Update the buttonColorMtx using partial update
        ButtonColorMtx partialUpdatedButtonColorMtx = new ButtonColorMtx();
        partialUpdatedButtonColorMtx.setId(buttonColorMtx.getId());

        partialUpdatedButtonColorMtx.mtxColor(UPDATED_MTX_COLOR).available(UPDATED_AVAILABLE).locked(UPDATED_LOCKED);

        restButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedButtonColorMtx.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedButtonColorMtx))
            )
            .andExpect(status().isOk());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        ButtonColorMtx testButtonColorMtx = buttonColorMtxList.get(buttonColorMtxList.size() - 1);
        assertThat(testButtonColorMtx.getMtxColor()).isEqualTo(UPDATED_MTX_COLOR);
        assertThat(testButtonColorMtx.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testButtonColorMtx.getAvailable()).isEqualTo(UPDATED_AVAILABLE);
        assertThat(testButtonColorMtx.getTimeLimit()).isEqualTo(DEFAULT_TIME_LIMIT);
        assertThat(testButtonColorMtx.getPointsRequired()).isEqualTo(DEFAULT_POINTS_REQUIRED);
        assertThat(testButtonColorMtx.getLocked()).isEqualTo(UPDATED_LOCKED);
    }

    @Test
    @Transactional
    void fullUpdateButtonColorMtxWithPatch() throws Exception {
        // Initialize the database
        buttonColorMtxRepository.saveAndFlush(buttonColorMtx);

        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();

        // Update the buttonColorMtx using partial update
        ButtonColorMtx partialUpdatedButtonColorMtx = new ButtonColorMtx();
        partialUpdatedButtonColorMtx.setId(buttonColorMtx.getId());

        partialUpdatedButtonColorMtx
            .mtxColor(UPDATED_MTX_COLOR)
            .price(UPDATED_PRICE)
            .available(UPDATED_AVAILABLE)
            .timeLimit(UPDATED_TIME_LIMIT)
            .pointsRequired(UPDATED_POINTS_REQUIRED)
            .locked(UPDATED_LOCKED);

        restButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedButtonColorMtx.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedButtonColorMtx))
            )
            .andExpect(status().isOk());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
        ButtonColorMtx testButtonColorMtx = buttonColorMtxList.get(buttonColorMtxList.size() - 1);
        assertThat(testButtonColorMtx.getMtxColor()).isEqualTo(UPDATED_MTX_COLOR);
        assertThat(testButtonColorMtx.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testButtonColorMtx.getAvailable()).isEqualTo(UPDATED_AVAILABLE);
        assertThat(testButtonColorMtx.getTimeLimit()).isEqualTo(UPDATED_TIME_LIMIT);
        assertThat(testButtonColorMtx.getPointsRequired()).isEqualTo(UPDATED_POINTS_REQUIRED);
        assertThat(testButtonColorMtx.getLocked()).isEqualTo(UPDATED_LOCKED);
    }

    @Test
    @Transactional
    void patchNonExistingButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();
        buttonColorMtx.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, buttonColorMtx.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();
        buttonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isBadRequest());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamButtonColorMtx() throws Exception {
        int databaseSizeBeforeUpdate = buttonColorMtxRepository.findAll().size();
        buttonColorMtx.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restButtonColorMtxMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(buttonColorMtx))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ButtonColorMtx in the database
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteButtonColorMtx() throws Exception {
        // Initialize the database
        buttonColorMtxRepository.saveAndFlush(buttonColorMtx);

        int databaseSizeBeforeDelete = buttonColorMtxRepository.findAll().size();

        // Delete the buttonColorMtx
        restButtonColorMtxMockMvc
            .perform(delete(ENTITY_API_URL_ID, buttonColorMtx.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ButtonColorMtx> buttonColorMtxList = buttonColorMtxRepository.findAll();
        assertThat(buttonColorMtxList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
