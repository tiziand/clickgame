package bt.browsergame.inventory.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ButtonColorMtx.
 */
@Entity
@Table(name = "button_color_mtx")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ButtonColorMtx implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "mtx_color", nullable = false)
    private String mtxColor;

    @NotNull
    @Column(name = "price", nullable = false)
    private Integer price;

    @NotNull
    @Column(name = "available", nullable = false)
    private Boolean available;

    @Column(name = "time_limit")
    private Double timeLimit;

    @Column(name = "points_required")
    private Long pointsRequired;

    @Column(name = "locked")
    private Boolean locked;

    @OneToMany(mappedBy = "buttonColorMtx")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "buttonColorMtx" }, allowSetters = true)
    private Set<EquippedButtonColorMtx> equippedButtonColorMtxes = new HashSet<>();

    @OneToMany(mappedBy = "buttonColorMtx")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "buttonColorMtx" }, allowSetters = true)
    private Set<OwnedButtonColorMtx> ownedButtonColorMtxes = new HashSet<>();

    @OneToMany(mappedBy = "buttonColorMtx")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "buttonColorMtx" }, allowSetters = true)
    private Set<BasketItem> basketItems = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ButtonColorMtx id(Long id) {
        this.id = id;
        return this;
    }

    public String getMtxColor() {
        return this.mtxColor;
    }

    public ButtonColorMtx mtxColor(String mtxColor) {
        this.mtxColor = mtxColor;
        return this;
    }

    public void setMtxColor(String mtxColor) {
        this.mtxColor = mtxColor;
    }

    public Integer getPrice() {
        return this.price;
    }

    public ButtonColorMtx price(Integer price) {
        this.price = price;
        return this;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    public ButtonColorMtx available(Boolean available) {
        this.available = available;
        return this;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Double getTimeLimit() {
        return this.timeLimit;
    }

    public ButtonColorMtx timeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
        return this;
    }

    public void setTimeLimit(Double timeLimit) {
        this.timeLimit = timeLimit;
    }

    public Long getPointsRequired() {
        return this.pointsRequired;
    }

    public ButtonColorMtx pointsRequired(Long pointsRequired) {
        this.pointsRequired = pointsRequired;
        return this;
    }

    public void setPointsRequired(Long pointsRequired) {
        this.pointsRequired = pointsRequired;
    }

    public Boolean getLocked() {
        return this.locked;
    }

    public ButtonColorMtx locked(Boolean locked) {
        this.locked = locked;
        return this;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Set<EquippedButtonColorMtx> getEquippedButtonColorMtxes() {
        return this.equippedButtonColorMtxes;
    }

    public ButtonColorMtx equippedButtonColorMtxes(Set<EquippedButtonColorMtx> equippedButtonColorMtxes) {
        this.setEquippedButtonColorMtxes(equippedButtonColorMtxes);
        return this;
    }

    public ButtonColorMtx addEquippedButtonColorMtx(EquippedButtonColorMtx equippedButtonColorMtx) {
        this.equippedButtonColorMtxes.add(equippedButtonColorMtx);
        equippedButtonColorMtx.setButtonColorMtx(this);
        return this;
    }

    public ButtonColorMtx removeEquippedButtonColorMtx(EquippedButtonColorMtx equippedButtonColorMtx) {
        this.equippedButtonColorMtxes.remove(equippedButtonColorMtx);
        equippedButtonColorMtx.setButtonColorMtx(null);
        return this;
    }

    public void setEquippedButtonColorMtxes(Set<EquippedButtonColorMtx> equippedButtonColorMtxes) {
        if (this.equippedButtonColorMtxes != null) {
            this.equippedButtonColorMtxes.forEach(i -> i.setButtonColorMtx(null));
        }
        if (equippedButtonColorMtxes != null) {
            equippedButtonColorMtxes.forEach(i -> i.setButtonColorMtx(this));
        }
        this.equippedButtonColorMtxes = equippedButtonColorMtxes;
    }

    public Set<OwnedButtonColorMtx> getOwnedButtonColorMtxes() {
        return this.ownedButtonColorMtxes;
    }

    public ButtonColorMtx ownedButtonColorMtxes(Set<OwnedButtonColorMtx> ownedButtonColorMtxes) {
        this.setOwnedButtonColorMtxes(ownedButtonColorMtxes);
        return this;
    }

    public ButtonColorMtx addOwnedButtonColorMtx(OwnedButtonColorMtx ownedButtonColorMtx) {
        this.ownedButtonColorMtxes.add(ownedButtonColorMtx);
        ownedButtonColorMtx.setButtonColorMtx(this);
        return this;
    }

    public ButtonColorMtx removeOwnedButtonColorMtx(OwnedButtonColorMtx ownedButtonColorMtx) {
        this.ownedButtonColorMtxes.remove(ownedButtonColorMtx);
        ownedButtonColorMtx.setButtonColorMtx(null);
        return this;
    }

    public void setOwnedButtonColorMtxes(Set<OwnedButtonColorMtx> ownedButtonColorMtxes) {
        if (this.ownedButtonColorMtxes != null) {
            this.ownedButtonColorMtxes.forEach(i -> i.setButtonColorMtx(null));
        }
        if (ownedButtonColorMtxes != null) {
            ownedButtonColorMtxes.forEach(i -> i.setButtonColorMtx(this));
        }
        this.ownedButtonColorMtxes = ownedButtonColorMtxes;
    }

    public Set<BasketItem> getBasketItems() {
        return this.basketItems;
    }

    public ButtonColorMtx basketItems(Set<BasketItem> basketItems) {
        this.setBasketItems(basketItems);
        return this;
    }

    public ButtonColorMtx addBasketItem(BasketItem basketItem) {
        this.basketItems.add(basketItem);
        basketItem.setButtonColorMtx(this);
        return this;
    }

    public ButtonColorMtx removeBasketItem(BasketItem basketItem) {
        this.basketItems.remove(basketItem);
        basketItem.setButtonColorMtx(null);
        return this;
    }

    public void setBasketItems(Set<BasketItem> basketItems) {
        if (this.basketItems != null) {
            this.basketItems.forEach(i -> i.setButtonColorMtx(null));
        }
        if (basketItems != null) {
            basketItems.forEach(i -> i.setButtonColorMtx(this));
        }
        this.basketItems = basketItems;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ButtonColorMtx)) {
            return false;
        }
        return id != null && id.equals(((ButtonColorMtx) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ButtonColorMtx{" +
            "id=" + getId() +
            ", mtxColor='" + getMtxColor() + "'" +
            ", price=" + getPrice() +
            ", available='" + getAvailable() + "'" +
            ", timeLimit=" + getTimeLimit() +
            ", pointsRequired=" + getPointsRequired() +
            ", locked='" + getLocked() + "'" +
            "}";
    }
}
