package bt.browsergame.inventory.repository;

import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the OwnedButtonColorMtx entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OwnedButtonColorMtxRepository extends JpaRepository<OwnedButtonColorMtx, Long> {}
