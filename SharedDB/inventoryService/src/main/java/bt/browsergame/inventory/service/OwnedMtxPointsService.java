package bt.browsergame.inventory.service;

import bt.browsergame.inventory.domain.OwnedMtxPoints;
import bt.browsergame.inventory.repository.OwnedMtxPointsRepository;
import bt.browsergame.inventory.security.SecurityUtils;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link OwnedMtxPoints}.
 */
@Service
@Transactional
public class OwnedMtxPointsService {

    private final Logger log = LoggerFactory.getLogger(OwnedMtxPointsService.class);

    private final OwnedMtxPointsRepository ownedMtxPointsRepository;

    public OwnedMtxPointsService(OwnedMtxPointsRepository ownedMtxPointsRepository) {
        this.ownedMtxPointsRepository = ownedMtxPointsRepository;
    }

    /**
     * Save a ownedMtxPoints.
     *
     * @param ownedMtxPoints the entity to save.
     * @return the persisted entity.
     */
    public OwnedMtxPoints save(OwnedMtxPoints ownedMtxPoints) {
        log.debug("Request to save OwnedMtxPoints : {}", ownedMtxPoints);
        return ownedMtxPointsRepository.save(ownedMtxPoints);
    }

    /**
     * Partially update a ownedMtxPoints.
     *
     * @param ownedMtxPoints the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OwnedMtxPoints> partialUpdate(OwnedMtxPoints ownedMtxPoints) {
        log.debug("Request to partially update OwnedMtxPoints : {}", ownedMtxPoints);

        return ownedMtxPointsRepository.findById(ownedMtxPoints.getId()).map(existingOwnedMtxPoints -> {
            if (ownedMtxPoints.getUserName() != null) {
                existingOwnedMtxPoints.setUserName(ownedMtxPoints.getUserName());
            }
            if (ownedMtxPoints.getMtxPoints() != null) {
                existingOwnedMtxPoints.setMtxPoints(ownedMtxPoints.getMtxPoints());
            }

            return existingOwnedMtxPoints;
        }).map(ownedMtxPointsRepository::save);
    }

    /**
     * Get all the ownedMtxPoints.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OwnedMtxPoints> findAll() {
        log.debug("Request to get all OwnedMtxPoints");
        return ownedMtxPointsRepository.findAll();
    }

    /**
     * Get one ownedMtxPoints by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OwnedMtxPoints> findOne(Long id) {
        log.debug("Request to get OwnedMtxPoints : {}", id);
        return ownedMtxPointsRepository.findById(id);
    }

    /**
     * Delete the ownedMtxPoints by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OwnedMtxPoints : {}", id);
        ownedMtxPointsRepository.deleteById(id);
    }

    /**
     * Get OwnedMtxPoints of the User logged in. OwnedMtxPoints will be created if
     * it doesn't exist already.
     *
     * @return the entity.
     */
    public OwnedMtxPoints getOwnedMtxPointsOfUser() {
        log.debug("Request to get OwnedMtxPoints of User : {}", SecurityUtils.getCurrentUserLogin().get());
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxPoints");
        OwnedMtxPoints exampleOwnedMtxPoints = new OwnedMtxPoints();
        exampleOwnedMtxPoints.setUserName(SecurityUtils.getCurrentUserLogin().get());
        Example<OwnedMtxPoints> example = Example.of(exampleOwnedMtxPoints, usernameMatcher);
        Optional<OwnedMtxPoints> result = ownedMtxPointsRepository.findOne(example);
        if (result.isPresent()) {
            return result.get();
        }
        exampleOwnedMtxPoints.setMtxPoints(0);
        return ownedMtxPointsRepository.save(exampleOwnedMtxPoints);
    }
}
