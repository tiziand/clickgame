package bt.browsergame.inventory.repository;

import bt.browsergame.inventory.domain.ButtonColorMtx;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ButtonColorMtx entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ButtonColorMtxRepository extends JpaRepository<ButtonColorMtx, Long> {}
