package bt.browsergame.inventory.service;

import bt.browsergame.inventory.domain.ButtonColorMtx;
import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.repository.ButtonColorMtxRepository;
import bt.browsergame.inventory.repository.OwnedButtonColorMtxRepository;
import bt.browsergame.inventory.security.SecurityUtils;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link OwnedButtonColorMtx}.
 */
@Service
@Transactional
public class OwnedButtonColorMtxService {

    private final Logger log = LoggerFactory.getLogger(OwnedButtonColorMtxService.class);

    private final OwnedButtonColorMtxRepository ownedButtonColorMtxRepository;
    private final ButtonColorMtxRepository buttonColorMtxRepository;

    public OwnedButtonColorMtxService(OwnedButtonColorMtxRepository ownedButtonColorMtxRepository,
            ButtonColorMtxRepository buttonColorMtxRepository) {
        this.ownedButtonColorMtxRepository = ownedButtonColorMtxRepository;
        this.buttonColorMtxRepository = buttonColorMtxRepository;
    }

    /**
     * Save a ownedButtonColorMtx.
     *
     * @param ownedButtonColorMtx the entity to save.
     * @return the persisted entity.
     */
    public OwnedButtonColorMtx save(OwnedButtonColorMtx ownedButtonColorMtx) {
        log.debug("Request to save OwnedButtonColorMtx : {}", ownedButtonColorMtx);
        return ownedButtonColorMtxRepository.save(ownedButtonColorMtx);
    }

    /**
     * Partially update a ownedButtonColorMtx.
     *
     * @param ownedButtonColorMtx the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OwnedButtonColorMtx> partialUpdate(OwnedButtonColorMtx ownedButtonColorMtx) {
        log.debug("Request to partially update OwnedButtonColorMtx : {}", ownedButtonColorMtx);

        return ownedButtonColorMtxRepository.findById(ownedButtonColorMtx.getId()).map(existingOwnedButtonColorMtx -> {
            if (ownedButtonColorMtx.getUserName() != null) {
                existingOwnedButtonColorMtx.setUserName(ownedButtonColorMtx.getUserName());
            }
            if (ownedButtonColorMtx.getMtxId() != null) {
                existingOwnedButtonColorMtx.setMtxId(ownedButtonColorMtx.getMtxId());
            }
            if (ownedButtonColorMtx.getCreationTime() != null) {
                existingOwnedButtonColorMtx.setCreationTime(ownedButtonColorMtx.getCreationTime());
            }

            return existingOwnedButtonColorMtx;
        }).map(ownedButtonColorMtxRepository::save);
    }

    /**
     * Get all the ownedButtonColorMtxes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OwnedButtonColorMtx> findAll() {
        log.debug("Request to get all OwnedButtonColorMtxes");
        return ownedButtonColorMtxRepository.findAll();
    }

    /**
     * Get one ownedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OwnedButtonColorMtx> findOne(Long id) {
        log.debug("Request to get OwnedButtonColorMtx : {}", id);
        return ownedButtonColorMtxRepository.findById(id);
    }

    /**
     * Delete the ownedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OwnedButtonColorMtx : {}", id);
        ownedButtonColorMtxRepository.deleteById(id);
    }

    /**
     * Get the equippedButtonColorMtx of the logged in user.
     *
     */
    public List<OwnedButtonColorMtx> getUserOwnedButtonColorMtx() {
        log.debug("Request to get UserOwnedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxId", "creationTime",
                "buttonColorMtx");
        OwnedButtonColorMtx exOButtonColorMtx = new OwnedButtonColorMtx();
        exOButtonColorMtx.setUserName(SecurityUtils.getCurrentUserLogin().get());
        Example<OwnedButtonColorMtx> example = Example.of(exOButtonColorMtx, usernameMatcher);
        List<OwnedButtonColorMtx> result = ownedButtonColorMtxRepository.findAll(example);
        if (result.size() != 0) {
            return result;
        }
        // add default mtx to inventory
        exOButtonColorMtx.setMtxId(Long.valueOf(1));
        exOButtonColorMtx.creationTime(Instant.now());
        ButtonColorMtx buttonColorMtx = buttonColorMtxRepository.findById(Long.valueOf(1)).get();
        exOButtonColorMtx.setButtonColorMtx(buttonColorMtx);
        ownedButtonColorMtxRepository.save(exOButtonColorMtx);
        result.add(exOButtonColorMtx);
        return result;
    }
}
