package bt.browsergame.inventory.service;

import bt.browsergame.inventory.domain.ButtonColorMtx;
import bt.browsergame.inventory.domain.EquippedButtonColorMtx;
import bt.browsergame.inventory.domain.OwnedButtonColorMtx;
import bt.browsergame.inventory.repository.ButtonColorMtxRepository;
import bt.browsergame.inventory.repository.EquippedButtonColorMtxRepository;
import bt.browsergame.inventory.security.SecurityUtils;
import bt.browsergame.inventory.web.rest.errors.BadRequestAlertException;

import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link EquippedButtonColorMtx}.
 */
@Service
@Transactional
public class EquippedButtonColorMtxService {

    private final Logger log = LoggerFactory.getLogger(EquippedButtonColorMtxService.class);

    private final EquippedButtonColorMtxRepository equippedButtonColorMtxRepository;

    private final ButtonColorMtxRepository buttonColorMtxRepository;

    private final OwnedButtonColorMtxService ownedButtonColorMtxService;

    public EquippedButtonColorMtxService(EquippedButtonColorMtxRepository equippedButtonColorMtxRepository,
            ButtonColorMtxRepository buttonColorMtxRepository, OwnedButtonColorMtxService ownedButtonColorMtxService) {
        this.equippedButtonColorMtxRepository = equippedButtonColorMtxRepository;
        this.buttonColorMtxRepository = buttonColorMtxRepository;
        this.ownedButtonColorMtxService = ownedButtonColorMtxService;
    }

    /**
     * Save a equippedButtonColorMtx.
     *
     * @param equippedButtonColorMtx the entity to save.
     * @return the persisted entity.
     */
    public EquippedButtonColorMtx save(EquippedButtonColorMtx equippedButtonColorMtx) {
        log.debug("Request to save EquippedButtonColorMtx : {}", equippedButtonColorMtx);
        return equippedButtonColorMtxRepository.save(equippedButtonColorMtx);
    }

    /**
     * Partially update a equippedButtonColorMtx.
     *
     * @param equippedButtonColorMtx the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<EquippedButtonColorMtx> partialUpdate(EquippedButtonColorMtx equippedButtonColorMtx) {
        log.debug("Request to partially update EquippedButtonColorMtx : {}", equippedButtonColorMtx);

        return equippedButtonColorMtxRepository.findById(equippedButtonColorMtx.getId())
                .map(existingEquippedButtonColorMtx -> {
                    if (equippedButtonColorMtx.getUserName() != null) {
                        existingEquippedButtonColorMtx.setUserName(equippedButtonColorMtx.getUserName());
                    }
                    if (equippedButtonColorMtx.getTimeSlot() != null) {
                        existingEquippedButtonColorMtx.setTimeSlot(equippedButtonColorMtx.getTimeSlot());
                    }

                    return existingEquippedButtonColorMtx;
                }).map(equippedButtonColorMtxRepository::save);
    }

    /**
     * Get all the equippedButtonColorMtxes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EquippedButtonColorMtx> findAll() {
        log.debug("Request to get all EquippedButtonColorMtxes");
        return equippedButtonColorMtxRepository.findAll();
    }

    /**
     * Get one equippedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EquippedButtonColorMtx> findOne(Long id) {
        log.debug("Request to get EquippedButtonColorMtx : {}", id);
        return equippedButtonColorMtxRepository.findById(id);
    }

    /**
     * Delete the equippedButtonColorMtx by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete EquippedButtonColorMtx : {}", id);
        equippedButtonColorMtxRepository.deleteById(id);
    }

    /**
     * Get the equippedButtonColorMtx of the logged in user.
     *
     */
    public EquippedButtonColorMtx getUserEquippedButtonColorMtx(Double timeSlot) {
        log.debug("Request to get getUserEquippedButtonColorMtx : {}", SecurityUtils.getCurrentUserLogin().get());
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxId", "buttonColorMtx");
        EquippedButtonColorMtx exEButtonColorMtx = new EquippedButtonColorMtx();
        exEButtonColorMtx.setUserName(SecurityUtils.getCurrentUserLogin().get());
        exEButtonColorMtx.setTimeSlot(timeSlot);
        Example<EquippedButtonColorMtx> example = Example.of(exEButtonColorMtx, usernameMatcher);
        Optional<EquippedButtonColorMtx> result = equippedButtonColorMtxRepository.findOne(example);
        if (result.isPresent()) {
            return result.get();
        }
        ButtonColorMtx buttonColorMtx = buttonColorMtxRepository.findById(Long.valueOf(1)).get();
        exEButtonColorMtx.setButtonColorMtx(buttonColorMtx);
        return equippedButtonColorMtxRepository.save(exEButtonColorMtx);
    }

    /**
     * Equipp MTX for the logged in user.
     *
     */
    public EquippedButtonColorMtx equippButtonColorMtx(Long id, Double timeSlot) {
        Optional<ButtonColorMtx> buttonColorMtxOptional = buttonColorMtxRepository.findById(id);
        if (!buttonColorMtxOptional.isPresent()) {
            throw new BadRequestAlertException("", "", "ButtonColorMtx doesn't exist!");
        }
        ButtonColorMtx buttonColorMtx = buttonColorMtxOptional.get();
        if (buttonColorMtx.getLocked() && !buttonColorMtx.getTimeLimit().equals(timeSlot)) {
            throw new BadRequestAlertException("", "", "Requirments not met!");
        }
        List<OwnedButtonColorMtx> list = ownedButtonColorMtxService.getUserOwnedButtonColorMtx();
        OwnedButtonColorMtx result = list.stream().filter(element -> element.getMtxId().equals(id)).findAny()
                .orElse(null);
        if (result == null)
            throw new BadRequestAlertException("", "", "MTX not owned!");

        EquippedButtonColorMtx mtx = getUserEquippedButtonColorMtx(timeSlot);
        mtx.setButtonColorMtx(buttonColorMtx);
        return equippedButtonColorMtxRepository.save(mtx);
    }
}
