package bt.browsergame.inventory.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A OwnedButtonColorMtx.
 */
@Entity
@Table(name = "owned_button_color_mtx")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class OwnedButtonColorMtx implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "user_name", nullable = false)
    private String userName;

    @NotNull
    @Column(name = "mtx_id", nullable = false)
    private Long mtxId;

    @NotNull
    @Column(name = "creation_time", nullable = false)
    private Instant creationTime;

    @ManyToOne
    @JsonIgnoreProperties(value = { "equippedButtonColorMtxes", "ownedButtonColorMtxes", "basketItems" }, allowSetters = true)
    private ButtonColorMtx buttonColorMtx;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OwnedButtonColorMtx id(Long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return this.userName;
    }

    public OwnedButtonColorMtx userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getMtxId() {
        return this.mtxId;
    }

    public OwnedButtonColorMtx mtxId(Long mtxId) {
        this.mtxId = mtxId;
        return this;
    }

    public void setMtxId(Long mtxId) {
        this.mtxId = mtxId;
    }

    public Instant getCreationTime() {
        return this.creationTime;
    }

    public OwnedButtonColorMtx creationTime(Instant creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public ButtonColorMtx getButtonColorMtx() {
        return this.buttonColorMtx;
    }

    public OwnedButtonColorMtx buttonColorMtx(ButtonColorMtx buttonColorMtx) {
        this.setButtonColorMtx(buttonColorMtx);
        return this;
    }

    public void setButtonColorMtx(ButtonColorMtx buttonColorMtx) {
        this.buttonColorMtx = buttonColorMtx;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OwnedButtonColorMtx)) {
            return false;
        }
        return id != null && id.equals(((OwnedButtonColorMtx) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OwnedButtonColorMtx{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", mtxId=" + getMtxId() +
            ", creationTime='" + getCreationTime() + "'" +
            "}";
    }
}
