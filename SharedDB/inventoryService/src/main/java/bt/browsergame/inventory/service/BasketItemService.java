package bt.browsergame.inventory.service;

import bt.browsergame.inventory.domain.BasketItem;
import bt.browsergame.inventory.repository.BasketItemRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link BasketItem}.
 */
@Service
@Transactional
public class BasketItemService {

    private final Logger log = LoggerFactory.getLogger(BasketItemService.class);

    private final BasketItemRepository basketItemRepository;

    public BasketItemService(BasketItemRepository basketItemRepository) {
        this.basketItemRepository = basketItemRepository;
    }

    /**
     * Save a basketItem.
     *
     * @param basketItem the entity to save.
     * @return the persisted entity.
     */
    public BasketItem save(BasketItem basketItem) {
        log.debug("Request to save BasketItem : {}", basketItem);
        return basketItemRepository.save(basketItem);
    }

    /**
     * Partially update a basketItem.
     *
     * @param basketItem the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<BasketItem> partialUpdate(BasketItem basketItem) {
        log.debug("Request to partially update BasketItem : {}", basketItem);

        return basketItemRepository
            .findById(basketItem.getId())
            .map(
                existingBasketItem -> {
                    if (basketItem.getUserName() != null) {
                        existingBasketItem.setUserName(basketItem.getUserName());
                    }
                    if (basketItem.getMtxId() != null) {
                        existingBasketItem.setMtxId(basketItem.getMtxId());
                    }

                    return existingBasketItem;
                }
            )
            .map(basketItemRepository::save);
    }

    /**
     * Get all the basketItems.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BasketItem> findAll() {
        log.debug("Request to get all BasketItems");
        return basketItemRepository.findAll();
    }

    /**
     * Get one basketItem by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BasketItem> findOne(Long id) {
        log.debug("Request to get BasketItem : {}", id);
        return basketItemRepository.findById(id);
    }

    /**
     * Delete the basketItem by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BasketItem : {}", id);
        basketItemRepository.deleteById(id);
    }
}
