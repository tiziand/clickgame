package bt.browsergame.mtxstore.service;

import bt.browsergame.mtxstore.domain.ButtonColorMtx;
import bt.browsergame.mtxstore.repository.ButtonColorMtxRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ButtonColorMtx}.
 */
@Service
@Transactional
public class ButtonColorMtxService {

    private final Logger log = LoggerFactory.getLogger(ButtonColorMtxService.class);

    private final ButtonColorMtxRepository buttonColorMtxRepository;

    public ButtonColorMtxService(ButtonColorMtxRepository buttonColorMtxRepository) {
        this.buttonColorMtxRepository = buttonColorMtxRepository;
    }

    /**
     * Save a buttonColorMtx.
     *
     * @param buttonColorMtx the entity to save.
     * @return the persisted entity.
     */
    public ButtonColorMtx save(ButtonColorMtx buttonColorMtx) {
        log.debug("Request to save ButtonColorMtx : {}", buttonColorMtx);
        return buttonColorMtxRepository.save(buttonColorMtx);
    }

    /**
     * Partially update a buttonColorMtx.
     *
     * @param buttonColorMtx the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ButtonColorMtx> partialUpdate(ButtonColorMtx buttonColorMtx) {
        log.debug("Request to partially update ButtonColorMtx : {}", buttonColorMtx);

        return buttonColorMtxRepository
            .findById(buttonColorMtx.getId())
            .map(
                existingButtonColorMtx -> {
                    if (buttonColorMtx.getMtxColor() != null) {
                        existingButtonColorMtx.setMtxColor(buttonColorMtx.getMtxColor());
                    }
                    if (buttonColorMtx.getPrice() != null) {
                        existingButtonColorMtx.setPrice(buttonColorMtx.getPrice());
                    }
                    if (buttonColorMtx.getAvailable() != null) {
                        existingButtonColorMtx.setAvailable(buttonColorMtx.getAvailable());
                    }
                    if (buttonColorMtx.getTimeLimit() != null) {
                        existingButtonColorMtx.setTimeLimit(buttonColorMtx.getTimeLimit());
                    }
                    if (buttonColorMtx.getPointsRequired() != null) {
                        existingButtonColorMtx.setPointsRequired(buttonColorMtx.getPointsRequired());
                    }
                    if (buttonColorMtx.getLocked() != null) {
                        existingButtonColorMtx.setLocked(buttonColorMtx.getLocked());
                    }

                    return existingButtonColorMtx;
                }
            )
            .map(buttonColorMtxRepository::save);
    }

    /**
     * Get all the buttonColorMtxes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ButtonColorMtx> findAll() {
        log.debug("Request to get all ButtonColorMtxes");
        return buttonColorMtxRepository.findAll();
    }

    /**
     * Get one buttonColorMtx by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ButtonColorMtx> findOne(Long id) {
        log.debug("Request to get ButtonColorMtx : {}", id);
        return buttonColorMtxRepository.findById(id);
    }

    /**
     * Delete the buttonColorMtx by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ButtonColorMtx : {}", id);
        buttonColorMtxRepository.deleteById(id);
    }
}
