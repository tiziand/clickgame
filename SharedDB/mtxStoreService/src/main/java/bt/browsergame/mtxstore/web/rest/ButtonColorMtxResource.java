package bt.browsergame.mtxstore.web.rest;

import bt.browsergame.mtxstore.domain.ButtonColorMtx;
import bt.browsergame.mtxstore.repository.ButtonColorMtxRepository;
import bt.browsergame.mtxstore.service.ButtonColorMtxService;
import bt.browsergame.mtxstore.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bt.browsergame.inventory.domain.ButtonColorMtx}.
 */
@RestController
@RequestMapping("/api")
public class ButtonColorMtxResource {

    private final Logger log = LoggerFactory.getLogger(ButtonColorMtxResource.class);

    private static final String ENTITY_NAME = "inventoryServiceButtonColorMtx";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ButtonColorMtxService buttonColorMtxService;

    private final ButtonColorMtxRepository buttonColorMtxRepository;

    public ButtonColorMtxResource(ButtonColorMtxService buttonColorMtxService, ButtonColorMtxRepository buttonColorMtxRepository) {
        this.buttonColorMtxService = buttonColorMtxService;
        this.buttonColorMtxRepository = buttonColorMtxRepository;
    }

    /**
     * {@code POST  /button-color-mtxes} : Create a new buttonColorMtx.
     *
     * @param buttonColorMtx the buttonColorMtx to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new buttonColorMtx, or with status {@code 400 (Bad Request)} if the buttonColorMtx has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/button-color-mtxes")
    public ResponseEntity<ButtonColorMtx> createButtonColorMtx(@Valid @RequestBody ButtonColorMtx buttonColorMtx)
        throws URISyntaxException {
        log.debug("REST request to save ButtonColorMtx : {}", buttonColorMtx);
        if (buttonColorMtx.getId() != null) {
            throw new BadRequestAlertException("A new buttonColorMtx cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ButtonColorMtx result = buttonColorMtxService.save(buttonColorMtx);
        return ResponseEntity
            .created(new URI("/api/button-color-mtxes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /button-color-mtxes/:id} : Updates an existing buttonColorMtx.
     *
     * @param id the id of the buttonColorMtx to save.
     * @param buttonColorMtx the buttonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated buttonColorMtx,
     * or with status {@code 400 (Bad Request)} if the buttonColorMtx is not valid,
     * or with status {@code 500 (Internal Server Error)} if the buttonColorMtx couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/button-color-mtxes/{id}")
    public ResponseEntity<ButtonColorMtx> updateButtonColorMtx(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ButtonColorMtx buttonColorMtx
    ) throws URISyntaxException {
        log.debug("REST request to update ButtonColorMtx : {}, {}", id, buttonColorMtx);
        if (buttonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, buttonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!buttonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ButtonColorMtx result = buttonColorMtxService.save(buttonColorMtx);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, buttonColorMtx.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /button-color-mtxes/:id} : Partial updates given fields of an existing buttonColorMtx, field will ignore if it is null
     *
     * @param id the id of the buttonColorMtx to save.
     * @param buttonColorMtx the buttonColorMtx to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated buttonColorMtx,
     * or with status {@code 400 (Bad Request)} if the buttonColorMtx is not valid,
     * or with status {@code 404 (Not Found)} if the buttonColorMtx is not found,
     * or with status {@code 500 (Internal Server Error)} if the buttonColorMtx couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/button-color-mtxes/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<ButtonColorMtx> partialUpdateButtonColorMtx(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ButtonColorMtx buttonColorMtx
    ) throws URISyntaxException {
        log.debug("REST request to partial update ButtonColorMtx partially : {}, {}", id, buttonColorMtx);
        if (buttonColorMtx.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, buttonColorMtx.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!buttonColorMtxRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ButtonColorMtx> result = buttonColorMtxService.partialUpdate(buttonColorMtx);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, buttonColorMtx.getId().toString())
        );
    }

    /**
     * {@code GET  /button-color-mtxes} : get all the buttonColorMtxes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of buttonColorMtxes in body.
     */
    @GetMapping("/button-color-mtxes")
    public List<ButtonColorMtx> getAllButtonColorMtxes() {
        log.debug("REST request to get all ButtonColorMtxes");
        return buttonColorMtxService.findAll();
    }

    /**
     * {@code GET  /button-color-mtxes/:id} : get the "id" buttonColorMtx.
     *
     * @param id the id of the buttonColorMtx to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the buttonColorMtx, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/button-color-mtxes/{id}")
    public ResponseEntity<ButtonColorMtx> getButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to get ButtonColorMtx : {}", id);
        Optional<ButtonColorMtx> buttonColorMtx = buttonColorMtxService.findOne(id);
        return ResponseUtil.wrapOrNotFound(buttonColorMtx);
    }

    /**
     * {@code DELETE  /button-color-mtxes/:id} : delete the "id" buttonColorMtx.
     *
     * @param id the id of the buttonColorMtx to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/button-color-mtxes/{id}")
    public ResponseEntity<Void> deleteButtonColorMtx(@PathVariable Long id) {
        log.debug("REST request to delete ButtonColorMtx : {}", id);
        buttonColorMtxService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
