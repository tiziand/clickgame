package bt.browsergame.payment.service;

import bt.browsergame.payment.domain.PointOption;
import bt.browsergame.payment.repository.PointOptionRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PointOption}.
 */
@Service
@Transactional
public class PointOptionService {

    private final Logger log = LoggerFactory.getLogger(PointOptionService.class);

    private final PointOptionRepository pointOptionRepository;

    public PointOptionService(PointOptionRepository pointOptionRepository) {
        this.pointOptionRepository = pointOptionRepository;
    }

    /**
     * Save a pointOption.
     *
     * @param pointOption the entity to save.
     * @return the persisted entity.
     */
    public PointOption save(PointOption pointOption) {
        log.debug("Request to save PointOption : {}", pointOption);
        return pointOptionRepository.save(pointOption);
    }

    /**
     * Partially update a pointOption.
     *
     * @param pointOption the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PointOption> partialUpdate(PointOption pointOption) {
        log.debug("Request to partially update PointOption : {}", pointOption);

        return pointOptionRepository
            .findById(pointOption.getId())
            .map(
                existingPointOption -> {
                    if (pointOption.getPointAmount() != null) {
                        existingPointOption.setPointAmount(pointOption.getPointAmount());
                    }
                    if (pointOption.getPrice() != null) {
                        existingPointOption.setPrice(pointOption.getPrice());
                    }
                    if (pointOption.getAvailable() != null) {
                        existingPointOption.setAvailable(pointOption.getAvailable());
                    }

                    return existingPointOption;
                }
            )
            .map(pointOptionRepository::save);
    }

    /**
     * Get all the pointOptions.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PointOption> findAll() {
        log.debug("Request to get all PointOptions");
        return pointOptionRepository.findAll();
    }

    /**
     * Get one pointOption by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PointOption> findOne(Long id) {
        log.debug("Request to get PointOption : {}", id);
        return pointOptionRepository.findById(id);
    }

    /**
     * Delete the pointOption by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PointOption : {}", id);
        pointOptionRepository.deleteById(id);
    }
}
