package bt.browsergame.payment.service;

import bt.browsergame.payment.domain.OwnedMtxPoints;
import bt.browsergame.payment.domain.PaymentInfo;
import bt.browsergame.payment.repository.OwnedMtxPointsRepository;
import bt.browsergame.payment.repository.PaymentInfoRepository;
import bt.browsergame.payment.security.SecurityUtils;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PaymentInfo}.
 */
@Service
@Transactional
public class PaymentInfoService {

    private final Logger log = LoggerFactory.getLogger(PaymentInfoService.class);

    private final PaymentInfoRepository paymentInfoRepository;

    private final OwnedMtxPointsRepository ownedMtxPointsRepository;

    public PaymentInfoService(PaymentInfoRepository paymentInfoRepository,
            OwnedMtxPointsRepository ownedMtxPointsRepository) {
        this.paymentInfoRepository = paymentInfoRepository;
        this.ownedMtxPointsRepository = ownedMtxPointsRepository;
    }

    /**
     * Save a paymentInfo.
     *
     * @param paymentInfo the entity to save.
     * @return the persisted entity.
     */
    public PaymentInfo save(PaymentInfo paymentInfo) {
        log.debug("Request to save PaymentInfo : {}", paymentInfo);
        ExampleMatcher usernameMatcher = ExampleMatcher.matching().withIgnorePaths("id", "mtxPoints");
        OwnedMtxPoints ownedMtxPoints = new OwnedMtxPoints();
        ownedMtxPoints.setUserName(SecurityUtils.getCurrentUserLogin().get());
        Example<OwnedMtxPoints> example = Example.of(ownedMtxPoints, usernameMatcher);
        Optional<OwnedMtxPoints> result = ownedMtxPointsRepository.findOne(example);
        if (result.isPresent()) {
            OwnedMtxPoints resultObj = result.get();
            resultObj.setMtxPoints(resultObj.getMtxPoints() + paymentInfo.getPointOption().getPointAmount());
            ownedMtxPointsRepository.save(resultObj);
        } else {
            ownedMtxPoints.setMtxPoints(paymentInfo.getPointOption().getPointAmount());
            ownedMtxPointsRepository.save(ownedMtxPoints);
        }
        paymentInfo.setCreationTime(Instant.now());
        return paymentInfoRepository.save(paymentInfo);
    }

    /**
     * Partially update a paymentInfo.
     *
     * @param paymentInfo the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PaymentInfo> partialUpdate(PaymentInfo paymentInfo) {
        log.debug("Request to partially update PaymentInfo : {}", paymentInfo);

        return paymentInfoRepository.findById(paymentInfo.getId()).map(existingPaymentInfo -> {
            if (paymentInfo.getUserName() != null) {
                existingPaymentInfo.setUserName(paymentInfo.getUserName());
            }
            if (paymentInfo.getCreationTime() != null) {
                existingPaymentInfo.setCreationTime(paymentInfo.getCreationTime());
            }

            return existingPaymentInfo;
        }).map(paymentInfoRepository::save);
    }

    /**
     * Get all the paymentInfos.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PaymentInfo> findAll() {
        log.debug("Request to get all PaymentInfos");
        return paymentInfoRepository.findAll();
    }

    /**
     * Get one paymentInfo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PaymentInfo> findOne(Long id) {
        log.debug("Request to get PaymentInfo : {}", id);
        return paymentInfoRepository.findById(id);
    }

    /**
     * Delete the paymentInfo by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PaymentInfo : {}", id);
        paymentInfoRepository.deleteById(id);
    }
}
