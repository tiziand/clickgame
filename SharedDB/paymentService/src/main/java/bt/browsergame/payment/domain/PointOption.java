package bt.browsergame.payment.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PointOption.
 */
@Entity
@Table(name = "point_option")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PointOption implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "point_amount", nullable = false)
    private Integer pointAmount;

    @NotNull
    @Column(name = "price", nullable = false)
    private Double price;

    @NotNull
    @Column(name = "available", nullable = false)
    private Boolean available;

    @OneToMany(mappedBy = "pointOption")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "pointOption" }, allowSetters = true)
    private Set<PaymentInfo> paymentInfos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PointOption id(Long id) {
        this.id = id;
        return this;
    }

    public Integer getPointAmount() {
        return this.pointAmount;
    }

    public PointOption pointAmount(Integer pointAmount) {
        this.pointAmount = pointAmount;
        return this;
    }

    public void setPointAmount(Integer pointAmount) {
        this.pointAmount = pointAmount;
    }

    public Double getPrice() {
        return this.price;
    }

    public PointOption price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getAvailable() {
        return this.available;
    }

    public PointOption available(Boolean available) {
        this.available = available;
        return this;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Set<PaymentInfo> getPaymentInfos() {
        return this.paymentInfos;
    }

    public PointOption paymentInfos(Set<PaymentInfo> paymentInfos) {
        this.setPaymentInfos(paymentInfos);
        return this;
    }

    public PointOption addPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfos.add(paymentInfo);
        paymentInfo.setPointOption(this);
        return this;
    }

    public PointOption removePaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfos.remove(paymentInfo);
        paymentInfo.setPointOption(null);
        return this;
    }

    public void setPaymentInfos(Set<PaymentInfo> paymentInfos) {
        if (this.paymentInfos != null) {
            this.paymentInfos.forEach(i -> i.setPointOption(null));
        }
        if (paymentInfos != null) {
            paymentInfos.forEach(i -> i.setPointOption(this));
        }
        this.paymentInfos = paymentInfos;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PointOption)) {
            return false;
        }
        return id != null && id.equals(((PointOption) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PointOption{" +
            "id=" + getId() +
            ", pointAmount=" + getPointAmount() +
            ", price=" + getPrice() +
            ", available='" + getAvailable() + "'" +
            "}";
    }
}
