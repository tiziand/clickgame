package bt.browsergame.payment;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("bt.browsergame.payment");

        noClasses()
            .that()
            .resideInAnyPackage("bt.browsergame.payment.service..")
            .or()
            .resideInAnyPackage("bt.browsergame.payment.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..bt.browsergame.payment.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
