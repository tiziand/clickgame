#!/bin/bash

(cd rankingService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag rankingservice tiziand/btsingledb:rankingservice; docker push tiziand/btsingledb:rankingservice)
(cd apiGateway; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag apigateway tiziand/btsingledb:apigateway; docker push tiziand/btsingledb:apigateway)
(cd inventoryService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag inventoryservice tiziand/btsingledb:inventoryservice; docker push tiziand/btsingledb:inventoryservice)
(cd basketService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag basketservice tiziand/btsingledb:basketservice; docker push tiziand/btsingledb:basketservice)
(cd mtxStoreService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag mtxstoreservice tiziand/btsingledb:mtxstoreservice; docker push tiziand/btsingledb:mtxstoreservice)
(cd paymentService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag paymentservice tiziand/btsingledb:paymentservice; docker push tiziand/btsingledb:paymentservice)
(cd statisticService; ./mvnw -ntp -DskipTests -Pprod verify jib:dockerBuild; docker tag statisticservice tiziand/btsingledb:statisticservice; docker push tiziand/btsingledb:statisticservice)
