import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './points-farmed.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPointsFarmedDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PointsFarmedDetail = (props: IPointsFarmedDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { pointsFarmedEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="pointsFarmedDetailsHeading">PointsFarmed</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{pointsFarmedEntity.id}</dd>
          <dt>
            <span id="userName">User Name</span>
          </dt>
          <dd>{pointsFarmedEntity.userName}</dd>
          <dt>
            <span id="points">Points</span>
          </dt>
          <dd>{pointsFarmedEntity.points}</dd>
          <dt>
            <span id="creationTime">Creation Time</span>
          </dt>
          <dd>
            {pointsFarmedEntity.creationTime ? (
              <TextFormat value={pointsFarmedEntity.creationTime} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="timeLimit">Time Limit</span>
          </dt>
          <dd>{pointsFarmedEntity.timeLimit}</dd>
        </dl>
        <Button tag={Link} to="/points-farmed" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/points-farmed/${pointsFarmedEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ pointsFarmed }: IRootState) => ({
  pointsFarmedEntity: pointsFarmed.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PointsFarmedDetail);
