import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PointsFarmed from './points-farmed';
import PointsFarmedDetail from './points-farmed-detail';
import PointsFarmedUpdate from './points-farmed-update';
import PointsFarmedDeleteDialog from './points-farmed-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PointsFarmedUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PointsFarmedUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PointsFarmedDetail} />
      <ErrorBoundaryRoute path={match.url} component={PointsFarmed} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PointsFarmedDeleteDialog} />
  </>
);

export default Routes;
