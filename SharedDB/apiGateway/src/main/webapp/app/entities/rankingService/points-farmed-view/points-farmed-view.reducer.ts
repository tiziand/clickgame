import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPointsFarmedView, defaultValue } from 'app/shared/model/rankingService/points-farmed-view.model';

export const ACTION_TYPES = {
  FETCH_RANKING_LIST: 'ranking/FETCH_POINTS_FARMED_VIEW_LIST',
  FETCH_RANKING: 'ranking/FETCH_POINTS_FARMED_VIEW',
  RESET: 'ranking/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPointsFarmedView>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type PointsFarmedViewState = Readonly<typeof initialState>;

// Reducer

export default (state: PointsFarmedViewState = initialState, action): PointsFarmedViewState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_RANKING_LIST):
    case REQUEST(ACTION_TYPES.FETCH_RANKING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_RANKING_LIST):
    case FAILURE(ACTION_TYPES.FETCH_RANKING):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_RANKING_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_RANKING):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/rankingservice/api/points-farmeds';

// Actions
export const getSumRanking: ICrudGetAllAction<IPointsFarmedView> = (page, size, sort) => {
  const requestUrl = `${apiUrl}/rankingSum?page=${page}&size=${size}`;
  return {
    type: ACTION_TYPES.FETCH_RANKING_LIST,
    payload: axios.get<IPointsFarmedView>(`${requestUrl}&cacheBuster=${new Date().getTime()}`),
  };
};

export const getMaxTimeRanking: ICrudGetAllAction<IPointsFarmedView> = (page, size, time) => {
  const requestUrl = `${apiUrl}/rankingMax/${time}?page=${page}&size=${size}`;
  return {
    type: ACTION_TYPES.FETCH_RANKING_LIST,
    payload: axios.get<IPointsFarmedView>(`${requestUrl}&cacheBuster=${new Date().getTime()}`),
  };
};

export const getSum: ICrudGetAction<IPointsFarmedView> = () => {
  const requestUrl = `${apiUrl}/userSum`;
  return {
    type: ACTION_TYPES.FETCH_RANKING,
    payload: axios.get<IPointsFarmedView>(requestUrl),
  };
};

export const getMaxTime: ICrudGetAction<IPointsFarmedView> = (timeLimit: number) => {
  const requestUrl = `${apiUrl}/userMaxTime/${timeLimit}`;
  return {
    type: ACTION_TYPES.FETCH_RANKING,
    payload: axios.get<IPointsFarmedView>(requestUrl),
  };
};

export const getSumUserName = (userName: string) => {
  const requestUrl = `${apiUrl}/userSum/${userName}`;
  return {
    type: ACTION_TYPES.FETCH_RANKING,
    payload: axios.get<IPointsFarmedView>(requestUrl),
  };
};

export const getMaxTimeUserName = (timeLimit: string, userName: string) => {
  const requestUrl = `${apiUrl}/userMaxTime/${timeLimit}/${userName}`;
  return {
    type: ACTION_TYPES.FETCH_RANKING,
    payload: axios.get<IPointsFarmedView>(requestUrl),
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
