import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './button-color-mtx.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IButtonColorMtxDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ButtonColorMtxDetail = (props: IButtonColorMtxDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { buttonColorMtxEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="buttonColorMtxDetailsHeading">ButtonColorMtx</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{buttonColorMtxEntity.id}</dd>
          <dt>
            <span id="mtxColor">Mtx Color</span>
          </dt>
          <dd>{buttonColorMtxEntity.mtxColor}</dd>
          <dt>
            <span id="price">Price</span>
          </dt>
          <dd>{buttonColorMtxEntity.price}</dd>
          <dt>
            <span id="available">Available</span>
          </dt>
          <dd>{buttonColorMtxEntity.available ? 'true' : 'false'}</dd>
          <dt>
            <span id="timeLimit">Time Limit</span>
          </dt>
          <dd>{buttonColorMtxEntity.timeLimit}</dd>
          <dt>
            <span id="pointsRequired">Points Required</span>
          </dt>
          <dd>{buttonColorMtxEntity.pointsRequired}</dd>
          <dt>
            <span id="locked">Locked</span>
          </dt>
          <dd>{buttonColorMtxEntity.locked ? 'true' : 'false'}</dd>
        </dl>
        <Button tag={Link} to="/button-color-mtx" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/button-color-mtx/${buttonColorMtxEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ buttonColorMtx }: IRootState) => ({
  buttonColorMtxEntity: buttonColorMtx.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ButtonColorMtxDetail);
