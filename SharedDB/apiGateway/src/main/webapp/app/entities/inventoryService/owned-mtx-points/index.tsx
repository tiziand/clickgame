import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import OwnedMtxPoints from './owned-mtx-points';
import OwnedMtxPointsDetail from './owned-mtx-points-detail';
import OwnedMtxPointsUpdate from './owned-mtx-points-update';
import OwnedMtxPointsDeleteDialog from './owned-mtx-points-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={OwnedMtxPointsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={OwnedMtxPointsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={OwnedMtxPointsDetail} />
      <ErrorBoundaryRoute path={match.url} component={OwnedMtxPoints} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={OwnedMtxPointsDeleteDialog} />
  </>
);

export default Routes;
