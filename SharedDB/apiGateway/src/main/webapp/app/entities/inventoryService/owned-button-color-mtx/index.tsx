import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import OwnedButtonColorMtx from './owned-button-color-mtx';
import OwnedButtonColorMtxDetail from './owned-button-color-mtx-detail';
import OwnedButtonColorMtxUpdate from './owned-button-color-mtx-update';
import OwnedButtonColorMtxDeleteDialog from './owned-button-color-mtx-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={OwnedButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={OwnedButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={OwnedButtonColorMtxDetail} />
      <ErrorBoundaryRoute path={match.url} component={OwnedButtonColorMtx} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={OwnedButtonColorMtxDeleteDialog} />
  </>
);

export default Routes;
