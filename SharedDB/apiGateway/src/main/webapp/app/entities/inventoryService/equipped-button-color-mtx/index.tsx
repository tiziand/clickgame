import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EquippedButtonColorMtx from './equipped-button-color-mtx';
import EquippedButtonColorMtxDetail from './equipped-button-color-mtx-detail';
import EquippedButtonColorMtxUpdate from './equipped-button-color-mtx-update';
import EquippedButtonColorMtxDeleteDialog from './equipped-button-color-mtx-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EquippedButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EquippedButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EquippedButtonColorMtxDetail} />
      <ErrorBoundaryRoute path={match.url} component={EquippedButtonColorMtx} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={EquippedButtonColorMtxDeleteDialog} />
  </>
);

export default Routes;
