import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './owned-button-color-mtx.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOwnedButtonColorMtxDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const OwnedButtonColorMtxDetail = (props: IOwnedButtonColorMtxDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { ownedButtonColorMtxEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="ownedButtonColorMtxDetailsHeading">OwnedButtonColorMtx</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{ownedButtonColorMtxEntity.id}</dd>
          <dt>
            <span id="userName">User Name</span>
          </dt>
          <dd>{ownedButtonColorMtxEntity.userName}</dd>
          <dt>
            <span id="mtxId">Mtx Id</span>
          </dt>
          <dd>{ownedButtonColorMtxEntity.mtxId}</dd>
          <dt>
            <span id="creationTime">Creation Time</span>
          </dt>
          <dd>
            {ownedButtonColorMtxEntity.creationTime ? (
              <TextFormat value={ownedButtonColorMtxEntity.creationTime} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>Button Color Mtx</dt>
          <dd>{ownedButtonColorMtxEntity.buttonColorMtx ? ownedButtonColorMtxEntity.buttonColorMtx.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/owned-button-color-mtx" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/owned-button-color-mtx/${ownedButtonColorMtxEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ ownedButtonColorMtx }: IRootState) => ({
  ownedButtonColorMtxEntity: ownedButtonColorMtx.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OwnedButtonColorMtxDetail);
