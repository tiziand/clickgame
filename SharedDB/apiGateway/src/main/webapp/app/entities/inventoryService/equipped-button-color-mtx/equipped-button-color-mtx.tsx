import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './equipped-button-color-mtx.reducer';
import { IEquippedButtonColorMtx } from 'app/shared/model/inventoryService/equipped-button-color-mtx.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEquippedButtonColorMtxProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const EquippedButtonColorMtx = (props: IEquippedButtonColorMtxProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { equippedButtonColorMtxList, match, loading } = props;
  return (
    <div>
      <h2 id="equipped-button-color-mtx-heading" data-cy="EquippedButtonColorMtxHeading">
        Equipped Button Color Mtxes
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Equipped Button Color Mtx
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {equippedButtonColorMtxList && equippedButtonColorMtxList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Time Slot</th>
                <th>Button Color Mtx</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {equippedButtonColorMtxList.map((equippedButtonColorMtx, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${equippedButtonColorMtx.id}`} color="link" size="sm">
                      {equippedButtonColorMtx.id}
                    </Button>
                  </td>
                  <td>{equippedButtonColorMtx.userName}</td>
                  <td>{equippedButtonColorMtx.timeSlot}</td>
                  <td>
                    {equippedButtonColorMtx.buttonColorMtx ? (
                      <Link to={`button-color-mtx/${equippedButtonColorMtx.buttonColorMtx.id}`}>
                        {equippedButtonColorMtx.buttonColorMtx.id}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button
                        tag={Link}
                        to={`${match.url}/${equippedButtonColorMtx.id}`}
                        color="info"
                        size="sm"
                        data-cy="entityDetailsButton"
                      >
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${equippedButtonColorMtx.id}/edit`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${equippedButtonColorMtx.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Equipped Button Color Mtxes found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ equippedButtonColorMtx }: IRootState) => ({
  equippedButtonColorMtxList: equippedButtonColorMtx.entities,
  loading: equippedButtonColorMtx.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EquippedButtonColorMtx);
