import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './equipped-button-color-mtx.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEquippedButtonColorMtxDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const EquippedButtonColorMtxDetail = (props: IEquippedButtonColorMtxDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { equippedButtonColorMtxEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="equippedButtonColorMtxDetailsHeading">EquippedButtonColorMtx</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{equippedButtonColorMtxEntity.id}</dd>
          <dt>
            <span id="userName">User Name</span>
          </dt>
          <dd>{equippedButtonColorMtxEntity.userName}</dd>
          <dt>
            <span id="timeSlot">Time Slot</span>
          </dt>
          <dd>{equippedButtonColorMtxEntity.timeSlot}</dd>
          <dt>Button Color Mtx</dt>
          <dd>{equippedButtonColorMtxEntity.buttonColorMtx ? equippedButtonColorMtxEntity.buttonColorMtx.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/equipped-button-color-mtx" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/equipped-button-color-mtx/${equippedButtonColorMtxEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ equippedButtonColorMtx }: IRootState) => ({
  equippedButtonColorMtxEntity: equippedButtonColorMtx.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(EquippedButtonColorMtxDetail);
