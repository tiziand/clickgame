import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IButtonColorMtx } from 'app/shared/model/mtxStoreService/button-color-mtx.model';
import { getEntities as getButtonColorMtxes } from 'app/entities/mtxStoreService/button-color-mtx/button-color-mtx.reducer';
import { getEntity, updateEntity, createEntity, reset } from './owned-button-color-mtx.reducer';
import { IOwnedButtonColorMtx } from 'app/shared/model/inventoryService/owned-button-color-mtx.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IOwnedButtonColorMtxUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const OwnedButtonColorMtxUpdate = (props: IOwnedButtonColorMtxUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { ownedButtonColorMtxEntity, buttonColorMtxes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/owned-button-color-mtx');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getButtonColorMtxes();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.creationTime = convertDateTimeToServer(values.creationTime);

    if (errors.length === 0) {
      const entity = {
        ...ownedButtonColorMtxEntity,
        ...values,
        buttonColorMtx: buttonColorMtxes.find(it => it.id.toString() === values.buttonColorMtxId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2
            id="apiGatewayApp.inventoryServiceOwnedButtonColorMtx.home.createOrEditLabel"
            data-cy="OwnedButtonColorMtxCreateUpdateHeading"
          >
            Create or edit a OwnedButtonColorMtx
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : ownedButtonColorMtxEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="owned-button-color-mtx-id">ID</Label>
                  <AvInput id="owned-button-color-mtx-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="userNameLabel" for="owned-button-color-mtx-userName">
                  User Name
                </Label>
                <AvField
                  id="owned-button-color-mtx-userName"
                  data-cy="userName"
                  type="text"
                  name="userName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="mtxIdLabel" for="owned-button-color-mtx-mtxId">
                  Mtx Id
                </Label>
                <AvField
                  id="owned-button-color-mtx-mtxId"
                  data-cy="mtxId"
                  type="string"
                  className="form-control"
                  name="mtxId"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="creationTimeLabel" for="owned-button-color-mtx-creationTime">
                  Creation Time
                </Label>
                <AvInput
                  id="owned-button-color-mtx-creationTime"
                  data-cy="creationTime"
                  type="datetime-local"
                  className="form-control"
                  name="creationTime"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.ownedButtonColorMtxEntity.creationTime)}
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="owned-button-color-mtx-buttonColorMtx">Button Color Mtx</Label>
                <AvInput
                  id="owned-button-color-mtx-buttonColorMtx"
                  data-cy="buttonColorMtx"
                  type="select"
                  className="form-control"
                  name="buttonColorMtxId"
                >
                  <option value="" key="0" />
                  {buttonColorMtxes
                    ? buttonColorMtxes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/owned-button-color-mtx" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  buttonColorMtxes: storeState.buttonColorMtx.entities,
  ownedButtonColorMtxEntity: storeState.ownedButtonColorMtx.entity,
  loading: storeState.ownedButtonColorMtx.loading,
  updating: storeState.ownedButtonColorMtx.updating,
  updateSuccess: storeState.ownedButtonColorMtx.updateSuccess,
});

const mapDispatchToProps = {
  getButtonColorMtxes,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OwnedButtonColorMtxUpdate);
