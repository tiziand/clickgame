import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './owned-button-color-mtx.reducer';
import { IOwnedButtonColorMtx } from 'app/shared/model/inventoryService/owned-button-color-mtx.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOwnedButtonColorMtxProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const OwnedButtonColorMtx = (props: IOwnedButtonColorMtxProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { ownedButtonColorMtxList, match, loading } = props;
  return (
    <div>
      <h2 id="owned-button-color-mtx-heading" data-cy="OwnedButtonColorMtxHeading">
        Owned Button Color Mtxes
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Owned Button Color Mtx
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {ownedButtonColorMtxList && ownedButtonColorMtxList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Mtx Id</th>
                <th>Creation Time</th>
                <th>Button Color Mtx</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {ownedButtonColorMtxList.map((ownedButtonColorMtx, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${ownedButtonColorMtx.id}`} color="link" size="sm">
                      {ownedButtonColorMtx.id}
                    </Button>
                  </td>
                  <td>{ownedButtonColorMtx.userName}</td>
                  <td>{ownedButtonColorMtx.mtxId}</td>
                  <td>
                    {ownedButtonColorMtx.creationTime ? (
                      <TextFormat type="date" value={ownedButtonColorMtx.creationTime} format={APP_DATE_FORMAT} />
                    ) : null}
                  </td>
                  <td>
                    {ownedButtonColorMtx.buttonColorMtx ? (
                      <Link to={`button-color-mtx/${ownedButtonColorMtx.buttonColorMtx.id}`}>{ownedButtonColorMtx.buttonColorMtx.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${ownedButtonColorMtx.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${ownedButtonColorMtx.id}/edit`}
                        color="primary"
                        size="sm"
                        data-cy="entityEditButton"
                      >
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button
                        tag={Link}
                        to={`${match.url}/${ownedButtonColorMtx.id}/delete`}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Owned Button Color Mtxes found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ ownedButtonColorMtx }: IRootState) => ({
  ownedButtonColorMtxList: ownedButtonColorMtx.entities,
  loading: ownedButtonColorMtx.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OwnedButtonColorMtx);
