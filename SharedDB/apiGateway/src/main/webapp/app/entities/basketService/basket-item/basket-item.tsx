import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './basket-item.reducer';
import { IBasketItem } from 'app/shared/model/basketService/basket-item.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IBasketItemProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const BasketItem = (props: IBasketItemProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { basketItemList, match, loading } = props;
  return (
    <div>
      <h2 id="basket-item-heading" data-cy="BasketItemHeading">
        Basket Items
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Basket Item
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {basketItemList && basketItemList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Mtx Id</th>
                <th>Button Color Mtx</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {basketItemList.map((basketItem, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${basketItem.id}`} color="link" size="sm">
                      {basketItem.id}
                    </Button>
                  </td>
                  <td>{basketItem.userName}</td>
                  <td>{basketItem.mtxId}</td>
                  <td>
                    {basketItem.buttonColorMtx ? (
                      <Link to={`button-color-mtx/${basketItem.buttonColorMtx.id}`}>{basketItem.buttonColorMtx.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${basketItem.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${basketItem.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${basketItem.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Basket Items found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ basketItem }: IRootState) => ({
  basketItemList: basketItem.entities,
  loading: basketItem.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BasketItem);
