import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { IBasketItem, defaultValue } from 'app/shared/model/basketService/basket-item.model';

export const ACTION_TYPES = {
  FETCH_BASKETITEM_LIST: 'basketItem/FETCH_BASKETITEM_LIST',
  FETCH_BASKETITEM: 'basketItem/FETCH_BASKETITEM',
  CREATE_BASKETITEM: 'basketItem/CREATE_BASKETITEM',
  UPDATE_BASKETITEM: 'basketItem/UPDATE_BASKETITEM',
  PARTIAL_UPDATE_BASKETITEM: 'basketItem/PARTIAL_UPDATE_BASKETITEM',
  DELETE_BASKETITEM: 'basketItem/DELETE_BASKETITEM',
  RESET: 'basketItem/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IBasketItem>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type BasketItemState = Readonly<typeof initialState>;

// Reducer

export default (state: BasketItemState = initialState, action): BasketItemState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_BASKETITEM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_BASKETITEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_BASKETITEM):
    case REQUEST(ACTION_TYPES.UPDATE_BASKETITEM):
    case REQUEST(ACTION_TYPES.DELETE_BASKETITEM):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_BASKETITEM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_BASKETITEM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_BASKETITEM):
    case FAILURE(ACTION_TYPES.CREATE_BASKETITEM):
    case FAILURE(ACTION_TYPES.UPDATE_BASKETITEM):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_BASKETITEM):
    case FAILURE(ACTION_TYPES.DELETE_BASKETITEM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_BASKETITEM_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_BASKETITEM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_BASKETITEM):
    case SUCCESS(ACTION_TYPES.UPDATE_BASKETITEM):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_BASKETITEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_BASKETITEM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/basketservice/api/basket-items';

// Actions

export const getEntitiesUser: ICrudGetAllAction<IBasketItem> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_BASKETITEM_LIST,
  payload: axios.get<IBasketItem>(`${apiUrl}/userBasket?cacheBuster=${new Date().getTime()}`),
});

export const purchaseBasket: ICrudGetAllAction<IBasketItem> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_BASKETITEM_LIST,
  payload: axios.get<IBasketItem>(`${apiUrl}/purchase?cacheBuster=${new Date().getTime()}`),
});

export const getEntities: ICrudGetAllAction<IBasketItem> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_BASKETITEM_LIST,
  payload: axios.get<IBasketItem>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IBasketItem> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_BASKETITEM,
    payload: axios.get<IBasketItem>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IBasketItem> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_BASKETITEM,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntitiesUser());
  return result;
};

export const updateEntity: ICrudPutAction<IBasketItem> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_BASKETITEM,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IBasketItem> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_BASKETITEM,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IBasketItem> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_BASKETITEM,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntitiesUser());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
