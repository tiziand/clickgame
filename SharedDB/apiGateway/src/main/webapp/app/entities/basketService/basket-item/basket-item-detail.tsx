import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import {} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './basket-item.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IBasketItemDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const BasketItemDetail = (props: IBasketItemDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { basketItemEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="basketItemDetailsHeading">BasketItem</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{basketItemEntity.id}</dd>
          <dt>
            <span id="userName">User Name</span>
          </dt>
          <dd>{basketItemEntity.userName}</dd>
          <dt>
            <span id="mtxId">Mtx Id</span>
          </dt>
          <dd>{basketItemEntity.mtxId}</dd>
          <dt>Button Color Mtx</dt>
          <dd>{basketItemEntity.buttonColorMtx ? basketItemEntity.buttonColorMtx.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/basket-item" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/basket-item/${basketItemEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ basketItem }: IRootState) => ({
  basketItemEntity: basketItem.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BasketItemDetail);
