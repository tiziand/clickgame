import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IButtonColorMtx } from 'app/shared/model/mtxStoreService/button-color-mtx.model';
import { getEntities as getButtonColorMtxes } from 'app/entities/mtxStoreService/button-color-mtx/button-color-mtx.reducer';
import { getEntity, updateEntity, createEntity, reset } from './basket-item.reducer';
import { IBasketItem } from 'app/shared/model/basketService/basket-item.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IBasketItemUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const BasketItemUpdate = (props: IBasketItemUpdateProps) => {
  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const { basketItemEntity, buttonColorMtxes, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/basket-item');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getButtonColorMtxes();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...basketItemEntity,
        ...values,
        buttonColorMtx: buttonColorMtxes.find(it => it.id.toString() === values.buttonColorMtxId.toString()),
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="apiGatewayApp.inventoryServiceBasketItem.home.createOrEditLabel" data-cy="BasketItemCreateUpdateHeading">
            Create or edit a BasketItem
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : basketItemEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="basket-item-id">ID</Label>
                  <AvInput id="basket-item-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="userNameLabel" for="basket-item-userName">
                  User Name
                </Label>
                <AvField
                  id="basket-item-userName"
                  data-cy="userName"
                  type="text"
                  name="userName"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="mtxIdLabel" for="basket-item-mtxId">
                  Mtx Id
                </Label>
                <AvField
                  id="basket-item-mtxId"
                  data-cy="mtxId"
                  type="string"
                  className="form-control"
                  name="mtxId"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label for="basket-item-buttonColorMtx">Button Color Mtx</Label>
                <AvInput
                  id="basket-item-buttonColorMtx"
                  data-cy="buttonColorMtx"
                  type="select"
                  className="form-control"
                  name="buttonColorMtxId"
                >
                  <option value="" key="0" />
                  {buttonColorMtxes
                    ? buttonColorMtxes.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/basket-item" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  buttonColorMtxes: storeState.buttonColorMtx.entities,
  basketItemEntity: storeState.basketItem.entity,
  loading: storeState.basketItem.loading,
  updating: storeState.basketItem.updating,
  updateSuccess: storeState.basketItem.updateSuccess,
});

const mapDispatchToProps = {
  getButtonColorMtxes,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BasketItemUpdate);
