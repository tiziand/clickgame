import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './payment-info.reducer';
import { IPaymentInfo } from 'app/shared/model/paymentService/payment-info.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPaymentInfoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const PaymentInfo = (props: IPaymentInfoProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const handleSyncList = () => {
    props.getEntities();
  };

  const { paymentInfoList, match, loading } = props;
  return (
    <div>
      <h2 id="payment-info-heading" data-cy="PaymentInfoHeading">
        Payment Infos
        <div className="d-flex justify-content-end">
          <Button className="mr-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} /> Refresh List
          </Button>
          <Link to={`${match.url}/new`} className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Payment Info
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {paymentInfoList && paymentInfoList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Creation Time</th>
                <th>Point Option</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {paymentInfoList.map((paymentInfo, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`${match.url}/${paymentInfo.id}`} color="link" size="sm">
                      {paymentInfo.id}
                    </Button>
                  </td>
                  <td>{paymentInfo.userName}</td>
                  <td>
                    {paymentInfo.creationTime ? <TextFormat type="date" value={paymentInfo.creationTime} format={APP_DATE_FORMAT} /> : null}
                  </td>
                  <td>
                    {paymentInfo.pointOption ? (
                      <Link to={`point-option/${paymentInfo.pointOption.id}`}>{paymentInfo.pointOption.id}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${paymentInfo.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${paymentInfo.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${paymentInfo.id}/delete`} color="danger" size="sm" data-cy="entityDeleteButton">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Payment Infos found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ paymentInfo }: IRootState) => ({
  paymentInfoList: paymentInfo.entities,
  loading: paymentInfo.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PaymentInfo);
