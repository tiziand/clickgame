import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './purchases-mtx-points.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPurchasesMtxPointsDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PurchasesMtxPointsDetail = (props: IPurchasesMtxPointsDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { purchasesMtxPointsEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="purchasesMtxPointsDetailsHeading">PurchasesMtxPoints</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{purchasesMtxPointsEntity.id}</dd>
          <dt>
            <span id="creationTime">Creation Time</span>
          </dt>
          <dd>
            {purchasesMtxPointsEntity.creationTime ? (
              <TextFormat value={purchasesMtxPointsEntity.creationTime} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="moneySpent">Money Spent</span>
          </dt>
          <dd>{purchasesMtxPointsEntity.moneySpent}</dd>
        </dl>
        <Button tag={Link} to="/purchases-mtx-points" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/purchases-mtx-points/${purchasesMtxPointsEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ purchasesMtxPoints }: IRootState) => ({
  purchasesMtxPointsEntity: purchasesMtxPoints.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PurchasesMtxPointsDetail);
