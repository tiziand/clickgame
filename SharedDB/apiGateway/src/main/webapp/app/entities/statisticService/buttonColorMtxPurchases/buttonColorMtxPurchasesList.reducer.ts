import { IPurchasesButtonColorMtxTotalPer, defaultValue } from './../../../shared/model/statisticService/purchasesButtonColorMtxTotalPer';
import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

export const ACTION_TYPES = {
  FETCH_PURCHASESBUTTONCOLORMTX_LIST: 'purchasesButtonColorMtxTotalPer/FETCH_PURCHASESBUTTONCOLORMTX_LIST',
  RESET: 'purchasesButtonColorMtxTotalPer/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPurchasesButtonColorMtxTotalPer>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PurchasesButtonColorMtxTotalPerState = Readonly<typeof initialState>;

// Reducer

export default (state: PurchasesButtonColorMtxTotalPerState = initialState, action): PurchasesButtonColorMtxTotalPerState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/purchases-button-color-mtxes/pruchasesSumList';

// Actions

export const getEntities = (startDate: string, endDate: string) => ({
  type: ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST,
  payload: axios.get<IPurchasesButtonColorMtxTotalPer>(`${apiUrl}/${startDate}/${endDate}?cacheBuster=${new Date().getTime()}`),
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
