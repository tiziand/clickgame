import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IClicksTotal, defaultValue } from 'app/shared/model/statisticService/clicksTotal.model';

export const ACTION_TYPES = {
  FETCH_CLICKS: 'clickPerMin/FETCH_CLICKS',
  RESET: 'clickPerMin/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IClicksTotal>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type ClicksTotalState = Readonly<typeof initialState>;

// Reducer

export default (state: ClicksTotalState = initialState, action): ClicksTotalState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CLICKS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CLICKS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLICKS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/click-per-mins';

// Actions

export const getEntity = (startDate: string, endDate: string) => {
  const requestUrl = `${apiUrl}/clicksSum/${startDate}/${endDate}`;
  return {
    type: ACTION_TYPES.FETCH_CLICKS,
    payload: axios.get<IClicksTotal>(requestUrl),
  };
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
