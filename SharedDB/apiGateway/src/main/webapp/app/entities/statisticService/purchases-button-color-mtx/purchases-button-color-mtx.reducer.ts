import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPurchasesButtonColorMtx, defaultValue } from 'app/shared/model/statisticService/purchases-button-color-mtx.model';

export const ACTION_TYPES = {
  FETCH_PURCHASESBUTTONCOLORMTX_LIST: 'purchasesButtonColorMtx/FETCH_PURCHASESBUTTONCOLORMTX_LIST',
  FETCH_PURCHASESBUTTONCOLORMTX: 'purchasesButtonColorMtx/FETCH_PURCHASESBUTTONCOLORMTX',
  CREATE_PURCHASESBUTTONCOLORMTX: 'purchasesButtonColorMtx/CREATE_PURCHASESBUTTONCOLORMTX',
  UPDATE_PURCHASESBUTTONCOLORMTX: 'purchasesButtonColorMtx/UPDATE_PURCHASESBUTTONCOLORMTX',
  PARTIAL_UPDATE_PURCHASESBUTTONCOLORMTX: 'purchasesButtonColorMtx/PARTIAL_UPDATE_PURCHASESBUTTONCOLORMTX',
  DELETE_PURCHASESBUTTONCOLORMTX: 'purchasesButtonColorMtx/DELETE_PURCHASESBUTTONCOLORMTX',
  RESET: 'purchasesButtonColorMtx/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPurchasesButtonColorMtx>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type PurchasesButtonColorMtxState = Readonly<typeof initialState>;

// Reducer

export default (state: PurchasesButtonColorMtxState = initialState, action): PurchasesButtonColorMtxState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PURCHASESBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.UPDATE_PURCHASESBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.DELETE_PURCHASESBUTTONCOLORMTX):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_PURCHASESBUTTONCOLORMTX):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.CREATE_PURCHASESBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.UPDATE_PURCHASESBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_PURCHASESBUTTONCOLORMTX):
    case FAILURE(ACTION_TYPES.DELETE_PURCHASESBUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PURCHASESBUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.UPDATE_PURCHASESBUTTONCOLORMTX):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_PURCHASESBUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PURCHASESBUTTONCOLORMTX):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/purchases-button-color-mtxes';

// Actions

export const getEntities: ICrudGetAllAction<IPurchasesButtonColorMtx> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX_LIST,
  payload: axios.get<IPurchasesButtonColorMtx>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IPurchasesButtonColorMtx> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PURCHASESBUTTONCOLORMTX,
    payload: axios.get<IPurchasesButtonColorMtx>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IPurchasesButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PURCHASESBUTTONCOLORMTX,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPurchasesButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PURCHASESBUTTONCOLORMTX,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IPurchasesButtonColorMtx> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_PURCHASESBUTTONCOLORMTX,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPurchasesButtonColorMtx> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PURCHASESBUTTONCOLORMTX,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
