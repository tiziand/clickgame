import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-count.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserCountDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const UserCountDetail = (props: IUserCountDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { userCountEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="userCountDetailsHeading">UserCount</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{userCountEntity.id}</dd>
          <dt>
            <span id="creationTime">Creation Time</span>
          </dt>
          <dd>
            {userCountEntity.creationTime ? <TextFormat value={userCountEntity.creationTime} type="date" format={APP_DATE_FORMAT} /> : null}
          </dd>
          <dt>
            <span id="userCount">User Count</span>
          </dt>
          <dd>{userCountEntity.userCount}</dd>
        </dl>
        <Button tag={Link} to="/user-count" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/user-count/${userCountEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ userCount }: IRootState) => ({
  userCountEntity: userCount.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(UserCountDetail);
