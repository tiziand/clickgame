import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import PurchasesButtonColorMtx from './purchases-button-color-mtx';
import PurchasesButtonColorMtxDetail from './purchases-button-color-mtx-detail';
import PurchasesButtonColorMtxUpdate from './purchases-button-color-mtx-update';
import PurchasesButtonColorMtxDeleteDialog from './purchases-button-color-mtx-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PurchasesButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PurchasesButtonColorMtxUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PurchasesButtonColorMtxDetail} />
      <ErrorBoundaryRoute path={match.url} component={PurchasesButtonColorMtx} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={PurchasesButtonColorMtxDeleteDialog} />
  </>
);

export default Routes;
