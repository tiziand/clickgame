import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IUserCount, defaultValue } from 'app/shared/model/statisticService/user-count.model';

export const ACTION_TYPES = {
  FETCH_USERCOUNT_LIST: 'userCount/FETCH_USERCOUNT_LIST',
  FETCH_USERCOUNT: 'userCount/FETCH_USERCOUNT',
  CREATE_USERCOUNT: 'userCount/CREATE_USERCOUNT',
  UPDATE_USERCOUNT: 'userCount/UPDATE_USERCOUNT',
  PARTIAL_UPDATE_USERCOUNT: 'userCount/PARTIAL_UPDATE_USERCOUNT',
  DELETE_USERCOUNT: 'userCount/DELETE_USERCOUNT',
  RESET: 'userCount/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUserCount>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type UserCountState = Readonly<typeof initialState>;

// Reducer

export default (state: UserCountState = initialState, action): UserCountState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_USERCOUNT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_USERCOUNT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_USERCOUNT):
    case REQUEST(ACTION_TYPES.UPDATE_USERCOUNT):
    case REQUEST(ACTION_TYPES.DELETE_USERCOUNT):
    case REQUEST(ACTION_TYPES.PARTIAL_UPDATE_USERCOUNT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_USERCOUNT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_USERCOUNT):
    case FAILURE(ACTION_TYPES.CREATE_USERCOUNT):
    case FAILURE(ACTION_TYPES.UPDATE_USERCOUNT):
    case FAILURE(ACTION_TYPES.PARTIAL_UPDATE_USERCOUNT):
    case FAILURE(ACTION_TYPES.DELETE_USERCOUNT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERCOUNT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERCOUNT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_USERCOUNT):
    case SUCCESS(ACTION_TYPES.UPDATE_USERCOUNT):
    case SUCCESS(ACTION_TYPES.PARTIAL_UPDATE_USERCOUNT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_USERCOUNT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'services/statisticservice/api/user-counts';

// Actions

export const getEntities: ICrudGetAllAction<IUserCount> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_USERCOUNT_LIST,
  payload: axios.get<IUserCount>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<IUserCount> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_USERCOUNT,
    payload: axios.get<IUserCount>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IUserCount> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_USERCOUNT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUserCount> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USERCOUNT,
    payload: axios.put(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const partialUpdate: ICrudPutAction<IUserCount> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.PARTIAL_UPDATE_USERCOUNT,
    payload: axios.patch(`${apiUrl}/${entity.id}`, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUserCount> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_USERCOUNT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
