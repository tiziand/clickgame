import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './purchases-button-color-mtx.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPurchasesButtonColorMtxDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PurchasesButtonColorMtxDetail = (props: IPurchasesButtonColorMtxDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { purchasesButtonColorMtxEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="purchasesButtonColorMtxDetailsHeading">PurchasesButtonColorMtx</h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">ID</span>
          </dt>
          <dd>{purchasesButtonColorMtxEntity.id}</dd>
          <dt>
            <span id="mtxId">Mtx Id</span>
          </dt>
          <dd>{purchasesButtonColorMtxEntity.mtxId}</dd>
          <dt>
            <span id="creationTime">Creation Time</span>
          </dt>
          <dd>
            {purchasesButtonColorMtxEntity.creationTime ? (
              <TextFormat value={purchasesButtonColorMtxEntity.creationTime} type="date" format={APP_DATE_FORMAT} />
            ) : null}
          </dd>
          <dt>
            <span id="amount">Amount</span>
          </dt>
          <dd>{purchasesButtonColorMtxEntity.amount}</dd>
        </dl>
        <Button tag={Link} to="/purchases-button-color-mtx" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/purchases-button-color-mtx/${purchasesButtonColorMtxEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ purchasesButtonColorMtx }: IRootState) => ({
  purchasesButtonColorMtxEntity: purchasesButtonColorMtx.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PurchasesButtonColorMtxDetail);
