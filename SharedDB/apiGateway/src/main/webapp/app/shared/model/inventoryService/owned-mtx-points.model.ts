export interface IOwnedMtxPoints {
  id?: number;
  userName?: string;
  mtxPoints?: number;
}

export const defaultValue: Readonly<IOwnedMtxPoints> = {};
