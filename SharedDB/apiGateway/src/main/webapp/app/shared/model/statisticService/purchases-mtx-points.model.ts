import dayjs from 'dayjs';

export interface IPurchasesMtxPoints {
  id?: number;
  creationTime?: string;
  moneySpent?: number;
}

export const defaultValue: Readonly<IPurchasesMtxPoints> = {};
