import { IPointOption } from 'app/shared/model/paymentService/point-option.model';

export interface IPaymentInfo {
  id?: number;
  userName?: string;
  creationTime?: string;
  pointOption?: IPointOption | null;
}

export const defaultValue: Readonly<IPaymentInfo> = {};
