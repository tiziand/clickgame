import { IButtonColorMtx } from 'app/shared/model/mtxStoreService/button-color-mtx.model';

export interface IBasketItem {
  id?: number;
  userName?: string;
  mtxId?: number;
  buttonColorMtx?: IButtonColorMtx | null;
}

export const defaultValue: Readonly<IBasketItem> = {};
