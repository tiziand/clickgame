import { IButtonColorMtx } from 'app/shared/model/mtxStoreService/button-color-mtx.model';

export interface IEquippedButtonColorMtx {
  id?: number;
  userName?: string;
  timeSlot?: number;
  buttonColorMtx?: IButtonColorMtx | null;
}

export const defaultValue: Readonly<IEquippedButtonColorMtx> = {};
