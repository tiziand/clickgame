import { IEquippedButtonColorMtx } from 'app/shared/model/inventoryService/equipped-button-color-mtx.model';
import { IOwnedButtonColorMtx } from 'app/shared/model/inventoryService/owned-button-color-mtx.model';
import { IBasketItem } from 'app/shared/model/basketService/basket-item.model';

export interface IButtonColorMtx {
  id?: number;
  mtxColor?: string;
  price?: number;
  available?: boolean;
  timeLimit?: number | null;
  pointsRequired?: number | null;
  locked?: boolean | null;
  equippedButtonColorMtxes?: IEquippedButtonColorMtx[] | null;
  ownedButtonColorMtxes?: IOwnedButtonColorMtx[] | null;
  basketItems?: IBasketItem[] | null;
}

export const defaultValue: Readonly<IButtonColorMtx> = {
  available: false,
  locked: false,
};
