import { IButtonColorMtx } from 'app/shared/model/mtxStoreService/button-color-mtx.model';

export interface IOwnedButtonColorMtx {
  id?: number;
  userName?: string;
  mtxId?: number;
  creationTime?: string;
  buttonColorMtx?: IButtonColorMtx | null;
}

export const defaultValue: Readonly<IOwnedButtonColorMtx> = {};
