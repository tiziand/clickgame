export interface IPointsFarmedView {
  userName?: string;
  points?: number;
  rank?: number;
}

export const defaultValue: Readonly<IPointsFarmedView> = {};
