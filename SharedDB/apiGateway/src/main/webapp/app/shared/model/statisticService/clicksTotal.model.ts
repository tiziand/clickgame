export interface IClicksTotal {
  clicks?: number;
}

export const defaultValue: Readonly<IClicksTotal> = {};
