import React from 'react';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const StatisticsMenu = props => (
  <NavItem>
    <NavLink tag={Link} to="/statistics" className="d-flex align-items-center">
      <span>Statistics</span>
    </NavLink>
  </NavItem>
);
