import React from 'react';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const GameMenu = props => (
  <NavItem>
    <NavLink tag={Link} to="/game" className="d-flex align-items-center">
      <span>Game</span>
    </NavLink>
  </NavItem>
);
