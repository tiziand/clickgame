import React from 'react';

import { NavItem, NavLink, NavbarBrand } from 'reactstrap';
import { NavLink as Link } from 'react-router-dom';

export const RankingMenu = props => (
  <NavItem>
    <NavLink tag={Link} to="/ranking" className="d-flex align-items-center">
      <span>Ranking</span>
    </NavLink>
  </NavItem>
);
