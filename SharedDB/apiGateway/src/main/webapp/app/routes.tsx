import React from 'react';
import { Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import Login from 'app/modules/login/login';
import Register from 'app/modules/account/register/register';
import Activate from 'app/modules/account/activate/activate';
import PasswordResetInit from 'app/modules/account/password-reset/init/password-reset-init';
import PasswordResetFinish from 'app/modules/account/password-reset/finish/password-reset-finish';
import Logout from 'app/modules/login/logout';
import Home from 'app/modules/home/home';
import Entities from 'app/entities';
import PrivateRoute from 'app/shared/auth/private-route';
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';
import PageNotFound from 'app/shared/error/page-not-found';
import { AUTHORITIES } from 'app/config/constants';
import Game from './modules/game/game';
import Inventory from './modules/inventory/inventory';
import BuyPoints from './modules/store/buyMtxPoints';
import Ranking from './modules/ranking/ranking';
import MtxStore from './modules/store/mtxStore';
import Basket from './modules/store/basket';
import Statistics from './modules/statistics/statistics';

const Account = Loadable({
  loader: () => import(/* webpackChunkName: "account" */ 'app/modules/account'),
  loading: () => <div>loading ...</div>,
});

const Admin = Loadable({
  loader: () => import(/* webpackChunkName: "administration" */ 'app/modules/administration'),
  loading: () => <div>loading ...</div>,
});

const Routes = () => {
  return (
    <div className="view-routes">
      <Switch>
        <ErrorBoundaryRoute path="/login" component={Login} />
        <ErrorBoundaryRoute path="/logout" component={Logout} />
        <ErrorBoundaryRoute path="/account/register" component={Register} />
        <ErrorBoundaryRoute path="/account/activate/:key?" component={Activate} />
        <ErrorBoundaryRoute path="/account/reset/request" component={PasswordResetInit} />
        <ErrorBoundaryRoute path="/account/reset/finish/:key?" component={PasswordResetFinish} />
        <PrivateRoute path="/admin" component={Admin} hasAnyAuthorities={[AUTHORITIES.ADMIN]} />
        <PrivateRoute path="/account" component={Account} hasAnyAuthorities={[AUTHORITIES.ADMIN, AUTHORITIES.USER]} />
        <PrivateRoute path="/game" component={Game} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <PrivateRoute path="/inventory" component={Inventory} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <PrivateRoute path="/buypoints" component={BuyPoints} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <PrivateRoute path="/ranking" component={Ranking} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <PrivateRoute path="/mtxStore" component={MtxStore} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <PrivateRoute path="/basket" component={Basket} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <PrivateRoute path="/statistics" component={Statistics} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <ErrorBoundaryRoute path="/" exact component={Home} />
        <PrivateRoute path="/" component={Entities} hasAnyAuthorities={[AUTHORITIES.USER]} />
        <ErrorBoundaryRoute component={PageNotFound} />
      </Switch>
    </div>
  );
};

export default Routes;
