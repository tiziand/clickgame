import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table, ButtonGroup } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntitiesOfUser as getOwnedButtonEntities } from 'app/entities/inventoryService/owned-button-color-mtx/owned-button-color-mtx.reducer';
import {
  getEntityOfUser as eBCMgetEntityOfUser,
  equippMTX,
} from 'app/entities/inventoryService/equipped-button-color-mtx/equipped-button-color-mtx.reducer';
import { getEntityOfUser as getOwnedMtxPoints } from 'app/entities/inventoryService/owned-mtx-points/owned-mtx-points.reducer';

export interface IInventoryProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Inventory = (props: IInventoryProps) => {
  const [timeLimit, setTimeLimit] = useState(0);

  useEffect(() => {
    props.getOwnedButtonEntities();
    props.getOwnedMtxPoints(null);
  }, []);

  useEffect(() => {
    props.eBCMgetEntityOfUser(timeLimit);
  }, [timeLimit]);

  const { ownedButtonColorMtxList, match, oBCMloading } = props;
  if (oBCMloading || props.eBCMloading || props.ownedMtxPointsLoading) return <div></div>;
  else
    return (
      <div>
        <h2>Owned MTX Points: {props.ownedMtxPoints.mtxPoints}</h2>
        <h2 id="inventory-heading">
          Inventory{' '}
          <p>
            <ButtonGroup>
              <Button
                color="info"
                onClick={() => {
                  setTimeLimit(0);
                }}
              >
                Infinite
              </Button>
              <Button
                color="success"
                onClick={() => {
                  setTimeLimit(6);
                }}
              >
                6s
              </Button>
              <Button
                color="warning"
                onClick={() => {
                  setTimeLimit(4);
                }}
              >
                4s
              </Button>
              <Button
                color="danger"
                onClick={() => {
                  setTimeLimit(2);
                }}
              >
                2s
              </Button>
            </ButtonGroup>
            {timeLimit === 0 ? ' Infinite' : ' ' + timeLimit + 's'}
          </p>
        </h2>
        <div className="table-responsive">
          {ownedButtonColorMtxList && ownedButtonColorMtxList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th>Mtx Id</th>
                  <th>Color Sample</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {props.ownedButtonColorMtxList.map((ownedButtonColorMtx, i) => (
                  <tr key={`entity-${i}`}>
                    {(!ownedButtonColorMtx.buttonColorMtx.locked || ownedButtonColorMtx.buttonColorMtx.timeLimit === timeLimit) && (
                      <>
                        <td>{ownedButtonColorMtx.buttonColorMtx.id}</td>
                        <td>{<Button style={{ backgroundColor: ownedButtonColorMtx.buttonColorMtx.mtxColor }}>Color</Button>}</td>
                        <td className="text-right">
                          {ownedButtonColorMtx.buttonColorMtx.id !== props.equippedButtonColorMtx.buttonColorMtx.id ? (
                            <div className="btn-group flex-btn-group-container">
                              <Button
                                onClick={() => props.equippMTX(ownedButtonColorMtx.buttonColorMtx.id, timeLimit)}
                                color="info"
                                size="sm"
                              >
                                <span className="d-none d-md-inline">Equip</span>
                              </Button>
                            </div>
                          ) : (
                            <div>equipped</div>
                          )}
                        </td>
                      </>
                    )}
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            !oBCMloading && <div className="alert alert-warning">No Owned Button Color Mtxes found</div>
          )}
        </div>
      </div>
    );
};

const mapStateToProps = ({ ownedButtonColorMtx, equippedButtonColorMtx, ownedMtxPoints }: IRootState) => ({
  ownedButtonColorMtxList: ownedButtonColorMtx.entities,
  oBCMloading: ownedButtonColorMtx.loading,
  eBCMloading: equippedButtonColorMtx.loading,
  equippedButtonColorMtx: equippedButtonColorMtx.entity,
  ownedMtxPoints: ownedMtxPoints.entity,
  ownedMtxPointsLoading: ownedMtxPoints.loading,
});

const mapDispatchToProps = {
  getOwnedButtonEntities,
  eBCMgetEntityOfUser,
  equippMTX,
  getOwnedMtxPoints,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Inventory);
