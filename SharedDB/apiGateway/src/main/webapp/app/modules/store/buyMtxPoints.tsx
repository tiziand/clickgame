import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table, Label } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from 'app/entities/paymentService/point-option/point-option.reducer';
import { createEntity } from 'app/entities/paymentService/payment-info/payment-info.reducer';
import { IPointOption } from 'app/shared/model/paymentService/point-option.model';
import { IPaymentInfo } from 'app/shared/model/paymentService/payment-info.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';

export interface IPointOptionProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const BuyPoints = (props: IPointOptionProps) => {
  const [selectedPointOption, setSelectedPointOption] = useState<IPointOption>(null);

  useEffect(() => {
    props.getEntities();
  }, []);

  const buyPoints = (event, errors, values) => {
    const newPaymentInfo: IPaymentInfo = {
      pointOption: selectedPointOption,
      userName: '',
    };
    props.createEntity(newPaymentInfo);
    setSelectedPointOption(null);
  };

  const { pointOptionList, match, loading } = props;
  if (selectedPointOption === null)
    return (
      <div>
        <h2 id="point-option-heading">Point Options</h2>
        <div className="table-responsive">
          {pointOptionList && pointOptionList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th>Point Amount</th>
                  <th>Price</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {pointOptionList.map((pointOption, i) => (
                  <tr key={`entity-${i}`}>
                    {pointOption.available && (
                      <>
                        <td>{pointOption.pointAmount}</td>
                        <td>{pointOption.price}</td>
                        <td className="text-right">
                          <div className="btn-group flex-btn-group-container">
                            <Button onClick={() => setSelectedPointOption(pointOption)} color="primary" size="sm">
                              <span className="d-none d-md-inline">Buy</span>
                            </Button>
                          </div>
                        </td>
                      </>
                    )}
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            !loading && <div className="alert alert-warning">No Point Options found</div>
          )}
        </div>
      </div>
    );
  else
    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="apiGatewayApp.paymentServicePointOption.home.createOrEditLabel">Enter Payment Info</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            <AvForm onSubmit={buyPoints}>
              <AvGroup>
                <Label id="paymentInfoLabel" for="paymentInfo">
                  Payment info
                </Label>
                <AvField
                  id="paymentInfo"
                  type="string"
                  className="form-control"
                  name="paymentInfo"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                  }}
                />
              </AvGroup>
              <Button onClick={() => setSelectedPointOption(null)} id="cancel-buy" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="buy-points" type="submit">
                confirm Purchase
              </Button>
            </AvForm>
          </Col>
        </Row>
      </div>
    );
};

const mapStateToProps = ({ pointOption }: IRootState) => ({
  pointOptionList: pointOption.entities,
  loading: pointOption.loading,
});

const mapDispatchToProps = {
  getEntities,
  createEntity,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(BuyPoints);
