package bt.browsergame.ranking;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("bt.browsergame.ranking");

        noClasses()
            .that()
            .resideInAnyPackage("bt.browsergame.ranking.service..")
            .or()
            .resideInAnyPackage("bt.browsergame.ranking.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..bt.browsergame.ranking.web.rest")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
