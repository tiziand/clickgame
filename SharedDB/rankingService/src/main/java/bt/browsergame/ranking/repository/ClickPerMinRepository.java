package bt.browsergame.ranking.repository;

import bt.browsergame.ranking.domain.ClickPerMin;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ClickPerMin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClickPerMinRepository extends JpaRepository<ClickPerMin, Long> {}
