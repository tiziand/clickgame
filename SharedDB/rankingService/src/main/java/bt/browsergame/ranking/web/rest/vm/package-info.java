/**
 * View Models used by Spring MVC REST controllers.
 */
package bt.browsergame.ranking.web.rest.vm;
