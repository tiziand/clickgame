package bt.browsergame.ranking.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import bt.browsergame.ranking.domain.PointsFarmed;
import bt.browsergame.ranking.domain.PointsFarmedView;
import bt.browsergame.ranking.repository.PointsFarmedRepository;
import bt.browsergame.ranking.security.SecurityUtils;
import bt.browsergame.ranking.service.PointsFarmedService;
import bt.browsergame.ranking.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing
 * {@link bt.browsergame.ranking.domain.PointsFarmed}.
 */
@RestController
@RequestMapping("/api")
public class PointsFarmedResource {

    private final Logger log = LoggerFactory.getLogger(PointsFarmedResource.class);

    private static final String ENTITY_NAME = "rankingServicePointsFarmed";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PointsFarmedService pointsFarmedService;

    private final PointsFarmedRepository pointsFarmedRepository;

    public PointsFarmedResource(PointsFarmedService pointsFarmedService,
            PointsFarmedRepository pointsFarmedRepository) {
        this.pointsFarmedService = pointsFarmedService;
        this.pointsFarmedRepository = pointsFarmedRepository;
    }

    /**
     * {@code POST  /points-farmeds} : Create a new pointsFarmed.
     *
     * @param pointsFarmed the pointsFarmed to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with
     *         body the new pointsFarmed, or with status {@code 400 (Bad Request)}
     *         if the pointsFarmed has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/points-farmeds")
    public ResponseEntity<PointsFarmed> createPointsFarmed(@Valid @RequestBody PointsFarmed pointsFarmed)
            throws URISyntaxException {
        log.debug("REST request to save PointsFarmed : {}", pointsFarmed);
        if (pointsFarmed.getId() != null) {
            throw new BadRequestAlertException("A new pointsFarmed cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PointsFarmed result = pointsFarmedService.save(pointsFarmed);
        return ResponseEntity
                .created(new URI("/api/points-farmeds/" + result.getId())).headers(HeaderUtil
                        .createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * {@code PUT  /points-farmeds/:id} : Updates an existing pointsFarmed.
     *
     * @param id           the id of the pointsFarmed to save.
     * @param pointsFarmed the pointsFarmed to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated pointsFarmed, or with status {@code 400 (Bad Request)} if
     *         the pointsFarmed is not valid, or with status
     *         {@code 500 (Internal Server Error)} if the pointsFarmed couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/points-farmeds/{id}")
    public ResponseEntity<PointsFarmed> updatePointsFarmed(@PathVariable(value = "id", required = false) final Long id,
            @Valid @RequestBody PointsFarmed pointsFarmed) throws URISyntaxException {
        log.debug("REST request to update PointsFarmed : {}, {}", id, pointsFarmed);
        if (pointsFarmed.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pointsFarmed.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pointsFarmedRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PointsFarmed result = pointsFarmedService.save(pointsFarmed);
        return ResponseEntity.ok().headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, pointsFarmed.getId().toString()))
                .body(result);
    }

    /**
     * {@code PATCH  /points-farmeds/:id} : Partial updates given fields of an
     * existing pointsFarmed, field will ignore if it is null
     *
     * @param id           the id of the pointsFarmed to save.
     * @param pointsFarmed the pointsFarmed to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the updated pointsFarmed, or with status {@code 400 (Bad Request)} if
     *         the pointsFarmed is not valid, or with status {@code 404 (Not Found)}
     *         if the pointsFarmed is not found, or with status
     *         {@code 500 (Internal Server Error)} if the pointsFarmed couldn't be
     *         updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/points-farmeds/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<PointsFarmed> partialUpdatePointsFarmed(
            @PathVariable(value = "id", required = false) final Long id,
            @NotNull @RequestBody PointsFarmed pointsFarmed) throws URISyntaxException {
        log.debug("REST request to partial update PointsFarmed partially : {}, {}", id, pointsFarmed);
        if (pointsFarmed.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, pointsFarmed.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!pointsFarmedRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PointsFarmed> result = pointsFarmedService.partialUpdate(pointsFarmed);

        return ResponseUtil.wrapOrNotFound(result, HeaderUtil.createEntityUpdateAlert(applicationName, true,
                ENTITY_NAME, pointsFarmed.getId().toString()));
    }

    /**
     * {@code GET  /points-farmeds} : get all the pointsFarmeds.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of pointsFarmeds in body.
     */
    @GetMapping("/points-farmeds")
    public ResponseEntity<List<PointsFarmed>> getAllPointsFarmeds(Pageable pageable) {
        log.debug("REST request to get a page of PointsFarmeds");
        Page<PointsFarmed> page = pointsFarmedService.findAll(pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /points-farmeds/:id} : get the "id" pointsFarmed.
     *
     * @param id the id of the pointsFarmed to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         the pointsFarmed, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/points-farmeds/{id}")
    public ResponseEntity<PointsFarmed> getPointsFarmed(@PathVariable Long id) {
        log.debug("REST request to get PointsFarmed : {}", id);
        Optional<PointsFarmed> pointsFarmed = pointsFarmedService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pointsFarmed);
    }

    /**
     * {@code DELETE  /points-farmeds/:id} : delete the "id" pointsFarmed.
     *
     * @param id the id of the pointsFarmed to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/points-farmeds/{id}")
    public ResponseEntity<Void> deletePointsFarmed(@PathVariable Long id) {
        log.debug("REST request to delete PointsFarmed : {}", id);
        pointsFarmedService.delete(id);
        return ResponseEntity.noContent()
                .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                .build();
    }

    /**
     * {@code GET  /points-farmeds/ranking} : get all the pointsFarmeds.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of pointsFarmeds in body.
     */
    @GetMapping("/points-farmeds/rankingSum")
    public ResponseEntity<List<PointsFarmedView>> getRankingListSum(Pageable pageable) {
        log.debug("REST request to get a page of PointsFarmeds");
        Page<PointsFarmedView> page = pointsFarmedService.getRankingListSum(pageable);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /points-farmeds/ranking} : get all the pointsFarmeds.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list
     *         of pointsFarmeds in body.
     */
    @GetMapping("/points-farmeds/rankingMax/{timeLimit}")
    public ResponseEntity<List<PointsFarmedView>> getRankingListMax(Pageable pageable, @PathVariable Double timeLimit) {
        log.debug("REST request to get a page of PointsFarmeds");
        Page<PointsFarmedView> page = pointsFarmedService.getRankingListMax(pageable, timeLimit);
        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /points-farmeds/userSum} : get the logged in user points sum.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the PointsFarmed
     */
    @GetMapping("/points-farmeds/userSum")
    public ResponseEntity<PointsFarmedView> getPointsFarmedSum() {
        PointsFarmedView pointsFarmedView = pointsFarmedService
                .getSumPointsFarmed(SecurityUtils.getCurrentUserLogin().get());
        return ResponseEntity.ok(pointsFarmedView);
    }

    /**
     * {@code GET /points-farmeds/userMaxTime/{time}} : get the logged in user
     * points max based on timeLimit.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the PointsFarmed
     */
    @GetMapping("/points-farmeds/userMaxTime/{timeLimit}")
    public ResponseEntity<PointsFarmedView> getPointsFarmedMaxTime(@PathVariable Double timeLimit) {
        PointsFarmedView pointsFarmedView = pointsFarmedService.getMaxPointsFarmed(timeLimit,
                SecurityUtils.getCurrentUserLogin().get());
        return ResponseEntity.ok(pointsFarmedView);
    }

    /**
     * {@code GET /points-farmeds/Sum/{userName}} : get the user points sum.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the PointsFarmed
     */
    @GetMapping("/points-farmeds/userSum/{userName}")
    public ResponseEntity<PointsFarmedView> getPointsFarmedSumUser(@PathVariable String userName) {
        Optional<PointsFarmedView> pointsFarmedView = pointsFarmedService.findSumPointsFarmed(userName);
        return ResponseUtil.wrapOrNotFound(pointsFarmedView);
    }

    /**
     * {@code GET /points-farmeds/maxTime/{time}/{userName}} : get the user points
     * max based on timeLimit.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body
     *         of the PointsFarmed
     */
    @GetMapping("/points-farmeds/userMaxTime/{timeLimit}/{userName}")
    public ResponseEntity<PointsFarmedView> getPointsFarmedMaxTimeUser(@PathVariable Double timeLimit,
            @PathVariable String userName) {
        Optional<PointsFarmedView> pointsFarmedView = pointsFarmedService.findMaxPointsFarmed(timeLimit, userName);
        return ResponseUtil.wrapOrNotFound(pointsFarmedView);
    }
}
