package bt.browsergame.ranking.domain;

public interface PointsFarmedView {
    String getUserName();

    Long getPoints();

    Long getRank();
}
